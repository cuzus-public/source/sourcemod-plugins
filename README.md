# Sourcemod Plugins

## Dependencies

Sourcemod: `sourcemod-1.10.0-git6499-windows`

## IDE setup

- [VS Code](https://code.visualstudio.com)
- [SourcePawn syntax support extension](https://marketplace.visualstudio.com/items?itemName=dreae.sourcepawn-vscode)

## Creating a plugin

**Avoid** [transitional syntax](https://wiki.alliedmods.net/SourcePawn_Transitional_Syntax) and any replacing features of the language in order to keep backward compatibility with **goldsrc**.

## How to compile plugins

- Open root folder in VS Code.
- Put plugin source file into `/scripting` directory.
- Open the plugin file and place cursor inside.
- Press `Ctrl + Shift + b` to compile.
- Check `/scripting` directory for complied **.smx** file.

**Please note:** If you use custom include files you should put them inside `scripting/include` directory before compilation. 

## Manuals and examples

- [Snippets](./snippets.md)