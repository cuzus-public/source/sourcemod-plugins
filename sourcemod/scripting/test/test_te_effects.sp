#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

/*
https://forums.alliedmods.net/showthread.php?t=303046

sourcemod\scripting\.trash\__effects\visual.sp

for models/sprites of effects
sourcemod\scripting\.trash\__mods\wowmod-master\source\wow\effects.inc
*/

public Plugin myinfo = {
  name = "test te effects"
};

new g_BeamSprite, g_HaloSprite, g_iBlueGlow, g_physring, g_SmokeSprite;

new String:g_size[6];
new String:g_speed[6];

new Handle:myTimer = INVALID_HANDLE;
new Handle:ricoTimer = INVALID_HANDLE;

public void OnPluginStart() {
  RegConsoleCmd("loop", LoopSetup);
  RegConsoleCmd("rico", Rico);

  g_BeamSprite = PrecacheModel("sprites/laser.vmt");
	g_HaloSprite = PrecacheModel("sprites/halo01.vmt");
  g_iBlueGlow = PrecacheModel("sprites/blueglow1.vmt");
  g_physring = PrecacheModel("sprites/physring1.vmt");
  g_SmokeSprite = PrecacheModel("sprites/steam1.vmt");
}

public Action:Rico(client, args) {
  if (ricoTimer == INVALID_HANDLE) {
    PrintToChat(client, "Rico timer started");
    CreateTimer(3.0, RicoTimer, client, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
  } else {
    PrintToChat(client, "Rico timer removed");
    KillTimer(ricoTimer);
    ricoTimer = INVALID_HANDLE;
  }
}

public Action:Sparks(client, args) {
  new mag = GetRandomInt(1, 10);
  new len = GetRandomInt(1, 10);
  PrintToChat(client, "mag %d len %d", mag, len);
  new Float:endPos[3], angles[3];
  doSparks(endPos, angles, mag, len);
}

public Action:RicoTimer(Handle:timer, any:client) {
  new Float:vec1[3];
  new Float:vec2[3];
  GetClientEyePosition(client, vec1);
  vec2[0] = GetRandomFloat(-vec1[0], vec1[0]);
  vec2[1] = GetRandomFloat(-vec1[1], vec1[1]);
  vec2[2] = GetRandomFloat(-vec1[2], vec1[2]);
  doArmorRicochet(vec1, vec2);
  PrintToChat(client, "Ricochet %f %f %f", vec2[0], vec2[1], vec2[2]);
}

public Action:LoopSetup(client, args) {
  PrintToChat(client, "Loop Setup");

  if (args > 0) {
    GetCmdArg(1, g_size, sizeof(g_size));
  } else {
    Format(g_size, sizeof(g_size), "150");
  }

  if (args > 1) {
    GetCmdArg(2, g_speed, sizeof(g_speed));
  } else {
    Format(g_speed, sizeof(g_speed), "100");
  }

  if (myTimer == INVALID_HANDLE) {
    PrintToChat(client, "Loop timer started");
    myTimer = CreateTimer(2.0, BeamPointsLoop, client, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
  } else {
    PrintToChat(client, "Loop timer removed");
    KillTimer(myTimer);
    myTimer = INVALID_HANDLE;
  }
}

public Action:BeamPointsLoop(Handle:timer, any:client) {
  PrintToChat(client, "Loop");

  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientConnected(i) && IsPlayerAlive(i)) {
      new Float:origin[3], Float:flToPos[3], Float:tempVec[3];
      GetClientAbsOrigin(i, origin);

      flToPos[0] = origin[0];
      flToPos[1] = origin[1];
      flToPos[2] = origin[2];
      flToPos[2] += 60.0;

      new Float:angles1[3];
      GetClientEyeAngles(i, angles1);

      // TE_SetupEnergySplash(flToPos, angles1, GetRandomInt(1,2) == 1);
      // TE_SetupDust(flToPos, angles1, 10.0, 50.0);
      // TE_SetupMetalSparks(flToPos, angles1);
      // TE_SetupMuzzleFlash(flToPos, angles1, StringToFloat(g_size) / 20, GetRandomInt(1, 2));
      // TE_SetupSmoke(flToPos, g_SmokeSprite, StringToFloat(g_size) / 10, 2);

      // TE_SetupSparks(flToPos, angles1, GetRandomInt(1, 10), GetRandomInt(1, 10));
      // TE_SendToAll();

      tempVec[0] = GetRandomFloat(-1.0, 1.0);
      tempVec[1] = GetRandomFloat(-1.0, 1.0);
      doArmorRicochet(flToPos, tempVec);

      // new color1[4] = {0, 200, 0, 200};
      // TE_SetupBeamPoints(origin, flToPos, g_physring, 0, 0, 0, 5.0, 1.0, StringToFloat(g_size), 0, 0.0, color1, StringToInt(g_speed));
      // TE_SendToAll();

      // TE_SetupBeamRingPoint(const float center[3], float Start_Radius, float End_Radius, int ModelIndex, int HaloIndex, int StartFrame,
      //  int FrameRate, float Life, float Width, float Amplitude, const int Color[4], int Speed, int Flags)
      // TE_SetupBeamRingPoint(origin, 5.0, StringToFloat(g_size), g_BeamSprite, 0, 0, 30, 3.0, StringToFloat(g_size) / 1.2, 0.0, color1, StringToInt(g_speed), 0);
      // TE_SendToAll();

      // TE_SetupGlowSprite(const float pos[3], int Model, float Life, float Size, int Brightness)
      // TE_SetupGlowSprite(origin, g_iBlueGlow, 3.0, 10.0, 235);
      // TE_SendToAll();
    }
  }

  if (1 < 2) {
    return;
  }

  if (IsClientConnected(client) && IsPlayerAlive(client)) {
    new Float:from[3], Float:angles[3];
    GetClientEyeAngles(client, angles);
    GetClientEyePosition(client, from);

    new Float:origin[3];
    GetClientAbsOrigin(client, origin);

    new Handle:tray = TR_TraceRayEx(from, angles, MASK_SHOT, RayType_Infinite);
    if (TR_DidHit(tray)) {
      new Float:end[3];
      TR_GetEndPosition(end, tray);
      new color[4] = {200, 200, 0, 200};
      // void TE_SetupBeamPoints(const float start[3], const float end[3], int ModelIndex, int HaloIndex,
        // int StartFrame, int FrameRate, float Life, float Width, float EndWidth, int FadeLength, float Amplitude,
        // const int Color[4], int Speed)
      TE_SetupBeamPoints(origin, end, g_BeamSprite, 0, 0, 0, 2.0, 1.0, 2.0, 1, 0.0, color, 30);
      TE_SendToAll();
    }
    CloseHandle(tray);
  }
}

stock SparksLoop(client) {
  new Float:vec[3];
  new Float:pos[3];
  new Float:end[3];
  GetClientAbsOrigin(client, pos);

  new Float:dir[3] = {0.0, 0.0, 0.0};
  TE_SetupSparks(pos, dir, 500, 100);
  TE_SendToAll();
}

// TE_ShowPole(flAreaCenter, {255, 0, 0, 255}, 10.0);
stock TE_ShowPole(float flPos[3], int Color[4], float flDuration = 0.1) {
	float flToPos[3];
	flToPos[0] = flPos[0];
	flToPos[1] = flPos[1];
	flToPos[2] = flPos[2];
	flToPos[2] += 60.0;

	//Show a giant vertical beam at our goal node
	TE_SetupBeamPoints(flPos, flToPos, g_BeamSprite, g_BeamSprite, 0, 30, flDuration, 5.0, 5.0, 5, 0.0, Color, 30);
	TE_SendToAll();
}

stock doArmorRicochet(const Float:pos[3], const Float:dir[3], Float:delay=0.0) {
  TE_SetupArmorRicochet(pos, dir);
  TE_SendToAll(delay);
}
