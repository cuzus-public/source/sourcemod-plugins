/*
 * DLL_MessageEnd: Refusing to send user message TextMsg of 256 bytes to client, user message size limit is 255 bytes
 */
#include <sourcemod>

public Plugin:myinfo = {
  name = "test_linebreak_messages",
  description = "Plugin for testing messages with linebreaks",
  author = "Geekrainian @ cuzus.games",
  version = "1.0"
};

public void OnPluginStart() {
	RegConsoleCmd("msg", TestCmd);
}

public Action TestCmd(client, args) {
  new String:arg1[2048], String:arg2[2048];
  new String:msg[2048], String:buffer[2048];

  Format(arg1, sizeof(arg1), "1");
  Format(arg2, sizeof(arg2), "5");

  if (args > 0) {
    GetCmdArg(1, arg1, sizeof(arg1));
  }
  if (args > 1) {
    GetCmdArg(2, arg2, sizeof(arg2));
  }

  //Format(msg, sizeof(msg), "╬ \x01Super \x03cool ® \x07Даже с Русским");
  Format(msg, sizeof(msg), "╬ Super cool ® Даже с Русским");
  // Format(msg, sizeof(msg), "<font color='#00ffff'>Super cool</font>");

  new count = StringToInt(arg2);

  if (StrEqual(arg1, "1")) {
    for (new i = 0; i < count; i++) {
      Format(buffer, sizeof(buffer), "%s\n%s - %d", buffer, msg, i);
    }
    PrintToChat(client, buffer);
  }

  if (StrEqual(arg1, "2")) {
    for (new i = 0; i < count; i++) {
      Format(buffer, sizeof(buffer), "%s\n%s - %d", buffer, msg, i);
    }
    PrintCenterText(client, buffer);
  }

  if (StrEqual(arg1, "3")) {
    for (new i = 0; i < count; i++) {
      Format(buffer, sizeof(buffer), "%s\n%s - %d", buffer, msg, i);
    }
    PrintHintText(client, buffer);
  }

  if (StrEqual(arg1, "4")) {
    for (new i = 0; i < count; i++) {
      Format(buffer, sizeof(buffer), "%s\n%s - %d", buffer, msg, i);
    }
    SetHudTextParams(0.3, 0.4, 10.0, 255, 0, 0, 50, 0, 0.0, 0.0, 0.0);
    ShowHudText(client, 2, buffer);
  }

  // Same as PrintHintText
  /*
  if (StrEqual(arg1, "5")) {
    for (new i = 0; i < 5; i++) {
      Format(arg1, sizeof(arg1), "%s\n%s", arg1, msg);
    }
    new Handle:HintMessage = StartMessageOne("HintText", client);
    BfWriteByte(HintMessage, -1);
    BfWriteString(HintMessage, arg1);
    EndMessage();
  }
  */

  if (StrEqual(arg1, "6")) {
    for (new i = 0; i < count; i++) {
      Format(buffer, sizeof(buffer), "%s\n%s", arg1, msg);
    }
    new Handle:hBuffer = StartMessageOne("KeyHintText", client);
    BfWriteByte(hBuffer, 1);
    BfWriteString(hBuffer, buffer);
    EndMessage();
  }

  /*
  if (StrEqual(arg1, "7")) {
    for (new i = 0; i < count; i++) {
      Format(buffer, sizeof(buffer), "%s\n%s", arg1, msg);
    }
    // Exception reported: Invalid message name: "StatusText"
    new Handle:hBuffer = StartMessageOne("StatusText", client);
    BfWriteByte(hBuffer, 1);
    BfWriteString(hBuffer, buffer);
    EndMessage();
  }
  */

  if (StrEqual(arg1, "8")) {
    for (new i = 0; i < count; i++) {
      Format(buffer, sizeof(buffer), "%s\n%s", arg1, msg);
    }
    UTIL_PrintToUpperLeft(255,0,0, buffer);
  }

  return Plugin_Handled;
}

UTIL_PrintToUpperLeft(r, g, b, const String:source[], any:...)
{
  new String:Buffer[64];
  new Handle:Msg;
  for ( new i = 1; i <= MaxClients; i++ ) {
    if ( IsClientInGame(i) && !IsFakeClient(i) ) {
      SetGlobalTransTarget(i);
      VFormat(Buffer, sizeof(Buffer), source, /*any arg index*/ 5);
      Msg = CreateKeyValues("msg");

      if ( Msg != INVALID_HANDLE ) {
        KvSetString(Msg, "title", Buffer);
        KvSetColor(Msg, "color", r, g, b, 255);
        KvSetNum(Msg, "level", 0);
        KvSetNum(Msg, "time", 20);

        CreateDialog(i, Msg, DialogType_Msg);
        // DialogType_Msg = 0,        /**< just an on screen message */
        // DialogType_Menu,         /**< an options menu */
        // DialogType_Text,         /**< a richtext dialog */
        // DialogType_Entry,         /**< an entry box */
        // DialogType_AskConnect    /**< ask the client to connect to a specified IP */

        CloseHandle(Msg);
      }
    }
  }
}
