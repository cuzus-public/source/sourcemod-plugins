const Rcon = require('srcds-rcon');

const host = '95.31.33.59';
const port = '30002';
const addr = `${host}:${port}`;
const pass = '9Agf1JtycW';

let rcon = Rcon({
  address: addr,
  password: pass
});

(async () => {
  try {
    await rcon.connect();
    console.log('connected');

    const res = await rcon.command('status');
    console.log('rcon res', res);
    console.log('type', typeof res);

    rcon.disconnect();
  } catch (e) {
    console.log('error', e);
  }
})();


