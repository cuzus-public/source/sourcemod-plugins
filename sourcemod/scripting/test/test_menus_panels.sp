#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "test menus panels",
};

// https://world-source.ru/forum/100-2299-1

public OnPluginStart() {
  RegConsoleCmd("menu", MenuTest);
  RegConsoleCmd("panel", PanelTest);
}

public Action:MenuTest(client, args) {
  new Handle:menu = CreateMenu(Menu_Handler);
  SetMenuTitle(menu, "fn @ CreateMenu");
  AddMenuItem(menu, "-", "-----", ITEMDRAW_DISABLED);
  AddMenuItem(menu, "text pref", "1st test");
  AddMenuItem(menu, "no sounds", "2nd no sounds");
  AddMenuItem(menu, "disabled", "this is disabled", ITEMDRAW_DISABLED);
  SetMenuExitButton(menu, true);
  DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public Action:PanelTest(client, args) {
  new Handle:pnl = CreatePanel();
  SetPanelTitle(pnl, "fn @ SetPanelTitle");
  DrawPanelItem(pnl," ",ITEMDRAW_SPACER);
  new String:buf[256];
  for (new i = 0; i < 30; i++) {
    Format(buf, sizeof(buf), "[%d] DrawPanelText (1)1234567890 (2)1234567890 (3)1234567890 (4)1234567890 (5)1234567890 (6)1234567890 (7)1234567890 (8)1234567890 \n", i);
    DrawPanelText(pnl, buf);
  }
  DrawPanelItem(pnl, "Next", ITEMDRAW_NOTEXT); // this is not visible
  DrawPanelItem(pnl, "Exit", ITEMDRAW_CONTROL);
  SendPanelToClient(pnl, client, Panel_Handler, MENU_TIME_FOREVER);
}

public Menu_Handler(Handle:menu, MenuAction:action, client, option) {
  if (action == MenuAction_Select)
  {
    // new String:userid[16];
    // GetMenuItem(menu, option, userid, sizeof(userid));
    // new target = GetClientOfUserId(StringToInt(userid));

    PrintToChat(client, "action MenuAction_Select %d", option);
  }
  else if (action == MenuAction_End)
  {
    PrintToChat(client, "action MenuAction_End");
    CloseHandle(menu);
  }
}

public Panel_Handler(Handle:menu, MenuAction:action, client, option) {
  if (action == MenuAction_Select)
  {
    PrintToChat(client, "action MenuAction_Select %d", option);
  }
  else if (action == MenuAction_Cancel)
  {
    PrintToChat(client, "action MenuAction_Cancel");
  }
}
