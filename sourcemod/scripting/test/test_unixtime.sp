#include <sourcemod>
#include <unixtime_sourcemod>

public Plugin myinfo = {
  name = "test unixtime",
};

/*
https://www.epochconverter.com/

=================================
GetTime( ) = 1673585799
GetTime( ) Time = 01/13/2023 04:56:39 -- GMT time
TimeToUnix Adjusted Time = 01/13/2023 17:56:39
UnixToTime Adjusted Time = 01/13/2023 17:56:39
=================================
Date -> Timestamp: 1488891600
Timestamp -> Date: 03/08/2017 02:00:00
=================================
*/
public OnPluginStart() {
  PrintToServer("=================================");

  int iTime, iTimeAdjusted, iYear, iMonth, iDay, iHour, iMinute, iSecond;

  iTime = GetTime( );

  // Display GetTime( ) value which is UTC time (no +/- adjustment for timezone)
  PrintToServer( "GetTime( ) = %d" , iTime );
  UnixToTime( iTime , iYear , iMonth , iDay , iHour , iMinute , iSecond );
  PrintToServer( "GetTime( ) Time = %02d/%02d/%d %02d:%02d:%02d" , iMonth , iDay , iYear , iHour , iMinute , iSecond );

  // Display time value adjusted with TimeToUnix( )
  iTimeAdjusted = TimeToUnix( iYear , iMonth , iDay , iHour , iMinute , iSecond , UT_TIMEZONE_SERVER );
  UnixToTime( iTimeAdjusted , iYear , iMonth , iDay , iHour , iMinute , iSecond );
  PrintToServer( "TimeToUnix Adjusted Time = %02d/%02d/%d %02d:%02d:%02d" , iMonth , iDay , iYear , iHour , iMinute , iSecond );

  // Display time value adjusted with UnixToTime( )
  UnixToTime( iTime , iYear , iMonth , iDay , iHour , iMinute , iSecond , UT_TIMEZONE_SERVER );
  PrintToServer( "UnixToTime Adjusted Time = %02d/%02d/%d %02d:%02d:%02d" , iMonth , iDay , iYear , iHour , iMinute , iSecond );

  PrintToServer("=================================");

  int iTimeStamp = DateToTimestamp( "3/7/2017 15:30:2" );

  PrintToServer( "Date -> Timestamp: %d", iTimeStamp );

  UnixToTime( iTimeStamp, iYear, iMonth, iDay, iHour, iMinute, iSecond, UT_TIMEZONE_SERVER );

  PrintToServer( "Timestamp -> Date: %02d/%02d/%d %02d:%02d:%02d", iMonth, iDay, iYear, iHour, iMinute, iSecond );

  PrintToServer("=================================");
}

// For date and time - Format: mm/dd/yyyy HH:MM:SS
stock DateTimeToTimestamp( const char[ ] szDate ) {
  char szBuffer[ 64 ];
  strcopy( szBuffer, sizeof( szBuffer ), szDate );

  ReplaceString( szBuffer, sizeof( szBuffer ), "/", " " );
  ReplaceString( szBuffer, sizeof( szBuffer ), ".", " " );
  ReplaceString( szBuffer, sizeof( szBuffer ), ":", " " );

  char szTime[ 6 ][ 6 ];
  ExplodeString( szBuffer, " ", szTime, sizeof( szTime ), sizeof( szTime[ ] ) );

  int iYear = StringToInt( szTime[ 2 ] );
  int iMonth  = StringToInt( szTime[ 0 ] );
  int iDay = StringToInt( szTime[ 1 ] );

  int iHour = StringToInt( szTime[ 3 ] );
  int iMinute  = StringToInt( szTime[ 4 ] );
  int iSecond = StringToInt( szTime[ 5 ] );

  return TimeToUnix( iYear, iMonth, iDay, iHour, iMinute, iSecond, UT_TIMEZONE_SERVER );
}

// Just for date - Format: mm/dd/yyyy
stock DateToTimestamp( const char[ ] szDate ) {
  char szBuffer[ 64 ];
  strcopy( szBuffer, sizeof( szBuffer ), szDate );

  ReplaceString( szBuffer, sizeof( szBuffer ), "/", " " );
  ReplaceString( szBuffer, sizeof( szBuffer ), ".", " " );

  char szTime[ 3 ][ 6 ];
  ExplodeString( szBuffer, " ", szTime, sizeof( szTime ), sizeof( szTime[ ] ) );

  int iYear = StringToInt( szTime[ 2 ] );
  int iMonth  = StringToInt( szTime[ 0 ] );
  int iDay = StringToInt( szTime[ 1 ] );
  int iMinute = 0;
  int iSecond = 0;
  int iHour = 0;

  return TimeToUnix( iYear, iMonth, iDay, iHour, iMinute, iSecond, UT_TIMEZONE_SERVER );
}
