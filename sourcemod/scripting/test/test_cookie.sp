#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <clientprefs>

public Plugin:myinfo = {
  name = "test cookie",
};

// https://forums.alliedmods.net/showthread.php?t=228244
// https://hlmod.ru/threads/sourcepawn-urok-14-rabota-s-kukami-clientprefs.38486/

new Handle:hCookie;
new bool:cookieValue[MAXPLAYERS + 1];

public void OnPluginStart() {
  hCookie = RegClientCookie("TestCookie1", "", CookieAccess_Public);
  // SetCookieMenuItem(QuakePrefSelected, 0, "Cookie Prefs"); // !settings
  SetCookiePrefabMenu(hCookie, CookieMenu_OnOff_Int, "Test Cookie", TestCookieHandler);

  // for (new i = 1; i <= GetMaxHumanPlayers(); i++) {
	// 	if (IsClientInGame(i) && !IsFakeClient(i)) {
	// 		if (AreClientCookiesCached(i)) {
	// 			LoadClientCookiesFor(i);
	// 		}
	// 	} else {
	// 		cookieValue[i] = 0;
	// 	}
	// }
}

public TestCookieHandler(client, CookieMenuAction:action, any:info, String:buffer[], maxlen) {
  switch (action) {
    case CookieMenuAction_DisplayOption: { }
    case CookieMenuAction_SelectOption: {
      OnClientCookiesCached(client);
    }
  }
}

public OnClientCookiesCached(client) {
  new String:sValue[8];
  GetClientCookie(client, hCookie, sValue, sizeof(sValue));
  cookieValue[client] = view_as<bool>(StringToInt(sValue)); // (sValue[0] != '\0' && StringToInt(sValue));
  PrintToChat(client, "OnClientCookiesCached value = %d", view_as<bool>(cookieValue[client]));
}

// public LoadClientCookiesFor(client) {
// 	new String:buffer[5];
// 	GetClientCookie(client, hCookie, buffer, 5);
// 	if (!StrEqual(buffer, "")) {
//     PrintToChat(client, "LoadClientCookiesFor GetClientCookie %s", buffer);
// 		cookieValue[client] = StringToInt(buffer);
// 	}
// }

// public OnClientPostAdminCheck(client) {
// 	if (!IsFakeClient(client)) {
//     PrintToServer("OnClientPostAdminCheck");
// 		if (AreClientCookiesCached(client)) {
// 			LoadClientCookiesFor(client);
// 		}
//   }
// }

// public OnClientCookiesCached(client) {
//   if (IsClientInGame(client) && !IsFakeClient(client)) {
//     PrintToServer("OnClientCookiesCached");
//     PrintToChat(client, "OnClientCookiesCached");
// 		LoadClientCookiesFor(client);
// 	}
// }

// public QuakePrefSelected(client,CookieMenuAction:action,any:info,String:buffer[],maxlen) {
// 	if (action == CookieMenuAction_SelectOption) {
// 		ShowQuakeMenu(client);
// 	}
// }

// public ShowQuakeMenu(client) {
// 	new Handle:menu = CreateMenu(MenuHandlerQuake);
// 	SetMenuTitle(menu, "test menu");
//   new String:buffer[100];
//   Format(buffer, 100, "cur value = %d", cookieValue[client]);
// 	AddMenuItem(menu, "1", buffer);
// 	SetMenuExitButton(menu, true);
// 	DisplayMenu(menu, client, 30);
// }

// public MenuHandlerQuake(Handle:menu, MenuAction:action, client, option) {
// 	if (action == MenuAction_Select) {
// 		if (option == 0) {
//       cookieValue[client] += 1;
// 		}
// 		new String:buffer[5];
// 		IntToString(cookieValue[client], buffer, 5);
// 		SetClientCookie(client, hCookie, buffer);
// 	} else if (action == MenuAction_End) {
// 		CloseHandle(menu);
// 	}
// }
