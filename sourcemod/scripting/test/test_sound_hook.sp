#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "test sound hook",
};

public OnPluginStart() {
  AddNormalSoundHook(SoundHook);
  AddAmbientSoundHook(AmbientSHook);
}

/* Block Knife sounds START */
// public OnClientPutInServer(id) {
//   // note: this hook presumably executes on every frame
//   SDKHook(id, SDKHook_WeaponEquip, WeaponEquip_CallBack);
// }
// public WeaponEquip_CallBack(id, weapons) {
//   if (IsValidEdict(weapons) && IsValidEntity(weapons)) {
//     new String:classname[32];
//     GetEdictClassname(weapons, classname, sizeof(classname));
//     PrintToChatAll("WeaponEquip_CallBack  %s", classname);
//     if (StrEqual(classname, "weapon_knife")) {
//       SDKHook(weapons, SDKHook_SetTransmit, SetTransmit_CallBack);
//     }
//   }
// }
// public Action:SetTransmit_CallBack(entity, viewer) {
//   new owner = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
//   if (viewer > 0 && viewer <= MaxClients) {
//     new weapons = GetEntPropEnt(viewer, Prop_Send, "m_hActiveWeapon");

//     new String:classname[32];
//     GetEdictClassname(weapons, classname, sizeof(classname));
//     PrintToChatAll("SetTransmit_CallBack  %s", classname);

//     if (owner == viewer && weapons == GetPlayerWeaponSlot(viewer, 2))
//       return Plugin_Handled;
//   }

//   if(owner > 0 && owner <= MaxClients && (!IsClientInGame(owner))) {
//     SDKUnhook(entity, SDKHook_SetTransmit, SetTransmit_CallBack);
//   }

//   return Plugin_Continue;
// }
/* Block Knife sounds END */

public Action:SoundHook(clients[64], &numClients, String:sound[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags) {
  new String:classname[120];
  if (entity > 0 && entity <= MaxClients) {
    GetEntityClassname(entity, classname, 120);
  }

  if (StrContains(sound, "player/footsteps") == -1) {
    PrintToServer("%s | %s", sound, classname);
    for (new i = 1; i <= MaxClients; i++) {
      if (IsClientInGame(i)) {
        PrintToChat(i, "%s | %s", sound, classname);
      }
    }
  }

  // not working (client side only?)
  // if (StrContains(sound, "weapons/knife/knife_slash", false) != -1) {
  //   PrintToChatAll("knife_slash stop");
  //   return Plugin_Stop;
  // }
  // if (StrContains(sound, "weapons/knife/knife_stab", false) != -1) {
  //   PrintToChatAll("knife_stab stop");
  //   return Plugin_Stop;
  // }
  // if (StrContains(sound, "weapons/knife/knife_deploy", false) != -1) {
  //   PrintToChatAll("knife_deploy stop");
  //   return Plugin_Stop;
  // }
  // if (StrContains(sound, "weapons/knife/knife_hit", false) != -1) {
  //   PrintToChatAll("knife_hit stop");
  //   return Plugin_Stop;
  // }
  // if (StrEqual(sound, "weapons/knife/knife_hitwall1.wav")) {
  //   PrintToChatAll("stop knife_hitwall1");
  //   return Plugin_Stop;
  // }
  // if (StrContains(sound, "player/footsteps/tile", false) != -1) {
  //   PrintToChatAll("stop footsteps/tile");
  //   flags = SND_STOP|SND_CHANGEVOL;
  //   volume = 0.0;
  //   EmitSoundToAll(kf_globalplayersnd[GetRandomInt(0, 4)], entity, channel, level, SND_NOFLAGS, 0.1);
  //   return Plugin_Stop;
  // }
  // if (StrContains(sound, "items/itempickup", false) != -1) {
  //   PrintToChatAll("items/itempickup stop");
  //   return Plugin_Stop;
  // }
  // if (StrContains(sound, "weapons/c4/c4_click", false) != -1) {
  //   PrintToChatAll("c4_click");
  //   return Plugin_Stop;
  // }
  // if (StrContains(sound, "weapons/c4/c4_plant", false) != -1) {
  //   PrintToChatAll("c4_plant");
  //   return Plugin_Stop;
  // }


  /*
  EmitSoundToAll(const char[] sample,
    int entity = SOUND_FROM_PLAYER,
    int channel = SNDCHAN_AUTO,
    int level = SNDLEVEL_NORMAL,
    int flags = SND_NOFLAGS,
    float volume = SNDVOL_NORMAL,
    int pitch = SNDPITCH_NORMAL,
    int speakerentity = -1,
    const float origin[3] = NULL_VECTOR,
    const float dir[3] = NULL_VECTOR,
    bool updatePos = true,
    float soundtime = 0.0)
  */

  // FIXME: replacing client sounds on server is not possible
  // https://forums.alliedmods.net/showthread.php?t=309867
  // https://forums.alliedmods.net/showthread.php?t=224894

  return Plugin_Continue;
}

public Action:AmbientSHook(String:sound[PLATFORM_MAX_PATH], &entity, &Float:volume, &level, &pitch, Float:pos[3], &flags, &Float:delay) {
  new String:classname[120];
  if (entity > 0 && entity <= MaxClients) {
    GetEntityClassname(entity, classname, 120);
  }

  PrintToServer("(ambient) %s | %s", sound, classname);
  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientInGame(i)) {
      PrintToChat(i, "(ambient) %s | %s", sound, classname);
    }
  }

	return Plugin_Continue;
}
