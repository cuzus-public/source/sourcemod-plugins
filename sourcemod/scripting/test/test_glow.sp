#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "test glow",
};

new ClientGlow[MAXPLAYERS + 1];
#define EF_BONEMERGE       (1 << 0)
#define EF_NOSHADOW        (1 << 4)
#define EF_NORECEIVESHADOW (1 << 6)
#define EF_PARENT_ANIMATES (1 << 9)

public OnPluginStart() {
  RegConsoleCmd("glow", SetGlow);
  RegConsoleCmd("glow2", SetGlow2); // makes glowing model attachment
}

public Action:SetGlow(client, args) {
  new String:arg1[4];
  new String:arg2[4];

  new rand1 = GetRandomInt(1,11);
  new rand2 = GetRandomInt(1,26);
  Format(arg1, sizeof(arg1), "%d", rand1);
  Format(arg2, sizeof(arg2), "%d", rand2);

  if (args > 0) {
    GetCmdArg(1, arg1, sizeof(arg1));
  }
  if (args > 1) {
    GetCmdArg(2, arg2, sizeof(arg2));
  }

  new r = GetRandomInt(0,255);
  new g = GetRandomInt(0,255);
  new b = GetRandomInt(0,255);
  new a = GetRandomInt(0,255);

  for (new i = 1; i <= MaxClients; i++) {
    if (!IsValidClient(i) || client == i) {
      continue;
    }

    // SetEntityRenderColor(i, _, _, _, a);
    SetEntityRenderColor(i, r, g, b, a);

    // https://sm.alliedmods.net/new-api/entity_prop_stocks/RenderMode
    if (StrEqual(arg1, "1")) {
      SetEntityRenderMode(i, RENDER_NORMAL);
    } else if (StrEqual(arg1, "2")) {
      SetEntityRenderMode(i, RENDER_TRANSCOLOR);
    } else if (StrEqual(arg1, "3")) {
      SetEntityRenderMode(i, RENDER_TRANSTEXTURE);
    } else if (StrEqual(arg1, "4")) {
      SetEntityRenderMode(i, RENDER_GLOW);
    } else if (StrEqual(arg1, "5")) {
      SetEntityRenderMode(i, RENDER_TRANSALPHA);
    } else if (StrEqual(arg1, "6")) {
      SetEntityRenderMode(i, RENDER_TRANSADD);
    } else if (StrEqual(arg1, "7")) {
      SetEntityRenderMode(i, RENDER_ENVIRONMENTAL); // transparent
    } else if (StrEqual(arg1, "8")) {
      SetEntityRenderMode(i, RENDER_TRANSADDFRAMEBLEND);
    } else if (StrEqual(arg1, "9")) {
      SetEntityRenderMode(i, RENDER_TRANSALPHAADD);
    } else if (StrEqual(arg1, "10")) {
      SetEntityRenderMode(i, RENDER_WORLDGLOW);
    } else if (StrEqual(arg1, "11")) {
      SetEntityRenderMode(i, RENDER_NONE);
    }

    // https://sm.alliedmods.net/new-api/entity_prop_stocks/RenderFx
    if (StrEqual(arg2, "1")) {
      SetEntityRenderFx(i, RENDERFX_PULSE_SLOW);
    } else if (StrEqual(arg2, "2")) {
      SetEntityRenderFx(i, RENDERFX_PULSE_FAST);
    } else if (StrEqual(arg2, "3")) {
      SetEntityRenderFx(i, RENDERFX_PULSE_SLOW_WIDE);
    } else if (StrEqual(arg2, "4")) {
      SetEntityRenderFx(i, RENDERFX_PULSE_FAST_WIDE);
    } else if (StrEqual(arg2, "5")) {
      SetEntityRenderFx(i, RENDERFX_FADE_SLOW); // transparent
    } else if (StrEqual(arg2, "6")) {
      SetEntityRenderFx(i, RENDERFX_FADE_FAST);
    } else if (StrEqual(arg2, "7")) {
      SetEntityRenderFx(i, RENDERFX_SOLID_SLOW);
    } else if (StrEqual(arg2, "8")) {
      SetEntityRenderFx(i, RENDERFX_SOLID_FAST);
    } else if (StrEqual(arg2, "9")) {
      SetEntityRenderFx(i, RENDERFX_STROBE_SLOW);
    } else if (StrEqual(arg2, "10")) {
      SetEntityRenderFx(i, RENDERFX_STROBE_FAST);
    } else if (StrEqual(arg2, "11")) {
      SetEntityRenderFx(i, RENDERFX_STROBE_FASTER);
    } else if (StrEqual(arg2, "12")) {
      SetEntityRenderFx(i, RENDERFX_FLICKER_SLOW);
    } else if (StrEqual(arg2, "13")) {
      SetEntityRenderFx(i, RENDERFX_FLICKER_FAST);
    } else if (StrEqual(arg2, "14")) {
      SetEntityRenderFx(i, RENDERFX_NO_DISSIPATION); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "15")) {
      SetEntityRenderFx(i, RENDERFX_DISTORT); // similar to hologram
    } else if (StrEqual(arg2, "16")) {
      SetEntityRenderFx(i, RENDERFX_HOLOGRAM);
    } else if (StrEqual(arg2, "17")) {
      SetEntityRenderFx(i, RENDERFX_EXPLODE); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "18")) {
      SetEntityRenderFx(i, RENDERFX_GLOWSHELL); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "19")) {
      SetEntityRenderFx(i, RENDERFX_CLAMP_MIN_SCALE); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "20")) {
      SetEntityRenderFx(i, RENDERFX_ENV_RAIN); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "21")) {
      SetEntityRenderFx(i, RENDERFX_ENV_SNOW); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "22")) {
      SetEntityRenderFx(i, RENDERFX_SPOTLIGHT); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "23")) {
      SetEntityRenderFx(i, RENDERFX_RAGDOLL); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "24")) {
      SetEntityRenderFx(i, RENDERFX_PULSE_FAST_WIDER);
    } else if (StrEqual(arg2, "25")) {
      SetEntityRenderFx(i, RENDERFX_MAX); // no effect with RENDER_NORMAL
    } else if (StrEqual(arg2, "26")) {
      SetEntityRenderFx(i, RENDERFX_NONE);
    }
  }

  PrintToServer("@@ GLOW  %s %s rgba %d %d %d %d", arg1, arg2, r, g, b, a);
  PrintToChatAll("@@ GLOW  %s %s rgba %d %d %d %d", arg1, arg2, r, g, b, a);
}

public Action:SetGlow2(client, args) {
  new String:arg1[4];
  new String:arg2[4];

  new rand1 = GetRandomInt(1,11);
  new rand2 = GetRandomInt(1,26);
  Format(arg1, sizeof(arg1), "%d", rand1);
  Format(arg2, sizeof(arg2), "%d", rand2);

  if (args > 0) {
    GetCmdArg(1, arg1, sizeof(arg1));
  }
  if (args > 1) {
    GetCmdArg(2, arg2, sizeof(arg2));
  }

  new color[3];
  color = {255,0,0};
  new rand = GetRandomInt(1,3);
  if (rand == 1) {
    color = {0,255,0};
  } else if (rand == 2) {
    color = {0,0,255};
  }

  for (new i = 1; i <= MaxClients; i++) {
    if (!IsValidClient(i) || client == i) {
      continue;
    }

    UC_TryDestroyGlow(i);
    UC_CreateGlow(i, color, arg1, arg2);
  }

  PrintToServer("@@ GLOW2  %s %s", arg1, arg2);
  PrintToChatAll("@@ GLOW2  %s %s", arg1, arg2);
}

stock bool:UC_TryDestroyGlow(client) {
	if (ClientGlow[client] != 0 && IsValidEntity(ClientGlow[client])) {
		AcceptEntityInput(ClientGlow[client], "TurnOff");
		AcceptEntityInput(ClientGlow[client], "Kill");
		ClientGlow[client] = 0;
		return true;
	}

	return false;
}

stock bool:UC_CreateGlow(client, Color[3], String:rendermode[4], String:renderfx[4]) {
	ClientGlow[client] = 0;
	new String:Model[PLATFORM_MAX_PATH];

	// Get the original model path
	GetEntPropString(client, Prop_Data, "m_ModelName", Model, sizeof(Model));

	new GlowEnt = CreateEntityByName("prop_dynamic");

	if (GlowEnt == -1)
		return false;

	DispatchKeyValue(GlowEnt, "model", Model);
	DispatchKeyValue(GlowEnt, "disablereceiveshadows", "1");
	DispatchKeyValue(GlowEnt, "disableshadows", "1");
	DispatchKeyValue(GlowEnt, "solid", "0");
	DispatchKeyValue(GlowEnt, "spawnflags", "256");
	DispatchKeyValue(GlowEnt, "renderamt", "0");
	SetEntProp(GlowEnt, Prop_Send, "m_CollisionGroup", 11);

  new String:sColor[25];

  Format(sColor, sizeof(sColor), "%i %i %i", Color[0], Color[1], Color[2]);

  DispatchKeyValue(GlowEnt, "rendermode", rendermode); // "3"
  DispatchKeyValue(GlowEnt, "renderamt", "255");
  DispatchKeyValue(GlowEnt, "renderfx", renderfx); // "14"
  DispatchKeyValue(GlowEnt, "rendercolor", sColor);

	// Spawn and teleport the entity
  DispatchSpawn(GlowEnt);

	new fEffects = GetEntProp(GlowEnt, Prop_Send, "m_fEffects");
	SetEntProp(GlowEnt, Prop_Send, "m_fEffects", fEffects | EF_BONEMERGE | EF_NOSHADOW | EF_NORECEIVESHADOW | EF_PARENT_ANIMATES);

	// Set the activator and group the entity
	SetVariantString("!activator");
	AcceptEntityInput(GlowEnt, "SetParent", client);

	SetVariantString("primary");
	AcceptEntityInput(GlowEnt, "SetParentAttachment", GlowEnt, GlowEnt, 0);

	AcceptEntityInput(GlowEnt, "TurnOn");

	SetEntPropEnt(GlowEnt, Prop_Send, "m_hOwnerEntity", client);

	SDKHook(GlowEnt, SDKHook_SetTransmit, Hook_ShouldSeeGlow);
	ClientGlow[client] = GlowEnt;

	return true;
}

public Action:Hook_ShouldSeeGlow(glow, viewer) {
	if (!IsValidEntity(glow)) {
		SDKUnhook(glow, SDKHook_SetTransmit, Hook_ShouldSeeGlow);
		return Plugin_Continue;
	}

	new client = GetEntPropEnt(glow, Prop_Send, "m_hOwnerEntity");

	if (client == viewer)
		return Plugin_Handled;

	new ObserverTarget = GetEntPropEnt(viewer, Prop_Send, "m_hObserverTarget"); // This is the player the viewer is spectating. No need to check if it's invalid ( -1 )

	if (ObserverTarget == client)
		return Plugin_Handled;

	return Plugin_Continue;
}

stock bool:IsValidClient(client) {
  return client >= 1 && client <= MaxClients && IsClientInGame(client);
}
