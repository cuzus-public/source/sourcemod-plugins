#include <sourcemod>

public Plugin:myinfo = {
  name = "test_motd_server_click",
  description = "",
  author = "Geekrainian @ cuzus.games",
  version = "1.0"
};

new g_playerVGUI[MAXPLAYERS + 1] = { 0, ... };

public void OnPluginStart() {
  HookUserMessage(GetUserMessageId("VGUIMenu"), VGUIMenu, true);

  RegConsoleCmd("web_to_server_click1", WebClick1);
}

public Action WebClick1(int client, int args) {
  // client - is a server here = 0
  if (!client) {
    PrintToServer("WebClick by server");

    new String:arg1[192], String:arg2[192], String:arg3[192], String:arg4[192];
    GetCmdArg(1, arg1, sizeof(arg1));
    GetCmdArg(2, arg2, sizeof(arg2));
    GetCmdArg(3, arg3, sizeof(arg3));
    GetCmdArg(4, arg4, sizeof(arg4));

    PrintToServer("args %s %s %s %s", arg1, arg2, arg3, arg4);
  }

  return Plugin_Handled;
}

public bool:OnClientConnect(client, String:rejectmsg[], maxlen) {
	g_playerVGUI[client] = 1;

	return true;
}

public OnClientDisconnect(client) {
  g_playerVGUI[client] = 0;
}

public Action:VGUIMenu(UserMsg:msg_id, Handle:bf, const players[], playersNum, bool:reliable, bool:init)
{
  new String:buffer[256];
  BfReadString(bf, buffer, sizeof(buffer));
  new client = players[0];

  // block initial motd
  if (StrEqual(buffer, "info") && IsClientConnected(client) && !IsFakeClient(client)) {
    if (g_playerVGUI[client] == 1) {
      return Plugin_Handled;
    }
  }

  return Plugin_Continue;
}

public OnClientPostAdminCheck(client) {
  DataPack pack;
  CreateDataTimer(0.01, DisplayMsg, pack);
  pack.WriteCell(client);
}

Action DisplayMsg(Handle timer, DataPack pack) {
  pack.Reset();
  new client = pack.ReadCell();

  if (IsClientConnected(client) && !IsFakeClient(client)) {
    g_playerVGUI[client] = 0;
    ShowMOTDPanel(client, "123", "http://cuzus.games/temp/motd/", MOTDPANEL_TYPE_URL);
  }
}

// Check if initial motd opened and disable use keys
/*
public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
  new iInitialButtons = buttons;

  if (buttons & IN_BACK && Blocked_Keys[KEY_IN_BACK]) {
    buttons &= ~IN_BACK;
  }
  if (buttons & IN_CANCEL && Blocked_Keys[KEY_IN_CANCEL]) {
    buttons &= ~IN_CANCEL;
  }
  if (buttons & IN_RUN && Blocked_Keys[KEY_IN_RUN]) {
    buttons &= ~IN_RUN;
  }
  if (buttons & IN_SCORE && Blocked_Keys[KEY_IN_SCORE]) {
    buttons &= ~IN_SCORE;
  }
  if (buttons & IN_USE && Blocked_Keys[KEY_IN_USE]) {
    buttons &= ~IN_USE;
  }

  if (iInitialButtons != buttons)
    return Plugin_Changed

  return Plugin_Continue;
}
*/
