#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin myinfo = {
  name = "test fade shake"
};

#define FFADE_IN		0x0001		// Just here so we don't pass 0 into the function
#define FFADE_OUT		0x0002		// Fade out (not in)
#define FFADE_MODULATE	0x0004		// Modulate (don't blend)
#define FFADE_STAYOUT	0x0008		// ignores the duration, stays faded out until new ScreenFade message received
#define FFADE_PURGE		0x0010		// Purges all other fades, replacing them with this one

public void OnPluginStart() {
  RegConsoleCmd("fd", FadeCmd);
  RegConsoleCmd("fd2", FadeCmd2);
  RegConsoleCmd("sh", ShakeCmd);
}

public Action:FadeCmd2(client, args) {
  CreateTimer(1.0, FadeTimer, client, TIMER_REPEAT);
}

public Action:FadeTimer(Handle:timer, any:client) {
  new alpha = GetRandomInt(80, 100);
  new time = GetRandomInt(120, 160);
  // Client_Fade(client, 0, 800, FFADE_PURGE|FFADE_IN, 255, 255, 0, alpha);
  PrintToChat(client, "alpha %d  time %d", alpha, time);

  Client_ScreenFade(client, time, FFADE_PURGE|FFADE_IN, 0, 255, 217, 102, alpha);
}


public Action:FadeCmd(client, args) {
  new String:damage[5];
  if (args > 0) {
    GetCmdArg(1, damage, sizeof(damage));
  } else {
    Format(damage, sizeof(damage), "100");
  }

  new String:duration[5];
  if (args > 1) {
    GetCmdArg(1, duration, sizeof(duration));
  } else {
    Format(duration, sizeof(duration), "1001");
  }

  new damageInt = StringToInt(damage);
  new durationMs = damageInt * 15;
  new red = RoundToNearest(damageInt * 10.0);
  if (red > 255) {
    red = 255;
  }

  new alpha = RoundToNearest(damageInt * 2.5);
  if (alpha > 255) {
    alpha = 255;
  }

  new durationFinal = durationMs < 500 ? 500 : (durationMs > 900 ? 900 : durationMs);

  Client_Fade(client, 0, durationFinal, FFADE_PURGE|FFADE_IN, red, 0, 0, alpha);

  PrintToChat(client, "fade  damage=%d duration=%d red=%d alpha=%d", damageInt, durationFinal, red, alpha);

  return Plugin_Handled;
}

public Action:ShakeCmd(client, args) {
  new String:damage[5];
  if (args > 0) {
    GetCmdArg(1, damage, sizeof(damage));
  } else {
    Format(damage, sizeof(damage), "100");
  }

  new damageInt = StringToInt(damage);
  new Float:amplitude = damageInt / 7.0;
  new Float:durationMs = damageInt / 50.0;

  if (durationMs < 0.6) {
    durationMs = 0.6;
  } else if (durationMs > 1.0) {
    durationMs = 1.0;
  }

  if (amplitude < 4.5) {
    amplitude = 4.5;
  }

  Client_Shake(client, amplitude, 1.0, durationMs);

  PrintToChat(client, "shake  damage=%d duration=%f amplitude=%f", damageInt, durationMs, amplitude);

  return Plugin_Handled;
}

stock bool:Client_ScreenFade(client, duration, mode, holdtime=-1, r=0, g=0, b=0, a=255, bool:reliable=true) {
	new Handle:userMessage = StartMessageOne("Fade", client, (reliable?USERMSG_RELIABLE:0));

	if (userMessage == INVALID_HANDLE) {
		return false;
	}

	BfWriteShort(userMessage,	duration);	// Fade duration
	BfWriteShort(userMessage,	holdtime);	// Fade hold time
	BfWriteShort(userMessage,	mode);		// What to do
	BfWriteByte(userMessage, r);			// Color R
	BfWriteByte(userMessage, g);			// Color G
	BfWriteByte(userMessage, b);			// Color B
	BfWriteByte(userMessage, a);			// Color Alpha
	EndMessage();

	return true;
}

stock Client_Fade(client, hold, length, type, r, g, b, a) {
	new Handle:message = StartMessageOne("Fade", client);
  if (message == INVALID_HANDLE) {
		return;
	}
  BfWriteShort(message, length);	// FIXED 16 bit, with SCREENFADE_FRACBITS fractional, milliseconds duration
  BfWriteShort(message, hold);	// FIXED 16 bit, with SCREENFADE_FRACBITS fractional, milliseconds duration until reset (fade & hold)
  BfWriteShort(message, type); // fade type (in / out) FFADE_PURGE|FFADE_IN
  BfWriteByte(message, r);	// fade red
  BfWriteByte(message, g);	// fade green
  BfWriteByte(message, b);	// fade blue
  BfWriteByte(message, a);// fade alpha
  EndMessage();
}

stock Client_Shake(client, Float:amplitude = 50.0, Float:frequency = 150.0, Float:duration = 3.0) {
	new Handle:message = StartMessageOne("Shake", client);
	if (message == INVALID_HANDLE) {
		return;
	}
	BfWriteByte(message,	0);	// Shake Command
	BfWriteFloat(message,	amplitude);	// shake magnitude/amplitude (use 0.0 to stop shake)
	BfWriteFloat(message,	frequency);	// shake noise frequency
	BfWriteFloat(message,	duration);	// shake lasts this long
	EndMessage();
}
