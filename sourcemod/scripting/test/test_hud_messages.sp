#include <sourcemod>

public Plugin:myinfo = {
  name = "test hud messages",
};

public void OnPluginStart() {
	CreateTimer(7.0, Repeat);
}

public Action:Repeat(Handle:timer) {
  new String:buf[4048];
  new String:orig[4048];

  // for (new i = 0; i < 6; i++) {
  Format(orig, sizeof(orig), "%s\n╬ Super cool and very loong message ® Даже с Русским Языком 1 2 3 4 5 6 7 8 9 0", orig);
  // }

  for (new d = 1; d <= MaxClients; d++) {
    if (IsClientConnected(d) && !IsFakeClient(d)) {
      SetHudTextParams(-1.0, 0.35, 3.0, 255, 255, 255, 0, 1, 1.0, 2.0, 2.0);
      Format(buf, sizeof(buf), "[hud0] %s", orig);
      ShowHudText(d, 0, buf);

      SetHudTextParams(-1.0, 0.4, 3.0, 255, 255, 0, 0, 1, 1.0, 2.0, 2.0);
      Format(buf, sizeof(buf), "[hud1] %s", orig);
      ShowHudText(d, 1, buf);

      SetHudTextParams(-1.0, 0.45, 3.0, 0, 255, 255, 0, 1, 1.0, 2.0, 2.0);
      Format(buf, sizeof(buf), "[hud2] %s", orig);
      ShowHudText(d, 2, buf);

      SetHudTextParams(-1.0, 0.5, 3.0, 255, 0, 255, 0, 1, 1.0, 2.0, 2.0);
      Format(buf, sizeof(buf), "[hud3] %s", orig);
      ShowHudText(d, 3, buf);

      SetHudTextParams(-1.0, 0.55, 3.0, 0, 0, 255, 0, 1, 1.0, 2.0, 2.0);
      Format(buf, sizeof(buf), "[hud4] %s", orig);
      ShowHudText(d, 4, buf);

      SetHudTextParams(-1.0, 0.6, 3.0, 255, 100, 100, 0, 1, 1.0, 2.0, 2.0);
      Format(buf, sizeof(buf), "[hud5] %s", orig);
      ShowHudText(d, 5, buf);

      // SetHudTextParams(-1.0, 0.65, 3.0, 100, 100, 255, 0, 1, 1.0, 2.0, 2.0);
      // Format(buf, sizeof(buf), "[hud6] %s", orig);
      // ShowHudText(d, 6, buf);

      // SetHudTextParams(-1.0, 0.7, 3.0, 100, 100, 255, 0, 1, 1.0, 2.0, 2.0);
      // Format(buf, sizeof(buf), "[hud7] %s", orig);
      // ShowHudText(d, 7, buf);

      // SetHudTextParams(-1.0, 0.75, 3.0, 100, 155, 255, 0, 1, 1.0, 2.0, 2.0);
      // Format(buf, sizeof(buf), "[hud8] %s", orig);
      // ShowHudText(d, 8, buf);

      PrintToConsole(d, buf);

      for (new q = 0; q < 10; q++) {
        Format(buf, sizeof(buf), "[PrintToChat] %s", orig);
        PrintToChat(d, buf);
      }

      for (new q = 0; q < 10; q++) {
        Format(buf, sizeof(buf), "[PrintCenterText] %s", orig);
        PrintCenterText(d, buf);
      }

      for (new q = 0; q < 10; q++) {
        Format(buf, sizeof(buf), "[PrintHintText] %s", orig);
        PrintHintText(d, buf);
      }
    }
  }

  CreateTimer(7.0, Repeat);

  return Plugin_Stop;
}
