#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
  name = "test third person camera",
};

public OnPluginStart() {
  RegConsoleCmd("3rd", Command_ThirdPerson);
}

public Action:Command_ThirdPerson(client, args) {
  if (GetRandomInt(0, 1) == 1) {
    SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", 0);
    SetEntProp(client, Prop_Send, "m_iObserverMode", 1);
    SetEntProp(client, Prop_Send, "m_bDrawViewmodel", 0);
    SetEntProp(client, Prop_Send, "m_iFOV", 120);
  } else {
    SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", -1);
    SetEntProp(client, Prop_Send, "m_iObserverMode", 0);
    SetEntProp(client, Prop_Send, "m_bDrawViewmodel", 1);
    SetEntProp(client, Prop_Send, "m_iFOV", 90);
  }
}
