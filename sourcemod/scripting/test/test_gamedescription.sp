
#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

public Plugin myinfo = {
  name = "test gamedescription"
};

// tested: not working
// public Action:OnGetGameDescription(String:gameDesc[64]) {
// 	Format(gameDesc, sizeof(gameDesc), "<<<TESTING XXX !@#$%>>>");
// 	return Plugin_Changed;
// }

// tested: not working
#define GAMEDESC "<<<TESTING XXX !@#$%>>>"
new bool:MS_overrideGameDesc = false;
public OnMapStart() {
	MS_overrideGameDesc = true;
}
public OnMapEnd() {
	MS_overrideGameDesc = false;
}
public Action:OnGetGameDescription(String:gameDesc[64]) {
	if (MS_overrideGameDesc) {
		strcopy(gameDesc, sizeof(gameDesc), GAMEDESC);
		return Plugin_Changed;
	}
	return Plugin_Continue;
}
