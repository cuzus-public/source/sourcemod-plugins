#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin myinfo = {
  name = "test blood"
};

new String:g_flags[128];
new String:g_amount[128];
new String:g_angles[128];
new Handle:myTimer = INVALID_HANDLE;

new bloodDecal[13];

public OnPluginStart() {
  RegConsoleCmd("b", CmdBloodOnce);
  RegConsoleCmd("lb", CmdBloodLoop);
  RegConsoleCmd("a", CmdSetAngles);
  RegConsoleCmd("dec", CmdGoreDecal);
}

public OnMapStart() {
  bloodDecal[0] = PrecacheDecal("decals/blood_splatter.vtf");
	bloodDecal[1] = PrecacheDecal("decals/bloodstain_003.vtf");
	bloodDecal[2] = PrecacheDecal("decals/bloodstain_101.vtf");
	bloodDecal[3] = PrecacheDecal("decals/bloodstain_002.vtf");
	bloodDecal[4] = PrecacheDecal("decals/bloodstain_001.vtf");
	bloodDecal[5] = PrecacheDecal("decals/blood8.vtf");
	bloodDecal[6] = PrecacheDecal("decals/blood7.vtf");
	bloodDecal[7] = PrecacheDecal("decals/blood6.vtf");
	bloodDecal[8] = PrecacheDecal("decals/blood5.vtf");
	bloodDecal[9] = PrecacheDecal("decals/blood4.vtf");
	bloodDecal[10] = PrecacheDecal("decals/blood3.vtf");
	bloodDecal[11] = PrecacheDecal("decals/blood2.vtf");
	bloodDecal[12] = PrecacheDecal("decals/blood1.vtf");
}

public Action:CmdSetAngles(client, args) {
  GetCmdArgString(g_angles, sizeof(g_angles));
  PrintToChat(client, "Set angles to %s", g_angles);
}

public Action:CmdGoreDecal(client, args) {
  new String:count[4];
  if (args > 0) {
    GetCmdArg(1, count, sizeof(count));
  } else {
    Format(count, sizeof(count), "%s", "30");
  }

  PrintToChat(client, "Decals created - %s", count);

  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientConnected(i) && IsPlayerAlive(i)) {
      TE_WorldDecal(i, StringToInt(count));
    }
  }
}

public Action:CmdBloodLoop(client, args) {
  if (args > 0) {
    GetCmdArg(1, g_flags, sizeof(g_flags));
  } else {
    Format(g_flags, sizeof(g_flags), "%s", "12");
  }

  if (args > 1) {
    GetCmdArg(2, g_amount, sizeof(g_amount));
  } else {
    Format(g_amount, sizeof(g_amount), "%s", "1000");
  }

  if (myTimer == INVALID_HANDLE) {
    PrintToChat(client, "Loop started");
    myTimer = CreateTimer(2.0, Loop, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
  }

  return Plugin_Handled;
}

public Action:Loop(Handle:timer) {
  PrintToChatAll("Loop timer");
  new Float:dir[3];
  new Float:entOrigin[3];
  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientConnected(i) && IsPlayerAlive(i)) {
      Env_Blood(i, g_flags, g_amount, dir, entOrigin, 0);
    }
  }
  // return Plugin_Handled;
}

public Action:CmdBloodOnce(client, args) {
	new String:flags[128];
  if (args > 0) {
    GetCmdArg(1, flags, sizeof(flags));
  } else {
    Format(flags, sizeof(flags), "%s", "12");
  }

  new String:amount[128];
  if (args > 1) {
    GetCmdArg(2, amount, sizeof(amount));
  } else {
    Format(amount, sizeof(amount), "%s", "1000");
  }

  // PrintToChat(client, "flags %s  amount %s", flags, amount);

  new Float:clientPos[3];
  new Float:targetPos[3];
  new Float:dir[3];
  new Float:entOrigin[3];
  GetEntPropVector(client, Prop_Send, "m_vecOrigin", clientPos);

  GetClientAbsOrigin(client, entOrigin);
  entOrigin[2] += GetRandomFloat(-200.0, 200.0);
  PrintToChat(client, "entOrigin Z  %f", entOrigin[2]);

  new rand = GetRandomInt(1, 3);
  new bitflags = 0;
  if (rand == 1) {
    bitflags = 1|4|20;
  } else if (rand == 2) {
    bitflags = 1|4|20|40;
  } else if (rand == 3) {
    bitflags = 1|4|40;
  }
  PrintToChat(client, "rand %d  bitflags %d", rand, bitflags);

  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientConnected(i) && IsPlayerAlive(i)) {
      GetEntPropVector(i, Prop_Send, "m_vecOrigin", targetPos);
      CalculateDirection(clientPos, targetPos, dir);
      // PrintToChat(client, "%d Dir %f %f %f", client, dir[0], dir[1], dir[2]);
      Env_Blood(i, flags, amount, dir, entOrigin, bitflags);
    }
  }

	return Plugin_Handled;
}

Env_Blood(client, String:flags[], String:amount[], Float:dir[3], Float:entOrigin[3], bitflags) {
  new ent = CreateEntityByName("env_blood");
	if ((ent == -1) || (!IsValidEdict(ent))) {
		return;
	}

  new String:angles[128];
  // dir[0] = GetRandomFloat(-1.0, 1.0);
  // dir[1] = GetRandomFloat(-1.0, 1.0);
  // dir[2] = 0.0; // -1.0;
	Format(angles, sizeof(angles), "%f %f %f", dir[0], dir[1], dir[2]);

  // PrintToChat(client, "client %d  angles %s", client, angles);

  if (strlen(g_angles) > 0) {
    Format(angles, sizeof(angles), "%s", g_angles);
  }

	DispatchSpawn(ent);
  DispatchKeyValue(ent, "color", "0");
	DispatchKeyValue(ent, "amount", amount);
  DispatchKeyValue(ent, "spraydir", angles);

  // tested - no difference  1|4|20|40   1|4|40
  // SetEntProp(ent, Prop_Data, "m_spawnflags", bitflags);

  // 4 5 12 13 large blood drop from head/chest (random particle)
  // 6 14 long blood spray from head/chest/outside (can change direction with spraydir)
	DispatchKeyValue(ent, "spawnflags", flags);

  // tested - no difference
  // DispatchKeyValueVector(ent, "origin", entOrigin);

	AcceptEntityInput(ent, "emitblood", client);
  AcceptEntityInput(ent, "kill");
}

stock CalculateDirection(Float:ClientOrigin[3], Float:AttackerOrigin[3], Float:Direction[3]) {
	decl Float:RatioDiviser, Float:Diviser, Float:MaxCoord;

	Direction[0] = (ClientOrigin[0] - AttackerOrigin[0]) + GetRandomFloat(-25.0, 25.0);
	Direction[1] = (ClientOrigin[1] - AttackerOrigin[1]) + GetRandomFloat(-25.0, 25.0);
	Direction[2] = (GetRandomFloat(-125.0, -75.0));

	if (FloatAbs(Direction[0]) >= FloatAbs(Direction[1]))
    MaxCoord = FloatAbs(Direction[0]);
	else
    MaxCoord = FloatAbs(Direction[1]);

	RatioDiviser = GetRandomFloat(100.0, 250.0);
	Diviser = MaxCoord / RatioDiviser;
	Direction[0] /= Diviser;
	Direction[1] /= Diviser;
}

TE_WorldDecal(client, count) {
	new decal;
	new Float:origin[3];

	GetClientAbsOrigin(client, origin);

	for (new i = 0; i < count; i++) {
		origin[0] += GetRandomFloat(-50.0, 50.0);
		origin[1] += GetRandomFloat(-50.0, 50.0);

		if (GetRandomInt(1, 3) == 3)
			decal = GetRandomInt(2, 4);
		else
			decal = GetRandomInt(5, 12);

		TE_Start("World Decal");
		TE_WriteVector("m_vecOrigin", origin);
		TE_WriteNum("m_nIndex", bloodDecal[decal]);
		TE_SendToAll();
	}
}
