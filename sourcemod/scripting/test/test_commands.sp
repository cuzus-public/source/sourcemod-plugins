#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "test commands",
};

#define EF_DIMLIGHT (1 << 2)
#define FLASHLIGHT_SOUND "items/flashlight1.wav"

#define IsClientValidAlive(%1) (1<=%1<=MaxClients && IsPlayerAlive(%1))

new g_thirdcam[MAXPLAYERS + 1] = 0;

new Float:moveSpeed = 250.0;

public OnPluginStart() {
  RegConsoleCmd("setcam", Set_Cam);
  RegConsoleCmd("addfake", AddFakeClient);
  RegConsoleCmd("setspeed", SetMoveSpeed);
  RegConsoleCmd("setduck", SetDuck);
  RegConsoleCmd("blockattack", BlockAttack);
  RegConsoleCmd("setflash", SetFlash);
  RegConsoleCmd("getammo", GetAmmo);
  RegConsoleCmd("getammo2", GetAmmo2);
  RegConsoleCmd("setobs", SetObs);
  RegConsoleCmd("freeze", Freeze);
  RegConsoleCmd("mvp", TestMvp);
  RegConsoleCmd("dead", TestDead);

  RegConsoleCmd("temp", TempCmd);
  // HookEvent("player_hurt",	EventPlayerHurt); // knockback test
}

public Action:TestDead(client, args) {
  new Handle:newEvent = CreateEvent("player_death");
  SetEventInt(newEvent, "userid", GetClientUserId(client));

  if (GetRandomInt(1, 2) == 1) {
    SetEventInt(newEvent, "attacker", GetClientUserId(0));
    PrintToChat(client, "player_death with attacker");
  } else {
    PrintToChat(client, "player_death self");
  }

  SetEventInt(newEvent, "weaponid", 0);

  SetEventString(newEvent, "weapon", "smokegrenade"); // knife

  // SetEventBool(newEvent, "headshot", true);

  // new String:weapon[64];
  // GetEventString(event, "weapon", weapon, sizeof(weapon));
  // SetEventString(newEvent, "weapon", weapon);
  // SetEventInt(newEvent, "killassistevent", 1);
  // SetEventInt(newEvent, "killassistattacker", fakeClient);
  FireEvent(newEvent, false);
}

new g_mvp[MAXPLAYERS + 1];
public Action:TestMvp(client, args) {
  CS_SetMVPCount(client, g_mvp[client]);
  PrintToChat(client, ">>> Set MVP to %d", g_mvp[client]);
  g_mvp[client]++;
}

public Action:TempCmd(client, args) {
  // PrintHintText(client, "<span style='color:red;'>Damage</span> : 123456");
}

public Action:Freeze(client, args) {
  for (new i = 1; i <= MaxClients; i++) {
    if (i == client || !IsValidClient(i) || !IsPlayerAlive(i)) {
      continue;
    }
    SetEntityMoveType(i, MOVETYPE_NONE);
  }
  PrintToChat(client, "FREEZE");
}

public OnMapStart() {
  PrecacheSound(FLASHLIGHT_SOUND);
}

public Action:EventPlayerHurt(Handle:event, const String:name[], bool:dontBroadcast) {
  new nClientVictim = GetClientOfUserId(GetEventInt(event, "userid"));
	new nClientAttacker	= GetClientOfUserId(GetEventInt(event, "attacker"));

  if (IsFakeClient(nClientAttacker))
    return Plugin_Continue;

  new Float:fKnockback = GetRandomFloat(0.0, 500.0);

  PrintToChat(nClientAttacker, "knock %f", fKnockback);
  if (fKnockback > 0.0)
    ImpactKnockback(nClientAttacker, nClientVictim, fKnockback, 23.5);

  return Plugin_Continue;
}

public OnClientPutInServer(x) {
  SDKHook(x, SDKHook_PreThinkPost, OnPreThinkPost);
}

public OnPreThinkPost(x) {
  if (IsClientValidAlive(x) && IsFakeClient(x)) {
    SetEntPropFloat(x, Prop_Data, "m_flMaxspeed", moveSpeed);
  }
}

stock ImpactKnockback(nClientAttacker, nClientVictim, Float:fKnockback, Float:fUp = 0.0) {
	decl Float:pfEyeAngles[3];
	GetClientEyeAngles(nClientAttacker, pfEyeAngles);

	decl Float:pfKnockbackAngles[3];
	pfKnockbackAngles[0] = FloatMul(Cosine(DegToRad(pfEyeAngles[1])), fKnockback);
	pfKnockbackAngles[1] = FloatMul(Sine(DegToRad(pfEyeAngles[1])), fKnockback);
	pfKnockbackAngles[2] = FloatMul(Sine(DegToRad(pfEyeAngles[0]+fUp)), fKnockback);

	TeleportEntity(nClientVictim, NULL_VECTOR, NULL_VECTOR, pfKnockbackAngles);
}

public Action:SetObs(client, args) {
  /*
  OBS_MODE_NONE = 0,	// not in spectator mode
	OBS_MODE_DEATHCAM,	// special mode for death cam animation
	OBS_MODE_FREEZECAM,	// zooms to a target, and freeze-frames on them
	OBS_MODE_FIXED,		// view from a fixed camera position
	OBS_MODE_IN_EYE,	// follow a player in first person view
	OBS_MODE_CHASE,		// follow a player in third person view
	OBS_MODE_ROAMING,	// free roaming
  */

  PrintToChat(client, "Set obs mode");

  SetEntPropVector(client, Prop_Data, "m_vecViewOffset", NULL_VECTOR);
  SetEntityMoveType(client, MOVETYPE_OBSERVER);
  SetEntProp(client, Prop_Send, "m_iObserverMode", 5);

  // SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", 0);
  // SetEntProp(client, Prop_Send, "m_iObserverMode", 1);
  // SetEntProp(client, Prop_Send, "m_bDrawViewmodel", 0);
  // SetEntProp(client, Prop_Send, "m_iFOV", 120);

  // Client_SetObserverMode(client, OBS_MODE_CHASE);
}

public Action:GetAmmo(client, args) {
  new ammo = GetPrimaryAmmo(client);
  PrintToChat(client, "ammo = %d", ammo);
}

public Action:GetAmmo2(client, args) {
  new ammo = GetReserveAmmo(client);
  PrintToChat(client, "ammo2 = %d", ammo);
}

public Action:SetFlash(client, args) {
  new effects = GetEntProp(client, Prop_Send, "m_fEffects");
  if (!(effects & EF_DIMLIGHT)) {
    SetEntProp(client, Prop_Data, "m_fEffects", effects|EF_DIMLIGHT);
    EmitSoundToAll(FLASHLIGHT_SOUND, client);
  } else {
    SetEntProp(client, Prop_Send, "m_fEffects", effects & ~EF_DIMLIGHT);
  }
}

public Action:SetDuck(client, args) {
  new flags = GetEntityFlags(client);
  if (!(flags & IN_DUCK)) {
    SetEntProp(client, Prop_Data, "m_fFlags", flags|IN_DUCK);
  } else {
    SetEntProp(client, Prop_Send, "m_fFlags", flags & ~IN_DUCK);
  }
}

public Action:BlockAttack(client, args) {
  for (new i = 1; i <= MaxClients; i++) {
    if (i != client && IsValidClient(i) && IsPlayerAlive(i)) {
      SetEntPropFloat(client, Prop_Data, "m_flNextAttack", GetGameTime() + 999999.0);
    }
  }
}

public Action:SetMoveSpeed(client, args) {
  new String:arg1[11];
  if (args > 0) {
    GetCmdArg(1, arg1, sizeof(arg1));
    moveSpeed = StringToInt(arg1) + 0.0;
  }

  PrintToChat(client, "Set move speed to %s", arg1);
}

// https://forums.alliedmods.net/showthread.php?t=287740
public Action:AddFakeClient(client, args) {
  new String:text[64];
  Format(text, sizeof(text), "bot%d", GetRandomInt(1, 999));
  new FakeClientId = CreateFakeClient(text);
  PrintToChat(client, "bot spawn  %d %s", FakeClientId, text);
  if (FakeClientId > 0) {
    ChangeClientTeam(FakeClientId, GetClientTeam(client));

    // new Handle:spawn = CreateEvent("player_spawn", true);
    // SetEventInt(spawn, "userid", FakeClientId);
    // FireEvent(spawn, false);
    CS_RespawnPlayer(FakeClientId);

    // ChangeClientTeam(FakeClientId, GetRandomInt(1,2));
    SetEntProp(FakeClientId, Prop_Data, "m_iFrags", -999);
    PrintToServer("@@ AddFakeClient  %s  m_iFrags=%d", text, GetEntProp(FakeClientId, Prop_Data, "m_iFrags"));
  }

  // KickClient(FakeClientId);
}

public Action:Set_Cam(client, args) {
  new String:fov[4];
  GetCmdArg(1, fov, sizeof(fov));

  if (StrEqual(fov, "")) {
    Format(fov, sizeof(fov), "120");
  }

  if (!g_thirdcam[client]) {
    SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", 0);
    SetEntProp(client, Prop_Send, "m_iObserverMode", 1);
    SetEntProp(client, Prop_Send, "m_bDrawViewmodel", 0);
    SetEntProp(client, Prop_Send, "m_iFOV", StringToInt(fov));
    g_thirdcam[client] = true;
  } else {
    SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", client);
    SetEntProp(client, Prop_Send, "m_iObserverMode", 0);
    SetEntProp(client, Prop_Send, "m_bDrawViewmodel", 1);
    SetEntProp(client, Prop_Send, "m_iFOV", 90);
    g_thirdcam[client] = false;
  }
}

stock bool:IsValidClient(client) {
  return client >= 1 && client <= MaxClients && IsClientInGame(client);
}

stock GetPrimaryAmmo(client) {
  new iWeapon = GetEntDataEnt2(client, FindSendPropInfo("CCSPlayer", "m_hActiveWeapon"));
  return GetEntData(iWeapon, FindSendPropInfo("CBaseCombatWeapon", "m_iClip1"));
}

stock GetReserveAmmo(client) {
  new weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
  if (weapon < 1) return -1;

  new ammotype = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
  if (ammotype == -1) return -1;

  return GetEntProp(client, Prop_Send, "m_iAmmo", _, ammotype);
}

// ===========================================================
// TODO: test this
#define BOUNDINGBOX_INFLATION_OFFSET 3
stock bool:IsPlayerStuck(Float:pos[3], entity) {
	new Float:mins[3];
	new Float:maxs[3];

	GetClientMins(entity, mins);
	GetClientMaxs(entity, maxs);

	for (new i=0; i<sizeof(mins); i++) {
		mins[i] -= BOUNDINGBOX_INFLATION_OFFSET;
		maxs[i] += BOUNDINGBOX_INFLATION_OFFSET;
	}

	TR_TraceHullFilter(pos, pos, mins, maxs, MASK_SOLID, TraceEntityFilterExcludeClient, entity);

	return TR_DidHit();
}

stock bool:IsClientBot(client) {
  new String:SteamID[256];
  GetClientAuthString(client, SteamID, sizeof(SteamID));
  if (StrEqual(SteamID, "BOT"))
    return true;
  return false;
}

stock DebugToFile(const String:szFormat[] = "", any:...) {
	new String:szMessage[251];
	SetGlobalTransTarget( 0 );
	VFormat( szMessage, sizeof( szMessage ), szFormat, 2 );

  new String:szFile[PLATFORM_MAX_PATH];
  FormatTime( szFile, sizeof( szFile ), "%Y%m%d" );
  BuildPath( Path_SM, szFile, sizeof( szFile ), "logs/nmp_deaths_%s.log", szFile );
  LogToFile( szFile, szMessage );
}

stock StartClientProgressBar( client, duration ) {
	SetEntPropFloat(client, Prop_Send, "m_flProgressBarStartTime", GetGameTime());
	SetEntProp(client, Prop_Send, "m_iProgressBarDuration", duration );
}

stock StopClientProgressBar( client ) {
	SetEntProp(client, Prop_Send, "m_iProgressBarDuration", 0 );
}
