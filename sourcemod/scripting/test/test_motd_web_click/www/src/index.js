const host = "ws://95.31.33.59:30100/";

let socket;
let connected = false;

function init() {
	socket = new WebSocket(host);

	socket.onopen = (e) => {
		console.log('onopen', e, socket.readyState);
		connected = true;
	};

	socket.onclose = (e) => {
		connected = false;
		console.log('onclose', e)
	};

	socket.onmessage = (e) => console.log('onmessage', e);
	socket.onerror = (e) => {
		connected = false;
		console.log('onerror', e);
	}
}

init();

var but1 = document.getElementById('but1');

but1.addEventListener('click', (e) => {
	console.log('click1');
	if (connected) {
		socket.send('but1');
	}
});

var but2 = document.getElementById('but2');

but2.addEventListener('click', (e) => {
	console.log('click1');
	if (connected) {
		socket.send('but2');
	}
});
