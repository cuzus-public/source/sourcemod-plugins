const Rcon = require("srcds-rcon");

const host = "10.211.1.26"; // internal ip is not working here
const port = "27015";
const addr = `${host}:${port}`;
const pass = "9Agf1JtycW";

let rcon = Rcon({
  address: addr,
  password: pass,
});

(async () => {
  try {
    await rcon.connect();
    console.log("connected");

    const res = await rcon.command("test_rcon");
    console.log("rcon res", res);
    console.log("type", typeof res);

    rcon.disconnect();
  } catch (e) {
    console.log("error", e);
  }
})();
