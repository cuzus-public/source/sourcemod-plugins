#include <sourcemod>

public Plugin:myinfo = {
  name = "MOTD Web Click",
};

// tested with websocket_test.sp

new g_playerVGUI[MAXPLAYERS + 1] = { 0, ... };

public void OnPluginStart() {
  HookUserMessage(GetUserMessageId("VGUIMenu"), VGUIMenu, true);

  RegConsoleCmd("web_to_server_click1", WebClick1);

  RegServerCmd("test_rcon", TestRconCmd, "");
}

public Action:TestRconCmd(args) {
	new String:command[100];
	GetCmdArg(1, command, sizeof(command));

  PrintToServer("rcon command  %s", command);
	ReplyToCommand(0, "HELLOWS!!!");

	return Plugin_Handled;
}

public Action:WebClick1(client, args) {
  // client - is a server here = 0
  if (!client) {
    PrintToServer("WebClick by server");

    new String:arg1[192], String:arg2[192], String:arg3[192], String:arg4[192];
    GetCmdArg(1, arg1, sizeof(arg1));
    GetCmdArg(2, arg2, sizeof(arg2));
    GetCmdArg(3, arg3, sizeof(arg3));
    GetCmdArg(4, arg4, sizeof(arg4));

    PrintToServer("args %s %s %s %s", arg1, arg2, arg3, arg4);
  }

  return Plugin_Handled;
}

public bool:OnClientConnect(client, String:rejectmsg[], maxlen) {
	g_playerVGUI[client] = 1;

	return true;
}

public OnClientDisconnect(client) {
  g_playerVGUI[client] = 0;
}

public Action:VGUIMenu(UserMsg:msg_id, Handle:bf, const players[], playersNum, bool:reliable, bool:init) {
  new String:buffer[256];
  BfReadString(bf, buffer, sizeof(buffer));
  new client = players[0];

  // block initial motd
  if (StrEqual(buffer, "info") && IsClientConnected(client) && !IsFakeClient(client)) {
    if (g_playerVGUI[client] == 1) {
      return Plugin_Handled;
    }
  }

  return Plugin_Continue;
}

public OnClientPostAdminCheck(client) {
  DataPack pack;
  CreateDataTimer(0.1, DisplayMsg, pack);
  pack.WriteCell(client);
}

Action:DisplayMsg(Handle:timer, DataPack:pack) {
  pack.Reset();
  new client = pack.ReadCell();

  if (IsClientConnected(client) && !IsFakeClient(client)) {
    g_playerVGUI[client] = 0;
    ShowMOTDPanel(client, "123", "http://cuzus.games/temp/motd/", MOTDPANEL_TYPE_URL);
  }
}
