#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "test commands",
};

#define DMG_GENERIC									0
#define DMG_CRUSH										(1 << 0)
#define DMG_BULLET									(1 << 1)
#define DMG_SLASH										(1 << 2)
#define DMG_BURN										(1 << 3)
#define DMG_VEHICLE									(1 << 4)
#define DMG_FALL										(1 << 5)
#define DMG_BLAST										(1 << 6)
#define DMG_CLUB										(1 << 7)
#define DMG_SHOCK										(1 << 8)
#define DMG_SONIC										(1 << 9)
#define DMG_ENERGYBEAM							(1 << 10)
#define DMG_PREVENT_PHYSICS_FORCE		(1 << 11)
#define DMG_NEVERGIB								(1 << 12)
#define DMG_ALWAYSGIB								(1 << 13)
#define DMG_DROWN										(1 << 14)
#define DMG_TIMEBASED								(DMG_PARALYZE | DMG_NERVEGAS | DMG_POISON | DMG_RADIATION | DMG_DROWNRECOVER | DMG_ACID | DMG_SLOWBURN)
#define DMG_PARALYZE								(1 << 15)
#define DMG_NERVEGAS								(1 << 16)
#define DMG_POISON									(1 << 17)
#define DMG_RADIATION								(1 << 18)
#define DMG_DROWNRECOVER						(1 << 19)
#define DMG_ACID										(1 << 20)
#define DMG_SLOWBURN								(1 << 21)
#define DMG_REMOVENORAGDOLL					(1 << 22)
#define DMG_PHYSGUN									(1 << 23)
#define DMG_PLASMA									(1 << 24)
#define DMG_AIRBOAT									(1 << 25)
#define DMG_DISSOLVE								(1 << 26)
#define DMG_BLAST_SURFACE						(1 << 27)
#define DMG_DIRECT									(1 << 28)
#define DMG_BUCKSHOT								(1 << 29)

public OnPluginStart() {
  RegConsoleCmd("dmg", Test);
}

public Action:Test(client, args) {
  new String:arg1[11];
  new flag = 0;
  if (args > 0) {
    GetCmdArg(1, arg1, sizeof(arg1));
    flag = StringToInt(arg1);
  }

  SDKHooks_TakeDamage(client, client, client, 1.0,  (1 << flag), GetPlayerWeaponSlot(client, 1));

  PrintToChat(client, "Damage with flag %d", flag);
}

stock void DealDamage(int nClientVictim, int nDamage, int nClientAttacker = 0, int nDamageType = DMG_GENERIC, char[] sWeapon = "") {
  // taken from: http://forums.alliedmods.net/showthread.php?t=111684
  // thanks to the authors!
  if(    nClientVictim > 0 &&
      IsValidEdict(nClientVictim) &&
      IsClientInGame(nClientVictim) &&
      IsPlayerAlive(nClientVictim) &&
      nDamage > 0)
  {
    int EntityPointHurt = CreateEntityByName("point_hurt");
    if(EntityPointHurt != 0)
    {
        char sDamage[16];
        IntToString(nDamage, sDamage, sizeof(sDamage));

        char sDamageType[32];
        IntToString(nDamageType, sDamageType, sizeof(sDamageType));

        DispatchKeyValue(nClientVictim,            "targetname",        "war3_hurtme");
        DispatchKeyValue(EntityPointHurt,        "DamageTarget",    "war3_hurtme");
        DispatchKeyValue(EntityPointHurt,        "Damage",                sDamage);
        DispatchKeyValue(EntityPointHurt,        "DamageType",        sDamageType);
        if(!StrEqual(sWeapon, ""))
            DispatchKeyValue(EntityPointHurt,    "classname",        sWeapon);
        DispatchSpawn(EntityPointHurt);
        AcceptEntityInput(EntityPointHurt,    "Hurt",                    (nClientAttacker != 0) ? nClientAttacker : -1);
        DispatchKeyValue(EntityPointHurt,        "classname",        "point_hurt");
        DispatchKeyValue(nClientVictim,            "targetname",        "war3_donthurtme");

        RemoveEdict(EntityPointHurt);
    }
  }
}

// DealDamage(victim_index,50,attacker_index,DMG_BULLET,"weapon_ak47");

/*
new Handle:event_hurt = CreateEvent("player_hurt");
SetEventInt(event_hurt, "userid", GetClientUserId(victim));
SetEventInt(event_hurt, "attacker", GetClientUserId(client));
SetEventInt(event_hurt, "dmg_health", health+1);
SetEventString(event_hurt, "weapon", weapon);
SetEventBool(event_hurt, "headshot", headshot);
FireEvent(event_hurt);
*/
