
#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks>
#include <dhooks>

public Plugin:myinfo = {
  name = "test block weapon sound",
};

new const String:SOUNDS[][] = {
  "silent_1-second.wav",
  "2-seconds-of-silence.wav",
};

new Handle:g_hConfig = INVALID_HANDLE;
new Handle:g_hPrimaryAttack = INVALID_HANDLE;

public OnMapStart() {
  new String:buffer[PLATFORM_MAX_PATH];
  for (new i = 0, len = sizeof(SOUNDS); i < len; i++) {
    PrecacheSound(SOUNDS[i], true);
    Format(buffer, sizeof(buffer), "sound/%s", SOUNDS[i]);
    AddFileToDownloadsTable(buffer);
  }
}

public OnPluginStart() {
  // HookEvent("weapon_fire", Event_WeaponFirePre, EventHookMode_Pre); // this is before ShotgunShot
  // HookEvent("weapon_fire", Event_WeaponFirePre, EventHookMode_Post);

  // AddTempEntHook("Shotgun Shot", HookShotgunShot); // this is after weapon_fire (Pre/Post)

  LoadDHooks();

  for (new i = 1; i <= MaxClients; i++) {
		if (IsClientInGame(i)) {
			SDKHook(i, SDKHook_FireBulletsPost, OnFireBullets); // this is not working in CSS?
			// SDKHook(i, SDKHook_TraceAttackPost, OnTraceAttack);
		}
	}
}

LoadDHooks() {
  if (g_hConfig == INVALID_HANDLE) {
    new String:sPath[PLATFORM_MAX_PATH];
    BuildPath(Path_SM, sPath, sizeof(sPath), "gamedata/primaryattacktest.gamedata.txt");
    /*
    "Games"
    {
        "cstrike"
        {
            "Offsets"
            {
                "PrimaryAttack"
                {
                    "windows" "273"
                    "linux" "274"
                }
            }
        }
    }
    */
    if (FileExists(sPath))
      g_hConfig = LoadGameConfigFile("primaryattacktest.gamedata");
  }
  if (g_hConfig != INVALID_HANDLE) {
    // new iPrimary = GameConfGetOffset(g_hConfig, "PrimaryAttack");
    // PrintToServer(">>> offset %d", iPrimary);
    // g_hPrimaryAttack = DHookCreate(iPrimary, HookType_Entity, ReturnType_Int, ThisPointer_CBaseEntity, DHook_PrimaryAttack);

    g_hPrimaryAttack = DHookCreateDetour(Address_Null, CallConv_THISCALL, ReturnType_Int, ThisPointer_CBaseEntity);

    // FIXME: [SM] Exception reported: Hook not setup for a detour.
    if (!DHookSetFromConf(g_hPrimaryAttack, g_hConfig, SDKConf_Virtual, "PrimaryAttack")) {
      PrintToServer("@@@ ERROR: cannot create detour hook");
    }
    // DHookAddParam(g_hPrimaryAttack, HookParamType_Int);
    if (!DHookEnableDetour(g_hPrimaryAttack, /*post*/false, DHookDetour_PrimaryAttack)) {
      PrintToServer("@@@ ERROR: cannot enable detour hook");
    }
  } else {
    PrintToServer("@@@ ERROR: Cannot load gamedata");
  }
}

public MRESReturn:DHookDetour_PrimaryAttack(ent, Handle:hReturn, Handle:hParams) {
  if (IsValidEntity(ent)) {
		char sClass[32];
		GetEdictClassname(ent, sClass, sizeof(sClass));
		if (StrContains(sClass, "weapon_m4a1")) {
      new client = GetEntPropEnt(ent, Prop_Send, "m_hOwnerEntity");
      PrintToChat(client, "DHookDetour");
      return MRES_Supercede;
    }
  }

  return MRES_Ignored; // MRES_Override;
}

public MRESReturn:DHook_PrimaryAttack(ent, Handle:hReturn) {
	new client = GetEntPropEnt(ent, Prop_Send, "m_hOwnerEntity");
	PrintToChat(client, "DHook");

  SetEntPropFloat(ent, Prop_Send, "m_flNextPrimaryAttack", GetGameTime() + 9999.0);

	return MRES_Supercede; //MRES_Ignored;

  /*
  Use dhook to hook primaryattack at pre, run anim and sound, create entity,
  set m_flPrimaryAttack, m_flSecondaryAttack time then return MRES_SUPERCEDE
  */
}

public OnEntityCreated(entity, const String:classname[]) {
  if (StrEqual(classname, "weapon_m4a1")) {
    DHookEntity(g_hPrimaryAttack, /*post*/false, entity);
  }
}

/*
public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2]) {
    if (buttons & IN_ATTACK) {
      PrintToChat(client, "OnPlayerRunCmd");
      EmitSoundToClient(client, SOUNDS[0], _, SNDCHAN_WEAPON);
      // buttons &= ~IN_ATTACK
      // return Plugin_Changed;
    }
    return Plugin_Continue;
}
*/

public OnClientPutInServer(client) {
	SDKHook(client, SDKHook_FireBulletsPost, OnFireBullets);
	// SDKHook(client, SDKHook_TraceAttackPost, OnTraceAttack);
}

public OnFireBullets(attacker, shots, String:weaponname[]) {
  PrintToChat(attacker, "event OnFireBullets");
  EmitSoundToClient(attacker, SOUNDS[0], _, SNDCHAN_WEAPON, .soundtime = 1.0);
}

public Action:HookShotgunShot( const String:szName[], const clients[], clientCount, Float:flDelay ) {
  new client = TE_ReadNum( "m_iPlayer" ) + 1;
  PrintToChat(client, "event %s", szName);
  // FakeClientCommand(client, "stopsound"); // sv_cheats required
  EmitSoundToClient(client, SOUNDS[0], _, SNDCHAN_WEAPON); // , .soundtime = 1.0
  return Plugin_Continue;
}

public Action:Event_WeaponFirePre(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));
  PrintToChat(client, "event %s", name);
  // StopSound(client, SNDCHAN_WEAPON, soundName);
  // FadeClientVolume(int client, float percent, float outtime, float holdtime, float intime);
  EmitSoundToClient(client, SOUNDS[0], _, SNDCHAN_WEAPON); // , .soundtime = 1.0
}
