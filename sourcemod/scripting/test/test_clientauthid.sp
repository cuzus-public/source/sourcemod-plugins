#pragma semicolon 1
#include <sourcemod>

public Plugin:myinfo = {
  name = "test GetClientAuthId",
};

// GetClientAuthId может вернуть вместо SteamId строку "STEAM_ID_STOP_IGNORING_RETVALS" в случае,
// когда сеть Steam недоступна, или клиент не успел авторизоваться.

public OnClientAuthorized(client, const String:auth[]) {
  new String:ip[16];
  new String:name[MAX_NAME_LENGTH];
  new String:steamid[64], String:steamid2[64], String:steamid3[64], String:steamid4[64];
  GetClientAuthId(client, AuthId_Engine, steamid, sizeof(steamid));
  GetClientAuthId(client, AuthId_Steam2, steamid2, sizeof(steamid2));
  GetClientAuthId(client, AuthId_Steam3, steamid3, sizeof(steamid3));
	GetClientAuthId(client, AuthId_SteamID64, steamid4, sizeof(steamid4));
  GetClientName(client, name, sizeof(name));
  GetClientIP(client, ip, sizeof(ip));

  // connect 1 [ [U:1:925642711]  STEAM_0:1:462821355  [U:1:925642711]  76561198885908439  10.0.0.112  Cuzus Online ]
  // auth STEAM_0:1:462821355
  PrintToServer("connect %d [ %s  %s  %s  %s  %s  %s ]", client, steamid, steamid2, steamid3, steamid4, ip, name);
  PrintToServer("auth %s", auth);
}

public OnClientPutInServer(client) {
  new String:ip[16];
  new String:name[MAX_NAME_LENGTH];
  new String:steamid[64], String:steamid2[64], String:steamid3[64], String:steamid4[64];
  GetClientAuthId(client, AuthId_Engine, steamid, sizeof(steamid));
  GetClientAuthId(client, AuthId_Steam2, steamid2, sizeof(steamid2));
  GetClientAuthId(client, AuthId_Steam3, steamid3, sizeof(steamid3));
	GetClientAuthId(client, AuthId_SteamID64, steamid4, sizeof(steamid4));
  GetClientName(client, name, sizeof(name));
  GetClientIP(client, ip, sizeof(ip));

  // [ [U:1:925642711]  STEAM_0:1:462821355  [U:1:925642711]  76561198885908439  10.0.0.112  Cuzus Online ]
  // PrintToServer("connect %d [ %s  %s  %s  %s  %s  %s ]", client, steamid, steamid2, steamid3, steamid4, ip, name);
}

public bool OnClientConnect(client, String:rejectmsg[], maxlen) {
  new String:ip[16];
  new String:name[MAX_NAME_LENGTH];
  new String:steamid[64], String:steamid2[64], String:steamid3[64], String:steamid4[64];
  GetClientAuthId(client, AuthId_Engine, steamid, sizeof(steamid));
  GetClientAuthId(client, AuthId_Steam2, steamid2, sizeof(steamid2));
  GetClientAuthId(client, AuthId_Steam3, steamid3, sizeof(steamid3));
	GetClientAuthId(client, AuthId_SteamID64, steamid4, sizeof(steamid4));
  GetClientName(client, name, sizeof(name));
  GetClientIP(client, ip, sizeof(ip));

  // [ STEAM_ID_STOP_IGNORING_RETVALS  STEAM_ID_STOP_IGNORING_RETVALS  STEAM_ID_STOP_IGNORING_RETVALS  STEAM_ID_STOP_IGNORING_RETVALS  10.0.0.112  Cuzus Online ]
  // PrintToServer("connect %d [ %s  %s  %s  %s  %s  %s ]", client, steamid, steamid2, steamid3, steamid4, ip, name);

  return true;
}
