// Since the notify=false parameter of the SetConVar Functions doesn't work anymore
// for orangebox/L4D games, and because it's often necessary to set the NOFITY flag
// for certain cvars that need to be public, here is how to block the notifcations
// that normally automatically goes to the clients when such a ConVar gets changed
#include <sourcemod>

#define PLUGIN_VERSION "1.0"

new String:HIDDEN_CVARS[][] = {"bot_quota"};

public Plugin:myinfo = {
	name = "Hide Cvar",
	author = "berni",
	description = "",
	version = PLUGIN_VERSION,
	url = "https://forums.alliedmods.net/showthread.php?t=144616"
};

public OnPluginStart() {
  HookEvent("server_cvar", Event_ServerCvar, EventHookMode_Pre);

  // hide from server query
  for (new i = 0; i < sizeof(HIDDEN_CVARS); i++) {
    // new Handle:CVarHandle = FindConVar(HIDDEN_CVARS[i]);
    ConVar CVarHandle = FindConVar(HIDDEN_CVARS[i]);

    if (CVarHandle != null) {
      // new oldValue = CVarHandle.IntValue;
      // CVarHandle.IntValue = 0;
      new flags = GetConVarFlags(CVarHandle);
      flags &= ~FCVAR_NOTIFY; // remove flag
      flags |= FCVAR_PROTECTED; // add flag
      SetConVarFlags(CVarHandle, flags);
      // CloseHandle(CVarHandle);
      // CVarHandle.IntValue = oldValue;
    }
  }
}

public Action:Event_ServerCvar(Handle:event, const String:name[], bool:dontBroadcast) {
  return Plugin_Handled;
}
