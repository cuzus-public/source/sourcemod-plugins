#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin myinfo = {
	name = "test particles",
};

new g_index = 0;

new String:g_allParticles[][] = {
  "achieved",
  "mini_firework_flare",
  "mini_fireworks",

  "blood_advisor_pierce_spray",
  "blood_advisor_pierce_spray_b",
  "blood_advisor_pierce_spray_c",
  "blood_advisor_puncture",
  "blood_advisor_puncture_withdraw",
  "blood_advisor_shrapnel_impact",
  "blood_advisor_shrapnel_spray_1",
  "blood_advisor_shrapnel_spray_2",
  "blood_advisor_shrapnel_spurt_1",
  "blood_advisor_shrapnel_spurt_2",
  "blood_antlionguard_injured_heavy",
  "blood_antlionguard_injured_heavy_",
  "blood_antlionguard_injured_heavy_tiny",
  "blood_antlionguard_injured_light",
  "blood_antlionguard_injured_light_tiny",
  "blood_drip_synth_01",
  "blood_drip_synth_01b",
  "blood_drip_synth_01c",
  "blood_impact_antlion_01",
  "blood_impact_antlion_worker_01",
  "blood_impact_green_01",
  "blood_impact_green_01_chunk",
  "blood_impact_green_01_droplets",
  "blood_impact_green_02_chunk",
  "blood_impact_green_02_droplets",
  "blood_impact_green_02_glow",
  "blood_impact_red_01",
  "blood_impact_red_01_chunk",
  "blood_impact_red_01_droplets",
  "blood_impact_red_01_goop",
  "blood_impact_red_01_mist",
  "blood_impact_red_01_smalldroplets",
  "blood_impact_synth_01",
  "blood_impact_synth_01_arc",
  "blood_impact_synth_01_arc2",
  "blood_impact_synth_01_arc3",
  "blood_impact_synth_01_arc4",
  "blood_impact_synth_01_arc_parent",
  "blood_impact_synth_01_arc_parents",
  "blood_impact_synth_01_armor",
  "blood_impact_synth_01_droplets",
  "blood_impact_synth_01_dust",
  "blood_impact_synth_01_short",
  "blood_impact_synth_01_spurt",
  "blood_impact_yellow_01",
  "blood_impact_zombie_01",
  "blood_spurt_synth_01",
  "blood_spurt_synth_01b",
  "blood_zombie_split",
  "blood_zombie_split_spray",
  "blood_zombie_split_spray_tiny",
  "blood_zombie_split_spray_tiny2",
  "vomit_barnacle",
  "vomit_barnacle_b",

  "burning_character",
  "burning_character_b",
  "burning_character_c",
  "burning_character_d",
  "burning_character_e",

  "error",

  "bomb_explosion_huge",
  "burning_engine_01",
  "burning_engine_fire",
  "burning_gib_01",
  "burning_gib_01_drag",
  "burning_gib_01_follower1",
  "burning_gib_01_follower2",
  "burning_gib_01b",
  "burning_vehicle",
  "burning_wood_01",
  "burning_wood_01b",
  "burning_wood_01c",
  "embers_large_01",
  "embers_large_02",
  "embers_medium_01",
  "embers_medium_03",
  "embers_small_01",
  "env_embers_large",
  "env_embers_medium",
  "env_embers_medium_spread",
  "env_embers_small",
  "env_embers_small_spread",
  "env_embers_tiny",
  "env_fire_large",
  "env_fire_large_b",
  "env_fire_large_smoke",
  "env_fire_large_smoke_b",
  "env_fire_medium",
  "env_fire_medium_b",
  "env_fire_medium_smoke",
  "env_fire_medium_spread",
  "env_fire_medium_spread_b",
  "env_fire_random_puff",
  "env_fire_small",
  "env_fire_small_b",
  "env_fire_small_base",
  "env_fire_small_coverage",
  "env_fire_small_coverage_b",
  "env_fire_small_coverage_base",
  "env_fire_small_coverage_base_smoke",
  "env_fire_small_coverage_c",
  "env_fire_small_coverage_smoke",
  "env_fire_small_smoke",
  "env_fire_tiny",
  "env_fire_tiny_b",
  "env_fire_tiny_smoke",
  "explosion_huge",
  "explosion_huge_b",
  "explosion_huge_burning_chunks",
  "explosion_huge_c",
  "explosion_huge_d",
  "explosion_huge_e",
  "explosion_huge_f",
  "explosion_huge_flames",
  "explosion_huge_flames_b",
  "explosion_huge_g",
  "explosion_huge_h",
  "explosion_huge_i",
  "explosion_huge_j",
  "explosion_huge_k",
  "explosion_huge_smoking_chunks",
  "explosion_silo",
  "fire_jet_01",
  "fire_jet_01_flame",
  "fire_large_01",
  "fire_large_02",
  "fire_large_02_filler",
  "fire_large_02_fillerb",
  "fire_large_base",
  "fire_medium_01",
  "fire_medium_01_glow",
  "fire_medium_02",
  "fire_medium_02_nosmoke",
  "fire_medium_03",
  "fire_medium_03_brownsmoke",
  "fire_medium_base",
  "fire_medium_burst",
  "fire_medium_heatwave",
  "fire_small_01",
  "fire_small_02",
  "fire_small_03",
  "fire_small_base",
  "fire_small_flameouts",
  "fire_verysmall_01",
  "smoke_burning_engine_01",
  "smoke_exhaust_01",
  "smoke_exhaust_01a",
  "smoke_exhaust_01b",
  "smoke_gib_01",
  "smoke_large_01",
  "smoke_large_01b",
  "smoke_large_02",
  "smoke_large_02b",
  "smoke_medium_01",
  "smoke_medium_02",
  "smoke_medium_02 Version #2",
  "Smoke_medium_02b",
  "Smoke_medium_02b Version #2",
  "smoke_medium_02c",
  "smoke_medium_02d",
  "smoke_small_01",
  "smoke_small_01b",

  "embers_medium_01",
  "fire_medium_01",
  "smoke_oily_01",

  "muzzle_autorifles",
  "muzzle_machinegun",
  "muzzle_pistols",
  "muzzle_rifles",
  "muzzle_shotguns",
  "muzzle_smgs",

  "slime_splash_01",
  "slime_splash_01_droplets",
  "slime_splash_01_reversed",
  "slime_splash_01_surface",
  "slime_splash_02",
  "slime_splash_03",
  "water_bubble_ambient_1",
  "water_bubble_trail_1",
  "water_foam_01",
  "water_foam_01b",
  "water_foam_01c",
  "water_foam_01d",
  "water_foam_line_long",
  "water_foam_line_longb",
  "water_foam_line_longc",
  "water_foam_line_longd",
  "water_foam_line_short",
  "water_foam_line_shortb",
  "water_foam_line_shortc",
  "water_foam_line_shortd",
  "water_gunk_1",
  "water_impact_bubbles_1",
  "water_impact_bubbles_1b",
  "water_impact_bubbles_1c",
  "water_impact_bubbles_1d",
  "water_splash_01",
  "water_splash_01_droplets",
  "water_splash_01_refract",
  "water_splash_01_surface1",
  "water_splash_01_surface2",
  "water_splash_01_surface3",
  "water_splash_01_surface4",
  "water_splash_02",
  "water_splash_02_animated",
  "water_splash_02_continuous",
  "water_splash_02_droplets",
  "water_splash_02_droplets Version #2",
  "water_splash_02_froth",
  "water_splash_02_froth2",
  "water_splash_02_refract",
  "water_splash_02_surface2",
  "water_splash_02_surface4",
  "water_splash_02_vertical",
  "water_splash_03",
  "water_splash_leakypipe_silo",
  "water_splash_leakypipe_silo_froth2",
  "water_splash_leakypipe_vertical",
  "water_trail_medium",
  "water_trail_medium_b"
};

public void OnPluginStart() {
  RegConsoleCmd("pl", Test);
}

public Action:Test(client, args) {
	// if (client == 0) {
  //   PrintToServer("Command In-game Only!");
	// }
	// if (args != 1) {
	// 	ReplyToCommand(client, "[Particles] Usage:sm_sp or sm_spawnparticle <particle_name>");
	// 	return Plugin_Handled;
	// }

	new String:particle[128];
  if (args > 0) {
    GetCmdArg(1, particle, sizeof(particle));
  } else {
    Format(particle, sizeof(particle), "%s", g_allParticles[g_index]);
    g_index++;
  }

  new bool:body = false;
  if (args > 1) {
    body = true;
    PrintToChat(client, "Use body");
  }
  new bool:head = false;
  if (args > 2) {
    PrintToChat(client, "Use head");
  }

  PrintToChat(client, "%s", particle);

  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientConnected(i) && IsPlayerAlive(i)) {
      CreateParticle(i, particle, 5.0, body, head);
    }
  }

	return Plugin_Handled;
}

stock CreateParticle(client, String:effectName[], Float:lifeTime, bool:attachToBody = false, bool:attachToHead = false) {
  new particle = CreateEntityByName("info_particle_system");

  if (IsValidEdict(particle)) {
    new Float:position[3];
    new Float:angles[3];
    GetEntPropVector(client, Prop_Send, "m_vecOrigin", position);
    // position[2] += 70.0;
    TeleportEntity(particle, position, angles, NULL_VECTOR);

    new String:clientName[64];
    GetEntPropString(client, Prop_Data, "m_iName", clientName, sizeof(clientName));

    DispatchKeyValue(particle, "targetname", "cssparticle");
    DispatchKeyValue(particle, "parentname", clientName);
    DispatchKeyValue(particle, "effect_name", effectName);
    DispatchSpawn(particle);

    if (attachToHead && AttachToBody(client, particle, true) != -1) {
      // attached
    } else if (attachToBody && AttachToBody(client, particle) != -1) {
      // attached
    } else {
      // no attachment (create on position)
      SetVariantString(clientName);
      AcceptEntityInput(particle, "SetParent", particle, particle, 0);
    }

    ActivateEntity(particle);
    AcceptEntityInput(particle, "start");

    if (lifeTime > 0) {
      CreateTimer(lifeTime, DeleteParticle, particle);
    }
  }

  PrintToChat(client, "created particle %d", particle);

  return particle > 0 ? particle : -1;
}

stock AttachToBody(client, particle, bool:head = false) {
  new clientBody = GetEntPropEnt(client, Prop_Send, "m_hRagdoll");
  if (!IsValidEdict(clientBody)) { // IsValidEntity(Body)
    return -1;
  }

  new String:entName[64], String:classname[64], String:modelPath[64];
  Format(entName, sizeof(entName), "Body%d", clientBody);
  GetEdictClassname(clientBody, classname, sizeof(classname));
  GetClientModel(client, modelPath, 64);

  if (StrEqual(classname, "cs_ragdoll", false)) {
    DispatchKeyValue(clientBody, "targetname", entName);
    GetEntPropString(clientBody, Prop_Data, "m_iName", entName, sizeof(entName));
    SetVariantString(entName);
    AcceptEntityInput(particle, "SetParent", particle, particle, 0);

    new bool:usePrimary = StrContains(modelPath, "sas", false) != -1 || StrContains(modelPath, "gsg", false) != -1;
    if (head) {
      if (usePrimary) {
        SetVariantString("primary"); // back
      } else {
        SetVariantString("forward"); // head
      }
    } else {
      SetVariantString("primary"); // back
    }

    AcceptEntityInput(particle, "SetParentAttachment", particle, particle, 0);

    PrintToChat(client, "body particle %d", clientBody);

    return clientBody;
  }

  return -1;
}

public Action:DeleteParticle(Handle:timer, any:particle) {
  if (IsValidEntity(particle)) {
    new String:classN[64];
    GetEdictClassname(particle, classN, sizeof(classN));
    if (StrEqual(classN, "info_particle_system", false)) {
      RemoveEdict(particle);
    }
  }
}

ForcePrecache(String:ParticleName[]) {
	new Particle = CreateEntityByName("info_particle_system");

	if (IsValidEdict(Particle)) {
		DispatchKeyValue(Particle, "effect_name", ParticleName);
		DispatchSpawn(Particle);
		ActivateEntity(Particle);
		AcceptEntityInput(Particle, "start");

		CreateTimer(0.3, DeleteParticle, Particle, TIMER_FLAG_NO_MAPCHANGE);
	}
}

public OnMapStart() {
  for (new i = 0; i < sizeof(g_allParticles); i++) {
    ForcePrecache(g_allParticles[i]);
  }
}
