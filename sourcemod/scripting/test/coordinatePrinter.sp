#pragma semicolon 1
#define CONVAR_PREFIX "sm_coordinates"
#define DEFAULT_UPDATE_SETTING "3"
#define UPD_LIBFUNC
#include <sdktools>
#include <ddhoward_updater>
#pragma newdecls required

public Plugin myinfo = {
	name = "Coordinate Printer",
	author = "Derek D. Howard (ddhoward)",
	description = "Provides several commands to facilitate speedy retrieval of coordinates.",
	version = "18.0114.0",
	url = "https://forums.alliedmods.net/showthread.php?t=249606"
};

int g_HUDUpdateType[MAXPLAYERS+1];
int g_HUDUpdateTarget[MAXPLAYERS+1];
Handle hudtext;
ConVar hudTextSettings;

float hudXCoord = -1.0;
float hudYCoord = 0.8;
int hudRed = 0;
int hudGreen = 182;
int hudBlue = 0;
int hudAlpha = 0;

public void OnPluginStart() {
	LoadTranslations("common.phrases");

	RegAdminCmd("sm_aimcoords", Command_Coords, 0, "Returns the coordinates of where a hitscan bullet would hit were the user to fire RIGHT NOW.");
	RegAdminCmd("sm_coords", Command_Coords, 0, "Returns the user's position as told by GetClientAbsOrigin().");	
	RegAdminCmd("sm_eyecoords", Command_Coords, 0, "Returns the user's eye position as told by GetClientEyePosition().");	
	RegAdminCmd("sm_eyeangles", Command_Coords, 0, "Returns the user's eye angles as told by GetClientEyeAngles().");	
	RegAdminCmd("sm_absangles", Command_Coords, 0, "Returns the user's angles as told by GetClientAbsAngles().");
	RegAdminCmd("sm_aimcoords_hud", Command_CoordsHud, 0, "Prints to the HUD the coordinates of where a hitscan bullet would hit were the user to fire RIGHT NOW.");
	RegAdminCmd("sm_coords_hud", Command_CoordsHud, 0, "Prints to the HUD the user's position as told by GetClientAbsOrigin().");	
	RegAdminCmd("sm_eyecoords_hud", Command_CoordsHud, 0, "Prints to the HUD the user's eye position as told by GetClientEyePosition().");	
	RegAdminCmd("sm_eyeangles_hud", Command_CoordsHud, 0, "Prints to the HUD the user's eye angles as told by GetClientEyeAngles().");	
	RegAdminCmd("sm_absangles_hud", Command_CoordsHud, 0, "Prints to the HUD the user's angles as told by GetClientAbsAngles().");
	
	hudtext = CreateHudSynchronizer();
	
	hudTextSettings = CreateConVar("sm_coordinatePrinter_hudtext", "-1.0, 0.8, 0, 182, 0, 0", "Settings for the hud text.");
	hudTextSettings.AddChangeHook(hudCvarChange);
}

public Action Command_Coords(int client, int args) {
	int target = GetTarget(client);
	if (target == -1) return Plugin_Handled;

	float vector[3];
	char functionName[21];
	char commandName[13];
	GetCmdArg(0, commandName, sizeof(commandName));
	if (StrEqual(commandName, "sm_coords", false)) {
		GetClientAbsOrigin(target, vector);
		functionName = "absolute origin";
	}
	else if (StrEqual(commandName, "sm_eyecoords", false)) {
		GetClientEyePosition(target, vector);
		functionName = "eye position";
	}
	else if (StrEqual(commandName, "sm_eyeangles", false)) {
		GetClientEyeAngles(target, vector);
		functionName = "eye angles";
	}
	else if (StrEqual(commandName, "sm_absangles", false)) {
		GetClientAbsAngles(target, vector);
		functionName = "absolute angles";
	}
	else if (StrEqual(commandName, "sm_aimcoords")) {
		functionName = "aim target";
		GetAimCoords(target, vector);
	}
	ReplyToCommand(client, "%N's %s: %f, %f, %f", target, functionName, vector[0], vector[1], vector[2]);
	return Plugin_Handled;
}

public Action Command_CoordsHud(int client, int args) {
	if (client == 0) {
		ReplyToCommand(client, "You must be in-game to use this command!");
		return Plugin_Handled;
	}
	
	int target = GetTarget(client);
	if (target == -1) {
		g_HUDUpdateType[client] = 0;
		g_HUDUpdateTarget[client] = 0;
		return Plugin_Handled;
	}
	
	char commandName[17];
	GetCmdArg(0, commandName, sizeof(commandName));

	int mode;
	
	if (StrEqual(commandName, "sm_coords_hud", false)) {
		mode = 1;
	}
	else if (StrEqual(commandName, "sm_eyecoords_hud", false)) {
		mode = 2;
	}
	else if (StrEqual(commandName, "sm_eyeangles_hud", false)) {
		mode = 3;
	}
	else if (StrEqual(commandName, "sm_absangles_hud", false)) {
		mode = 4;
	}
	else if (StrEqual(commandName, "sm_aimcoords_hud", false)) {
		mode = 5;
	}
	
	if (mode == g_HUDUpdateType[client] && target == g_HUDUpdateTarget[client]) {
		g_HUDUpdateType[client] = 0;
		g_HUDUpdateTarget[client] = 0;
		return Plugin_Handled;
	}
	
	g_HUDUpdateTarget[client] = target;
	g_HUDUpdateType[client] = mode;
	return Plugin_Handled;
}

int GetTarget(int client) {
	char argstring[MAX_NAME_LENGTH];
	GetCmdArgString(argstring, sizeof(argstring));
	if (argstring[0] == '\0' || !CheckCommandAccess(client, "sm_coordinates_targetothers", ADMFLAG_CHEATS, true)) {
		if (client == 0) {
			ReplyToCommand(client, "You must be in-game to use this command!");
			return -1;
		}
		else {
			return client;
		}
	}
	else {
		return FindTarget(client, argstring, false, true);
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon) {
	if (g_HUDUpdateType[client] != 0 && g_HUDUpdateTarget[client] != 0) {
		float vector[3];
		char functionName[16];
		switch (g_HUDUpdateType[client]) {
			case 1: {
				GetClientAbsOrigin(g_HUDUpdateTarget[client], vector);
				functionName = "absolute origin";
			}
			case 2: {
				GetClientEyePosition(g_HUDUpdateTarget[client], vector);
				functionName = "eye position";
			}
			case 3: {
				GetClientEyeAngles(g_HUDUpdateTarget[client], vector);
				functionName = "eye angles";
			}
			case 4: {
				GetClientAbsAngles(g_HUDUpdateTarget[client], vector);
				functionName = "absolute angles";
			}
			case 5: {
				functionName = "aim target";
				GetAimCoords(g_HUDUpdateTarget[client], vector);
			}
		}
		SetHudTextParams(hudXCoord, hudYCoord, 0.5, hudRed, hudGreen, hudBlue, hudAlpha, 2, 0.0, 0.0, 0.0);
		ShowSyncHudText(client, hudtext, "%N's %s: %f, %f, %f", g_HUDUpdateTarget[client], functionName, vector[0], vector[1], vector[2]);
	}
}

public void GetAimCoords(int client, float vector[3]) {
	float vAngles[3];
	float vOrigin[3];
	GetClientEyePosition(client, vOrigin);
	GetClientEyeAngles(client, vAngles);

	Handle trace = TR_TraceRayFilterEx(vOrigin, vAngles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
	if (TR_DidHit(trace)) {   	 
		TR_GetEndPosition(vector, trace);
	}
	trace.Close();
}

public void OnClientDisconnect(int client) {
	g_HUDUpdateType[client] = 0;
	g_HUDUpdateTarget[client] = 0;
	for (int i = 1; i <= MaxClients; i++) {
		if (g_HUDUpdateTarget[i] == client) {
			g_HUDUpdateTarget[i] = 0;
			g_HUDUpdateType[i] = 0;
		}
	}
}

public bool TraceEntityFilterPlayer(int entity, int contentsMask) {
	return (entity > MaxClients || entity < 1);
}

public void hudCvarChange(Handle hHandle, const char[] strOldValue, const char[] strNewValue) {
	char settings[6][5];
	char convar[32];
	hudTextSettings.GetString(convar, sizeof(convar));
	ExplodeString(convar, ",", settings, 6, 5);
	hudXCoord = StringToFloat(settings[0]);
	hudYCoord = StringToFloat(settings[1]);
	hudRed = StringToInt(settings[2]);
	hudGreen = StringToInt(settings[3]);
	hudBlue = StringToInt(settings[4]);
	hudAlpha = StringToInt(settings[5]);
	
	if (hudXCoord > 1.0 || hudXCoord < 0.0) hudXCoord = -1.0;
	if (hudYCoord > 1.0 || hudYCoord < 0.0) hudYCoord = -1.0;
	if (hudRed > 255) hudRed = 255;
	else if (hudRed < 0) hudRed = 0;
	if (hudGreen > 255) hudGreen = 255;
	else if (hudGreen < 0) hudGreen = 0;
	if (hudBlue > 255) hudBlue = 255;
	else if (hudBlue < 0) hudBlue = 0;
	if (hudAlpha > 255) hudAlpha = 255;
	else if (hudAlpha < 0) hudAlpha = 0;
}

public void OnConfigsExecuted() {
	hudCvarChange(INVALID_HANDLE, "0", "0");
}