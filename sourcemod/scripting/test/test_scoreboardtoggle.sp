#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>

public Plugin:myinfo = {
  name = "Scoreboard Toggle",
};

#define IN_SCORE (1 << 16)

#define MAX_BUTTONS 25

new g_LastButtons[MAXPLAYERS+1];

public OnClientDisconnect_Post(client) {
  g_LastButtons[client] = 0;
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon) {
  static i;

  for (i = 0; i < MAX_BUTTONS; i++) {
    new button = (1 << i);

    if (buttons & button) {
      if (!(g_LastButtons[client] & button)) {
        OnButtonPress(client, button);
      }
    } else if (g_LastButtons[client] & button) {
      OnButtonRelease(client, button);
    }
  }

  g_LastButtons[client] = buttons;

  return Plugin_Continue;
}

OnButtonPress(client, button) {
  if (button == IN_SCORE) {
    PrintToChat(client, "+ press");
    ShowMOTDPanel(client, "title", "Hello world!!!11", MOTDPANEL_TYPE_TEXT);
  }
}

OnButtonRelease(client, button) {
  if (button == IN_SCORE) {
    PrintToChat(client, "- release");
    ShowVGUIPanel(client, "info", _, false);
    // ShowVGUIPanel(client, "info", INVALID_HANDLE, false);
  }
}
