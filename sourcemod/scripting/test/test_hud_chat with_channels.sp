#include <sourcemod>

public Plugin:myinfo = {
  name = "Hud Chat With Channels",
};

#define MESSAGE_DISPLAY_TIME 10
#define MAX_CHAT_MESSAGES 4
#define MAX_CHAT_TEXT_LENGTH 256

new String:g_messageText[MAX_CHAT_MESSAGES + 1][MAX_CHAT_TEXT_LENGTH];
new g_timestamp[MAX_CHAT_MESSAGES + 1];
new g_channel[MAX_CHAT_MESSAGES + 1];
new Handle:Timers[MAX_CHAT_MESSAGES + 1];

// new Handle:g_hHudSync[MAX_CHAT_MESSAGES + 1];

public void OnPluginStart() {
  // for (new iChannel = 0; iChannel < MAX_CHAT_MESSAGES; iChannel++) {
	// 	g_hHudSync[iChannel] = CreateHudSynchronizer();
	// 	if (g_hHudSync[iChannel] == INVALID_HANDLE)
	// 		SetFailState("HUD synchronisation is not supported by this mod");
	// }

  AddCommandListener(Command_Say, "say");
  AddCommandListener(Command_Say, "say_team");
}

bool:IsMessageHasText(messageIndex) {
  return strlen(g_messageText[messageIndex]) > 0;
}

bool:IsAllMessagesHasText() {
  for (new i = 0; i < MAX_CHAT_MESSAGES; i++) {
    if (!IsMessageHasText(i)) {
      return false;
    }
  }

  return true;
}

// bool:IsMessageVisible(messageIndex) {
//   return GetTime() - g_timestamp[messageIndex] < MESSAGE_DISPLAY_TIME;
// }

GetMessageChannel(messageIndex) {
  return g_channel[messageIndex];
}

GetNextMessageIndex(client) {
  for (new i = 0; i < MAX_CHAT_MESSAGES; i++) {
    if (!IsMessageHasText(i)) {
      return i;
    }
  }

  return MAX_CHAT_MESSAGES - 1;
}

float GetMessageY(messageIndex) {
  new channel = GetMessageChannel(messageIndex);
  new Float:value = 0.7;

  for (new i = 1; i < MAX_CHAT_MESSAGES + 1; i++) {
    if (channel == i) {
      return value;
    }

    value += 0.04;
  }

  return value;
}

public Action:UpdateMessageVisibility(Handle timer, DataPack pack) {
  pack.Reset();

  new messageIndex = pack.ReadCell();

  g_messageText[messageIndex] = "";
  g_timestamp[messageIndex] = 0;
  g_channel[messageIndex] = -1;

  Timers[messageIndex] = INVALID_HANDLE;
}

void AddToQueue(messageIndex, const String:message[MAX_CHAT_TEXT_LENGTH]) {
  g_messageText[messageIndex] = message;
  g_timestamp[messageIndex] = GetTime();
  g_channel[messageIndex] = messageIndex + 1;
}

// Replace last message in queue and remove first
void UpdateLastInQueue(const String:message[MAX_CHAT_TEXT_LENGTH]) {
  for (new i = 0; i < MAX_CHAT_MESSAGES; i++) {
    if (i == MAX_CHAT_MESSAGES - 1) {
      AddToQueue(i, message);
    } else {
      g_messageText[i] = g_messageText[i + 1];
      g_timestamp[i] = g_timestamp[i + 1];
      g_channel[i] = g_channel[i + 1];
    }
  }
}

void ClearHudChannelsForAll() {
  for (new p = 1; p <= MaxClients; p++) {
    if (IsClientConnected(p) && !IsFakeClient(p)) {
      ClearHudChannelsForPlayer(p);
    }
  }
}

void ClearHudChannelsForPlayer(client) {
  for (new i = 0; i < MAX_CHAT_MESSAGES; i++) {
    new channel = i + 1;
    ShowHudText(client, channel, "");

    new messageIndex = i;
    if (Timers[messageIndex] != INVALID_HANDLE) {
      KillTimer(Timers[messageIndex], false);
      Timers[messageIndex] = INVALID_HANDLE;
    }
  }
  // for (new iChannel = 0; iChannel < MAX_CHAT_MESSAGES; iChannel++)
  //   ClearSyncHud(client, g_hHudSync[iChannel]);
}

void DisplayMessage(client, messageIndex, const String:message[]) {
  new String:buf[MAX_CHAT_TEXT_LENGTH];

  if (IsMessageHasText(messageIndex)) {
    new Float:Y = GetMessageY(messageIndex);
    new channel = GetMessageChannel(messageIndex);

    // new Handle:syncHud1 = CreateHudSynchronizer();

    // SetHudTextParams(float x, float y, float holdTime, int r, int g, int b, int a, int effect, float fxTime, float fadeIn, float fadeOut)
    SetHudTextParams(0.05, Y, float(MESSAGE_DISPLAY_TIME), 255, 255, 255, 200, 0, 0.0, 0.0, 0.0);
    Format(buf, sizeof(buf), "%N: %s", client, message);

    for (new p = 1; p <= MaxClients; p++) {
      if (IsClientConnected(p) && !IsFakeClient(p)) {
        // ShowSyncHudText(p, syncHud1, buf);
        ShowHudText(p, channel, buf);
        // ShowSyncHudText(p, g_hHudSync[channel], buf);
      }
    }

    // CloseHandle(syncHud1);

    if (Timers[messageIndex] != INVALID_HANDLE) {
      KillTimer(Timers[messageIndex], false);
      Timers[messageIndex] = INVALID_HANDLE;
    }

    DataPack pack;
    Timers[messageIndex] = CreateDataTimer(float(MESSAGE_DISPLAY_TIME), UpdateMessageVisibility, pack);
    pack.WriteCell(messageIndex);
  }
}

public DisplayQueue(client) {
  for (new i = 0; i < MAX_CHAT_MESSAGES; i++) {
    DisplayMessage(client, i, g_messageText[i]);
  }
}

void SendMessage(client, const String:message[MAX_CHAT_TEXT_LENGTH]) {
  new messageIndex = GetNextMessageIndex(client);

  if (IsAllMessagesHasText()) {
    UpdateLastInQueue(message);
    // ClearHudChannelsForAll();
    DisplayQueue(client);
  } else {
    AddToQueue(messageIndex, message);
    DisplayMessage(client, messageIndex, message);
  }
}

public Action:Command_Say(client, const String:command[], argc)
{
  new String:argString[MAX_CHAT_TEXT_LENGTH];

  GetCmdArgString(argString, sizeof(argString));
  StripQuotes(argString);

  SendMessage(client, argString);

  return Plugin_Handled;
}
