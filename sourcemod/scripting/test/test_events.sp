#pragma semicolon 1
#include <sourcemod>

public OnClientAuthorized(client, const String:auth[])
{
  LogMessage("OnClientAuthorized fired for %L with SteamID %s", client, auth);
}

public Action:OnClientPreAdminCheck(client)
{
  LogMessage("OnClientPreAdminCheck fired for %L", client);
}

public OnClientPostAdminCheck(client)
{
  LogMessage("OnClientPostAdminCheck fired for %L", client);
}

public OnClientPutInServer(client)
{
  LogMessage("OnClientPutInServer fired for %L", client);
}

public OnMapEnd()
{
  LogMessage("OnMapEnd");
}

public OnMapStart()
{
  LogMessage("OnMapStart");
}

public OnPluginStart() {
  LogMessage("OnPluginStart");
}
