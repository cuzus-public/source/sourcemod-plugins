#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>

public Plugin:myinfo = {
  name = "Test PlayerRunCmd",
};

new g_buttons[MAXPLAYERS+1];
new g_impulse[MAXPLAYERS+1];

public OnPluginStart() {
  RegConsoleCmd("testjump", PlusJump);
  RegConsoleCmd("testflash", PlusFlash);
}

public OnClientPutInServer(client) {
	g_buttons[client] = 0;
  g_impulse[client] = 0;
}

public Action:PlusFlash(client, args) {
  for (new i = 1; i <= MaxClients; i++) {
    g_impulse[i] = 100;
    CreateTimer(3.0, ClearFlags, i);
  }
	return Plugin_Handled;
}

public Action:PlusJump(client, args) {
  for (new i = 1; i <= MaxClients; i++) {
    g_buttons[i] |= IN_JUMP;
    CreateTimer(0.1, ClearFlags, i);
  }
	return Plugin_Handled;
}

public Action:ClearFlags(Handle:timer, any:client) {
	if (IsClientInGame(client)) {
		// g_buttons[client] &= ~IN_JUMP;
    // g_buttons[client] &= ~IN_DUCK;
    g_buttons[client] = 0;
    g_impulse[client] = 0;
	}
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon) {
  if (IsFakeClient(client)) {
    new bool:hasButtons = g_buttons[client] != 0;
    new bool:hasImpulse = g_impulse[client] != 0;

    if (hasButtons) {
      buttons = g_buttons[client];
    }
    if (hasImpulse) {
      impulse = g_impulse[client];
    }

    if (hasButtons || hasImpulse) {
      return Plugin_Changed;
    }
  }

  return Plugin_Continue;
}
