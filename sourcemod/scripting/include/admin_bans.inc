#if defined _admin_bans_included
  #endinput
#endif
#define _admin_bans_included

native AddClientBan(adminClient, client, const String:banReason[], banDuration = 0);
