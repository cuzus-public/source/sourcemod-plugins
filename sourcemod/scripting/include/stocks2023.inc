// TODO: collecting all useful stocks here
// TODO: get more from SMLib

// sourcemod\scripting\.trash\_include\msharedutil\ents.inc
// sourcemod\scripting\.trash\_include\smlib\*

stock getHourDifference(hour, otherHour) {
  new timeDiff = (24 - hour) + otherHour;
  if(timeDiff < 6) {
    timeDiff += 24;
  }
  return timeDiff;
}

stock SafeCloseHandle(&Handle:rHandle) {
	if (rHandle != INVALID_HANDLE) {
		CloseHandle(rHandle);
		rHandle = INVALID_HANDLE;
	}
}

stock bool:IsNumeric(String:sString[]) {
	for (new i = 0; i < strlen(sString); i++)
		if (!IsCharNumeric(sString[i]) && sString[i])
			return false;
	return true;
}

stock StringToLower(String:szStr[]) {
	for (new i = 0; szStr[i] != 0; i++) {
		szStr[i] = CharToLower(szStr[i]);
	}
}

// How high off the ground is the player?
stock Float:DistanceAboveGround(victim) {
	new Float:vStart[3];
	new Float:vEnd[3];
	new Float:vAngles[3] = {90.0,0.0,0.0};
	GetClientAbsOrigin(victim,vStart);
	new Handle:trace = TR_TraceRayFilterEx(vStart, vAngles, MASK_PLAYERSOLID, RayType_Infinite,TraceEntityFilterPlayer);

	new Float:distance = -1.0;
	if(TR_DidHit(trace)) {
		TR_GetEndPosition(vEnd, trace);
		distance = GetVectorDistance(vStart, vEnd, false);
	}

	CloseHandle(trace);
	return distance;
}

// Finds entites, and won't error out when searching invalid entities
stock FindEntityByClassname2(startEnt, const String:classname[]) {
	// If startEnt isn't valid shifting it back to the nearest valid one
	while (startEnt > -1 && !IsValidEntity(startEnt)) startEnt--;
	return FindEntityByClassname(startEnt, classname);
}


stock Debug(String:format[], any:...) {
  new String:pluginName[PLATFORM_MAX_PATH];
  GetPluginFilename(INVALID_HANDLE, pluginName, sizeof(pluginName));
  new String:buffer[256];
  VFormat(buffer, sizeof(buffer), format, 2);
  PrintToServer("<%s> %s", pluginName, buffer);
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}

stock bool:IsInTeam(client) {
  if (!IsValidClient(client)) {
    return false;
  }
  new team = GetClientTeam(client);
  return team == CS_TEAM_T || team == CS_TEAM_CT;
}

stock SetClientFrags(client, value) {
  SetEntProp(client, Prop_Data, "m_iFrags", value);
}

stock GetClientMoney(client) {
  return GetEntProp(client, Prop_Send, "m_iAccount");
}

stock SetClientMoney(client, value) {
  SetEntProp(client, Prop_Send, "m_iAccount", value);
}

stock bool:IsPlayerCrouched(player) {
	return GetEntProp(player, Prop_Send, "m_bDucked", 1) > 0;
}

stock SetGod(client, bool:isGod) {
  SetEntProp(client, Prop_Data, "m_takedamage", isGod ? 0 : 2, 1);
}

stock bool:IsNade(entity) {
	if (!IsValidEntity(entity))
		return false;

	new String:classname[32];
	GetEntityClassname(entity, classname, 32);

	return (StrEqual(classname, "smokegrenade_projectile") || StrEqual(classname, "flashbang_projectile") || StrEqual(classname, "hegrenade_projectile"));
}

stock GetThrowedNadeOwner(entity) {
	if (!IsValidEntity(entity))
		return 0;
	return GetEntPropEnt(entity, Prop_Send, "m_hThrower");
}

stock bool:IsHostage(entity) {
	if (!IsValidEntity(entity))
		return false;

	new String:classname[32];
	GetEntityClassname(entity, classname, 32);

	return (StrEqual(classname, "hostage_entity"));
}

stock bool:IsPlayersInSamePlace(int player, int other) {
	float player_pos[3];
	GetClientAbsOrigin(player, player_pos);

	float other_pos[3];
	GetClientAbsOrigin(other, other_pos);

	player_pos[2] = 0.0;
	other_pos[2] = 0.0;

	float distance = GetVectorDistance(player_pos, other_pos, false);

	if(sm_blockcontrol_debug.BoolValue)
		PrintToServer("Distance between %N and %N is: %f", player, other, distance);

	return distance < 33.0;
}

stock bool:IsPlayerAboveAnotherOne(player, other) {
	float player_pos[3];
	GetClientAbsOrigin(player, player_pos);

	float other_pos[3];
	GetClientAbsOrigin(other, other_pos);

	float y_diff = FloatAbs(player_pos[2] - other_pos[2]);

	if(sm_blockcontrol_debug.BoolValue)
		PrintToServer("Player %N is on player %N with diff: [%f, %f]", player, other, player_pos[2], other_pos[2]);

	if(IsPlayerCrouched(player) || IsPlayerCrouched(other))
		return (y_diff > 45.031250);
	else
		return (y_diff >= 62.086700);
}

stock bool IsEntNearWall(int ent) {
  float vOrigin[3], vec[3], vAngles[3];
  Handle trace;
  GetEntPropVector(ent, Prop_Data, "m_vecAbsOrigin", vOrigin);
  GetEntPropVector(ent, Prop_Data, "m_angAbsRotation", vAngles);  // <-- This dont works, because on SetAttachment this get currupt
  // PrintToChatAll("%f %f %f |  %f %f %f", vOrigin[0], vOrigin[1], vOrigin[2], vAngles[0], vAngles[1], vAngles[2] );
  trace = TR_TraceRayFilterEx(vOrigin, vAngles, MASK_PLAYERSOLID, RayType_Infinite, TraceRayDontHitSelf, ent);
  if (TR_DidHit(trace)) {
    TR_GetEndPosition(vec, trace);
    if (GetVectorDistance(vec, vOrigin) < 40) {
      CloseHandle(trace);
      return true;
    }
  }
  CloseHandle(trace);
  return false;
}

// Trace Position From Eyes
stock bool:TraceEye(int client, float pos[3]) {
  float vAngles[3];
  float vOrigin[3];
  GetClientEyePosition(client, vOrigin);
  GetClientEyeAngles(client, vAngles);
  Handle traceRay = TR_TraceRayFilterEx(vOrigin, vAngles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
  if (TR_DidHit(traceRay)) {
    TR_GetEndPosition(pos, traceRay);
    delete traceRay;
    return true;
  }
  delete traceRay;
  return false;
}

public bool:TraceEntityFilterPlayer(entity, contentsMask) {
  return (entity > MaxClients || !entity);
}

stock GetClientLanguageCode(client, String:outBuffer[], bufferLen) {
  new String:code[3];
  new langId = 0;
  if (IsClientConnected(client) && IsClientInGame(client) && !IsFakeClient(client)) {
    langId = GetClientLanguage(client);
  }
  GetLanguageInfo(langId, outBuffer, bufferLen, code, sizeof(code));
}

stock void GetServerIP(char[] buffer, int len) {
	int ips[4];
	int ip = GetConVarInt(FindConVar("hostip"));
	int port = GetConVarInt(FindConVar("hostport"));
	ips[0] = (ip >> 24) & 0x000000FF;
	ips[1] = (ip >> 16) & 0x000000FF;
	ips[2] = (ip >> 8) & 0x000000FF;
	ips[3] = ip & 0x000000FF;
	FormatEx(buffer, len, "%d.%d.%d.%d:%d", ips[0], ips[1], ips[2], ips[3], port);
}

// Convert String color to Int array
stock ParseColor(const char[] sColor, int aColor[4]) {
  int iColor = StringToInt(sColor, 16);
  aColor[0]  = iColor >> 16;
  aColor[1]  = iColor >> 8 & 255;
  aColor[2]  = iColor & 255;
  aColor[3]  = 255;
}

//
// Entity stocks
//
stock KillEntityIn(iEnt, Float:flSeconds) {
  decl String:szAddOutput[32];
  Format(szAddOutput, sizeof(szAddOutput), "OnUser1 !self,Kill,,%0.2f,1", flSeconds);
  SetVariantString(szAddOutput);
  AcceptEntityInput(iEnt, "AddOutput");
  AcceptEntityInput(iEnt, "FireUser1");
}

/*
  Fire entity input on an entity after a certain delay
*/
stock DelayEntityInput(iEnt, const String:szInput[], const Float:flSeconds, const String:szVariant[] = "") {
  decl String:szAddOutput[128];
  Format(szAddOutput, sizeof(szAddOutput), "OnUser1 !self,%s,%s,%0.2f,1", szInput, szVariant, flSeconds);
  SetVariantString(szAddOutput);
  AcceptEntityInput(iEnt, "AddOutput");
  AcceptEntityInput(iEnt, "FireUser1");
}

stock FireEntityInput(const String:szTargetname[], const String:szInput[], const String:szParam[]="", const Float:flDelay=0.0) {
  decl String:szBuffer[255];
  Format(szBuffer, sizeof(szBuffer), "OnUser1 %s:%s:%s:%0.2f:1", szTargetname, szInput, szParam, flDelay);

  static iSaveEnt = EntIndexToEntRef(SpawnEntityByName("info_target"));

  new iEnt = EntRefToEntIndex(iSaveEnt); // Dummy entity. (Pretty sure every Source game has this.)
  if (iEnt == -1 || !IsValidEntity(iEnt)) {
    iSaveEnt = EntIndexToEntRef(SpawnEntityByName("info_target"));

    iEnt = EntRefToEntIndex(iSaveEnt);
    if (iEnt == -1 || !IsValidEntity(iEnt)) {
      return false; // Failed to recreate dummy entity
    }
  }

  SetVariantString(szBuffer);
  AcceptEntityInput(iEnt, "AddOutput");
  AcceptEntityInput(iEnt, "FireUser1");

  //AcceptEntityInput(iEnt, "Kill");

  return true;
}

stock SpawnEntityByName(const String:szClassname[], iForceEdictIndex = -1) {
  new iEnt = CreateEntityByName(szClassname, iForceEdictIndex);
  if (iEnt != -1) {
    DispatchSpawn(iEnt);
    // ActivateEntity(iEnt);
  }
  return iEnt;
}

// SMLib
stock Entity_SetParent(entity, parent) {
	SetVariantString("!activator");
	AcceptEntityInput(entity, "SetParent", parent);
}
