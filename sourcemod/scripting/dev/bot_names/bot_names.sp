#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
  name = "Bot Names",
  description = "Based on botnames plugin by Aaron Griffith",
  author = "Geekrainian @ cuzus.games",
  version = "2023.01.20",
  url = "https://geekrainian.com",
};

#define IsBot(%1) (IsFakeClient(%1) && GetEntProp(%1, Prop_Data, "m_iFrags") != -999)

new Handle:g_BotNamesArr = INVALID_HANDLE;

#define BOT_NAMES_FILE "configs/bot_names.cfg"
#define MIN_NAME_LENGTH 3

public OnPluginStart() {
  g_BotNamesArr = CreateArray(MAX_NAME_LENGTH + 1);

  LoadNamesFromFile();
}

public OnClientPostAdminCheck(client) {
  if (IsBot(client)) {
    SetRandomName(client);
  }
}

public OnClientDisconnect(client) {
  if (IsBot(client)) {
    // move name to available names array
    new String:exName[MAX_NAME_LENGTH + 1];
    GetClientName(client, exName, sizeof(exName));
    AddValueToList(exName);
  }
}

LoadNamesFromFile() {
  if (g_BotNamesArr != INVALID_HANDLE) {
    ClearArray(g_BotNamesArr);
  }

  new String:path[PLATFORM_MAX_PATH];
  BuildPath(Path_SM, path, sizeof(path), BOT_NAMES_FILE);

  new Handle:file = OpenFile(path, "r");
  if (file == INVALID_HANDLE) {
    Debug("Could not open file: %s", path);
    return;
  }

  new String:line[MAX_NAME_LENGTH * 4]; // use larger length to support comments

  while (!IsEndOfFile(file) && ReadFileLine(file, line, sizeof(line))) {
    if (String_StartsWith(line, "#") || String_StartsWith(line, "//") || String_StartsWith(line, " ")) {
      continue;
    }
    AddValueToList(line);
  }

  Debug("Loaded %d bot names from file", GetArraySize(g_BotNamesArr));

  CloseHandle(file);
}

AddValueToList(String:value[]) {
  if (strlen(value) < MIN_NAME_LENGTH || strlen(value) > MAX_NAME_LENGTH) {
    return;
  }

  TrimString(value);

  // check for duplicate
  if (FindStringInArray(g_BotNamesArr, value) == -1) {
    PushArrayString(g_BotNamesArr, value);
  }
}

bool:IsNameTaken(const String:playerName[MAX_NAME_LENGTH + 1]) {
  new String:exName[MAX_NAME_LENGTH + 1];

  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientConnected(i)) {
      GetClientName(i, exName, sizeof(exName));

      if (StrEqual(exName, playerName) || StrContains(exName, playerName, false) != -1) {
        return true;
      }
    }
  }

  return false;
}

Out_PickName(String:outName[MAX_NAME_LENGTH + 1]) {
  Format(outName, sizeof(outName), "");

  new arraySize = GetArraySize(g_BotNamesArr);
  if (arraySize == 0) {
    return;
  }

  new maxAttempts = 50;

  new String:playerName[MAX_NAME_LENGTH + 1];

  for (new i = 0; i < maxAttempts; i++) {
    new index = GetRandomInt(0, arraySize - 1);
    GetArrayString(g_BotNamesArr, index, playerName, sizeof(playerName));
    RemoveFromArray(g_BotNamesArr, index);

    if (!IsNameTaken(playerName)) {
      Format(outName, sizeof(outName), playerName);
      break;
    }
  }
}

SetRandomName(client) {
  if (!IsClientConnected(client)) {
    return;
  }

  new String:pickedName[MAX_NAME_LENGTH + 1];
  Out_PickName(pickedName);

  if (strlen(pickedName) == 0) {
    Debug("Warning: cannot pick name for bot");
    return;
  }

  SetClientName(client, pickedName);
}

stock Debug(String:format[], any:...) {
  new String:pluginName[PLATFORM_MAX_PATH];
  GetPluginFilename(INVALID_HANDLE, pluginName, sizeof(pluginName));
  new String:buffer[256];
  VFormat(buffer, sizeof(buffer), format, 2);
  PrintToServer("<%s> %s", pluginName, buffer);
}

// SMLib
stock bool:String_StartsWith(const String:str[], const String:subString[]) {
	new n = 0;
	while (subString[n] != '\0') {
		if (str[n] == '\0' || str[n] != subString[n]) {
			return false;
		}
		n++;
	}
	return true;
}
