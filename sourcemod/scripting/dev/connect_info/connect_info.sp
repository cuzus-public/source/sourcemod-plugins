#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

#include <hud_message_queue>
#define MESSAGE_PLUGIN_LIBRARY "hud_message_queue"

public Plugin:myinfo = {
  name = "Connect Info",
  description = "Custom connect-disconnect message and sound",
  author = "Geekrainian @ cuzus.games",
  version = "2023.01.10",
  url = "https://geekrainian.com",
};

#define TRANSLATIONS_FILE "connect_info.phrases"

#define MAX_CHAT_TEXT_LENGTH 192

new const String:S_SOUND[][] = {
	"sys_player_ingame.wav", // skillsound/soil_shot.wav
  "sys_player_join.wav", // itemsound/sys_party_invite.wav
  "sys_player_leave.wav", // itemsound/sys_party_leave.wav
};

enum {
  Type_Connect,
  Type_PutInServer,
  Type_Disconnected,
};

new bool:g_isNativesLoaded = false;

new String:g_playerName[MAXPLAYERS][MAX_NAME_LENGTH + 1];

new Float:g_lastSoundTime;
new Float:g_lastMessageTime;

new Handle:cvar_soundDelay, Handle:cvar_messageDelay, Handle:cvar_connectColor, Handle:cvar_joinColor, Handle:cvar_leaveColor;
new Handle:cvar_hudChannel, Handle:cvar_hudPositionX, Handle:cvar_hudPositionY, Handle:cvar_messageTime;

public OnAllPluginsLoaded() {
	if (LibraryExists(MESSAGE_PLUGIN_LIBRARY)) {
		g_isNativesLoaded = true;
	}
}

public OnLibraryAdded(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = true;
  }
}

public OnLibraryRemoved(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = false;
  }
}

public OnPluginStart() {
  LoadTranslations(TRANSLATIONS_FILE);

  cvar_hudChannel = CreateConVar("sm_connectinfo_hud_channel", "0", "", FCVAR_PROTECTED);
  cvar_hudPositionX = CreateConVar("sm_connectinfo_position_x", "0.0235", "", FCVAR_PROTECTED);
  cvar_hudPositionY = CreateConVar("sm_connectinfo_position_y", "0.6024", "", FCVAR_PROTECTED);
  cvar_messageTime = CreateConVar("sm_connectinfo_message_time", "10", "", FCVAR_PROTECTED, true, 1.0);
  cvar_soundDelay = CreateConVar("sm_connectinfo_sound_delay", "1.5", "", FCVAR_PROTECTED);
  cvar_messageDelay = CreateConVar("sm_connectinfo_message_delay", "1.5", "", FCVAR_PROTECTED);
  cvar_connectColor = CreateConVar("sm_connectinfo_connect_color", "102 204 255", "", FCVAR_PROTECTED);
  cvar_joinColor = CreateConVar("sm_connectinfo_join_color", "77 255 77", "", FCVAR_PROTECTED);
  cvar_leaveColor = CreateConVar("sm_connectinfo_leave_color", "204 51 0", "", FCVAR_PROTECTED);
}

public OnMapStart() {
  new String:buf[PLATFORM_MAX_PATH];

  for (new i = 0, len = sizeof(S_SOUND); i < len; i++) {
    PrecacheSound(S_SOUND[i], true);
    Format(buf, sizeof(buf), "sound/%s", S_SOUND[i]);
    AddFileToDownloadsTable(buf);
  }
}

public bool:OnClientConnect(client) {
  PackAndSend(client, Type_Connect, 1.0);
  // Return true otherwise player will be kicked
  return true;
}

public OnClientPostAdminCheck(client) {
  PackAndSend(client, Type_PutInServer, 2.0);
}

public OnClientDisconnect(client) {
  if (IsClientConnected(client)) {
    GetClientName(client, g_playerName[client], MAX_NAME_LENGTH + 1);
  }
  PackAndSend(client, Type_Disconnected, 1.0);
}

PackAndSend(client, type, Float:delay) {
  new DataPack:pack;
  CreateDataTimer(delay, DisplayMsg, pack);
  pack.WriteCell(client);
  pack.WriteCell(type);
}

Action:DisplayMsg(Handle:timer, DataPack:pack) {
  new Float:delay = GetConVarFloat(cvar_messageDelay);
  if (delay > 0 && g_lastMessageTime > 0 && (GetGameTime() - g_lastMessageTime < delay)) {
    return Plugin_Stop;
  }

  pack.Reset();
  new client = pack.ReadCell();
  new type = pack.ReadCell();

  new String:playerName[MAX_NAME_LENGTH + 1];

  if (IsClientConnected(client)) {
    GetClientName(client, playerName, sizeof(playerName));
  } else {
    Format(playerName, sizeof(playerName), g_playerName[client]);
  }

  if (strlen(playerName) == 0) {
    return Plugin_Stop;
  }

  new String:hudColor[32];
  new String:buf[MAX_CHAT_TEXT_LENGTH];

  if (type == Type_Connect) {
    GetConVarString(cvar_connectColor, hudColor, sizeof(hudColor));
    Format(buf, sizeof(buf), "%T", "connect", client, playerName);
    SendToAll(buf, S_SOUND[0], hudColor);
  } else if (type == Type_PutInServer) {
    GetConVarString(cvar_joinColor, hudColor, sizeof(hudColor));
    Format(buf, sizeof(buf), "%T", "putinserver", client, playerName);
    SendToAll(buf, S_SOUND[1], hudColor);
  } else if (type == Type_Disconnected) {
    GetConVarString(cvar_leaveColor, hudColor, sizeof(hudColor));
    Format(buf, sizeof(buf), "%T", "leave", client, playerName);
    SendToAll(buf, S_SOUND[2], hudColor);
    g_playerName[client] = "";
  }

  g_lastMessageTime = GetGameTime();

  return Plugin_Stop;
}

SendToAll(const String:message[], const String:sound[], const String:hudColor[]) {
  if (g_isNativesLoaded) {
    new String:colors[4][4];
    ExplodeString(hudColor, " ", colors, 4, 4);

    for (new p = 1; p <= MaxClients; p++) {
      if (IsValidClient(p) && !IsFakeClient(p)) {
        ShowQueuedHudText(
          p,
          GetConVarInt(cvar_hudChannel),
          message,
          StringToInt(colors[0]),
          StringToInt(colors[1]),
          StringToInt(colors[2]),
          GetConVarFloat(cvar_hudPositionX),
          GetConVarFloat(cvar_hudPositionY),
          GetConVarFloat(cvar_messageTime)
        );
      }
    }
  } else {
    PrintToChatAll(message);
  }

  if (strlen(sound) > 0) {
    if (GetGameTime() - g_lastSoundTime > GetConVarFloat(cvar_soundDelay)) {
      EmitSoundToAll(sound);
      g_lastSoundTime = GetGameTime();
    }
  }
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
