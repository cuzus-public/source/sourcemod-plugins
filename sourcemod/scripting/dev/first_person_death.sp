#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
  name = "First Person Death",
  version = "2022.11.05",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define IsBot(%1) (IsFakeClient(%1) && GetEntProp(%1, Prop_Data, "m_iFrags") != -999)

new g_clientCamera[MAXPLAYERS + 1];

new Handle:cvar_camResetTime;

public OnPluginStart() {
  cvar_camResetTime = CreateConVar("sm_fpd_reset_time", "3", "Reset first person camera after timeout, put 0 to keep camera on the dead player forever", FCVAR_PROTECTED);

  HookEvent("player_death", Event_PlayerDeath, EventHookMode_Pre);
  HookEvent("player_spawn", Event_PlayerSpawn);
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));

  if (!IsValidClient(client)) {
    return Plugin_Continue;
  }

  new clientBody = GetEntPropEnt(client, Prop_Send, "m_hRagdoll");
  if (!IsValidEdict(clientBody)) {
    return Plugin_Continue;
  }

  new ent = CreateCameraEnt(client, clientBody, "forward");
  if (ent != -1) {
    SetClientViewEntity(client, ent);
    g_clientCamera[client] = ent;

    if (GetConVarFloat(cvar_camResetTime) > 0.0) {
      CreateTimer(GetConVarFloat(cvar_camResetTime), ClearCamTimer, client);
    }
  }

  return Plugin_Continue;
}

public Action:Event_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));
  if (!IsValidClient(client)) {
    return Plugin_Continue;
  }

  new String:modelName[64];
  GetEntPropString(client, Prop_Data, "m_ModelName", modelName, sizeof(modelName));

  // FIXME: this replaces CT model on spawn, maybe can create dummy/invisible entity on player death and attach camera on it
  // GSG and SAS got not the attachment forward, only two of the CTs models have the attachment necessary for the view to work properly
  if (StrContains(modelName, "ct_gsg9.mdl", false) > -1 || StrContains(modelName, "ct_sas.mdl", false) > -1) {
    SetEntityModel(client, "models/player/ct_urban.mdl");
  }

  ClearCam(client);

  return Plugin_Continue;
}

CreateCameraEnt(client, ragdollEnt, const String:attachmentName[]) {
  // Precache dummy model
  new String:model[64];
  Format(model, sizeof(model), "models/blackout.mdl");
  PrecacheModel(model, true);

  // Generate unique id for the client so we can set the parenting through parent name
  new String:entityName[64];
  Format(entityName, sizeof(entityName), "fpd_Ragdoll%d", client);
  DispatchKeyValue(ragdollEnt, "targetname", entityName);

  new ent = CreateEntityByName("prop_dynamic");
  if (!IsValidEdict(ent)) {
    return -1;
  }

  // Generate unique id for the entity
  new String:entityCamName[64];
  Format(entityCamName, sizeof(entityCamName), "fpd_RagdollCam%d", ent);

  // Setup entity
  DispatchKeyValue(ent, "targetname", entityCamName);
  DispatchKeyValue(ent, "parentname", entityName);
  DispatchKeyValue(ent, "model", model);
  DispatchKeyValue(ent, "solid", "0");
  DispatchKeyValue(ent, "rendermode", "10"); // dont render
  // SetEntityRenderMode(ent, RENDER_NONE);
  DispatchKeyValue(ent, "disableshadows", "1"); // no shadows

  new Float:angles[3];
  GetClientEyeAngles(client, angles);
  new String:camTargetAngles[64];
  Format(camTargetAngles, sizeof(camTargetAngles), "%f %f %f", angles[0], angles[1], angles[2]);
  DispatchKeyValue(ent, "angles", camTargetAngles);

  SetEntityModel(ent, model);
  DispatchSpawn(ent);

  SetVariantString(entityName);
  AcceptEntityInput(ent, "SetParent", ent, ent, 0);

  SetVariantString(attachmentName); // attachment of this name should be present in the model, otherwise server can crash with an error
  AcceptEntityInput(ent, "SetParentAttachment", ent, ent, 0);

  // Activate
  // AcceptEntityInput(ent, "TurnOn");

  return IsValidEntity(ent) ? ent : -1;
}

public Action:ClearCamTimer(Handle:timer, client) {
  ClearCam(client);
}

ClearCam(client) {
  if (g_clientCamera[client]) {
    g_clientCamera[client] = false;
  }
  if (IsValidClient(client)) {
    SetClientViewEntity(client, client);
  }
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
