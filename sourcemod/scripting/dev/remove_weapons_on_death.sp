#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>

public Plugin:myinfo = {
  name = "Remove Weapons On Death",
  version = "2023.01.18",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

public OnPluginStart() {
  // late load support
  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientInGame(i)) {
      OnClientPutInServer(i);
    }
  }
}

public OnClientPutInServer(client) {
  SDKHook(client, SDKHook_OnTakeDamageAlive, OnTakeDamageAlive);
}

public Action:OnTakeDamageAlive(victim, &attacker, &inflictor, &Float:damage, &damagetype) {
  if (!IsValidClient(victim)) {
    return Plugin_Continue;
  }

  if (damage >= GetClientHealth(victim)) {
    new weaponIndex;

    for (new i = 0; i <= CS_SLOT_C4; i++) {
      if (i == CS_SLOT_KNIFE) {
        continue;
      }
      while ((weaponIndex = GetPlayerWeaponSlot(victim, i)) != -1) {
        RemovePlayerItem(victim, weaponIndex);
        AcceptEntityInput(weaponIndex, "kill");
      }
    }
  }

  return Plugin_Continue;
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
