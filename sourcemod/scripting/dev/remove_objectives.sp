#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "Remove Objectives",
  version = "2022.12.27",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

new g_weaponParent;
new Handle:cvar_disableWeapons, Handle:cvar_disableItems, Handle:cvar_disableBuyZones, Handle:cvar_disableC4, Handle:cvar_disableHostages;

public OnPluginStart() {
  cvar_disableWeapons = CreateConVar("sm_remove_objectives_no_weapons", "1", "Remove weapons", FCVAR_PROTECTED);
  cvar_disableItems = CreateConVar("sm_remove_objectives_no_items", "1", "Remove items", FCVAR_PROTECTED);
  cvar_disableBuyZones = CreateConVar("sm_remove_objectives_no_buyzones", "1", "Remove buy zones", FCVAR_PROTECTED);
  cvar_disableC4 = CreateConVar("sm_remove_objectives_no_c4", "1", "Remove C4 and bomb sites", FCVAR_PROTECTED);
  cvar_disableHostages = CreateConVar("sm_remove_objectives_no_hostages", "1", "Remove hostages and rescue zones", FCVAR_PROTECTED);

  g_weaponParent = FindSendPropInfo("CBaseCombatWeapon", "m_hOwnerEntity");

  HookEvent("item_pickup", Event_ItemPickup);
  HookEvent("round_start", Event_RoundStart);
}

public OnMapStart() {
  if (GetConVarBool(cvar_disableC4)) {
    KillEntityByName("func_bomb_target");
  }

  if (GetConVarBool(cvar_disableBuyZones)) {
    KillEntityByName("func_buyzone");
  }

  if (GetConVarBool(cvar_disableHostages)) {
    KillEntityByName("func_hostage_rescue");
  }
}

public Action:Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast) {
  new count = 0;

  if (GetConVarBool(cvar_disableWeapons)) {
    count = KillEntityByName("weapon_*", true);
    Debug("Removed %d weapon entities", count);
  }

  if (GetConVarBool(cvar_disableItems)) {
    count = KillEntityByName("item_*", true);
    Debug("Removed %d item entities", count);
  }

  return Plugin_Continue;
}

public Action:Event_ItemPickup(Handle:event, const String:name[], bool:dontBroadcast) {
  if (GetConVarBool(cvar_disableC4)) {
    new client = GetClientOfUserId(GetEventInt(event, "userid"));

    new String:temp[32];
    GetEventString(event, "item", temp, sizeof(temp));

    // find the bomb carrier
    if (StrEqual(temp, "weapon_c4", false)) {
      new iWeaponIndex = GetPlayerWeaponSlot(client, 4);
      RemovePlayerItem(client, iWeaponIndex); // remove the bomb
    }
  }

	return Plugin_Continue;
}

stock KillEntityByName(const String:pattern[], bool:checkParent = false) {
  new ent = -1;
  new countKilled = 0;

  if (strlen(pattern) == 0) {
    return countKilled;
  }

  while ((ent = FindEntityByClassname(ent, pattern)) != -1) {
    if (checkParent && GetEntDataEnt2(ent, g_weaponParent) != -1) {
      continue;
    }

    AcceptEntityInput(ent, "Kill");
    countKilled++;
  }

  return countKilled;
}

stock Debug(String:format[], any:...) {
  new String:pluginName[PLATFORM_MAX_PATH];
  GetPluginFilename(INVALID_HANDLE, pluginName, sizeof(pluginName));
  new String:buffer[256];
  VFormat(buffer, sizeof(buffer), format, 2);
  PrintToServer("<%s> %s", pluginName, buffer);
}
