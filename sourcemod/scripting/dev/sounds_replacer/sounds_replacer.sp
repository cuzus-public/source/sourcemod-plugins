#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

/*
  TODO: try to replace footsteps
  https://forums.alliedmods.net/showpost.php?p=2046613&postcount=14
  https://forums.alliedmods.net/showpost.php?p=2346529&postcount=17
*/

public Plugin:myinfo = {
  name = "Sounds Replacer",
  version = "2022.12.26",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

new const String:HEADSHOT_SOUNDS[][] = {
  "kf_enemyglobalsndtwo/Impact_Skull01.wav",
  "kf_enemyglobalsndtwo/Impact_Skull02.wav",
  "kf_enemyglobalsndtwo/Impact_Skull03.wav",
  "kf_enemyglobalsndtwo/Impact_Skull04.wav",
  // "kf_enemyglobalsndtwo/Impact_Skull05.wav",
};

new const String:HIT_SOUNDS[][] = {
  "KF_BImpactSnd/Impact_Meat01.wav",
  "KF_BImpactSnd/Impact_Meat02.wav",
  "KF_BImpactSnd/Impact_Meat03.wav",
  "KF_BImpactSnd/Impact_Meat04.wav",
  "KF_BImpactSnd/Impact_Meat05.wav",
};

new const String:GLASS_BREAK_SOUNDS[][] = {
  "KF_BImpactSnd/Impact_Glass1.wav",
  "KF_BImpactSnd/Impact_Glass2.wav",
};

new const String:GRENADE_SOUNDS[][] = {
  // "kf_grenadesnd/Nade_HitSurf1.wav",
  "kf_grenadesnd/Nade_HitSurf2.wav",
  "kf_grenadesnd/Nade_HitSurf3.wav",
  // "kf_grenadesnd/Nade_HitSurf4.wav",
  "kf_grenadesnd/Nade_Explode1.wav",
  "kf_grenadesnd/Nade_Explode2.wav",
  // "kf_grenadesnd/Nade_Explode3.wav",
  "kf_grenadesnd/KF_Grenade_Medic_Explode_M.wav",
};

public OnPluginStart() {
  AddNormalSoundHook(SoundHook);
}

public OnMapStart() {
  new String:buffer[PLATFORM_MAX_PATH];

  for (new i = 0, len = sizeof(HEADSHOT_SOUNDS); i < len; i++) {
    PrecacheSound(HEADSHOT_SOUNDS[i], true);
    Format(buffer, sizeof(buffer), "sound/%s", HEADSHOT_SOUNDS[i]);
    AddFileToDownloadsTable(buffer);
  }

  for (new i = 0, len = sizeof(HIT_SOUNDS); i < len; i++) {
    PrecacheSound(HIT_SOUNDS[i], true);
    Format(buffer, sizeof(buffer), "sound/%s", HIT_SOUNDS[i]);
    AddFileToDownloadsTable(buffer);
  }

  for (new i = 0, len = sizeof(GLASS_BREAK_SOUNDS); i < len; i++) {
    PrecacheSound(GLASS_BREAK_SOUNDS[i], true);
    Format(buffer, sizeof(buffer), "sound/%s", GLASS_BREAK_SOUNDS[i]);
    AddFileToDownloadsTable(buffer);
  }

  for (new i = 0, len = sizeof(GRENADE_SOUNDS); i < len; i++) {
    PrecacheSound(GRENADE_SOUNDS[i], true);
    Format(buffer, sizeof(buffer), "sound/%s", GRENADE_SOUNDS[i]);
    AddFileToDownloadsTable(buffer);
  }
}

public Action:SoundHook(clients[64], &numClients, String:sound[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags) {
  if (StrContains(sound, "player/headshot", false) != -1) {
    EmitSoundToAll(HEADSHOT_SOUNDS[GetRandomInt(0, 3)], entity);
    return Plugin_Stop;
  }
  else if (StrContains(sound, "physics/flesh/flesh_impact_bullet", false) != -1) {
    EmitSoundToAll(HIT_SOUNDS[GetRandomInt(0, 4)], entity);
    return Plugin_Stop;
  }
  else if (StrContains(sound, "player/death", false) != -1) {
    return Plugin_Stop;
  }
  else if (StrContains(sound, "weapons/hegrenade/he_bounce", false) != -1) {
    EmitSoundToAll(GRENADE_SOUNDS[GetRandomInt(0, 1)], entity);
    return Plugin_Stop;
  }
  else if (StrContains(sound, "weapons/hegrenade/explode", false) != -1) {
    EmitSoundToAll(GRENADE_SOUNDS[GetRandomInt(2, 3)], entity);
    return Plugin_Stop;
  }
  else if (StrContains(sound, "weapons/smokegrenade/sg_explode", false) != -1) {
    EmitSoundToAll(GRENADE_SOUNDS[4], entity);
    return Plugin_Stop;
  }
  else if (StrContains(sound, "physics/glass/glass_largesheet_break", false) != -1) {
    EmitSoundToAll(GLASS_BREAK_SOUNDS[GetRandomInt(0, 1)], entity);
    return Plugin_Stop;
  }

  return Plugin_Continue;
}
