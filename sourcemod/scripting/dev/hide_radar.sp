#pragma semicolon 1
#include <sourcemod>

public Plugin:myinfo = {
  name = "Hide Radar",
  version = "2022.11.05",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

new bool:isModCstrike;

public OnPluginStart() {
  new String:modName[10];
  GetGameFolderName(modName, sizeof(modName));
  isModCstrike = StrEqual(modName, "cstrike");

  HookEvent("player_spawn", Event_PlayerSpawnPre, EventHookMode_Pre);
  HookEvent("player_spawn", Event_PlayerSpawnPost, EventHookMode_Post);

  if (isModCstrike) {
    HookEvent("player_blind", Event_PlayerBlind, EventHookMode_Post);
  }

  // Removes error message: "DLL_MessageEnd:  Refusing to send user message UpdateRadar of 256 bytes to client, user message size limit is 255 bytes"
  // https://forums.alliedmods.net/showthread.php?t=170105
  HookUserMessage(GetUserMessageId("UpdateRadar"), Hook_UpdateRadar, true);
}

public Event_PlayerSpawnPre(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));
  if (client && !IsFakeClient(client) && isModCstrike) {
    // RemoveRadar(INVALID_HANDLE, client);
    CreateTimer(0.1, RemoveRadar, client);
  }
}

public Event_PlayerSpawnPost(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));
  if (client && !IsFakeClient(client) && isModCstrike) {
    // RemoveRadar(INVALID_HANDLE, client);
    CreateTimer(0.1, RemoveRadar, client);
  }
}

public Event_PlayerBlind(Handle:event, const String:name[], bool:dontBroadcast) {
	new client = GetClientOfUserId(GetEventInt(event, "userid"));

	if (client && !IsFakeClient(client) && GetClientTeam(client) > 1) {
		new Float:fDuration = GetEntPropFloat(client, Prop_Send, "m_flFlashDuration");
		CreateTimer(fDuration, RemoveRadar, client, TIMER_FLAG_NO_MAPCHANGE);
	}
}

public Action:RemoveRadar(Handle:timer, client) {
	if (client) {
    CSSHideRadar(client);
  }
  return Plugin_Stop;
}

public Action:Hook_UpdateRadar(UserMsg:msg_id, Handle:bf, const players[], playersNum, bool:reliable, bool:init) {
	if (BfGetNumBytesLeft(bf) > 253) {
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

CSSHideRadar(client) {
	SetEntPropFloat(client, Prop_Send, "m_flFlashDuration", 3600.0);
	SetEntPropFloat(client, Prop_Send, "m_flFlashMaxAlpha", 0.5);
}
