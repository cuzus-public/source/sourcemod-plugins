#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
	name = "Block Sound Flood",
  version = "2022.12.23",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

new Float:g_lastFlashUse[MAXPLAYERS + 1];

public OnPluginStart() {
	AddNormalSoundHook(NormalSoundHook);
}

public Action:NormalSoundHook(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags) {
  if (!IsValidClient(entity)) {
    return Plugin_Continue;
  }

  if (StrEqual(sample, "items/flashlight1.wav", false)) {
    if (g_lastFlashUse[entity] > 0 && GetEngineTime() - g_lastFlashUse[entity] < 0.4) {
      return Plugin_Stop;
    }
    g_lastFlashUse[entity] = GetEngineTime();
  }

	return Plugin_Continue;
}

stock bool IsValidClient(client) {
	if (client <= 0) return false;
	if (client > MaxClients) return false;
	if (!IsClientConnected(client)) return false;
	return IsClientInGame(client);
}
