#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <protobuf>

public Plugin:myinfo = {
  name = "Block Messages",
  version = "2022.10.14",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define MAX_MESSAGE_LENGTH 255
#define COMMANDS_DELIMITER "@@"

new Handle:cvar_blockCvarMessage;
new Handle:cvar_blockRadioText;
new Handle:cvar_blockConnectMessage;
new Handle:cvar_blockDisconnectMessage;
new Handle:cvar_blockTeamMessage;
new Handle:cvar_blockRoundEndPanel, Handle:cvar_blockNameChangeMessage, Handle:cvar_blockClanTags, Handle:cvar_blockKillFeed;
// new Handle:cvar_blockChatCommandsList;

new g_clientCommandTime[MAXPLAYERS + 1];


// CS 1.6 Hints
/*
"hint_win_round_by_killing_enemy",
"hint_press_buy_to_purchase",
"hint_spotted_an_enemy",
"hint_use_nightvision",
"hint_lost_money",
"hint_removed_for_next_hostage_killed",
"hint_careful_around_hostages",
"hint_careful_around_teammates",
"hint_reward_for_killing_vip",
"hint_win_round_by_killing_enemy",
"hint_try_not_to_injure_teammates",
"hint_you_are_in_targetzone",
"hint_hostage_rescue_zone",
"hint_terrorist_escape_zone",
"hint_ct_vip_zone",
"hint_terrorist_vip_zone",
"hint_cannot_play_because_tk",
"hint_use_hostage_to_stop_him",
"hint_lead_hostage_to_rescue_point",
"hint_you_have_the_bomb",
"hint_you_are_the_vip",
"hint_out_of_ammo",
"hint_spotted_a_friend",
"hint_spotted_an_enemy",
"hint_prevent_hostage_rescue",
"hint_rescue_the_hostages",
"hint_press_use_so_hostage_will_follow"
*/

new String:g_blockedHints[][] = {
  "Spec_Duck",
  "Hint_spotted_a_friend",
  "Hint_spotted_an_enemy",
  "Hint_win_round_by_killing_enemy",
  "Hint_press_buy_to_purchase",
  "Hint_out_of_ammo",
  "Hint_you_have_the_bomb"
};

new String:g_blockedChatCommands[][] = {
  "#Game_scoring",
  "#Game_connected",
  // Player: %s1 - Damage Taken
  // Player: %s1 - Damage Given
  // Damage Taken from "%s1" - %s2
  "Damage Taken",
  "Damage Given",
  "teammate_attack",
  "Killed_Teammate"
};

public OnPluginStart() {
  // convert default commands to CVAR string
  // new String:blockedChatCommandsStr[1024];
  // for (new i = 0; i < sizeof(DEFAULT_BLOCKED_CHAT_COMMANDS); i++) {
  //   Format(blockedChatCommandsStr, sizeof(blockedChatCommandsStr), "%s%s", DEFAULT_BLOCKED_CHAT_COMMANDS[i], COMMANDS_DELIMITER);
  // }

  // cvar_blockChatCommandsList = CreateConVar("sm_blockchatcommandslist", blockedChatCommandsStr, "", FCVAR_PROTECTED, false, 0.0, false, 0.0);
  // convert CVAR value back to commands defined in array
  // new String:buffer[1024];
  // GetConVarString(cvar_blockChatCommandsList, buffer, sizeof(buffer));
  // ExplodeString(buffer, COMMANDS_DELIMITER, g_blockedChatCommands, sizeof(g_blockedChatCommands), sizeof(g_blockedChatCommands[]));

  cvar_blockCvarMessage = CreateConVar("sm_blockmessages_cvar_change", "1", "", FCVAR_PROTECTED);
  cvar_blockRadioText = CreateConVar("sm_blockmessages_radio_text", "1", "", FCVAR_PROTECTED);
  cvar_blockConnectMessage = CreateConVar("sm_blockmessages_connect", "1", "", FCVAR_PROTECTED);
  cvar_blockDisconnectMessage = CreateConVar("sm_blockmessages_disconnect", "1", "", FCVAR_PROTECTED);
  cvar_blockTeamMessage = CreateConVar("sm_blockmessages_team", "1", "", FCVAR_PROTECTED);
  cvar_blockRoundEndPanel = CreateConVar("sm_blockmessages_roundendpanel", "1", "", FCVAR_PROTECTED);
  cvar_blockNameChangeMessage = CreateConVar("sm_blockmessages_name_change", "1", "", FCVAR_PROTECTED);
  cvar_blockClanTags = CreateConVar("sm_blockmessages_clan_tags", "1", "", FCVAR_PROTECTED);
  cvar_blockKillFeed = CreateConVar("sm_blockmessages_kill_feed", "0", "", FCVAR_PROTECTED);

  HookUserMessage(GetUserMessageId("TextMsg"), UserMsg_TextMsg, /*intercept*/ true);
  HookUserMessage(GetUserMessageId("RadioText"), UserMsg_RadioText, true);
  HookUserMessage(GetUserMessageId("HintText"), UserMsg_HintText, true);
  HookUserMessage(GetUserMessageId("SayText2"), UserMsg_SayText2, true);

  HookEvent("player_connect_client", Event_PlayerConnect, EventHookMode_Pre); // player_connect
  HookEvent("player_disconnect", Event_PlayerDisconnect, EventHookMode_Pre);
  HookEvent("player_team", Event_PlayerTeam, EventHookMode_Pre);
  HookEvent("cs_win_panel_round", WinPanel, EventHookMode_Pre); // https://forums.alliedmods.net/showthread.php?t=326217
  HookEvent("server_cvar", Event_ServerCvar, EventHookMode_Pre);
  HookEvent("round_end", Event_RoundEnd, EventHookMode_Pre);
  HookEvent("player_death", Event_PlayerDeath, EventHookMode_Pre);

  // late load
  for (new i; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			OnClientSettingsChanged(i);
		}
	}
}

public OnClientSettingsChanged(client) {
  if (GetConVarBool(cvar_blockClanTags)) {
    if (g_clientCommandTime[client] == 0 || g_clientCommandTime[client] <= (GetTime() - 1)) {
      g_clientCommandTime[client] = GetTime();
      ResetClanTag(client);
    }
  }
}

public Action:OnClientCommand(client, args) {
  if (GetConVarBool(cvar_blockClanTags)) {
    if (g_clientCommandTime[client] == 0 || g_clientCommandTime[client] <= (GetTime() - 1)) {
      g_clientCommandTime[client] = GetTime();
      ResetClanTag(client);
    }
  }

  return Plugin_Continue;
}

ResetClanTag(client) {
  if (IsValidClient(client)) {
    CS_SetClientClanTag(client, "");
  }
}

public Action:UserMsg_HintText(UserMsg:msg_id, BfRead:msg, const players[], playersNum, bool:reliable, bool:init) {
  new String:message[MAX_MESSAGE_LENGTH];
  msg.ReadByte(); // cpSender
  msg.ReadString(message, sizeof(message));

  for (new i = 0; i < sizeof(g_blockedHints); i++) {
    if (strlen(g_blockedHints[i]) > 0 && StrContains(message, g_blockedHints[i], false) != -1) {
      return Plugin_Handled;
    }
  }

  // PrintToServer("[HintText] message [ %s ]", message);

  return Plugin_Continue;
}

public Action:UserMsg_SayText2(UserMsg:msg_id, Handle:bf, const players[], playersNum, bool:reliable, bool:init) {
	new String:message[MAX_MESSAGE_LENGTH];
	BfReadShort(bf); // team color
	BfReadString(bf, message, sizeof(message));

	if (StrContains(message, "Name_Change", false) != -1 && GetConVarBool(cvar_blockNameChangeMessage)) {
		return Plugin_Handled;
  }

	return Plugin_Continue;
}

public Action:UserMsg_TextMsg(UserMsg:msg_id, BfRead:msg, const players[], playersNum, bool:reliable, bool:init) {
  new String:message[MAX_MESSAGE_LENGTH];
  msg.ReadByte(); // type
  msg.ReadString(message, sizeof(message));

  for (new i = 0; i < sizeof(g_blockedChatCommands); i++) {
    if (strlen(g_blockedChatCommands[i]) > 0 && StrContains(message, g_blockedChatCommands[i], false) != -1) {
      return Plugin_Handled;
    }
  }

  // PrintToServer("[TextMsg] message [ %s ]", message);

  return Plugin_Continue;
}

public Action:UserMsg_RadioText(UserMsg:msg_id, BfRead:msg, const players[], playersNum, bool:reliable, bool:init) {
  if (GetConVarBool(cvar_blockRadioText)) {
    return Plugin_Handled;
  }
  return Plugin_Continue;
}

public Action:Event_PlayerConnect(Handle:event, const String:name[], bool:dontBroadcast) {
  if (GetConVarBool(cvar_blockConnectMessage)) {
    SetEventBroadcast(event, /*dontBroadcast*/ true);
  }
  return Plugin_Continue;
}

public Action:Event_PlayerDisconnect(Handle:event, const String:name[], bool:dontBroadcast) {
  if (GetConVarBool(cvar_blockDisconnectMessage)) {
    SetEventBroadcast(event, /*dontBroadcast*/ true);
  }
  return Plugin_Continue;
}

public Action:Event_PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast) {
  if (GetConVarBool(cvar_blockTeamMessage)) {
    SetEventBroadcast(event, /*dontBroadcast*/ true);
  }
  return Plugin_Continue;
}

public Action:WinPanel(Handle:event, const String:name[], bool:dontBroadcast) {
  if (GetConVarBool(cvar_blockRoundEndPanel)) {
    return Plugin_Handled;
  }
  return Plugin_Continue;
}

public Action:Event_RoundEnd(Handle:event, const String:name[], bool:dontBroadcast) {
  new reason = GetEventInt(event, "reason");
  // blocks #Game_Commencing message
  if (reason == 15) {
    SetEventBroadcast(event, /*dontBroadcast*/ true);
  }
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  if (GetConVarBool(cvar_blockKillFeed)) {
    // blocks kill feed and console messages
    SetEventBroadcast(event, /*dontBroadcast*/ true);
  }
  return Plugin_Continue;
}

public Action:Event_ServerCvar(Handle:event, const String:name[], bool:dontBroadcast) {
  if (GetConVarBool(cvar_blockCvarMessage)) {
    SetEventBroadcast(event, /*dontBroadcast*/ true);
	}
  return Plugin_Continue;
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
