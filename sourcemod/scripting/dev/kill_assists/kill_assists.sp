#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <stocks2023>

#include <hint_text_queue>
#define MESSAGE_PLUGIN_LIBRARY "hint_text_queue"

public Plugin:myinfo = {
  name = "Kill Assists",
  version = "2023.01.24",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

/*
  TODO:
    test if this works for armor damage
*/

#define MONEY_FOR_KILL 300
#define TRANSLATIONS_FILE "kill_assists.phrases"

new g_iDamage[MAXPLAYERS + 1][MAXPLAYERS + 1]; // [attacker][victim]

new Handle:cvar_damagePercentForAssist, Handle:cvar_killerRewardForAssister;

new bool:g_isNativesLoaded = false;

public OnAllPluginsLoaded() {
	if (LibraryExists(MESSAGE_PLUGIN_LIBRARY)) {
		g_isNativesLoaded = true;
	}
}

public OnLibraryAdded(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = true;
  }
}

public OnLibraryRemoved(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = false;
  }
}

public OnPluginStart() {
  LoadTranslations(TRANSLATIONS_FILE);

  cvar_damagePercentForAssist = CreateConVar("sm_killassists_damage_percent", "51.0", "% of HP needed to count assist", FCVAR_PROTECTED, true, 0.0, true, 100.0);
  cvar_killerRewardForAssister = CreateConVar("sm_killassists_killer_reward_for_assister", "1", "Give reward to assister instead of killer based on damage percent", FCVAR_PROTECTED);

  HookEvent("player_spawn", Event_PlayerSpawn);
  HookEvent("player_death", Event_PlayerDeath, EventHookMode_Pre);

  // late load support
  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientInGame(i)) {
      OnClientPutInServer(i);
    }
  }
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new victim = GetClientOfUserId(GetEventInt(event, "userid"));
  new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));

  if (victim == attacker) {
    return Plugin_Continue;
  }

  new assister = 0;
  new assisterDmg = 0;

  new Float:damagePercentForAssist = GetConVarFloat(cvar_damagePercentForAssist);
  new damageRequired = RoundFloat((GetEntProp(victim, Prop_Data, "m_iMaxHealth") * damagePercentForAssist) / 100);

  for (new p = 1; p <= MaxClients; p++) {
    if (p != attacker && g_iDamage[p][victim] >= damageRequired && g_iDamage[p][victim] > assisterDmg) {
      assister = p;
      assisterDmg = g_iDamage[p][victim];
    }

    g_iDamage[p][victim] = 0;
  }

  if (IsValidClient(assister)) {
    new String:victimName[MAX_NAME_LENGTH + 1];
    GetClientName(victim, victimName, sizeof(victimName));

    new String:buf[128];
    Format(buf, sizeof(buf), "%T", "kill", assister, victimName);

    if (g_isNativesLoaded) {
      PrintQueuedHintText(assister, buf);
    } else {
      PrintToChat(assister, buf);
    }

    if (GetConVarBool(cvar_killerRewardForAssister)) {
      if (IsValidClient(attacker)) {
        new oldFrags = GetClientFrags(attacker);
        if (oldFrags - 1 >= 0) {
          SetClientFrags(attacker, oldFrags - 1);
        }
        new oldMoney = GetClientMoney(attacker);
        if (oldMoney - MONEY_FOR_KILL >= 0) {
          SetClientMoney(attacker, oldMoney - MONEY_FOR_KILL);
        } else {
          SetClientMoney(attacker, 0);
        }
      }

      SetClientFrags(assister, GetClientFrags(assister) + 1);
      SetClientMoney(assister, GetClientMoney(assister) + MONEY_FOR_KILL);
    }
  }

  return Plugin_Continue;
}

public OnClientPutInServer(client) {
  SDKHook(client, SDKHook_OnTakeDamageAlive, OnTakeDamageAlive);
}

public Action:OnTakeDamageAlive(victim, &attacker, &inflictor, &Float:damage, &damagetype) {
	if (!IsValidClient(victim) || !IsValidClient(attacker)) {
    return Plugin_Continue;
  }

  // do not count team damage
  if (GetClientTeam(victim) == GetClientTeam(attacker)) {
    return Plugin_Continue;
  }

  g_iDamage[attacker][victim] += RoundToFloor(damage);

  return Plugin_Continue;
}

public Action:Event_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast) {
	new client = GetClientOfUserId(GetEventInt(event, "userid"));

	for (new p = 1; p <= MaxClients; p++) {
    g_iDamage[client][p] = 0;
  }
}
