#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>

public Plugin:myinfo = {
  name = "Bot Join Leave",
  version = "2023.01.21",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

/*
TODO:
- join spec team first and then select team
- hibernate fix: save and restore bot teams/skins
- native: add/remove bot via admin commands
*/

#define IsBot(%1) (IsFakeClient(%1) && GetEntProp(%1, Prop_Data, "m_iFrags") != -999)

enum {
  Night_MinOnline = 0,
  Day_AvgOnline,
  Eve_MaxOnline
};

enum {
  COUNT_TYPE_ALL = 0,
  COUNT_TYPE_PLAYER,
  COUNT_TYPE_BOT
};

#define BOT_DIFF_MIN 1
#define BOT_DIFF_MAX 3

new Handle:cvar_botsEnabled, Handle:cvar_maxBotsNum, Handle:cvar_chancedMode, Handle:cvar_botsOnMapStart;
new Handle:cvar_chancedTimerMin, Handle:cvar_chancedTimerMax;
new Handle:cvar_unbalancedMode, Handle:cvar_hibernateRestoreTime;

new Handle:cv_bot_quota, Handle:cv_bot_difficulty, Handle:cv_mp_autoteambalance, Handle:cv_mp_limitteams;

new Handle:g_HibernatedBotNamesArr = INVALID_HANDLE;
new Float:g_lastHibernateTime;

public OnPluginStart() {
  g_HibernatedBotNamesArr = CreateArray(MAX_NAME_LENGTH + 1);

  HookEvent("server_cvar", Event_ServerCvarPre, EventHookMode_Pre);

  cvar_botsEnabled = CreateConVar("sm_bot_joinleave_enabled", "1", "Enable/disable join-leave for bots", FCVAR_PROTECTED);
  cvar_maxBotsNum = CreateConVar("sm_bot_joinleave_max_bots_num", "32", "Maximum bots number can be added to the game, set 0 to disable limitation", FCVAR_PROTECTED, true, 0.0);
  cvar_botsOnMapStart = CreateConVar("sm_bot_joinleave_bots_on_map_start", "15", "How many bots will be added on map start (use for testing)", FCVAR_PROTECTED, true, 0.0, true, 128.0);
  cvar_hibernateRestoreTime = CreateConVar("sm_bot_joinleave_hibernate_restore_time", "300", "Restore bots on hibernate within specified time in seconds or put 0 to disable", FCVAR_PROTECTED, true, 0.0);

  cvar_chancedMode = CreateConVar("sm_bot_joinleave_chanced_mode", "1", "Allow bots to join-leave randomly", FCVAR_PROTECTED);
  cvar_unbalancedMode = CreateConVar("sm_bot_joinleave_unbalanced_mode", "0", "Allow join-leave without checking teams balance", FCVAR_PROTECTED);
  cvar_chancedTimerMin = CreateConVar("sm_bot_joinleave_timer_delay_min", "60", "Minimal timer delay", FCVAR_PROTECTED, true, 0.0);
  cvar_chancedTimerMax = CreateConVar("sm_bot_joinleave_timer_delay_max", "300", "Maximal timer delay", FCVAR_PROTECTED, true, 0.0);

  cv_bot_quota = FindConVar("bot_quota");
  cv_bot_difficulty = FindConVar("bot_difficulty");
  cv_mp_autoteambalance = FindConVar("mp_autoteambalance");
  cv_mp_limitteams = FindConVar("mp_limitteams");

  // HookConVarChange(cv_bot_quota, OnConVarChange);
  /*
  public OnConVarChange(Handle:hConvar, String:oldValue[], String:newValue[]) {
    // if (StringToInt(newValue) < 2)
    // 	SetConVarInt(convar, 2);
    Debug("OnConVarChange %s -> %s", oldValue, newValue);
  }
  */
}

public OnMapStart() {
  if (g_HibernatedBotNamesArr != INVALID_HANDLE) {
    ClearArray(g_HibernatedBotNamesArr);
  }

  if (!GetConVarBool(cvar_botsEnabled)) {
    Debug("Bots Disabled");
    return;
  }

  new botsOnMapStart = GetConVarInt(cvar_botsOnMapStart);
  if (botsOnMapStart > 0) {
    new maxBotsNum = GetConVarInt(cvar_maxBotsNum);
    new count = maxBotsNum > 0 ? (botsOnMapStart > maxBotsNum ? maxBotsNum : botsOnMapStart) : botsOnMapStart;

    Debug("OnMapStart() Adding %d bots", count);

    for (new i = 0; i < count; i++) {
      CreateTimer(i + 1.0, AddBot);
    }
  }

  if (GetConVarBool(cvar_chancedMode)) {
    new Float:timerDelay = GetRandomFloat(GetConVarFloat(cvar_chancedTimerMin), GetConVarFloat(cvar_chancedTimerMax));
    new Float:val = botsOnMapStart + 1.0 + timerDelay;
    Debug("Scheduler() First timer will we triggered in %fs", val);
    CreateTimer(botsOnMapStart + 1.0 + val, Scheduler);
  }
}

// Supress chat message "Server cvar 'bot_quota' changed to"
public Action:Event_ServerCvarPre(Handle:event, String:name[], bool:dontBroadcast) {
  new String:cvarName[32];
  GetEventString(event, "cvarname", cvarName, sizeof(cvarName));

  if (StrEqual(cvarName, "bot_quota", false)) {
    return Plugin_Handled;
  }

  return Plugin_Continue;
}

public OnClientDisconnect(client) {
  if (!GetConVarBool(cvar_botsEnabled)) {
    return;
  }

  if (!IsBot(client)) {
    new playersNum = GetPlayerCount(COUNT_TYPE_PLAYER, 0);
    new botsNum = GetPlayerCount(COUNT_TYPE_BOT, 0);
    if (botsNum > 0 && playersNum == 1) {
      SaveHibernateBots();
    }
  }
}

public OnClientPostAdminCheck(client) {
  if (!GetConVarBool(cvar_botsEnabled)) {
    return;
  }

  if (IsBot(client)) {
    RestoreBotFromHibernate(client);
  } else {
    // TODO: 21/01/2023 disabled for test
    // FakeConVars(client);
  }
}
// FakeConVars(client) {
//   SendConVarValue(client, cv_bot_quota, "0");
// }

RestoreBotFromHibernate(client) {
  new Float:restoreTime = GetConVarFloat(cvar_hibernateRestoreTime);
  new bool:shouldRestore = restoreTime > 0 && (g_lastHibernateTime == 0 || (g_lastHibernateTime > 0 && GetGameTime() - g_lastHibernateTime <= restoreTime));
  if (!shouldRestore) {
    Debug("RestoreBotFromHibernate() Time limit reached, skipping restore");
    return;
  }

  // delay name change because of initial change in 'bot_names' plugin
  new Handle:pack = CreateDataPack();
  CreateDataTimer(0.1, SetBotName, pack);
  WritePackCell(pack, client);
}

SaveHibernateBots() {
  // last player disconnect causes "Server is hibernating"
  // all bots will be kicked by the server

  g_lastHibernateTime = GetGameTime();

  ClearArray(g_HibernatedBotNamesArr);

  new String:playerName[MAX_NAME_LENGTH + 1];

  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientConnected(i) && IsBot(i)) {
      GetClientName(i, playerName, sizeof(playerName));
      if (strlen(playerName) > 0) {
        PushArrayString(g_HibernatedBotNamesArr, playerName);
      }
    }
  }

  new botsNum = GetArraySize(g_HibernatedBotNamesArr);
  if (botsNum < 1) {
    return;
  }

  // this is necessary because value is not changing when server kicking bots
  SetConVarInt(cv_bot_quota, 0, true);

  Debug("SaveHibernateBots() Adding %d hibernated bots", botsNum);

  new Float:delay = 1.5; // should wait for hibernation message

  for (new i = 0; i < botsNum; i++) {
    CreateTimer(delay + i * 0.2, AddBot);
  }
}

Action:Scheduler(Handle:timer) {
  new totalBotsNum = GetPlayerCount(COUNT_TYPE_BOT, 0);
  new maxBotsNum = GetConVarInt(cvar_maxBotsNum);

  Debug("Scheduler() bots total: %d", totalBotsNum);

  if (maxBotsNum == 0 || (maxBotsNum > 0 && totalBotsNum < maxBotsNum)) {
    new bool:shouldJoin = GetRandomInt(1, 100) <= 50;
    if (totalBotsNum == 0 || shouldJoin) {
      ChancedBotJoin();
    } else {
      ChancedBotLeave();
    }
  } else if (maxBotsNum > 0 && totalBotsNum >= maxBotsNum) {
    ChancedBotLeave();
  }

  new Float:timerDelay = GetRandomFloat(GetConVarFloat(cvar_chancedTimerMin), GetConVarFloat(cvar_chancedTimerMax));
  Debug("Scheduler() Next check in %fs", timerDelay);
  CreateTimer(timerDelay, Scheduler);
}

ChancedBotJoin() {
  new onlineDayTime = GetOnlineDayTime();
  new chance = 0;
  if (onlineDayTime == Night_MinOnline) {
    chance = 15;
  } else if (onlineDayTime == Day_AvgOnline) {
    chance = 50;
  } else if (onlineDayTime == Eve_MaxOnline) {
    chance = 65;
  }

  Debug("ChancedBotJoin() chance %d", chance);

  if (GetRandomInt(1, 100) <= chance) {
    CreateTimer(1.0, AddBot);
  }
}

ChancedBotLeave() {
  new onlineDayTime = GetOnlineDayTime();
  new chance = 0;
  if (onlineDayTime == Night_MinOnline) {
    chance = 60;
  } else if (onlineDayTime == Day_AvgOnline) {
    chance = 50;
  } else if (onlineDayTime == Eve_MaxOnline) {
    chance = 25;
  }

  Debug("ChancedBotLeave() chance %d", chance);

  if (GetRandomInt(1, 100) <= chance) {
    CreateTimer(1.0, RemoveBot);
  }
}

Action:SetBotName(Handle:timer, DataPack pack) {
  // FIXME: rewrite to old syntax
  pack.Reset();
  new client = pack.ReadCell();

  if (client < 1 || !IsClientConnected(client)) {
    return;
  }

  // change name only if any values saved in array
  if (GetArraySize(g_HibernatedBotNamesArr) < 1) {
    return;
  }

  new String:playerName[MAX_NAME_LENGTH + 1];
  GetArrayString(g_HibernatedBotNamesArr, 0, playerName, sizeof(playerName));
  RemoveFromArray(g_HibernatedBotNamesArr, 0);

  if (strlen(playerName) == 0) {
    return;
  }

  Debug("SetBotName() Changing bot name to %s", playerName);

  SetClientInfo(client, "name", playerName);
}

Action:AddBot(Handle:timer) {
  new old_mp_autoteambalance = GetConVarInt(cv_mp_autoteambalance);
  new old_mp_limitteams = GetConVarInt(cv_mp_limitteams);
  new old_bot_difficulty = GetConVarInt(cv_bot_difficulty);

  SetConVarInt(cv_mp_autoteambalance, 0, true);
  SetConVarInt(cv_mp_limitteams, 0, true);
  new diff = GetRandomInt(BOT_DIFF_MIN, BOT_DIFF_MAX); // 1=normal 2=hard 3=expert
  SetConVarInt(cv_bot_difficulty, diff, true);

  new String:teamStr[3];

  if (GetConVarBool(cvar_unbalancedMode)) {
    Format(teamStr, sizeof(teamStr), "%s", (GetRandomInt(0, 1) == 1 ? "t" : "ct"));
  } else {
    new ctPlayersNum = GetPlayerCount(COUNT_TYPE_ALL, CS_TEAM_CT);
    new tPlayersNum = GetPlayerCount(COUNT_TYPE_ALL, CS_TEAM_T);
    Format(teamStr, sizeof(teamStr), "%s", (ctPlayersNum >= tPlayersNum ? "t" : "ct"));
  }

  new String:cmd[24];
  Format(cmd, sizeof(cmd), "bot_add %s", teamStr);

  Debug("AddBot() command %s", cmd);

  // TODO: is not adding new bot until player joins
  // SetConVarInt(cv_bot_quota, ctPlayersNum + tPlayersNum + 1, true);

  ServerCommand(cmd);

  SetConVarInt(cv_mp_autoteambalance, old_mp_autoteambalance, true);
  SetConVarInt(cv_mp_limitteams, old_mp_limitteams, true);
  SetConVarInt(cv_bot_difficulty, old_bot_difficulty, true);
}

Action:RemoveBot(Handle:timer) {
  new String:teamStr[3];

  if (GetConVarBool(cvar_unbalancedMode)) {
    Format(teamStr, sizeof(teamStr), "%s", GetRandomInt(0, 1) == 1 ? "t" : "ct");
  } else {
    new ctBotsNum = GetPlayerCount(COUNT_TYPE_BOT, CS_TEAM_CT);
    new tBotsNum = GetPlayerCount(COUNT_TYPE_BOT, CS_TEAM_T);
    Format(teamStr, sizeof(teamStr), "%s", ctBotsNum >= tBotsNum ? "t" : "ct");
  }

  new String:cmd[24];
  Format(cmd, sizeof(cmd), "bot_kick %s", teamStr);

  Debug("RemoveBot() command %s", cmd);

  ServerCommand(cmd);
}

stock GetOnlineDayTime() {
  new String:hour[4];
  FormatTime(hour, sizeof(hour), "%H", GetTime());
  new hourInt = StringToInt(hour); // 0-23

  // 01:00 - 09:00 (Night, min online)
  if (hourInt >= 1 && hourInt < 9) {
    return Night_MinOnline;
  }
  // 09:01 - 17:00 (Day, avg online)
  else if (hourInt >= 9 && hourInt < 5) {
    return Day_AvgOnline;
  }
  // 17:00 - 00:59 (Evening, max online)
  else {
    return Eve_MaxOnline;
  }
}

stock GetPlayerCount(type, team) {
  new countAll = 0;
  new countBot = 0;
  new countPlayer = 0;

  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientConnected(i) && IsClientInGame(i) && (!team || team == GetClientTeam(i))) {
      countAll++;

      if (IsBot(i)) {
        countBot++;
      } else {
        countPlayer++;
      }
    }
  }

  if (type == COUNT_TYPE_PLAYER) {
    return countPlayer;
  }

  if (type == COUNT_TYPE_BOT) {
    return countBot;
  }

  return countAll;
}

stock Debug(String:format[], any:...) {
  new String:pluginName[PLATFORM_MAX_PATH];
  GetPluginFilename(INVALID_HANDLE, pluginName, sizeof(pluginName));
  new String:buffer[256];
  VFormat(buffer, sizeof(buffer), format, 2);
  PrintToServer("<%s> %s", pluginName, buffer);
  // LogMessage(buffer); // log to file
}
