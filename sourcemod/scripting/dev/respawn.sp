#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <stocks2023>

/*
TODO:
  - disable spawn protection if player used fire
  - translations for respawn messages
  https://forums.alliedmods.net/showthread.php?p=2589195
*/

public Plugin:myinfo = {
  name = "Respawn",
  version = "2022.11.05",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define IsBot(%1) (IsFakeClient(%1) && GetEntProp(%1, Prop_Data, "m_iFrags") != -999)

new Float:g_playerRespawnSeconds[MAXPLAYERS + 1] = { 0.0, ... };
new Float:g_playerDeathOrigin[MAXPLAYERS + 1][3];
new Float:g_playerDeathAngles[MAXPLAYERS + 1][3];

new bool:g_pressFire[MAXPLAYERS + 1];

new Handle:cvar_respawnTime;
new Handle:cvar_respawnBots;
new Handle:cvar_respawnOnSamePlace;
new Handle:cvar_respawnProtectionTime;
new Handle:cvar_spawnProtectionColor;

new bool:g_roundIsInProgress;

new bool:isModCstrike;

public OnPluginStart() {
  new String:modName[10];
  GetGameFolderName(modName, sizeof(modName));
  isModCstrike = StrEqual(modName, "cstrike");

  cvar_respawnTime = CreateConVar("sm_respawn_time", "3", "Time to respawn in seconds", FCVAR_PROTECTED, true, 0.0);
  cvar_respawnProtectionTime = CreateConVar("sm_respawn_protection_time", "2", "Spawn protection in seconds, set 0 to disable", FCVAR_PROTECTED, true, 0.0);
  cvar_respawnBots = CreateConVar("sm_respawn_bots", "1", "Enable bots respawn", FCVAR_PROTECTED);
  cvar_respawnOnSamePlace = CreateConVar("sm_respawn_on_same_place", "0", "Enable teleporting player to death position on respawn", FCVAR_PROTECTED);
  cvar_spawnProtectionColor = CreateConVar("sm_respawn_protection_color", "255 255 255 200", "Color for spawn protection effect", FCVAR_PROTECTED);

  // when a player is spectating after death, the client will not send IN_ATTACK, IN_ATTACK2, IN_JUMP
  RegConsoleCmd("spec_mode", Command_Spec);
  RegConsoleCmd("spec_next", Command_Spec);
  RegConsoleCmd("spec_prev", Command_Spec);

  HookEvent("player_spawn", Event_PlayerSpawn);
  HookEvent("player_death", Event_PlayerDeath);
  HookEvent("round_start", Event_RoundStart, EventHookMode_PostNoCopy);
  HookEvent("round_end", Event_RoundEnd, EventHookMode_PostNoCopy);
  HookEvent("player_team", Event_PlayerTeam);
  HookEvent("weapon_fire", Event_WeaponFire);
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon) {
  if (IsClientConnected(client)
  && IsClientInGame(client)
  && !IsPlayerAlive(client)
  && g_playerRespawnSeconds[client] <= 0) {
    if ((buttons & IN_ATTACK) || IsFakeClient(client)) {
      g_pressFire[client] = true;
    }
  }
  return Plugin_Continue;
}

public Action:Command_Spec(client, args) {
	if (client > 0) {
    g_pressFire[client] = true;
  }
	return Plugin_Continue;
}

ResetPlayerData(client) {
  if (client < 1) {
    return;
  }
  g_playerRespawnSeconds[client] = 0.0;
  g_pressFire[client] = false;

  for (new i = 0, len = sizeof(g_playerDeathOrigin[][]); i < len; i++) {
    g_playerDeathOrigin[client][i] = 0.0;
    g_playerDeathAngles[client][i] = 0.0;
  }
}

public OnClientDisconnect(client) {
	ResetPlayerData(client);
}

public Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast) {
  g_roundIsInProgress = true;
}

public Event_RoundEnd(Handle:event, const String:name[], bool:dontBroadcast) {
  g_roundIsInProgress = false;

  // stop all respawn timers
  for (new client = 1; client <= MAXPLAYERS; client++) {
    ResetPlayerData(client);
  }
}

public Event_PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));

  if (client < 1 || !IsClientInGame(client)) {
    return;
  }

  ResetPlayerData(client);

  if (g_roundIsInProgress) {
    // adding small delay because IsPlayerAlive=true right after changing team
    CreateTimer(0.2, VerifyAndRespawn, client);
  }
}

public Action:Event_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast) {
  new Float:respawnProtectionTime = GetConVarFloat(cvar_respawnProtectionTime);
  if (respawnProtectionTime < 1.0) {
    return Plugin_Continue;
  }

  new client = GetClientOfUserId(GetEventInt(event, "userid"));

  if (!IsPlayerAlive(client) || !IsInTeam(client)) {
    return Plugin_Continue;
  }

  new String:colorStr[32];
  new String:colors[4][4];

  GetConVarString(cvar_spawnProtectionColor, colorStr, sizeof(colorStr));
  ExplodeString(colorStr, " ", colors, 4, 4);

  SetGod(client, true);

  SetEntityRenderColor(client, StringToInt(colors[0]), StringToInt(colors[1]), StringToInt(colors[2]), StringToInt(colors[3]));
  SetEntityRenderFx(client, RENDERFX_DISTORT);

  CreateTimer(respawnProtectionTime, RemoveProtection, client);

  return Plugin_Continue;
}

public Action:Event_WeaponFire(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));
  RemoveProtection(INVALID_HANDLE, client);

  return Plugin_Continue;
}

public Action:RemoveProtection(Handle:timer, any:client) {
  if (client > 0 && IsClientInGame(client)) {
    SetGod(client, false);
    SetEntityRenderMode(client, RENDER_NORMAL);
    SetEntityRenderFx(client, RENDERFX_NONE);
  }
}

public Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  if (!g_roundIsInProgress) {
    return;
  }

  new client = GetClientOfUserId(GetEventInt(event, "userid"));

  if (client < 1 || !IsClientInGame(client)) {
    return;
  }

  if (!GetConVarBool(cvar_respawnBots) && IsBot(client)) {
    return;
  }

  CreateTimer(0.2, VerifyAndRespawn, client);
}

public Action:VerifyAndRespawn(Handle:timer, client) {
  if (!IsClientInGame(client) || !IsInTeam(client) || IsPlayerAlive(client) || g_playerRespawnSeconds[client] > 0) {
    return Plugin_Stop;
  }

  if (GetConVarBool(cvar_respawnOnSamePlace)) {
		GetClientAbsOrigin(client, g_playerDeathOrigin[client]);
		GetClientAbsAngles(client, g_playerDeathAngles[client]);
	}

  g_playerRespawnSeconds[client] = GetConVarFloat(cvar_respawnTime);
  CreateTimer(1.0, RespawnTimer, client);
  ShowRespawnMessage(client);

  return Plugin_Continue;
}

public Action:RespawnTimer(Handle:timer, client) {
  // do not respawn CS_TEAM_NONE or CS_TEAM_SPECTATOR
  // spectators cause the spectated player to freeze
  if (!IsClientInGame(client) || !IsInTeam(client) || IsPlayerAlive(client) || !g_roundIsInProgress) {
    ResetPlayerData(client);
    return Plugin_Stop;
  }

  new bool:shouldRespawn = g_playerRespawnSeconds[client] <= 1 && g_pressFire[client];

  if (!shouldRespawn) {
    g_playerRespawnSeconds[client] -= 1.0;
    ShowRespawnMessage(client);
    CreateTimer(1.0, RespawnTimer, client);
    return Plugin_Stop;
  }

  g_pressFire[client] = false;
  ClearMessage(client);

  if (isModCstrike) {
    CS_RespawnPlayer(client);

    new Float:spawnPos[3];
    GetEntPropVector(client, Prop_Send, "m_vecOrigin", spawnPos);
    spawnPos[2] += 15.0; // fix spawning underground

    TeleportEntity(client, spawnPos, NULL_VECTOR, NULL_VECTOR);

    if (GetConVarBool(cvar_respawnOnSamePlace)) {
      TeleportEntity(client, g_playerDeathOrigin[client], g_playerDeathAngles[client], NULL_VECTOR);
    }
  }

  ResetPlayerData(client);

  return Plugin_Stop;
}

ShowRespawnMessage(client) {
  if (client < 1 || IsFakeClient(client)) {
    return;
  }
  new timeLeftInt = RoundToCeil(g_playerRespawnSeconds[client]);
  if (timeLeftInt > 0) {
    PrintCenterText(client, "You'll respawn in %d seconds.", timeLeftInt);
  } else {
    PrintCenterText(client, "Press [FIRE] button to respawn");
  }
}

ClearMessage(client) {
  PrintCenterText(client, "");
}
