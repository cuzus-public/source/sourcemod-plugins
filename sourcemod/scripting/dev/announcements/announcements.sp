#pragma semicolon 1
#include <sourcemod>

#include <hud_message_queue>
#define MESSAGE_PLUGIN_LIBRARY "hud_message_queue"

public Plugin:myinfo = {
  name = "Announcements",
  description = "Display announcements based on timer",
  author = "Geekrainian @ cuzus.games",
  version = "2023.01.10",
  url = "https://geekrainian.com",
};

#define TRANSLATIONS_FILE "announcements.phrases"
#define TRANSLATIONS_PATH "translations/announcements.phrases.txt"

#define MAX_MESSAGE_TEXT_LEN  256

new bool:g_isNativesLoaded = false;

new Handle:g_MessagesObj = INVALID_HANDLE;
new g_iCurrentMessage;

new Handle:cvar_messageInterval, Handle:cvar_randomOrder;
new Handle:cvar_hudChannel, Handle:cvar_hudPositionX, Handle:cvar_hudPositionY, Handle:cvar_messageTime;

public OnAllPluginsLoaded() {
	if (LibraryExists(MESSAGE_PLUGIN_LIBRARY)) {
		g_isNativesLoaded = true;
	}
}

public OnLibraryAdded(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = true;
  }
}

public OnLibraryRemoved(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = false;
  }
}

public OnPluginStart() {
  LoadTranslations(TRANSLATIONS_FILE);

  g_MessagesObj = CreateArray();

  cvar_hudChannel = CreateConVar("sm_announcements_hud_channel", "0", "", FCVAR_PROTECTED);
  cvar_hudPositionX = CreateConVar("sm_announcements_position_x", "0.0235", "", FCVAR_PROTECTED);
  cvar_hudPositionY = CreateConVar("sm_announcements_position_y", "0.6024", "", FCVAR_PROTECTED);
  cvar_messageTime = CreateConVar("sm_announcements_message_time", "10.0", "", FCVAR_PROTECTED, true, 1.0);

  cvar_messageInterval = CreateConVar("sm_announcements_message_interval", "300.0", "", FCVAR_PROTECTED, true, 10.0);
  cvar_randomOrder = CreateConVar("sm_announcements_random_order", "1", "", FCVAR_PROTECTED);
}

public OnPluginEnd() {
  for (new i = 0; i < GetArraySize(g_MessagesObj); i++) {
    if (GetArrayCell(g_MessagesObj, i) != INVALID_HANDLE) {
      CloseHandle(GetArrayCell(g_MessagesObj, i));
    }
	}
  delete g_MessagesObj;
}

public OnConfigsExecuted() {
  ClearArray(g_MessagesObj);

  ParseAds();

  new size = GetArraySize(g_MessagesObj);
  if (size == 0) {
    Debug("No announcement messages to display");
    return;
  }

  g_iCurrentMessage = 0;

  CreateTimer(GetConVarFloat(cvar_messageInterval), Scheduler, _, TIMER_REPEAT);
}

// public void OnMapEnd() {}

ConvertMessage(Handle:keyValues) {
  if (KvGetNum(keyValues, "enabled", 0) == 0) {
    return;
  }

  new String:tempMessage[3][MAX_MESSAGE_TEXT_LEN];
  new len = sizeof(tempMessage[]);

  KvGetString(keyValues, "en", tempMessage[0], len, "");
  KvGetString(keyValues, "ru", tempMessage[1], len, "");
  KvGetString(keyValues, "color", tempMessage[2], len, "");

  if (strlen(tempMessage[0]) == 0) {
    return;
  }

  new Handle:pack = CreateDataPack();
  WritePackString(pack, tempMessage[0]);
  WritePackString(pack, tempMessage[1]);
  WritePackString(pack, tempMessage[2]);

  PushArrayCell(g_MessagesObj, pack);
}

ParseAds() {
  new String:configPath[PLATFORM_MAX_PATH];
  BuildPath(Path_SM, configPath, sizeof(configPath), TRANSLATIONS_PATH);

  if (FileExists(configPath) == false) {
    SetFailState("Unable to find config: %s", configPath);
    return;
  }

  new Handle:keyValuesHandle = CreateKeyValues("Phrases");

  if (!FileToKeyValues(keyValuesHandle, configPath)) {
    CloseHandle(keyValuesHandle);
    SetFailState("Cannot read key-values from file.");
    return;
  }

  KvGotoFirstSubKey(keyValuesHandle);
  ConvertMessage(keyValuesHandle);

  while (KvGotoNextKey(keyValuesHandle)) {
    ConvertMessage(keyValuesHandle);
  }

  Debug("Messages loaded: %d", GetArraySize(g_MessagesObj));

  CloseHandle(keyValuesHandle);
}

public Action:Scheduler(Handle:timer) {
  new size = GetArraySize(g_MessagesObj);
  if (size == 0) {
    return Plugin_Continue;
  }

  new String:en[MAX_MESSAGE_TEXT_LEN], String:ru[MAX_MESSAGE_TEXT_LEN], String:color[MAX_MESSAGE_TEXT_LEN];
  new String:message[MAX_MESSAGE_TEXT_LEN];
  new len = sizeof(en);

  new index = GetConVarBool(cvar_randomOrder) ? GetRandomInt(0, size - 1) : g_iCurrentMessage;
  new Handle:pack = GetArrayCell(g_MessagesObj, index);
  ResetPack(pack);
  ReadPackString(pack, en, len);
  ReadPackString(pack, ru, len);
  ReadPackString(pack, color, len);

  if (strlen(en) == 0) {
    return Plugin_Continue;
  }

  new String:colors[4][4];
  ExplodeString(color, " ", colors, sizeof(colors), sizeof(colors[]));
  new R = StringToInt(colors[0]);
  new G = StringToInt(colors[1]);
  new B = StringToInt(colors[2]);

  new String:msgId[2];
  Format(msgId, sizeof(msgId), "%d", g_iCurrentMessage + 1);

  for (new i = 1; i <= MaxClients; i++) {
    if (IsValidPlayer(i)) {
      Format(message, sizeof(message), "%T", msgId, i);

      if (g_isNativesLoaded) {
        ShowQueuedHudText(
          i,
          GetConVarInt(cvar_hudChannel),
          message,
          R,
          G,
          B,
          GetConVarFloat(cvar_hudPositionX),
          GetConVarFloat(cvar_hudPositionY),
          GetConVarFloat(cvar_messageTime),
          true
        );
      } else {
        PrintToChat(i, message);
      }
    }
  }

  if (g_iCurrentMessage < size - 1) {
    g_iCurrentMessage++;
  } else {
    g_iCurrentMessage = 0;
  }

  return Plugin_Continue;
}

stock bool:IsValidPlayer(client) {
  return client > 0 && IsClientConnected(client) && IsClientInGame(client) && !IsFakeClient(client);
}

stock Debug(String:format[], any:...) {
  new String:pluginName[PLATFORM_MAX_PATH];
  GetPluginFilename(INVALID_HANDLE, pluginName, sizeof(pluginName));
  new String:buffer[256];
  VFormat(buffer, sizeof(buffer), format, 2);
  PrintToServer("<%s> %s", pluginName, buffer);
}
