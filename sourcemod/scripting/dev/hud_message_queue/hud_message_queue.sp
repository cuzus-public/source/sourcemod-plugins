#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

#define THIS_PLUGIN_LIBRARY "hud_message_queue"

public Plugin:myinfo = {
  name = "Hud Message Queue",
  description = "Queue for HUD text messages using linebreaks",
  author = "Geekrainian @ cuzus.games",
  version = "2023.01.10",
  url = "https://geekrainian.com",
};

#define MAX_HUD_CHANNELS 5 // 0-5
#define MAX_CHAT_TEXT_LENGTH 192
#define BUFFER_MESSAGE_LENGTH 512

new Handle:g_messageQueueArray;
new Handle:g_timerHandleArray;

public OnPluginStart() {
  g_messageQueueArray = CreateArray();
  g_timerHandleArray = CreateArray();

  // create arrays for all hud channels
  for (new i = 0; i <= MAX_HUD_CHANNELS; i++) {
    new Handle:playersArr = CreateArray();

    for (new k = 0; k <= MAXPLAYERS + 1; k++) {
      new Handle:messagesArr = CreateArray(MAX_CHAT_TEXT_LENGTH);
      PushArrayCell(playersArr, messagesArr);
    }

    PushArrayCell(g_messageQueueArray, playersArr);

    new Handle:playersArr2 = CreateArray();
    for (new k = 0; k <= MAXPLAYERS + 1; k++) {
      new Handle:timerHandle = null;
      PushArrayCell(playersArr2, timerHandle);
    }

    PushArrayCell(g_timerHandleArray, playersArr2);
  }
}

public APLRes:AskPluginLoad2(Handle:hPlugin, bool:bLate, String:strError[], maxErrors) {
  CreateNative("ShowQueuedHudText", Native_ShowQueuedHudText);
  RegPluginLibrary(THIS_PLUGIN_LIBRARY);
  return APLRes_Success;
}

public Native_ShowQueuedHudText(Handle:hPlugin, numParams) {
  new String:message[BUFFER_MESSAGE_LENGTH];

  new client = GetNativeCell(1);
  new channel = GetNativeCell(2);
  new strLen = 0;
  GetNativeStringLength(3, strLen);
  GetNativeString(3, message, strLen * 2); // increasing length twice due to special characters, eg "❱"
  new R = GetNativeCell(4);
  new G = GetNativeCell(5);
  new B = GetNativeCell(6);
  new Float:X = GetNativeCell(7);
  new Float:Y = GetNativeCell(8);
  new Float:time = view_as<float>(GetNativeCell(9));
  new bool:replace = view_as<bool>(GetNativeCell(10));
  new maxLines = GetNativeCell(11);

  return ShowQueuedHudText(client, channel, message, R, G, B, X, Y, time, replace, maxLines);
}

public OnClientPostAdminCheck(client) {
  if (IsFakeClient(client)) {
    return;
  }

  for (new i = 0; i < MAX_HUD_CHANNELS; i++) {
    ResetQueue(client, i);
    ResetTimer(client, i);
  }
}

bool:ShowQueuedHudText(
  client,
  channel,
  const String:message[],
  R,
  G,
  B,
  Float:X,
  Float:Y,
  Float:time,
  bool:replace,
  maxLines
) {
  if (!IsValidClient(client) || IsFakeClient(client)) {
    return false;
  }
  if (channel < 0 || channel > MAX_HUD_CHANNELS) {
    return false;
  }
  if (strlen(message) < 1) {
    return false;
  }
  if (GetArraySize(g_messageQueueArray) == 0) {
    return false;
  }

  if (replace) {
    ResetQueue(client, channel);
  }

  QueueMessage(client, channel, message, maxLines);

  new String:outBuffer[BUFFER_MESSAGE_LENGTH];
  GetQueueAsBuffer(client, channel, outBuffer, sizeof(outBuffer));

  if (ShowHudTextEx(client, channel, outBuffer, R, G, B, X, Y, time) != -1) {
    SetMessageTimer(client, channel, time);
    return true;
  }

  return false;
}

ResetTimer(client, channel) {
  if (!IsClient(client)) {
    return;
  }

  new Handle:playersArr = GetArrayCell(g_timerHandleArray, channel);
  new Handle:timerHandle = GetArrayCell(playersArr, client);

  // if (timerHandle != null && timerHandle != INVALID_HANDLE) {
  //   KillTimer(timerHandle);
  // }
  delete timerHandle;
}

ResetQueue(client, channel) {
  new Handle:playersArr = GetArrayCell(g_messageQueueArray, channel);
  if (playersArr == INVALID_HANDLE) {
    return;
  }
  new Handle:messagesArr = GetArrayCell(playersArr, client);
  if (messagesArr == INVALID_HANDLE) {
    return;
  }
  ClearArray(messagesArr);
}

QueueMessage(client, channel, const String:message[], maxLines) {
  if (!IsValidClient(client)) {
    return;
  }
  if (maxLines < 1) {
    return;
  }

  new Handle:playersArr = GetArrayCell(g_messageQueueArray, channel);
  new Handle:messagesArr = GetArrayCell(playersArr, client);

  // remove first item
  if (GetArraySize(messagesArr) == maxLines) {
    RemoveFromArray(messagesArr, 0);
  }

  PushArrayString(messagesArr, message);
}

public Action:ResetQueueTimer(Handle:timer, any:data) {
  if (data == INVALID_HANDLE) {
		return Plugin_Stop;
	}
  ResetPack(data);
  new client = ReadPackCell(data);
  if (!IsClient(client)) {
    return Plugin_Stop;
  }
  new channel = ReadPackCell(data);
  if (channel < 0 || channel > MAX_HUD_CHANNELS) {
    return Plugin_Stop;
  }

  new Handle:playersArr = GetArrayCell(g_timerHandleArray, channel);
  new Handle:timerHandle = GetArrayCell(playersArr, client);
  timerHandle = null; // https://forums.alliedmods.net/showpost.php?p=2418982&postcount=6
  SetArrayCell(playersArr, client, timerHandle);

  ResetQueue(client, channel);

  return Plugin_Stop;
}

SetMessageTimer(client, channel, Float:time) {
  ResetTimer(client, channel);

  new Handle:pack;
  new Handle:timerHandle = CreateDataTimer(time, ResetQueueTimer, pack);
  WritePackCell(pack, client);
  WritePackCell(pack, channel);

  new Handle:playersArr = GetArrayCell(g_timerHandleArray, channel);
  SetArrayCell(playersArr, client, timerHandle);
}

GetQueueAsBuffer(client, channel, String:buffer[], bufferLength) {
  new Handle:playersArr = GetArrayCell(g_messageQueueArray, channel);
  if (playersArr == INVALID_HANDLE) {
    return;
  }

  new Handle:messagesArr = GetArrayCell(playersArr, client);
  if (messagesArr == INVALID_HANDLE) {
    return;
  }

  new size = GetArraySize(messagesArr);

  if (size == 0) {
    Format(buffer, bufferLength, "");
  } else {
    new String:msg[MAX_CHAT_TEXT_LENGTH];

    for (new i = 0; i < size; i++) {
      GetArrayString(messagesArr, i, msg, sizeof(msg));

      if (strlen(msg) > 0) {
        if (i == 0) {
          Format(buffer, bufferLength, "%s", msg);
        } else {
          Format(buffer, bufferLength, "%s\n%s", buffer, msg);
        }
      }
    }
  }
}

ShowHudTextEx(client, channel, const String:message[], R, G, B, Float:X, Float:Y, Float:time) {
  SetHudTextParams(
    X,
    Y,
    time,
    R,
    G,
    B,
    50,
    0 /*effect*/,
    0.0 /*fxTime*/,
    0.25 /*fadeIn*/,
    0.5 /*fadeOut*/
  );
  return ShowHudText(client, channel, message);
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
