#if defined _hud_message_queue_included
  #endinput
#endif
#define _hud_message_queue_included

/**
 * @returns true if message sent successfully.
 */
native bool:ShowQueuedHudText(
  client,
  channel,
  const String:message[],
  R = 255,
  G = 255,
  B = 255,
  Float:X = 0.5,
  Float:Y = 0.7,
  Float:time = 1.0,
  bool:replace = false,
  maxHudLines = 5 // HUD Text do not accept more than 5 lines
);
