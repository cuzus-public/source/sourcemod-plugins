#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "Bot Actions",
  version = "2022.12.23",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

new Float:g_lastDamageTime[MAXPLAYERS + 1];

new bool:g_isAfk[MAXPLAYERS + 1];

new g_flashlightMaxTicks[MAXPLAYERS + 1];
new g_flashlightCurTicks[MAXPLAYERS + 1];
new bool:g_shouldFlashlight[MAXPLAYERS + 1];

new g_weaponSwitchMaxTicks[MAXPLAYERS + 1];
new g_weaponSwitchCurTicks[MAXPLAYERS + 1];

new g_lookTarget[MAXPLAYERS + 1];

new g_duckMaxTicks[MAXPLAYERS + 1];
new g_duckCurTicks[MAXPLAYERS + 1];
new bool:g_shouldDuck[MAXPLAYERS + 1];

new g_jumpMaxTicks[MAXPLAYERS + 1];
new g_jumpCurTicks[MAXPLAYERS + 1];
new bool:g_shouldJump[MAXPLAYERS + 1];

new g_attackMaxTicks[MAXPLAYERS + 1];
new g_attackCurTicks[MAXPLAYERS + 1];
new bool:g_shouldAttack[MAXPLAYERS + 1];

new Handle:cv_maxPerAction, Handle:cv_maxAfkPlayers;

public OnPluginStart() {
  cv_maxPerAction = CreateConVar("sm_bot_actions_max_per_action", "3", "Max bots can use action simultaneously", FCVAR_PROTECTED, true, 1);
  cv_maxAfkPlayers = CreateConVar("sm_bot_actions_max_afk_players", "2", "Max bots can be in AFK", FCVAR_PROTECTED, true, 1);

  HookEvent("player_spawn", Event_PlayerSpawn);
  HookEvent("player_hurt", Event_PlayerHurt);

  CreateTimer(10.0, Scheduler);
}

public Action:Event_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast) {
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
  if (!IsClientConnected(client))
    return Plugin_Continue;

  if (g_isAfk[client]) {
    MakeAfkPlayer(client, true);
  }

  return Plugin_Continue;
}

public Action:Event_PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast) {
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
  if (!IsClientConnected(client))
    return Plugin_Continue;

  g_lastDamageTime[client] = GetEngineTime();

  return Plugin_Continue;
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon, &subtype, &cmdnum, &tickcount, &seed, mouse[2]) {
  if (IsFakeClient(client) && IsPlayerAlive(client)) {
    if (g_lookTarget[client] > 0) {
      LookAtTarget(client, g_lookTarget[client]);
    }

    // prevent stupid actions while attack
    if (!(buttons & IN_ATTACK)) {
      if (g_shouldFlashlight[client]) {
        impulse = 100;
        return Plugin_Changed;
      } else if (impulse != 0) {
        impulse = 0;
        return Plugin_Changed;
      }

      if (g_shouldAttack[client]) {
        buttons |= IN_ATTACK;
        return Plugin_Changed;
      }
    }

    if (g_shouldDuck[client]) {
      buttons |= IN_DUCK;
      return Plugin_Changed;
    }
    if (g_shouldJump[client]) {
      buttons |= IN_JUMP;
      return Plugin_Changed;
    }
  }
  return Plugin_Continue;
}

Action:Scheduler(Handle:timer) {
  new maxPerAction = GetRandomInt(1, GetConVarInt(cv_maxPerAction));

  new Handle:hPlayersIdsArr = CreateArray();
  for (new i = 0; i < maxPerAction; i++) {
    new randomId = GetRandomBotId();

    if (FindValueInArray(hPlayersIdsArr, randomId) == -1) {
      PushArrayCell(hPlayersIdsArr, randomId);
      BotAction(randomId);
    }
  }
  ClearArray(hPlayersIdsArr);

  CreateTimer(GetRandomFloat(10.0, 30.0), Scheduler);
}

BotAction(client) {
  if (!IsClientConnected(client)) {
    return;
  }

  // new String:playerName[MAX_NAME_LENGTH + 1];
  // GetClientName(client, playerName, sizeof(playerName));

  new action = GetRandomInt(1, 8);

  if (action == 1) {
    if (IsLastDamagedAfterTime(client, 3.0)) {
      TriggerAttack(client);
    }
  } else if (action == 2) {
    TriggerJumping(client);
  } else if (action == 3) {
    TriggerDucking(client);
  } else if (action == 4) {
    if (IsLastDamagedAfterTime(client, 3.0)) {
      TriggerUseFlashlight(client);
    }
  } else if (action == 5) {
    if (IsLastDamagedAfterTime(client, 3.0)) {
      TriggerChangeWeapon(client);
    }
  } else if (action == 6) {
    if (IsLastDamagedAfterTime(client, 3.0)) {
      TriggerDropWeapon(client);
    }
  } else if (action == 7) {
    if (IsLastDamagedAfterTime(client, 3.0)) {
      new target = GetClosestTeamPlayer(client);
      if (target > 0) {
        TriggerLookingAtTarget(client, target);
      }
    }
  } else if (action == 8) {
    new afkPlayersCount = GetAfkPlayersCount();
    new maxAfkCount = GetConVarInt(cv_maxAfkPlayers);
    if (afkPlayersCount < maxAfkCount) {
      MakeAfkPlayer(client, true);
      CreateTimer(GetRandomFloat(5.0, 180.0), RemoveAfkPlayer, client);
    }
  }
}

TriggerDropWeapon(client) {
  new weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
  if (weapon != -1) {
    new ammo = GetPrimaryAmmo(client);
    new ammo2 = GetReserveAmmo(client);

    if (ammo == 0 && ammo2 < 10) {
      FakeClientCommand(client, "drop");
    }
  }
}

TriggerAttack(client) {
  g_attackMaxTicks[client] = GetRandomInt(1, 10);
  CreateTimer(0.0, UseAttackTask, client);
}

public Action:UseAttackTask(Handle:timer, any:client) {
  if (!IsClientConnected(client) || !IsPlayerAlive(client)) {
    return;
  }

  g_shouldAttack[client] = !g_shouldAttack[client];

  g_attackCurTicks[client]++;

  if (g_attackCurTicks[client] < g_attackMaxTicks[client]) {
    CreateTimer(GetRandomFloat(0.1, 0.5), UseAttackTask, client);
  } else {
    g_shouldAttack[client] = false;
    g_attackMaxTicks[client] = 0;
    g_attackCurTicks[client] = 0;
  }
}

TriggerJumping(client) {
  g_jumpMaxTicks[client] = GetRandomInt(2, 16);
  CreateTimer(0.0, UseJumpTask, client);
}

public Action:UseJumpTask(Handle:timer, any:client) {
  if (!IsClientConnected(client) || !IsPlayerAlive(client)) {
    return;
  }

  g_shouldJump[client] = !g_shouldJump[client];

  g_jumpCurTicks[client]++;

  if (g_jumpCurTicks[client] < g_jumpMaxTicks[client]) {
    CreateTimer(GetRandomFloat(0.3, 0.6), UseJumpTask, client);
  } else {
    g_shouldJump[client] = false;
    g_jumpMaxTicks[client] = 0;
    g_jumpCurTicks[client] = 0;
  }
}

TriggerDucking(client) {
  g_duckMaxTicks[client] = GetRandomInt(2, 16);
  CreateTimer(0.0, UseDuckTask, client);
}

public Action:UseDuckTask(Handle:timer, any:client) {
  if (!IsClientConnected(client) || !IsPlayerAlive(client)) {
    return;
  }

  g_shouldDuck[client] = !g_shouldDuck[client];

  g_duckCurTicks[client]++;

  if (g_duckCurTicks[client] < g_duckMaxTicks[client]) {
    CreateTimer(GetRandomFloat(0.3, 0.5), UseDuckTask, client);
  } else {
    g_shouldDuck[client] = false;
    g_duckMaxTicks[client] = 0;
    g_duckCurTicks[client] = 0;
  }
}

TriggerLookingAtTarget(client, target) {
  g_lookTarget[client] = target;
  CreateTimer(GetRandomFloat(5.0, 10.0), CancelLookAtTarget, client);
}

public Action:CancelLookAtTarget(Handle:timer, any:client) {
  g_lookTarget[client] = 0;
}

TriggerUseFlashlight(client) {
  g_flashlightMaxTicks[client] = GetRandomInt(1, 6);
  CreateTimer(0.0, UseFlashlightTask, client);
}

public Action:UseFlashlightTask(Handle:timer, any:client) {
  if (!IsClientConnected(client) || !IsPlayerAlive(client)) {
    return;
  }

  g_shouldFlashlight[client] = !g_shouldFlashlight[client];

  g_flashlightCurTicks[client]++;

  if (g_flashlightCurTicks[client] < g_flashlightMaxTicks[client]) {
    CreateTimer(GetRandomFloat(0.3, 0.7), UseFlashlightTask, client);
  } else {
    g_flashlightMaxTicks[client] = 0;
    g_flashlightCurTicks[client] = 0;
  }
}

TriggerChangeWeapon(client) {
  g_weaponSwitchMaxTicks[client] = GetRandomInt(1, 8);
  CreateTimer(0.0, SwitchWeaponTask, client);
}

public Action:SwitchWeaponTask(Handle:timer, any:client) {
  if (!IsClientConnected(client) || !IsPlayerAlive(client)) {
    return;
  }

  new String:curWeaponName[128];
  GetClientWeapon(client, curWeaponName, sizeof(curWeaponName));

  new primaryWeaponIndex = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
  if (primaryWeaponIndex == -1) {
    primaryWeaponIndex = GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY);
  }
  new String:primaryWeaponName[128];
  if (IsValidEdict(primaryWeaponIndex)) {
    GetEdictClassname(primaryWeaponIndex, primaryWeaponName, sizeof(primaryWeaponName));
  }

  new knifeWeaponIndex = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
  new String:knifeWeaponName[128];
  if (IsValidEdict(knifeWeaponIndex)) {
    GetEdictClassname(knifeWeaponIndex, knifeWeaponName, sizeof(knifeWeaponName));
  }

  if (StrEqual(curWeaponName, primaryWeaponName, false)) {
    FakeClientCommand(client, "use %s", knifeWeaponName);
    SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", knifeWeaponIndex);
  } else if (strlen(primaryWeaponName) > 0) {
    FakeClientCommand(client, "use %s", primaryWeaponName);
    SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", primaryWeaponIndex);
  }

  // this is not working (empty hands)
  // EquipPlayerWeapon(client, GetPlayerWeaponSlot(client, slot));
  // SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", weapon);
  // ChangeEdictState(client, FindDataMapOffs(i, "m_hActiveWeapon"));

  g_weaponSwitchCurTicks[client]++;

  if (g_weaponSwitchCurTicks[client] < g_weaponSwitchMaxTicks[client]) {
    CreateTimer(GetRandomFloat(0.3, 0.5), SwitchWeaponTask, client);
  } else {
    g_weaponSwitchMaxTicks[client] = 0;
    g_weaponSwitchCurTicks[client] = 0;
  }
}

public Action:RemoveAfkPlayer(Handle:timer, any:client) {
  MakeAfkPlayer(client, false);
}

MakeAfkPlayer(client, bool:afk) {
  if (IsClientInGame(client)) {
    if (afk) {
      SetEntityFlags(client, GetEntityFlags(client) | FL_FROZEN);
      g_isAfk[client] = true;
    } else {
      SetEntityFlags(client, GetEntityFlags(client) & ~FL_FROZEN);
      g_isAfk[client] = false;
    }
  }
}

GetAfkPlayersCount() {
  new count = 0;
  for (new i = 0; i < sizeof(g_isAfk); i++) {
    if (g_isAfk[i]) {
      count++;
    }
  }
  return count;
}

bool:IsLastDamagedAfterTime(client, Float:time = 0.0) {
  return g_lastDamageTime[client] == 0 || GetEngineTime() - g_lastDamageTime[client] > time;
}

GetRandomBotId() {
  new botsIds[MAXPLAYERS + 1];

  for (new i = 1; i <= MaxClients; i++) {
    if (!IsClientConnected(i)) {
      continue;
    }
    if (!IsFakeClient(i)) {
      continue;
    }
    botsIds[i - 1] = i;
  }

  new lastNonZeroId = 0;

  for (new i = 0; i < sizeof(botsIds); i++) {
    new bool:isLastEntry = i == sizeof(botsIds) - 1;
    new bool:chancedPick = GetRandomInt(1, 100) <= 10;

    if (botsIds[i] > 0) {
      lastNonZeroId = botsIds[i];
    }

    if (lastNonZeroId > 0 && (chancedPick || isLastEntry)) {
      return lastNonZeroId;
    }
  }

  return 0;
}

GetClosestTeamPlayer(iClient) {
  if( iClient <= 0 || iClient > MaxClients || !IsClientInGame(iClient) || !IsPlayerAlive(iClient) )
    return -1;

  new iTeam = GetClientTeam(iClient); // == 3 ? 2 : 3;
  new iClosest = -1, Float:flMinDistance = -1.0;
  new Float:flDistance, Float:vecClientOrigin[3], Float:vecOtherOrigin[3];
  GetClientAbsOrigin(iClient, vecClientOrigin);

  for (new iOther = 1; iOther <= MaxClients; iOther++)
    if (IsClientInGame(iOther) && IsPlayerAlive(iOther) && GetClientTeam(iOther) == iTeam) {
      GetClientAbsOrigin(iOther, vecOtherOrigin);

      flDistance = GetVectorDistance(vecClientOrigin, vecOtherOrigin);
      if (flDistance < 0.0 )
        flDistance *= -1.0;

      if (flMinDistance == -1.0 || flDistance < flMinDistance) {
        flMinDistance = flDistance;
        iClosest = iOther;
      }
    }

  return iClosest;
}

stock LookAtTarget(client, target) {
  new Float:angles[3], Float:clientEyes[3], Float:targetEyes[3], Float:resultant[3];
  GetClientEyePosition(client, clientEyes);

  if (target > 0 && target <= MaxClients && IsClientInGame(target)) {
    GetClientEyePosition(target, targetEyes);
  } else {
    GetEntPropVector(target, Prop_Send, "m_vecOrigin", targetEyes);
  }

  MakeVectorFromPoints(targetEyes, clientEyes, resultant);
  GetVectorAngles(resultant, angles);

  if (angles[0] >= 270) {
    angles[0] -= 270;
    angles[0] = (90 - angles[0]);
  } else {
    if (angles[0] <= 90){
      angles[0] *= -1;
    }
  }

  angles[1] -= 180;

  TeleportEntity(client, NULL_VECTOR, angles, NULL_VECTOR);
}

/*
stock void LookAtClient(int iClient, int iTarget)
{
	float fTargetPos[3]; float fTargetAngles[3]; float fClientPos[3]; float fFinalPos[3];
	GetClientEyePosition(iClient, fClientPos);
	GetClientEyePosition(iTarget, fTargetPos);
	GetClientEyeAngles(iTarget, fTargetAngles);

	float fVecFinal[3];
	AddInFrontOf(fTargetPos, fTargetAngles, 7.0, fVecFinal);
	MakeVectorFromPoints(fClientPos, fVecFinal, fFinalPos);

	GetVectorAngles(fFinalPos, fFinalPos);

	//Recoil Control System
	if (g_cvRecoilMode.IntValue == 2)
	{
		float vecPunchAngle[3];

		if (GetEngineVersion() == Engine_CSGO || GetEngineVersion() == Engine_CSS)
		{
			GetEntPropVector(iClient, Prop_Send, "m_aimPunchAngle", vecPunchAngle);
		}
		else
		{
			GetEntPropVector(iClient, Prop_Send, "m_vecPunchAngle", vecPunchAngle);
		}

		if(g_cvPredictionConVars[5] != null)
		{
			fFinalPos[0] -= vecPunchAngle[0] * GetConVarFloat(g_cvPredictionConVars[5]);
			fFinalPos[1] -= vecPunchAngle[1] * GetConVarFloat(g_cvPredictionConVars[5]);
		}
	}

	TeleportEntity(iClient, NULL_VECTOR, fFinalPos, NULL_VECTOR);
}
stock void AddInFrontOf(float fVecOrigin[3], float fVecAngle[3], float fUnits, float fOutPut[3])
{
	float fVecView[3]; GetViewVector(fVecAngle, fVecView);

	fOutPut[0] = fVecView[0] * fUnits + fVecOrigin[0];
	fOutPut[1] = fVecView[1] * fUnits + fVecOrigin[1];
	fOutPut[2] = fVecView[2] * fUnits + fVecOrigin[2];
}

stock void GetViewVector(float fVecAngle[3], float fOutPut[3])
{
	fOutPut[0] = Cosine(fVecAngle[1] / (180 / FLOAT_PI));
	fOutPut[1] = Sine(fVecAngle[1] / (180 / FLOAT_PI));
	fOutPut[2] = -Sine(fVecAngle[0] / (180 / FLOAT_PI));
}
*/

/*
stock int GetClosestClient(int iClient)
{
	float fClientOrigin[3], fTargetOrigin[3];

	GetClientAbsOrigin(iClient, fClientOrigin);

	int iClientTeam = GetClientTeam(iClient);
	int iClosestTarget = -1;

	float fClosestDistance = -1.0;
	float fTargetDistance;

	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsValidClient(i))
		{
			if (iClient == i || GetClientTeam(i) == iClientTeam || !IsPlayerAlive(i))
			{
				continue;
			}

			GetClientAbsOrigin(i, fTargetOrigin);
			fTargetDistance = GetVectorDistance(fClientOrigin, fTargetOrigin);

			if (fTargetDistance > fClosestDistance && fClosestDistance > -1.0)
			{
				continue;
			}

			if (!ClientCanSeeTarget(iClient, i))
			{
				continue;
			}

			if (GetEngineVersion() == Engine_CSGO)
			{
				if (GetEntPropFloat(i, Prop_Send, "m_fImmuneToGunGameDamageTime") > 0.0)
				{
					continue;
				}
			}

			if (g_cvDistance.FloatValue != 0.0 && fTargetDistance > g_cvDistance.FloatValue)
			{
				continue;
			}

			if (g_cvFov.FloatValue != 0.0 && !IsTargetInSightRange(iClient, i, g_cvFov.FloatValue, g_cvDistance.FloatValue))
			{
				continue;
			}

			if (g_cvFlashbang.BoolValue && g_bFlashed[iClient])
			{
				continue;
			}

			fClosestDistance = fTargetDistance;
			iClosestTarget = i;
		}
	}

	return iClosestTarget;
}

stock bool ClientCanSeeTarget(int iClient, int iTarget, float fDistance = 0.0, float fHeight = 50.0)
{
	float fClientPosition[3]; float fTargetPosition[3];

	GetEntPropVector(iClient, Prop_Send, "m_vecOrigin", fClientPosition);
	fClientPosition[2] += fHeight;

	GetClientEyePosition(iTarget, fTargetPosition);

	if (fDistance == 0.0 || GetVectorDistance(fClientPosition, fTargetPosition, false) < fDistance)
	{
		Handle hTrace = TR_TraceRayFilterEx(fClientPosition, fTargetPosition, MASK_SOLID_BRUSHONLY, RayType_EndPoint, Base_TraceFilter);

		if (TR_DidHit(hTrace))
		{
			delete hTrace;
			return false;
		}

		delete hTrace;
		return true;
	}

	return false;
}
public bool Base_TraceFilter(int iEntity, int iContentsMask, int iData)
{
	return iEntity == iData;
}
*/

/*
stock bool IsTargetInSightRange(int client, int target, float angle = 90.0, float distance = 0.0, bool heightcheck = true, bool negativeangle = false)
{
	if (angle > 360.0)
		angle = 360.0;

	if (angle < 0.0)
		return false;

	float clientpos[3];
	float targetpos[3];
	float anglevector[3];
	float targetvector[3];
	float resultangle;
	float resultdistance;

	GetClientEyeAngles(client, anglevector);
	anglevector[0] = anglevector[2] = 0.0;
	GetAngleVectors(anglevector, anglevector, NULL_VECTOR, NULL_VECTOR);
	NormalizeVector(anglevector, anglevector);
	if (negativeangle)
		NegateVector(anglevector);

	GetClientAbsOrigin(client, clientpos);
	GetClientAbsOrigin(target, targetpos);

	if (heightcheck && distance > 0)
		resultdistance = GetVectorDistance(clientpos, targetpos);

	clientpos[2] = targetpos[2] = 0.0;
	MakeVectorFromPoints(clientpos, targetpos, targetvector);
	NormalizeVector(targetvector, targetvector);

	resultangle = RadToDeg(ArcCosine(GetVectorDotProduct(targetvector, anglevector)));

	if (resultangle <= angle / 2)
	{
		if (distance > 0)
		{
			if (!heightcheck)
				resultdistance = GetVectorDistance(clientpos, targetpos);

			if (distance >= resultdistance)
				return true;
			else return false;
		}
		else return true;
	}

	return false;
}
*/

stock GetPrimaryAmmo(client) {
  new weapon = GetEntDataEnt2(client, FindSendPropInfo("CCSPlayer", "m_hActiveWeapon"));
  if (weapon < 1) return -1;
  return GetEntData(weapon, FindSendPropInfo("CBaseCombatWeapon", "m_iClip1"));
}

stock GetReserveAmmo(client) {
  new weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
  if (weapon < 1) return -1;

  new ammotype = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
  if (ammotype == -1) return -1;

  return GetEntProp(client, Prop_Send, "m_iAmmo", _, ammotype);
}
