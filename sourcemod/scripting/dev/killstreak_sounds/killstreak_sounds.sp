/*
TODO:

Failed to load sound "ut\payback.wav", file probably missing from disk/repository

- round_end (pre) / other event - stop kill sounds because it can block "Terrorists Win" sound
- menu to turn on/off sounds/text and save in cookie
- ? calc distance between victim and sound receiver (reduce sound volume or do not play if too far)
*/

#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>

public Plugin:myinfo = {
  name = "Killstreak Sounds (Unreal Tournament)",
  version = "2023.01.16",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

// do not change items in array due to index usage
new const String:SOUNDS[][] = {
  "ut99/headshot.wav",
  "ut2003/headshot.wav",
  "ut2003/hattrick.wav",
  "ut2003/headhunter.wav",

  // 4
  "ut99/firstblood.wav",
  "ut2003/firstblood.wav",

  // 6
  "ut99/doublekill.wav",
  "ut2003/doublekill.wav",
  "ut99/triplekill.wav",
  "ut99/multikill.wav",
  "ut2003/multikill.wav",
  "ut99/megakill.wav",
  "ut2003/megakill.wav",
  "ut99/ultrakill.wav",
  "ut2003/ultrakill.wav",
  "ut99/monsterkill.wav",
  "ut2003/monsterkill.wav",
  "ut2003/ludicrouskill.wav",
  "ut2003/holyshit.wav",

  // 19
  "ut99/killingspree.wav",
  "ut2003/killingspree.wav",
  "ut99/rampage.wav",
  "ut2003/rampage.wav",
  "ut99/dominating.wav",
  "ut2003/dominating.wav",
  "ut99/unstoppable.wav",
  "ut2003/unstoppable.wav",
  "ut99/godlike.wav",
  "ut2003/godlike.wav",
  "ut2003/wickedsick.wav",

  // 30
  "ut/teamkiller.wav",
  "ut/payback.wav",
  "ut/humiliation.wav",
  "ut/ownage.wav",
};

#define MAX_MESSAGES_ON_SCREEN 2

#define TRANSLATIONS_FILE "killstreak_sounds.phrases"

new g_totalKills = 0;
new g_consecutiveKills[MAXPLAYERS + 1];
new g_consecutiveHeadshots[MAXPLAYERS + 1];
new g_comboScore[MAXPLAYERS + 1];
new String:g_revengeKill[MAXPLAYERS + 1][MAX_NAME_LENGTH];
new g_teamScore[4];
new Float:g_lastKillTime[MAXPLAYERS + 1];
new Float:g_lastSoundTime[MAXPLAYERS + 1];

new Handle:hComboTime, Handle:hAnnounceDelay, Handle:hVolume, Handle:hOwnageScore;
new Handle:cvar_hudChannel, Handle:cvar_hudPositionX, Handle:cvar_hudPositionY, Handle:cvar_messageTime;
new Handle:cvar_sendToConsole, Handle:cvar_firstBloodMinPlayers;

public OnPluginStart() {
  LoadTranslations(TRANSLATIONS_FILE);

  cvar_hudChannel = CreateConVar("sm_killstreaksounds_hud_channel", "1", "", FCVAR_PROTECTED);
  cvar_hudPositionX = CreateConVar("sm_killstreaksounds_position_x", "-1.0", "", FCVAR_PROTECTED, true, -1.0, true, 1.0);
  cvar_hudPositionY = CreateConVar("sm_killstreaksounds_position_y", "0.17", "", FCVAR_PROTECTED, true, -1.0, true, 1.0);
  cvar_messageTime = CreateConVar("sm_killstreaksounds_message_time", "3", "", FCVAR_PROTECTED, true, 1.0);
  cvar_sendToConsole = CreateConVar("sm_killstreaksounds_send_to_console", "1", "", FCVAR_PROTECTED);
  cvar_firstBloodMinPlayers = CreateConVar("sm_killstreaksounds_firstblood_min_players", "4", "", FCVAR_PROTECTED);

  // official UT = 13.5s
  hComboTime = CreateConVar("sm_killstreaksounds_combo_time", "3.0", "Max time between combo kills", FCVAR_PROTECTED, true, 2.0);

  hAnnounceDelay = CreateConVar("sm_killstreaksounds_sound_delay", "2", "Delay between killstreak sounds for each player", FCVAR_PROTECTED, true, 1.0);
  hVolume = CreateConVar("sm_killstreaksounds_volume", "0.75", "Sound Volume: should be a number between 0.0 and 1.0", FCVAR_PROTECTED, true, 0.0, true, 1.0);
  hOwnageScore = CreateConVar("sm_killstreaksounds_ownage_score", "10", "When a team gets X or more kills without anyone in the team dying in the process", FCVAR_PROTECTED, true, 1.0);

  HookEvent("round_start", Event_RoundStart, EventHookMode_PostNoCopy);
  HookEvent("player_death", Event_PlayerDeath);
}

public OnMapStart() {
  LoadTranslations("killstreak_sounds");

  new String:buffer[PLATFORM_MAX_PATH];

  for (new i = 0, len = sizeof(SOUNDS); i < len; i++) {
    PrecacheSound(SOUNDS[i], true);
    Format(buffer, sizeof(buffer), "sound/%s", SOUNDS[i]);
    AddFileToDownloadsTable(buffer);
  }

  OnNewRound();
}

public Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast) {
  OnNewRound();
}

OnNewRound() {
  g_totalKills = 0;

  for (new i = 1; i <= MaxClients; i++) {
    ResetPlayerData(i);
  }
  for (new i = 0; i < sizeof(g_teamScore); i++) {
    g_teamScore[i] = 0;
  }
}

public OnClientPostAdminCheck(client) {
  ResetPlayerData(client);
}

ResetPlayerData(client) {
  g_consecutiveKills[client] = 0;
  g_consecutiveHeadshots[client] = 0;
  g_lastKillTime[client] = 0.0;
  g_revengeKill[client] = "";
  g_lastSoundTime[client] = 0.0;
}

public Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new victimClient = GetClientOfUserId(GetEventInt(event, "userid"));
  if (!IsValidClient(victimClient)) {
    return;
  }

  new String:victimName[MAX_NAME_LENGTH];
  GetClientName(victimClient, victimName, MAX_NAME_LENGTH);
  new victimTeam = GetClientTeam(victimClient);

  new attackerClient = GetClientOfUserId(GetEventInt(event, "attacker"));

  new String:attackerName[MAX_NAME_LENGTH];
  new attackerTeam;
  if (IsValidClient(attackerClient)) {
    GetClientName(attackerClient, attackerName, MAX_NAME_LENGTH);
    attackerTeam = GetClientTeam(attackerClient);
  }

  g_teamScore[victimTeam] = 0;
  new bool:resetAttackerTeamScore;

  // suicide
  if (!attackerClient || attackerClient == victimClient) {
    KillStreakToAll(32, "selfkill", attackerName);
  }
  // team kill
  else if (attackerTeam == victimTeam) {
    KillStreakToAll(30, "teamkill", attackerName, victimName);
  }
  // normal kill
  else if (IsValidClient(attackerClient)) {
    new String:weapon[64];
    GetEventString(event, "weapon", weapon, sizeof(weapon));
    new bool:isKnifeKill = StrEqual(weapon, "knife");
    new bool:isHeadshot = GetEventBool(event, "headshot");

    g_totalKills++;

    new bool:isFirstBlood = g_totalKills == 1;

    g_consecutiveKills[attackerClient]++;
    new consecutiveKills = g_consecutiveKills[attackerClient];

    new comboKills = CalculateComboKills(attackerClient);

    if (isHeadshot) {
      g_consecutiveHeadshots[attackerClient]++;
    }
    new headshotKills = g_consecutiveHeadshots[attackerClient];

    g_teamScore[attackerTeam]++;

    g_revengeKill[victimClient] = attackerName;
    new bool:isRevengeKill = StrEqual(victimName, g_revengeKill[attackerClient], false);

    if (isRevengeKill) {
      KillStreak(attackerClient, 31, "revengekiller", victimName);
      KillStreak(victimClient, -1, "revengevictim", attackerName);
      g_revengeKill[attackerClient] = "";
    } else if (isKnifeKill) {
      KillStreak(victimClient, 32); // humiliation (no message)
    }

    new countPlayers = GetClientCount(true);
    new minPlayers = GetConVarInt(cvar_firstBloodMinPlayers);

    if (isFirstBlood && (minPlayers == 0 || countPlayers >= minPlayers)) {
      KillStreakToAll(GetRandomInt(4, 5), "firstblood", attackerName);
    } else if (comboKills >= 2) {
      if (comboKills == 2) {
        KillStreakToAll(GetRandomInt(6, 7), "doublekill", attackerName);
      } else if (comboKills == 3) {
        KillStreakToAll(GetRandomInt(8, 10), "triplekill", attackerName);
      } else if (comboKills == 4) {
        KillStreakToAll(GetRandomInt(11, 12), "megakill", attackerName);
      } else if (comboKills == 5) {
        KillStreakToAll(GetRandomInt(13, 14), "ultrakill", attackerName);
      } else if (comboKills == 6) {
        KillStreakToAll(GetRandomInt(15, 16), "monsterkill", attackerName);
      } else if (comboKills == 7) {
        KillStreakToAll(17, "ludicrouskill", attackerName);
      } else if (comboKills >= 8) {
        KillStreakToAll(18, "holyshit", attackerName);
      }
    } else if (consecutiveKills == 5
    || consecutiveKills == 10
    || consecutiveKills == 15
    || consecutiveKills == 20
    || consecutiveKills == 25
    || consecutiveKills == 30) {
      if (consecutiveKills == 5) {
        KillStreakToAll(GetRandomInt(19, 20), "killingspree", attackerName);
      } else if (consecutiveKills == 10) {
        KillStreakToAll(GetRandomInt(20, 21), "rampage", attackerName);
      } else if (consecutiveKills == 15) {
        KillStreakToAll(GetRandomInt(22, 23), "dominating", attackerName);
      } else if (consecutiveKills == 20) {
        KillStreakToAll(GetRandomInt(24, 25), "unstoppable", attackerName);
      } else if (consecutiveKills == 25) {
        KillStreakToAll(GetRandomInt(26, 27), "godlike", attackerName);
      } else if (consecutiveKills == 30) {
        KillStreakToAll(29, "wickedsick", attackerName);
      }
    } else if (headshotKills >= 1) {
      if (headshotKills == 1) {
        KillStreak(attackerClient, GetRandomInt(0, 1)); // headshot
      } else if (headshotKills == 3) {
        KillStreak(attackerClient, 2); // hattrick
      } else if (headshotKills == 5) {
        KillStreak(attackerClient, 3); // head hunter
      }
    } else if (g_teamScore[attackerTeam] >= GetConVarInt(hOwnageScore)) {
      resetAttackerTeamScore = true;
      new String:buf[32];
      TeamIndexToName(attackerTeam, buf, sizeof(buf));
      KillStreakToAll(33, "teamowning", buf);
    }
  }

  if (resetAttackerTeamScore) {
    g_teamScore[attackerTeam] = 0;
  }

  g_consecutiveKills[victimClient] = 0;
  g_consecutiveHeadshots[victimClient] = 0;
}

CalculateComboKills(attackerClient) {
  if (g_lastKillTime[attackerClient] == 0.0 || (GetGameTime() - g_lastKillTime[attackerClient] > GetConVarFloat(hComboTime)))
  {
    g_comboScore[attackerClient] = 1;
  }
  else
  {
    g_comboScore[attackerClient]++;
  }

  g_lastKillTime[attackerClient] = GetGameTime();

  return g_comboScore[attackerClient];
}

KillStreakToAll(soundIndex = -1, const String:translationString[] = "", const String:attackerName[] = "", const String:victimName[] = "") {
  for (new i = 1; i <= MaxClients; i++) {
    KillStreak(i, soundIndex, translationString, attackerName, victimName);
  }
}

KillStreak(client, soundIndex = -1, const String:translationString[] = "", const String:attackerName[] = "", const String:victimName[] = "") {
  if (!IsValidClient(client) || IsFakeClient(client)) {
    return;
  }

  if (strlen(translationString) > 0) {
    new String:message[256];
    Format(message, sizeof(message), "%T", translationString, client, attackerName, victimName);

    SetHudTextParams(
      GetConVarFloat(cvar_hudPositionX),
      GetConVarFloat(cvar_hudPositionY),
      GetConVarFloat(cvar_messageTime),
      GetRandomInt(0, 255),
      GetRandomInt(0, 255),
      GetRandomInt(0, 255),
      255
    );
    ShowHudText(client, GetConVarInt(cvar_hudChannel), message);

    if (GetConVarBool(cvar_sendToConsole)) {
      PrintToConsole(client, message);
    }
  }

  new bool:skipSound = g_lastSoundTime[client] > 0.0 && GetGameTime() - g_lastSoundTime[client] < GetConVarFloat(hAnnounceDelay);

  if (!skipSound && soundIndex > -1) {
    EmitSoundToClientEx(client, SOUNDS[soundIndex]);
    g_lastSoundTime[client] = GetGameTime();
  }
}

stock TeamIndexToName(index, String:outName[], len) {
  if (index == CS_TEAM_T) {
    Format(outName, len, "T");
  } else if (index == CS_TEAM_CT) {
    Format(outName, len, "CT");
  }
}

stock EmitSoundToClientEx(client, const String:sound[]) {
  new pitch = GetRandomInt(92, 102);
  EmitSoundToClient(client, sound, _, _, _, _, GetConVarFloat(hVolume), pitch);
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
