#pragma semicolon 1
#include <sourcemod>

public Plugin:myinfo = {
  name = "Reconnect Restore",
  version = "2023.01.21",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define STEAM_ID_LENGTH 17

new Handle:g_reconnectPlayers = INVALID_HANDLE;
new Handle:cvar_keepPlayerDataTime;

public OnPluginStart() {
  g_reconnectPlayers = CreateTrie();

  cvar_keepPlayerDataTime = CreateConVar("sm_reconnect_restore_keep_player_data_time", "300", "Player can restore data within specified period of time (seconds)", FCVAR_PROTECTED, true, 0.0);
}

public OnMapStart() {
  if (g_reconnectPlayers != INVALID_HANDLE) {
    ClearTrie(g_reconnectPlayers);
  }
}

public OnClientDisconnect(client) {
  new String:steamid[MAX_NAME_LENGTH];
  if (IsFakeClient(client)) {
    GetClientName(client, steamid, sizeof(steamid));
  } else {
    GetClientAuthId(client, AuthId_SteamID64, steamid, sizeof(steamid));
  }

  new Handle:datapack = CreateDataPack();
  WritePackFloat(datapack, GetGameTime());
  WritePackCell(datapack, GetDeaths(client));
  WritePackCell(datapack, GetFrags(client));
  WritePackCell(datapack, GetMoney(client));

  SetTrieValue(g_reconnectPlayers, steamid, datapack, true);
}

public OnClientPostAdminCheck(client) {
  if (!IsFakeClient(client)) {
    RestorePlayer(client);
  }
}

RestorePlayer(client) {
  CreateTimer(0.5, RestoreTimer, client);
}

public OnClientPutInServer(client) {
  if (!IsFakeClient(client)) {
    new playersNum = 0;
    new botsNum = 0;

    for (new i = 1; i <= MaxClients; i++) {
      if (IsClientConnected(i)) {
        if (IsFakeClient(i)) {
          botsNum++;
        } else {
          playersNum++;
        }
      }
    }

    // restore bots after hibernate
    if (playersNum == 1 && botsNum > 0) {
      for (new i = 1; i <= MaxClients; i++) {
        if (IsClientConnected(i) && IsFakeClient(i)) {
          RestorePlayer(i);
        }
      }
    }
  }
}

Action:RestoreTimer(Handle:timer, any:client) {
  if (!IsValidClient(client)) {
    return;
  }

  new String:steamid[MAX_NAME_LENGTH];
  if (IsFakeClient(client)) {
    GetClientName(client, steamid, sizeof(steamid));
  } else {
    GetClientAuthId(client, AuthId_SteamID64, steamid, sizeof(steamid));
  }

  new Handle:playerData = INVALID_HANDLE;
  if (!GetTrieValue(g_reconnectPlayers, steamid, playerData)) {
    return;
  }

  if (playerData == INVALID_HANDLE) {
    return;
  }

  ResetPack(playerData);
  new Float:disconnectTime = ReadPackFloat(playerData);

  if (disconnectTime > 0 && GetGameTime() - disconnectTime > GetConVarFloat(cvar_keepPlayerDataTime)) {
    delete playerData;
    RemoveFromTrie(g_reconnectPlayers, steamid);
    return;
  }

  SetDeaths(client, ReadPackCell(playerData));
  SetFrags(client, ReadPackCell(playerData));
  SetMoney(client, ReadPackCell(playerData));

  delete playerData;
  RemoveFromTrie(g_reconnectPlayers, steamid);
}

stock GetDeaths(client) {
  return GetEntProp(client, Prop_Data, "m_iDeaths");
}

stock SetDeaths(client, value) {
  SetEntProp(client, Prop_Data, "m_iDeaths", value);
}

stock GetFrags(client) {
  return GetClientFrags(client);
}

stock SetFrags(client, value) {
  SetEntProp(client, Prop_Data, "m_iFrags", value);
}

stock GetMoney(client) {
  return GetEntProp(client, Prop_Send, "m_iAccount");
}

stock SetMoney(client, value) {
  SetEntProp(client, Prop_Send, "m_iAccount", value);
}

stock bool:IsNumeric(const String:str[]) {
	for (new i = 0; i < strlen(str); i++)
		if (!IsCharNumeric(str[i]) && str[i])
			return false;
	return true;
}

stock bool:IsValidSteamId(const String:steamid[]) {
	return strlen(steamid) == STEAM_ID_LENGTH && IsNumeric(steamid);
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
