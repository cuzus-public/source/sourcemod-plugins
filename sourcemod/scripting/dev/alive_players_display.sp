#pragma semicolon 1
#include <sourcemod>
#include <cstrike>

public Plugin:myinfo = {
  name = "Alive players display",
  version = "2023.01.14",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

new Handle:cvar_ctHudChannel, Handle:cvar_tHudChannel;
new Handle:cvar_ctTeamColor, Handle:cvar_tTeamColor;
new g_alivePlayersCT, g_alivePlayersT;

public OnPluginStart() {
  cvar_ctHudChannel = CreateConVar("sm_round_stats_ct_hud_channel", "3", "", FCVAR_PROTECTED);
  cvar_tHudChannel = CreateConVar("sm_round_stats_t_hud_channel", "2", "", FCVAR_PROTECTED);
  cvar_ctTeamColor = CreateConVar("sm_round_stats_ct_team_color", "50 50 255", "", FCVAR_PROTECTED);
  cvar_tTeamColor = CreateConVar("sm_round_stats_t_team_color", "255 50 50", "", FCVAR_PROTECTED);

  HookEvent("player_death", Event_PlayerDeath);
  HookEvent("player_spawn", Event_PlayerSpawn);
  HookEvent("round_start", Event_RoundStart, EventHookMode_PostNoCopy);
}

CalcAlivePlayers() {
  g_alivePlayersCT = 0;
  g_alivePlayersT = 0;

  for (new p = 1; p <= MaxClients; p++) {
    if (IsValidClient(p) && IsPlayerAlive(p)) {
      new team = GetClientTeam(p);
      if (team == CS_TEAM_CT) {
        g_alivePlayersCT++;
      } else if (team == CS_TEAM_T) {
        g_alivePlayersT++;
      }
    }
  }
}

public Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast) {
  CreateTimer(0.2, RoundStartTimer);
}

public Action:RoundStartTimer(Handle:timer, any:data) {
  CalcAlivePlayers();
  UpdateHudTextAll();
}

public Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  CalcAlivePlayers();
  UpdateHudTextAll();
}

public Event_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast) {
  CalcAlivePlayers();
  UpdateHudTextAll();
}

UpdateHudTextAll() {
  for (new p = 1; p <= MaxClients; p++) {
    if (IsValidClient(p) && !IsFakeClient(p)) {
      UpdateHudText(p);
    }
  }
}

UpdateHudText(client) {
  if (!IsValidClient(client) || IsFakeClient(client)) {
    return;
  }

  new Float:basicX = 0.455;
  new Float:X = basicX;
  new Float:Y = 0.004;
  new Float:holdTime = 999999999.0;
  new Float:symbolOffset = 0.0104;
  new Float:distanceBetween = 0.055;

  new String:text[128];

  if (g_alivePlayersCT > 0) {
    Format(text, sizeof(text), "[");
    for (new i = 0; i < g_alivePlayersCT; i++) {
      Format(text, sizeof(text), "%s■", text);
      X -= symbolOffset;
    }
    Format(text, sizeof(text), "%s] CT", text);
  } else if (g_alivePlayersCT == 0) {
    Format(text, sizeof(text), "× CT");
  }

  new String:colors[4][4];
  new String:buf[128];
  GetConVarString(cvar_ctTeamColor, buf, sizeof(buf));
  ExplodeString(buf, " ", colors, 4, 4);

  SetHudTextParams(
    X,
    Y,
    holdTime,
    StringToInt(colors[0]),
    StringToInt(colors[1]),
    StringToInt(colors[2]),
    255
  );
  ShowHudText(client, GetConVarInt(cvar_ctHudChannel), text);

  X = basicX + distanceBetween;

  if (g_alivePlayersT > 0) {
    Format(text, sizeof(text), "T [", text);
    for (new i = 0; i < g_alivePlayersT; i++) {
      Format(text, sizeof(text), "%s■", text);
    }
    Format(text, sizeof(text), "%s]", text);
  } else if (g_alivePlayersT == 0) {
    Format(text, sizeof(text), "T ×");
  }

  GetConVarString(cvar_tTeamColor, buf, sizeof(buf));
  ExplodeString(buf, " ", colors, 4, 4);

  SetHudTextParams(
    X,
    Y,
    holdTime,
    StringToInt(colors[0]),
    StringToInt(colors[1]),
    StringToInt(colors[2]),
    255
  );
  ShowHudText(client, GetConVarInt(cvar_tHudChannel), text);
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
