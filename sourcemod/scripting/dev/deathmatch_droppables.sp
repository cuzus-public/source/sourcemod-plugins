#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "Deathmatch Droppables",
  version = "2023.01.19",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define FFADE_IN		0x0001  // Just here so we don't pass 0 into the function
#define FFADE_PURGE 0x0010  // Purges all other fades, replacing them with this one

new String:SOUND_FILES[][] = {
  "items/smallmedkit1.wav",
};

new String:MODEL_FILES[][] = {
  "models/items/healthkit.mdl",
	"models/items/healthkit.dx80.vtx",
	"models/items/healthkit.dx90.vtx",
	"models/items/healthkit.phy",
	"models/items/healthkit.sw.vtx",
	"models/items/healthkit.vvd",

	"materials/models/items/healthkit01.vmt",
	"materials/models/items/healthkit01.vtf",
	"materials/models/items/healthkit01_mask.vtf"
};

new g_damageBeforeDeath[MAXPLAYERS + 1];

new Handle:cvar_lifeTime, Handle:cvar_healthKitScale, Handle:cvar_damageToHpMult, Handle:cvar_restoreHealthMin;

public OnPluginStart() {
  cvar_damageToHpMult = CreateConVar("dm_droppables_damage_to_health_mult", "3.0", "Health restoration multiplier", FCVAR_PROTECTED, true, 0.1);
  cvar_restoreHealthMin = CreateConVar("dm_droppables_restore_health_min", "25", "Min. health to restore by healthkit", FCVAR_PROTECTED, true, 0.0);
  cvar_lifeTime = CreateConVar("dm_droppables_lifetime", "10.0", "How long healthkit stay", FCVAR_PROTECTED, true, 1.0);
  cvar_healthKitScale = CreateConVar("dm_droppables_healthkit_scale", "0.75", "Physical size of healthkit", FCVAR_PROTECTED, true, 0.1, true, 10.0);

  // late load support
  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientInGame(i)) {
      OnClientPutInServer(i);
    }
  }
}

public OnClientPutInServer(client) {
  g_damageBeforeDeath[client] = 0;
  SDKHook(client, SDKHook_OnTakeDamageAlive, OnTakeDamageAlive);
}

public OnMapStart() {
  new String:buf[PLATFORM_MAX_PATH];

  for (new i = 0, len = sizeof(SOUND_FILES); i < len; i++) {
    PrecacheSound(SOUND_FILES[i], true);
    Format(buf, sizeof(buf), "sound/%s", SOUND_FILES[i]);
    AddFileToDownloadsTable(buf);
  }

  for (new i = 0, len = sizeof(MODEL_FILES); i < len; i++) {
    if (FileExists(MODEL_FILES[i], false)) {
      PrecacheModel(MODEL_FILES[i], true);
      AddFileToDownloadsTable(MODEL_FILES[i]);
    }
  }
}

public Action:OnTakeDamageAlive(victim, &attacker, &inflictor, &Float:damage, &damagetype) {
  if (!IsValidClient(victim)) {
    return Plugin_Continue;
  }

  if (damage >= GetClientHealth(victim)) {
    g_damageBeforeDeath[victim] = RoundFloat(damage);
    DropHealthKit(victim);
  }

  return Plugin_Continue;
}

DropHealthKit(client) {
  new ent;

  // https://developer.valvesoftware.com/wiki/Prop_physics_multiplayer
  // https://developer.valvesoftware.com/wiki/Prop_dynamic_override
  if ((ent = CreateEntityByName("prop_physics_multiplayer")) != -1) {
		new Float:pos[3], Float:vel[3], String:targetname[100];

		GetClientEyePosition(client, pos);

		vel[0] = GetRandomFloat(-200.0, 200.0);
		vel[1] = GetRandomFloat(-200.0, 200.0);
		vel[2] = GetRandomFloat(100.0, 200.0);

		Format(targetname, sizeof(targetname), "healthkit_%i", ent);

		DispatchKeyValue(ent, "model", MODEL_FILES[0]);
		DispatchKeyValue(ent, "physicsmode", "2"); // Non-Solid, Server-side
		DispatchKeyValue(ent, "massScale", "1.0"); // A scale multiplier for the object's mass.
		DispatchKeyValue(ent, "targetname", targetname); // The name that other entities refer to this entity by.
		DispatchSpawn(ent);

    SetEntPropFloat(ent, Prop_Send, "m_flModelScale", GetConVarFloat(cvar_healthKitScale));

    TeleportEntity(ent, pos, NULL_VECTOR, vel); // Teleport kit and throw

    SetEntProp(ent, Prop_Send, "m_CollisionGroup", 1); // No block health kits

    new Float:lifeTime = GetConVarFloat(cvar_lifeTime);

    Format(targetname, sizeof(targetname), "OnUser1 !self:kill::%0.2f:-1", lifeTime);
    SetVariantString(targetname);
    AcceptEntityInput(ent, "AddOutput");
    AcceptEntityInput(ent, "FireUser1");

    SetEntProp(ent, Prop_Send, "m_usSolidFlags", 8); // FSOLID_TRIGGER = 0x0008,
    // This is something may be collideable but fires touch functions
		// even when it's not collideable (when the FSOLID_NOT_SOLID flag is set)

    SetEntityRenderFx(ent, RENDERFX_PULSE_FAST);
    SetEntityRenderColor(ent, 20, 255, 20, 200);

    SDKHook(ent, SDKHook_StartTouchPost, Ent_StartTouchPost);
    SDKHook(ent, SDKHook_OnTakeDamage, Ent_OnTakeDamage);
	}
}

public Ent_StartTouchPost(entity, other) {
	if (!IsValidClient(other)) {
    return;
	}

  new health = GetEntProp(other, Prop_Send, "m_iHealth");
  new maxHealth = GetEntProp(other, Prop_Data, "m_iMaxHealth");

  if (health >= maxHealth) {
    return;
  }

  new addHp = RoundFloat(g_damageBeforeDeath[other] * GetConVarFloat(cvar_damageToHpMult));
  new restoreMin = GetConVarInt(cvar_restoreHealthMin);
  if (addHp < restoreMin) {
    addHp = restoreMin;
  }

  if (health + addHp > maxHealth) {
    health = maxHealth;
  } else {
    health += addHp;
  }

  SetEntProp(other, Prop_Send, "m_iHealth", health);

  EmitSoundToAll(SOUND_FILES[0], other, SNDCHAN_ITEM, _, _, 0.4);

  AcceptEntityInput(entity, "Kill");

  Client_ScreenFade(other, 120, FFADE_PURGE|FFADE_IN, 0, 255, 217, 102, 75);

  g_damageBeforeDeath[other] = 0;
}

public Action:Ent_OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype) {
	if (damage > 0 && IsValidEntity(victim) && IsValidEdict(victim) && IsValidClient(attacker)) {
    SetEntityRenderFx(victim, RENDERFX_STROBE_FASTER);
    SetEntityRenderColor(victim, 255, 40, 40, 200);
    CreateTimer(1.5, DestroyEntityTimer, victim);
	}
}

public Action:DestroyEntityTimer(Handle:timer, any:entity) {
  if (IsValidEntity(entity) && IsValidEdict(entity)) {
    AcceptEntityInput(entity, "Kill");
  }
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}

// SMLib
stock bool:Client_ScreenFade(client, duration, mode, holdtime=-1, r=0, g=0, b=0, a=255, bool:reliable=true) {
	new Handle:userMessage = StartMessageOne("Fade", client, (reliable?USERMSG_RELIABLE:0));
	if (userMessage == INVALID_HANDLE) {
		return false;
	}

	BfWriteShort(userMessage,	duration);	// Fade duration
	BfWriteShort(userMessage,	holdtime);	// Fade hold time
	BfWriteShort(userMessage,	mode);		// What to do
	BfWriteByte(userMessage, r);			// Color R
	BfWriteByte(userMessage, g);			// Color G
	BfWriteByte(userMessage, b);			// Color B
	BfWriteByte(userMessage, a);			// Color Alpha
	EndMessage();

	return true;
}
