#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

public Plugin:myinfo = {
  name = "Bullet Sounds",
  version = "2022.12.18",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define RADRIGHTANGLE (FLOAT_PI / 2.0) // this value will be genarated on runtime, so it can hope always work correctly
#define SOUND_PATH_LEN 64

new const String:DEFAULT_NEARMISS_SOUNDS[][] = {
  "weapons/fx/nearmiss/bulletltor03.wav",
  "weapons/fx/nearmiss/bulletltor04.wav",
  "weapons/fx/nearmiss/bulletltor05.wav",
  "weapons/fx/nearmiss/bulletltor06.wav",
  "weapons/fx/nearmiss/bulletltor07.wav",
};
new const String:SHOTGUNS_NEARMISS_SOUNDS[][] = {
  "weapons/fx/nearmiss/bulletltor10.wav",
  "weapons/fx/nearmiss/bulletltor13.wav",
  "weapons/fx/nearmiss/bulletltor14.wav",
};
new const String:AWP_NEARMISS_SOUNDS[][] = {
  "weapons/fx/nearmiss/bulletltor09.wav",
  "weapons/fx/nearmiss/bulletltor11.wav",
  "weapons/fx/nearmiss/bulletltor12.wav",
};

new Float:g_fClientDelay[MAXPLAYERS + 1];
new Float:lasthitposition[MAXPLAYERS + 1][3];
new bool:hitpositionqueue[MAXPLAYERS + 1] = {false, ...};

new Handle:hDelay, Handle:hNearmissDistance, Handle:hNearmissAdjust, Handle:hMinFlightDistance;
new Float:fDelay, Float:fNearmissDistance, Float:fNearmissAdjust, Float:fMinFlightDistance;

public OnPluginStart() {
  hDelay = CreateConVar("sm_bullet_sounds_delay", "0.5", "Delay between multiple bullet sounds", FCVAR_PROTECTED, true, 0.1);
  fDelay = GetConVarFloat(hDelay);
  HookConVarChange(hDelay, OnConVarChanges);

  hNearmissDistance = CreateConVar("sm_bullet_sounds_nearmiss_distance", "64.0", "Distance to client from bullet line that let you hear the nearmiss sound", FCVAR_PROTECTED);
  fNearmissDistance = GetConVarFloat(hNearmissDistance);
  HookConVarChange(hNearmissDistance, OnConVarChanges);

  hNearmissAdjust = CreateConVar("sm_bullet_sounds_nearmiss_adjust", "16.0", "Positive value will make clients hear nearmiss sound even bullet didnt passed away the client; negative value will force the bullet to pass away client at least absolute value of this value", FCVAR_PROTECTED);
  fNearmissAdjust = GetConVarFloat(hNearmissAdjust);
  HookConVarChange(hNearmissAdjust, OnConVarChanges);

  hMinFlightDistance = CreateConVar("sm_bullet_sounds_min_flight_distance", "256.0", "Bullet will make nearmiss sound if bullet has flied longer than this", FCVAR_PROTECTED);
  fMinFlightDistance = GetConVarFloat(hMinFlightDistance);
  HookConVarChange(hMinFlightDistance, OnConVarChanges);

  HookEvent("bullet_impact", Event_BulletImpact);
}

public OnConVarChanges(Handle:convar, const String:oldVal[], const String:newVal[]) {
  if (convar == hDelay) {
    fDelay = StringToFloat(newVal);
  } else if (convar == hNearmissDistance) {
    fNearmissDistance = StringToFloat(newVal);
  } else if (convar == hNearmissAdjust) {
    fNearmissAdjust = StringToFloat(newVal);
  } else if (convar == hMinFlightDistance) {
    fMinFlightDistance = StringToFloat(newVal);
  }
}

public OnMapStart() {
  // this config file doesn`t automatically add files to download table. you need to do that with another plugin
  for (new i = 0; i < sizeof(DEFAULT_NEARMISS_SOUNDS); i++) {
    PrecacheSound(DEFAULT_NEARMISS_SOUNDS[i], true);
  }
  for (new i = 0; i < sizeof(SHOTGUNS_NEARMISS_SOUNDS); i++) {
    PrecacheSound(SHOTGUNS_NEARMISS_SOUNDS[i], true);
  }
  for (new i = 0; i < sizeof(AWP_NEARMISS_SOUNDS); i++) {
    PrecacheSound(AWP_NEARMISS_SOUNDS[i], true);
  }
}

public OnClientPutInServer(client) {
	hitpositionqueue[client] = false;
	// weapon's fire is handled in ItemPostFrame (i expect it is same in css), and its called by player entity's postthink
	SDKHook(client, SDKHook_PostThinkPost, PostThinkPostHook);
}

public OnClientDisconnect_Post(client) {
	g_fClientDelay[client] = 0.0;
}

public PostThinkPostHook(client) {
	if (hitpositionqueue[client]) {
		hitpositionqueue[client] = false;
		MakeBulletSound(client);
	}
}

public Event_BulletImpact(Handle:event, const String:name[], bool:dontBroadcast) {
	new client = GetClientOfUserId(GetEventInt(event, "userid"));

  new Float:gameTime = GetGameTime();
  if (g_fClientDelay[client] > gameTime) {
    return;
  }

  lasthitposition[client][0] = GetEventFloat(event, "x");
  lasthitposition[client][1] = GetEventFloat(event, "y");
  lasthitposition[client][2] = GetEventFloat(event, "z");

  new weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
  new String:g_szWeapon[32];
  if (weapon != -1) {
    GetEdictClassname(weapon, g_szWeapon, sizeof(g_szWeapon));
  }
  if (StrEqual(g_szWeapon, "weapon_xm1014", false) || StrEqual(g_szWeapon, "weapon_m3", false)) {
    MakeBulletSound(client);
  } else {
    hitpositionqueue[client] = true;
  }

  g_fClientDelay[client] = gameTime + fDelay;
}

MakeBulletSound(client) {
  new Float:clientEyePos[3], Float:clientBulletPos[3];
  new Float:targetEyePos[3], Float:targetBulletPos[3];
  new Float:clientToBulletDistance;
  new Float:bulletLineLength, Float:hearDistance;
  new Float:targetBulletAngle, Float:clientToTargetDistance;
  new weapon;
  new String:weaponName[32];

  weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
  if (weapon != -1) {
    GetEdictClassname(weapon, weaponName, sizeof(weaponName));
  }

  GetClientEyePosition(client, clientEyePos);
  MakeVectorFromPoints(clientEyePos, lasthitposition[client], clientBulletPos);
  clientToBulletDistance = GetVectorDistance(clientEyePos, lasthitposition[client]);
  NormalizeVector(clientBulletPos, clientBulletPos);

  for (new target = 1; target <= MaxClients; target++) {
    if (target == client || !IsClientInGame(target) || IsFakeClient(target) || !IsPlayerAlive(target)) {
      continue;
    }

    GetClientEyePosition(target, targetEyePos);
    MakeVectorFromPoints(clientEyePos, targetEyePos, targetBulletPos);
    NormalizeVector(targetBulletPos, targetBulletPos);

    targetBulletAngle = ArcCosine(GetVectorDotProduct(targetBulletPos, clientBulletPos)); // this is radian angle
    new bool:canHearFromAngle = targetBulletAngle < RADRIGHTANGLE;

    if (!canHearFromAngle) {
      continue;
    }

    clientToTargetDistance = GetVectorDistance(clientEyePos, targetEyePos);
    hearDistance = Sine(targetBulletAngle) * clientToTargetDistance;
    bulletLineLength = Cosine(targetBulletAngle) * clientToTargetDistance;

    if (hearDistance > fNearmissDistance
    || bulletLineLength > clientToBulletDistance + fNearmissAdjust
    || clientToBulletDistance < fMinFlightDistance) {
      continue;
    }

    new String:randomSound[SOUND_PATH_LEN];

    if (StrEqual(weaponName, "weapon_awp", false)) {
      Format(randomSound, sizeof(randomSound), "%s", AWP_NEARMISS_SOUNDS[GetRandomInt(0, sizeof(AWP_NEARMISS_SOUNDS) - 1)]);
    } else if (StrEqual(weaponName, "weapon_glock", false)
    || StrEqual(weaponName, "weapon_usp", false)
    || StrEqual(weaponName, "weapon_p228", false)
    || StrEqual(weaponName, "weapon_elite", false)
    || StrEqual(weaponName, "weapon_fiveseven", false)
    || StrEqual(weaponName, "weapon_mac10", false)
    || StrEqual(weaponName, "weapon_mp5navy", false)
    || StrEqual(weaponName, "weapon_p90", false)
    || StrEqual(weaponName, "weapon_tmp", false)
    || StrEqual(weaponName, "weapon_ump45", false)
    || StrEqual(weaponName, "weapon_m3", false)
    || StrEqual(weaponName, "weapon_xm1014", false)) {
      Format(randomSound, sizeof(randomSound), "%s", SHOTGUNS_NEARMISS_SOUNDS[GetRandomInt(0, sizeof(SHOTGUNS_NEARMISS_SOUNDS) - 1)]);
    } else if (!StrEqual(weaponName, "weapon_knife", false) && strlen(weaponName) > 0) {
      Format(randomSound, sizeof(randomSound), "%s", DEFAULT_NEARMISS_SOUNDS[GetRandomInt(0, sizeof(DEFAULT_NEARMISS_SOUNDS) - 1)]);
    }

    EmitSoundToClient(target, randomSound, 0, SNDCHAN_STATIC, 140, SND_NOFLAGS, 0.7, GetRandomInt(95, 105), -1, clientEyePos, clientBulletPos, false);
  }
}
