#pragma semicolon 1
#include <sourcemod>
#include <cstrike>

public Plugin:myinfo = {
  name = "Deathmatch Frags Display",
  version = "2023.01.16",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

new Handle:cvar_fragsToWin;
new Handle:cvar_ctHudChannel, Handle:cvar_tHudChannel;
new Handle:cvar_ctTeamColor, Handle:cvar_tTeamColor, Handle:cvar_hudTextHoldTime;

new g_score_CT, g_score_T;

public OnPluginStart() {
  cvar_tHudChannel = CreateConVar("sm_dmfrags_t_hud_channel", "2", "", FCVAR_PROTECTED);
  cvar_ctHudChannel = CreateConVar("sm_dmfrags_ct_hud_channel", "3", "", FCVAR_PROTECTED);
  cvar_ctTeamColor = CreateConVar("sm_dmfrags_ct_team_color", "50 50 255", "", FCVAR_PROTECTED);
  cvar_tTeamColor = CreateConVar("sm_dmfrags_t_team_color", "255 50 50", "", FCVAR_PROTECTED);
  cvar_hudTextHoldTime = CreateConVar("sm_dmfrags_hudTextHoldTime", "1.5", "", FCVAR_PROTECTED, true, 0.1);
  cvar_fragsToWin = CreateConVar("sm_dmfrags_frags_to_win", "300", "", FCVAR_PROTECTED);

  HookEvent("player_death", Event_PlayerDeath);
}

public OnMapStart() {
  g_score_CT = 0;
  g_score_T = 0;

  CreateTimer(GetConVarFloat(cvar_hudTextHoldTime), UpdateTeamScoreTimer, _, TIMER_REPEAT);
}

public Action:UpdateTeamScoreTimer(Handle:timer, any:data) {
  UpdateHudText();
}

UpdateTeamScore(killer) {
  if (!IsValidClient(killer)) {
    return;
  }

  new team = GetClientTeam(killer);
  if (team == CS_TEAM_CT) {
    g_score_CT++;
  } else if (team == CS_TEAM_T) {
    g_score_T++;
  }

  // TODO: check if max score reached and end game (via native?)
  // disable display on team win
}

public Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new victim = GetClientOfUserId(GetEventInt(event, "userid"));
  new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
  if (victim != attacker) {
    UpdateTeamScore(attacker);
  }
}

UpdateHudText(client = 0) {
  new Float:basicX = 0.487;
  new Float:X = basicX;
  new Float:Y = 0.004;
  new Float:holdTime = GetConVarFloat(cvar_hudTextHoldTime);
  new Float:symbolOffset = 0.0104;
  new Float:distanceBetween = 0.025;

  new String:text[128];

  new winFrags = GetConVarInt(cvar_fragsToWin);
  new ctPerc = g_score_CT > 0 ? RoundToCeil(g_score_CT * 100.0 / winFrags) : 0;
  new tPerc = g_score_T > 0 ? RoundToCeil(g_score_T * 100.0 / winFrags) : 0;

  for (new i = 1; i <= 10; i++) {
    if (ctPerc / (10 * i) >= 1) {
      Format(text, sizeof(text), "%s▓", text);
    } else {
      Format(text, sizeof(text), "%s░", text);
    }

    X -= symbolOffset;
  }

  new String:prefix[24];
  Format(prefix, sizeof(prefix), "%d٪ CT", ctPerc);

  for (new i = 0; i < strlen(prefix); i++) {
    X -= symbolOffset;
  }

  Format(text, sizeof(text), "%s %s", text, prefix);

  new String:colors[4][4];
  new String:buf[128];
  GetConVarString(cvar_ctTeamColor, buf, sizeof(buf));
  ExplodeString(buf, " ", colors, 4, 4);

  SetHudTextParams(
    X,
    Y,
    holdTime,
    StringToInt(colors[0]),
    StringToInt(colors[1]),
    StringToInt(colors[2]),
    255
  );

  for (new p = 1; p <= MaxClients; p++) {
    if (client == 0 || (client > 0 && p == client)) {
      if (IsValidClient(p) && !IsFakeClient(p)) {
        ShowHudText(p, GetConVarInt(cvar_ctHudChannel), text);
      }
    }
  }

  X = basicX + distanceBetween;

  Format(text, sizeof(text), "");
  for (new i = 1; i <= 10; i++) {
    if (tPerc / (10 * i) >= 1) {
      Format(text, sizeof(text), "%s▓", text);
    } else {
      Format(text, sizeof(text), "%s░", text);
    }
  }
  Format(text, sizeof(text), "T %d٪ %s", tPerc, text);

  GetConVarString(cvar_tTeamColor, buf, sizeof(buf));
  ExplodeString(buf, " ", colors, 4, 4);

  SetHudTextParams(
    X,
    Y,
    holdTime,
    StringToInt(colors[0]),
    StringToInt(colors[1]),
    StringToInt(colors[2]),
    255
  );

  for (new p = 1; p <= MaxClients; p++) {
    if (client == 0 || (client > 0 && p == client)) {
      if (IsValidClient(p) && !IsFakeClient(p)) {
        ShowHudText(p, GetConVarInt(cvar_tHudChannel), text);
      }
    }
  }
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
