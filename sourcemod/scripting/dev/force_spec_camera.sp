#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <cstrike>

public Plugin:myinfo = {
  name = "Force Spec Camera",
  version = "2022.12.28",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

public OnPluginStart() {
  RegConsoleCmd("spec_mode", Command_SpecMode);
  RegConsoleCmd("spec_prev", Command_SpecPrevNext);
  RegConsoleCmd("spec_next", Command_SpecPrevNext);

  HookEvent("player_team", Event_PlayerTeam, EventHookMode_Post);
  HookEvent("player_death", Event_PlayerDeath, EventHookMode_Post);
}

public Event_PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));

  if (!IsValidClient(client) || IsFakeClient(client)) {
    return;
  }

  new team = GetEventInt(event, "team");
  if (team == CS_TEAM_SPECTATOR) {
    CreateTimer(0.5, SpectateRandomPlayerTimer, client);
  }
}

public Action:SpectateRandomPlayerTimer(Handle:timer, any:client) {
  if (!IsValidClient(client) || IsPlayerAlive(client)) {
    return Plugin_Stop;
  }

  SetObserverMode(client);
  new id = GetRandomPlayer(client);
  if (id > 0) {
    SetObserverTarget(client, id);
  }

  return Plugin_Stop;
}

public Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));

  if (!IsValidClient(client)) {
    return;
  }

  CreateTimer(0.5, SpectateTeamPlayerTimer, client, TIMER_REPEAT);
}

public Action:SpectateTeamPlayerTimer(Handle:timer, any:client) {
  if (!IsValidClient(client) || IsPlayerAlive(client)) {
    return Plugin_Stop;
  }
  // wait for m_lifeState change to LIFE_DEAD
  else if (GetEntProp(client, Prop_Send, "m_lifeState") == 2) {
    SetObserverMode(client);
    new id = GetRandomTeamPlayer(client);
    if (id > 0) {
      SetObserverTarget(client, id);
    }
    return Plugin_Stop;
  }

  return Plugin_Continue;
}

public Action:Command_SpecPrevNext(client, args) {
  SetObserverMode(client);
  return Plugin_Continue;
}

public Action:Command_SpecMode(client, args) {
  SetObserverMode(client);
  return Plugin_Handled;
}

stock SetObserverTarget(client, target) {
  if (IsValidClient(client) && IsValidClient(target)) {
    SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", target);
  }
}

stock SetObserverMode(client) {
  if (IsValidClient(client)) {
    SetEntPropVector(client, Prop_Data, "m_vecViewOffset", NULL_VECTOR);
    SetEntityMoveType(client, MOVETYPE_OBSERVER);
    SetEntProp(client, Prop_Send, "m_iObserverMode", 5); // follow a player in third person view
  }
}

stock IsValidClient(client) {
  return client > 0 && IsClientConnected(client) && IsClientInGame(client);
}

stock GetRandomTeamPlayer(client) {
  new team1 = GetClientTeam(client);
  for (new idClient = MaxClients; idClient >= 1; --idClient) {
    if (client != idClient && IsClientInGame(idClient) && IsPlayerAlive(idClient)) {
      if (team1 == GetClientTeam(idClient)) {
        return idClient;
      }
    }
  }
  return 0;
}

stock GetRandomPlayer(client) {
	for (new idClient = MaxClients; idClient >= 1; --idClient) {
		if (client != idClient && IsClientInGame(idClient) && IsPlayerAlive(idClient)) {
      return idClient;
		}
	}
	return 0;
}
