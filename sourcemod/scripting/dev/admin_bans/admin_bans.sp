#pragma semicolon 1
#include <sourcemod>

#define THIS_PLUGIN_LIBRARY "admin_bans"

public Plugin:myinfo = {
  name = "Admin Bans",
  version = "2023.01.12",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

/*
  TODO:
    command to get banned players + info from db as a menu
    menu with online players/spectators list and select/confirm ban option
*/

#define STEAM_ID_LENGTH 17
#define SQL_QUERY_MAX_LEN 2048

#define CONFIG_FILE_PATH "configs/admin_bans_db.sql"
#define LOGS_FILE_PATH "logs/admin_bans.log"
#define TRANSLATIONS_FILE "admin_bans.phrases"

new Handle:cvar_dbTableName;
new String:g_dbTableName[32];

new Handle:cvar_dbConfigurationName;
new String:g_dbConfigurationName[32];

new Handle:g_hDatabaseConnection = INVALID_HANDLE;

public APLRes:AskPluginLoad2(Handle:hPlugin, bool:bLate, String:strError[], maxErrors) {
  CreateNative("AddClientBan", Native_AddClientBan);
  RegPluginLibrary(THIS_PLUGIN_LIBRARY);
  return APLRes_Success;
}

public Native_AddClientBan(Handle:hPlugin, numParams) {
  new String:banReason[100];

  new adminClient = GetNativeCell(1);
  new client = GetNativeCell(2);
  new strLen = 0;
  GetNativeStringLength(3, strLen);
  GetNativeString(3, banReason, strLen * 2); // increasing length twice due to special characters, eg "❱"
  new banDuration = GetNativeCell(4);

  AddClientBan(adminClient, client, banReason, banDuration);
}

public OnPluginStart() {
  LoadTranslations(TRANSLATIONS_FILE);

  cvar_dbConfigurationName = CreateConVar("sm_adminbans_db_configuration_name", "default", "", FCVAR_PROTECTED);
  GetConVarString(cvar_dbConfigurationName, g_dbConfigurationName, sizeof(g_dbConfigurationName));

  cvar_dbTableName = CreateConVar("sm_adminbans_db_table_name", "admin_bans", "", FCVAR_PROTECTED);
  GetConVarString(cvar_dbTableName, g_dbTableName, sizeof(g_dbTableName));

  InitDbConnection();
}

public OnClientAuthorized(client, const String:auth[]) {
  if (IsFakeClient(client)) {
    return;
  }

  new String:steamid[MAX_NAME_LENGTH];
  GetClientAuthId(client, AuthId_SteamID64, steamid, sizeof(steamid));

  if (!IsValidSteamId(steamid)) {
    DebugAndFileLog("Cannot check ban for invalid steamid: %s", steamid);
    return;
  }

  new String:ip[16];
  new String:playerName[MAX_NAME_LENGTH];

  GetClientName(client, playerName, sizeof(playerName));
  GetClientIP(client, ip, sizeof(ip));

  CheckForBanAsync(client, steamid, ip, playerName);
}

InitDbConnection() {
  new String:error[256];

  if (!SQL_CheckConfig(g_dbConfigurationName)) {
    Format(error, sizeof(error), "Invalid DB configuration: %s", g_dbConfigurationName);
    DebugAndFail(error);
    return;
	}

  SQL_TConnect(InitDbConnectionCallback, g_dbConfigurationName);
}

public InitDbConnectionCallback(Handle:hOwner, Handle:hQuery, const String:error[], any:data) {
	if (hQuery == INVALID_HANDLE) {
		Debug("Database connection failed");
	} else {
    if (!SQL_FastQuery(hQuery, "SET NAMES 'utf8';")) {
      DebugAndFail("Error: SQL Fast query is not working");
    } else {
      g_hDatabaseConnection = hQuery;
      Debug("Database connection succeed");

      CreateTableIfNotExists();
    }
	}
}

ReadDbCreationQuery(String:outBuffer[], outBufferLen) {
  new String:configPath[PLATFORM_MAX_PATH];
  BuildPath(Path_SM, configPath, sizeof(configPath), CONFIG_FILE_PATH);

  if (FileExists(configPath) == false) {
    Debug("Unable to find config file: %s", configPath);
    return;
  }

  new Handle:hFile = OpenFile(configPath, "r");
  new String:line[SQL_QUERY_MAX_LEN];
  new String:query[SQL_QUERY_MAX_LEN];
  while (!IsEndOfFile(hFile) && ReadFileLine(hFile, line, sizeof(line))) {
    Format(query, sizeof(query), "%s%s", query, line);
  }

  CloseHandle(hFile);

  Format(outBuffer, outBufferLen, "%s", query);
}

CreateTableIfNotExists() {
  if (g_hDatabaseConnection != INVALID_HANDLE) {
    new String:query[SQL_QUERY_MAX_LEN];
    ReadDbCreationQuery(query, sizeof(query));

    if (strlen(query) == 0) {
      Debug("Database query is corrupt: %s", query);
    } else {
      SQL_TQuery(g_hDatabaseConnection, CreateTableIfNotExistsCallback, query);
    }
  } else {
    DebugAndFail("Error: database disconnected on init.");
  }
}

public CreateTableIfNotExistsCallback(Handle:dbHandle, Handle:queryHandle, const String:error[], any:data) {
  if (queryHandle == INVALID_HANDLE || !StrEqual("", error)) {
    DebugAndFileLog("Unable to verify db table: \n%s", error);
    return;
  }
  Debug("Database created/verified successfully");
}

AddClientBan(adminClient, client, const String:banReason[], banDuration = 0) {
  if (!IsValidClient(client) || !IsValidClient(adminClient)) {
    DebugAndFileLog("Cannot ban invalid client: %d->%d", adminClient, client);
    return;
  }

  new String:steamid[MAX_NAME_LENGTH];
  new String:playerName[MAX_NAME_LENGTH];
  new String:ip[16];

  GetClientAuthId(client, AuthId_SteamID64, steamid, sizeof(steamid));
  GetClientName(client, playerName, sizeof(playerName));
  GetClientIP(client, ip, sizeof(ip));

  new String:adminName[MAX_NAME_LENGTH];
  GetClientName(adminClient, adminName, sizeof(adminName));

  AddDbBanAsync(steamid, ip, playerName, adminName, banReason, banDuration);
}

CheckForBanAsync(client, const String:steamid[], const String:ip[], const String:playerName[]) {
  if (!IsValidSteamId(steamid)) {
    return;
  }

  if (g_hDatabaseConnection == INVALID_HANDLE) {
    DebugAndFileLog("CheckForBanAsync: Database connection failed");
    return;
  }

  new String:query[SQL_QUERY_MAX_LEN];
  Format(query, sizeof(query), "SELECT ban_duration, ban_reason, admin_name, (ROUND(TIME_TO_SEC(TIMEDIFF(ADDTIME(ban_date, SEC_TO_TIME(ban_duration * 60)), CURRENT_TIMESTAMP)) / 60)) AS ban_minutes FROM %s WHERE steam_id = '%s';", g_dbTableName, steamid);

  new Handle:datapack = CreateDataPack();
  WritePackCell(datapack, client);
  WritePackString(datapack, steamid);
  WritePackString(datapack, ip);
  WritePackString(datapack, playerName);

  SQL_TQuery(g_hDatabaseConnection, CheckForBanAsyncCallback, query, datapack);
}

public CheckForBanAsyncCallback(Handle:dbHandle, Handle:queryHandle, const String:error[], any:data) {
  if (queryHandle == INVALID_HANDLE || !StrEqual("", error)) {
    DebugAndFileLog("CheckForBanAsyncCallback error: \n%s", error);
    return;
  }

  new String:steamid[MAX_NAME_LENGTH];
  new String:playerName[MAX_NAME_LENGTH];
  new String:ip[16];

  new Handle:datapack = data;
  new client = 0;
  if (datapack != INVALID_HANDLE) {
    ResetPack(datapack);
    client = ReadPackCell(datapack);
    ReadPackString(datapack, steamid, sizeof(steamid));
    ReadPackString(datapack, ip, sizeof(ip));
    ReadPackString(datapack, playerName, sizeof(playerName));

    CloseHandle(datapack);
  }

  if (!IsValidClient(client)) {
    DebugAndFileLog("Invalid client while checking for ban: %d - %s", client, steamid);
    return;
  }

  new rows = SQL_GetRowCount(queryHandle);
  if (rows < 1) {
    return;
  }

  Debug("Checking db ban for %s %s %s", steamid, ip, playerName);

  SQL_FetchRow(queryHandle);

  new banDuration = SQL_FetchInt(queryHandle, 0);
  new String:banReason[100];
  SQL_FetchString(queryHandle, 1, banReason, sizeof(banReason));
  new String:adminName[MAX_NAME_LENGTH];
  SQL_FetchString(queryHandle, 2, adminName, sizeof(adminName));
  new banMinutes = SQL_FetchInt(queryHandle, 3);

  if (banMinutes > 0) {
    new String:buf[256];
    Format(buf, sizeof(buf), "%T", "kickmessage", client, adminName, banReason, banDuration == 0 ? 999999 : banMinutes); // indefinite ban
    KickClient(client, buf);
  } else {
    // TODO: player is not in ban anymore, remove db entry?
  }
}

AddDbBanAsync(const String:steamid[], const String:ip[], const String:playerName[], const String:adminName[], const String:banReason[], banDuration) {
  if (!IsValidSteamId(steamid)) {
    DebugAndFileLog("AddDbBanAsync: Invalid steamid for ban: %s", steamid);
    return;
  }

  if (g_hDatabaseConnection == INVALID_HANDLE) {
    DebugAndFileLog("AddDbBanAsync: Database connection failed");
    return;
  }

  new String:playerNameSafe[MAX_NAME_LENGTH * 2 + 1];
  new String:adminNameSafe[MAX_NAME_LENGTH * 2 + 1];
  new String:banReasonSafe[100 * 2 + 1];

  SQL_EscapeString(g_hDatabaseConnection, playerName, playerNameSafe, sizeof(playerNameSafe));
  SQL_EscapeString(g_hDatabaseConnection, adminName, adminNameSafe, sizeof(adminNameSafe));
  SQL_EscapeString(g_hDatabaseConnection, banReason, banReasonSafe, sizeof(banReasonSafe));

  new String:query[SQL_QUERY_MAX_LEN];

  Format(query, sizeof(query), "INSERT INTO %s (steam_id, player_name, ip, ban_duration, ban_reason, admin_name, ban_date) VALUES", g_dbTableName);
  Format(query, sizeof(query), "%s ('%s', '%s', '%s', %d, '%s', '%s', CURRENT_TIMESTAMP)", query, steamid, playerNameSafe, ip, banDuration, banReasonSafe, adminNameSafe);
  Format(query, sizeof(query), "%s ON DUPLICATE KEY UPDATE player_name = '%s', ip = '%s', ban_duration = %d, ban_reason = '%s', admin_name = '%s';", query, playerNameSafe, ip, banDuration, banReasonSafe, adminNameSafe);

  new Handle:datapack = CreateDataPack();
  WritePackString(datapack, steamid);

  SQL_TQuery(g_hDatabaseConnection, AddDbBanAsyncCallback, query, datapack);
}

public AddDbBanAsyncCallback(Handle:dbHandle, Handle:queryHandle, const String:error[], any:data) {
  if (queryHandle == INVALID_HANDLE || !StrEqual("", error)) {
    DebugAndFileLog("AddDbBanAsyncCallback error: \n%s", error);
    return;
  }

  new Handle:datapack = data;
  if (datapack != INVALID_HANDLE) {
    ResetPack(datapack);

    new String:steamid[MAX_NAME_LENGTH];
    ReadPackString(datapack, steamid, sizeof(steamid));

		CloseHandle(datapack);

    DebugAndFileLog("SQL Ban has been added for: %s", steamid);
	}
}

stock DebugAndFail(String:message[]) {
  DebugAndFileLog(message);
  SetFailState(message);
}

stock bool:IsValidClient(client) {
  return client >= 1 && client <= MaxClients;
}

stock bool:IsConnectedInGame(client) {
  return IsValidClient(client) && IsClientConnected(client) && IsClientInGame(client);
}

stock bool:IsNumeric(const String:str[]) {
	for (new i = 0; i < strlen(str); i++)
		if (!IsCharNumeric(str[i]) && str[i])
			return false;
	return true;
}

stock bool:IsValidSteamId(const String:steamid[]) {
	return strlen(steamid) == STEAM_ID_LENGTH && IsNumeric(steamid);
}

stock Debug(String:format[], any:...) {
  new String:pluginName[PLATFORM_MAX_PATH];
  GetPluginFilename(INVALID_HANDLE, pluginName, sizeof(pluginName));

  new String:buffer[2048];
  VFormat(buffer, sizeof(buffer), format, 2);

  PrintToServer("<%s> %s", pluginName, buffer);
}

stock DebugAndFileLog(String:format[], any:...) {
  new String:buffer[2048];
  VFormat(buffer, sizeof(buffer), format, 2);

  Debug(buffer);

  new String:path[PLATFORM_MAX_PATH];
  BuildPath(Path_SM, path, sizeof(path), LOGS_FILE_PATH);

  LogToFileEx(path, "%s", buffer);
}
