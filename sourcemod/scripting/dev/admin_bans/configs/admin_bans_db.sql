CREATE TABLE IF NOT EXISTS `admin_bans` (
  `id` INT(11) NOT NULL auto_increment,
  `steam_id` VARCHAR(32) NOT NULL,
  `player_name` VARCHAR(65),
  `ip` VARCHAR(16),
  `ban_duration` INT(11) NOT NULL DEFAULT 0,
  `ban_reason` VARCHAR(100),
  `admin_name` VARCHAR(65),
  `ban_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, -- 2023-01-11 18:16:02
  PRIMARY KEY (`id`),
  UNIQUE KEY `steam_id` (`steam_id`)
);
