#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <stocks2023>

public Plugin:myinfo = {
  name = "Bullet Tracers",
  version = "2022.12.10",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define TRACER_SPRITE "materials/sprites/laser.vmt"

new Float:g_fClientDelay[MAXPLAYERS + 1];

new Handle:hRGB, String:sRGB[18],
	Handle:hSpeed, Float:fSpeed,
	Handle:hDelay, Float:fDelay,
	Handle:hStartWidth, Float:fStartWidth,
  Handle:hEndWidth, Float:fEndWidth,
  Handle:hMinDistance, Float:fMinDistance,
  Handle:cvar_hostTimescale;

public OnPluginStart() {
  cvar_hostTimescale = FindConVar("host_timescale");

  hDelay = CreateConVar("sm_bullet_tracers_delay", "0.2", "Delay between next bullet trace", FCVAR_PROTECTED, true, 0.1);
  fDelay = GetConVarFloat(hDelay);
  HookConVarChange(hDelay, OnConVarChanges);

  hRGB = CreateConVar("sm_bullet_tracers_color", "255 255 153", "RGB color of bullet trace", FCVAR_PROTECTED);
  GetConVarString(hRGB, sRGB, sizeof(sRGB));
  HookConVarChange(hRGB, OnConVarChanges);

  hSpeed = CreateConVar("sm_bullet_tracers_speed", "10000.0", "The speed of bullet trace", FCVAR_PROTECTED, true, 100.0);
  fSpeed = GetConVarFloat(hSpeed);
  HookConVarChange(hSpeed, OnConVarChanges);

  hStartWidth = CreateConVar("sm_bullet_tracers_startwidth", "0.3", "The start width of bullet trace", FCVAR_PROTECTED, true, 0.1);
  fStartWidth = GetConVarFloat(hStartWidth);
  HookConVarChange(hStartWidth, OnConVarChanges);

  hEndWidth = CreateConVar("sm_bullet_tracers_endwidth", "0.2", "The end width of bullet trace", FCVAR_PROTECTED, true, 0.1);
  fEndWidth = GetConVarFloat(hEndWidth);
  HookConVarChange(hEndWidth, OnConVarChanges);

  hMinDistance = CreateConVar("sm_bullet_tracers_min_distance", "500.0", "Min distance between two points to create tracer", FCVAR_PROTECTED, true, 0.0);
  fMinDistance = GetConVarFloat(hMinDistance);
  HookConVarChange(hMinDistance, OnConVarChanges);

  HookEvent("bullet_impact", Event_BulletImpact);
}

public OnConVarChanges(Handle:convar, const String:oldVal[], const String:newVal[]) {
  if (convar == hDelay) {
    fDelay = StringToFloat(newVal);
  } else if (convar == hRGB) {
    strcopy(sRGB, sizeof(sRGB), newVal);
  } else if (convar == hSpeed) {
    fSpeed = StringToFloat(newVal);
  } else if (convar == hStartWidth) {
    fStartWidth = StringToFloat(newVal);
  } else if (convar == hEndWidth) {
    fEndWidth = StringToFloat(newVal);
  } else if (convar == hMinDistance) {
    fMinDistance = StringToFloat(newVal);
  }
}

public OnClientDisconnect_Post(client) {
	g_fClientDelay[client] = 0.0;
}

public Action:Event_BulletImpact(Handle:event, const String:name[], bool:dontBroadcast) {
	new client = GetClientOfUserId(GetEventInt(event, "userid"));

	new Float:gameTime = GetGameTime();

	if (g_fClientDelay[client] > gameTime) {
    return Plugin_Continue;
  }

	new Float:bulletDestination[3];
	bulletDestination[0] = GetEventFloat(event, "x");
	bulletDestination[1] = GetEventFloat(event, "y");
	bulletDestination[2] = GetEventFloat(event, "z");

	new Float:bulletOrigin[3];
	GetClientEyePosition(client, bulletOrigin);

  new Float:hostTimescale = GetConVarFloat(cvar_hostTimescale);

	new Float:distance = GetVectorDistance(bulletOrigin, bulletDestination);

  // skip min distance condition if time scale is lower than usual
  if (hostTimescale == 1.0 && fMinDistance > 0 && distance < fMinDistance) {
    return Plugin_Continue;
  }

	new Float:percentage = 0.4 / (distance / 100);

	new Float:newBulletOrigin[3];
	newBulletOrigin[0] = bulletOrigin[0] + ((bulletDestination[0] - bulletOrigin[0]) * percentage);
	newBulletOrigin[1] = bulletOrigin[1] + ((bulletDestination[1] - bulletOrigin[1]) * percentage) - 0.08;
	newBulletOrigin[2] = bulletOrigin[2] + ((bulletDestination[2] - bulletOrigin[2]) * percentage);

  // TODO: AK-47 and some other guns have lower 3rd person position for bullet impact
  // newBulletOrigin[2] -= 10.0;

  new Float:speedCalc = hostTimescale < 1 ? (fSpeed * hostTimescale) / 2 : fSpeed;

	new ent = CreateBulletTrace(
    client,
    newBulletOrigin,
    bulletDestination,
    speedCalc,
    fStartWidth,
    fEndWidth,
    sRGB
  );

  if (!IsValidEdict(ent)) {
    return Plugin_Continue;
  }

  if (hostTimescale < 1) {
    CreateTrigger(ent);
  }

	// Make us ignore shotguns for the delay
	new weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
  new String:g_szWeapon[32];
	if (weapon != -1) {
		GetEdictClassname(weapon, g_szWeapon, sizeof(g_szWeapon));
	}
  if (StrEqual(g_szWeapon, "weapon_xm1014", false) || StrEqual(g_szWeapon, "weapon_m3", false)) {
    return Plugin_Continue;
  }

	g_fClientDelay[client] = gameTime + fDelay;	// setting delay to avoid lagging beacuse of entity spam

  return Plugin_Continue;
}

stock CreateBulletTrace(owner, const Float:origin[3], const Float:dest[3], const Float:speed = 6000.0,
  const Float:startwidth = 0.5, const Float:endwidth = 0.2, const String:color[] = "200 200 0") {
  new entity = CreateEntityByName("env_spritetrail");
  if (entity == -1) {
    Debug("Couldn't create env_spritetrail");
    return -1;
  }

  DispatchKeyValue(entity, "classname", "bullet_trace");
  DispatchKeyValue(entity, "spritename", TRACER_SPRITE);
  DispatchKeyValue(entity, "renderamt", "255");
  DispatchKeyValue(entity, "rendercolor", color);
  DispatchKeyValue(entity, "rendermode", "5");
  DispatchKeyValue(entity, "disablereceiveshadows", "1");
  DispatchKeyValueFloat(entity, "startwidth", startwidth);
  DispatchKeyValueFloat(entity, "endwidth", endwidth);
  DispatchKeyValueFloat(entity, "lifetime", 240.0 / speed);

  if (!DispatchSpawn(entity)) {
    AcceptEntityInput(entity, "Kill");
    Debug("Couldn't spawn env_spritetrail");
    return -1;
  }

  SetEntPropFloat(entity, Prop_Send, "m_flTextureRes", 0.05);

  SetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", owner);

  new Float:vecVeloc[3], Float:angRotation[3];
  MakeVectorFromPoints(origin, dest, vecVeloc);
  GetVectorAngles(vecVeloc, angRotation);
  NormalizeVector(vecVeloc, vecVeloc);
  ScaleVector(vecVeloc, speed);

  TeleportEntity(entity, origin, angRotation, vecVeloc);

  new String:varStr[128];
  new Float:timeToDelete = GetVectorDistance(origin, dest) / speed;
  FormatEx(varStr, sizeof(varStr), "OnUser1 !self:kill::%f:-1", timeToDelete);
  SetVariantString(varStr);
  AcceptEntityInput(entity, "AddOutput");
  AcceptEntityInput(entity, "FireUser1");

  return entity;
}

CreateTrigger(parent) {
  new ent = CreateEntityByName("trigger_multiple");
  if (!ent) {
    Debug("Couldn't create trigger_multiple");
    return -1;
  }

  DispatchKeyValue(ent, "spawnflags", "64");
  Entity_SetParent(ent, parent);
  if (!DispatchSpawn(ent)) {
    Debug("Couldn't spawn trigger_multiple");
    AcceptEntityInput(ent, "Kill");
    return -1;
  }
  ActivateEntity(ent);

  new Float:minbounds[3] = {-36.0, -36.0, -36.0};
  new Float:maxbounds[3] = {36.0, 36.0, 36.0};
  SetEntPropVector(ent, Prop_Send, "m_vecMins", minbounds);
  SetEntPropVector(ent, Prop_Send, "m_vecMaxs", maxbounds);

  SetEntProp(ent, Prop_Send, "m_nSolidType", 2);

  new enteffects = GetEntProp(ent, Prop_Send, "m_fEffects");
  enteffects |= 32; // EF_NODRAW
  SetEntProp(ent, Prop_Send, "m_fEffects", enteffects);

  new Float:origin[3];
  TeleportEntity(ent, origin, NULL_VECTOR, NULL_VECTOR);

  SDKHook(ent, SDKHook_StartTouchPost, Trigger_StartTouchPost);

  return ent;
}

public Trigger_StartTouchPost(entity, other) {
  AcceptEntityInput(entity, "Kill");

  new Float:hostTimescale = GetConVarFloat(cvar_hostTimescale);
  if (hostTimescale == 1.0) {
    return;
  }

	if (!IsValidClient(other)) {
    return;
  }

  new parent = GetEntPropEnt(entity, Prop_Data, "m_pParent");
  new owner = GetEntPropEnt(parent, Prop_Send, "m_hOwnerEntity");

  if (!IsValidClient(owner)) {
    return;
  }

  // SDKHooks_TakeDamage(int entity, int inflictor, int attacker, float damage, int damageType, int weapon, const float damageForce[3], const float damagePosition[3], bool bypassHooks)
  // TODO: set attacker waepon
  // get damage from trace+hitbox
  SDKHooks_TakeDamage(other, owner, owner, GetRandomFloat(20.0, 40.0), DMG_GENERIC, 0);
}
