#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
  name = "Bot Ping",
  version = "2022.11.06",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define IsBot(%1) (IsFakeClient(%1) && GetEntProp(%1, Prop_Data, "m_iFrags") != -999)

#define MIN_PING_BASE 8
#define MAX_PING_BASE 148

#define MIN_PING_OFFSET 0
#define MAX_PING_OFFSET 6

#define PING_FLUCTATE_CHANCE 25

#define PING_UPDATE_DELAY 5.0
#define PING_REPLICATE_DELAY 0.2

new String:g_szPlayerManager[20] = "";
new g_iPlayerManager = -1;
new g_iPing = -1;

new g_BotPingBase[MAXPLAYERS + 1] = { 0, ... };
new g_BotPingValue[MAXPLAYERS + 1] = { 0, ... };

public OnPluginStart() {
  SetupPlayerResource();
  SetupPlayerManagerType();
}

SetupPlayerResource() {
  g_iPing = FindSendPropInfo("CPlayerResource", "m_iPing");
}

SetupPlayerManagerType() {
  new String:modName[100];
  GetGameFolderName(modName, sizeof(modName));

  if (StrEqual("cstrike", modName))
    strcopy(g_szPlayerManager, sizeof(g_szPlayerManager), "cs_player_manager");
  else
    strcopy(g_szPlayerManager, sizeof(g_szPlayerManager), "player_manager");
}

SetupPlayerManagerEntity() {
  // https://forums.alliedmods.net/showthread.php?t=279570
  // Exception reported: Entity 64 (64) is invalid
  g_iPlayerManager = FindEntityByClassname(MaxClients + 1, g_szPlayerManager);
}

public OnMapStart() {
  SetupPlayerManagerEntity();
  CreateTimer(PING_UPDATE_DELAY, UpdatePing, INVALID_HANDLE, TIMER_REPEAT);
  CreateTimer(PING_REPLICATE_DELAY, ReplicatePing, INVALID_HANDLE, TIMER_REPEAT);
}

public OnClientPostAdminCheck(client) {
  if (IsBot(client)) {
    g_BotPingBase[client] = GetRandomInt(MIN_PING_BASE, MAX_PING_BASE);
    SetPingValue(client, g_BotPingBase[client]);
  }
}

public OnClientDisconnect(client) {
  if (IsBot(client)) {
    g_BotPingBase[client] = 0;
    g_BotPingValue[client] = 0;
  }
}

SetPingValue(client, value) {
  if (IsValidClient(client)) {
    SetEntData(g_iPlayerManager, g_iPing + (client * 4), value);
  }
}

public Action:UpdatePing(Handle:timer) {
  for (new i = 1; i <= MaxClients; i++) {
    if (!IsValidClient(i) || !IsBot(i)) {
      continue;
    }

    if (g_BotPingBase[i] == 0) {
      continue;
    }

    new offset = GetRandomInt(MIN_PING_OFFSET, MAX_PING_OFFSET);
    new nextPing = g_BotPingBase[i] + offset;

    if (nextPing > MAX_PING_BASE) {
      nextPing = MAX_PING_BASE;
    } else if (nextPing < MIN_PING_BASE) {
      nextPing = MIN_PING_BASE;
    }

    new shouldFluctate = GetRandomInt(0, 100) <= PING_FLUCTATE_CHANCE;
    if (shouldFluctate) {
      g_BotPingValue[i] = nextPing;
    } else {
      g_BotPingValue[i] = g_BotPingBase[i];
    }
  }
}

public Action:ReplicatePing(Handle:timer) {
  for (new i = 1; i <= MaxClients; i++) {
    if (!IsValidClient(i) || !IsBot(i)) {
      continue;
    }

    if (g_BotPingValue[i] > 0) {
      SetPingValue(i, g_BotPingValue[i]);
    }
  }
}

stock bool:IsValidClient(client) {
  return client >= 1 && client <= MaxClients && IsClientInGame(client);
}
