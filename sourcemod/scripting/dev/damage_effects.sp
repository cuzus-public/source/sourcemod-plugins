#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
  name = "Damage Effects",
  version = "2022.12.26",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define HITGROUP_GENERIC     0
#define HITGROUP_HEAD        1
#define HITGROUP_CHEST       2
#define HITGROUP_STOMACH     3
#define HITGROUP_LEFTARM     4
#define HITGROUP_RIGHTARM    5
#define HITGROUP_LEFTLEG     6
#define HITGROUP_RIGHTLEG    7
#define HITGROUP_GEAR        10

#define FFADE_IN		0x0001		// Fade In
#define FFADE_OUT		0x0002		// Fade out
#define FFADE_PURGE		0x0010		// Purges all other fades, replacing them with this one

public OnPluginStart() {
  HookEvent("player_hurt", Event_PlayerHurt);
  AddTempEntHook("Shotgun Shot", HookShotgunShot);
}

public Action:HookShotgunShot(const String:te_name[], const players[], numClients, Float:delay) {
  new client = TE_ReadNum("m_iPlayer") + 1;

  new Float:origin[3], Float:angles[3];
  TE_ReadVector("m_vecOrigin", origin);
  angles[0] = TE_ReadFloat("m_vecAngles[0]");
  angles[1] = TE_ReadFloat("m_vecAngles[1]");
  angles[2] = 0.0;

  new Float:endPos[3];

  new Handle:trace = TR_TraceRayFilterEx(origin, angles, (CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_HITBOX), RayType_Infinite, TRDontHitSelf, client);
  if (TR_DidHit(trace)) {
    new ent = TR_GetEntityIndex(trace);
    if (ent > 0 && ent <= MaxClients && IsClientConnected(ent) && IsClientInGame(ent)) {
      new hitbox = TR_GetHitGroup(trace);

      if (hitbox == HITGROUP_HEAD || hitbox == HITGROUP_CHEST || hitbox == HITGROUP_STOMACH
      || hitbox == HITGROUP_LEFTLEG || hitbox == HITGROUP_RIGHTLEG) {
        new armor = GetEntProp(ent, Prop_Send, "m_ArmorValue");
        new helmet = GetEntProp(ent, Prop_Send, "m_bHasHelmet");

        new bool:showSparks = (armor > 0 && (hitbox == HITGROUP_CHEST || hitbox == HITGROUP_STOMACH)) || (helmet > 0 && hitbox == HITGROUP_HEAD);
        new bool:showDust = hitbox != HITGROUP_HEAD && GetRandomInt(0, 100) <= 25;

        if (showSparks || showDust) {
          TR_GetEndPosition(endPos, trace);
          angles[0] = GetRandomFloat(-1.0, 1.0);
          angles[1] = GetRandomFloat(-1.0, 1.0);
          angles[2] = GetRandomFloat(-1.0, 1.0);

          if (showSparks) {
            doArmorRicochet(endPos, angles);
          } else {
            new Float:size = GetRandomFloat(0.1, 1.0);
            new Float:speed = GetRandomFloat(1.0, 100.0);
            doDust(endPos, angles, size, speed);
          }
        }
      }
    }
  }
  delete trace;

  return Plugin_Continue;
}

public bool:TRDontHitSelf(entity, mask, any:client) {
	return (1 <= entity <= MaxClients) && (entity != client);
}

public Action:Event_PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast) {
  new victim = GetClientOfUserId(GetEventInt(event, "userid"));

  if (victim > 0 && !IsFakeClient(victim)) {
    new damage = GetEventInt(event, "dmg_health");

    new duration = damage * 15;
    new red = RoundToNearest(damage * 10.0);
    if (red > 255) {
      red = 255;
    }
    new alpha = RoundToNearest(damage * 2.5);
    if (alpha > 255) {
      alpha = 255;
    }

    duration = duration < 400 ? 400 : (duration > 800 ? 800 : duration);

    Client_Fade(victim, 0, duration, FFADE_PURGE|FFADE_IN, red, 0, 0, alpha);

    new Float:amplitude = damage / 7.0;
    new Float:shakeDuration = damage / 50.0;
    amplitude = amplitude < 4.5 ? 4.5 : amplitude;
    shakeDuration = shakeDuration < 0.6 ? 0.6 : (shakeDuration > 1.0 ? 1.0 : shakeDuration);

    Client_Shake(victim, amplitude, 1.0, shakeDuration);

    // this disables standard screen shaking effect on damage
    new Float:vec[3];
    SetEntPropVector(victim, Prop_Send, "m_vecPunchAngle", vec);
    SetEntPropVector(victim, Prop_Send, "m_vecPunchAngleVel", vec);

    // FIXME: maybe use this instead - OnTakeDamage
    // SetEntPropVector(c, Prop_Send, "m_aimPunchAngle", NULL_VECTOR);
    // SetEntPropVector(c, Prop_Send, "m_aimPunchAngleVel", NULL_VECTOR);
  }

  return Plugin_Continue;
}

stock Client_Fade(client, hold, length, type, r, g, b, a) {
  new Handle:message = StartMessageOne("Fade", client);
  if (message == INVALID_HANDLE) {
    return;
  }
  BfWriteShort(message, length);	// FIXED 16 bit, with SCREENFADE_FRACBITS fractional, milliseconds duration
  BfWriteShort(message, hold);	// FIXED 16 bit, with SCREENFADE_FRACBITS fractional, milliseconds duration until reset (fade & hold)
  BfWriteShort(message, type); // fade type (in / out) FFADE_PURGE|FFADE_IN
  BfWriteByte(message, r);	// fade red
  BfWriteByte(message, g);	// fade green
  BfWriteByte(message, b);	// fade blue
  BfWriteByte(message, a);// fade alpha
  EndMessage();
}

stock Client_Shake(client, Float:amplitude = 50.0, Float:frequency = 150.0, Float:duration = 3.0) {
	new Handle:message = StartMessageOne("Shake", client);
	if (message == INVALID_HANDLE) {
		return;
	}
	BfWriteByte(message,	0);	// Shake Command
	BfWriteFloat(message,	amplitude);	// shake magnitude/amplitude (use 0.0 to stop shake)
	BfWriteFloat(message,	frequency);	// shake noise frequency
	BfWriteFloat(message,	duration);	// shake lasts this long
	EndMessage();
}

stock doArmorRicochet(const Float:pos[3], const Float:dir[3], Float:delay=0.0) {
  TE_SetupArmorRicochet(pos, dir);
  TE_SendToAll(delay);
}

stock doDust(const Float:pos[3], const Float:dir[3], Float:Size, Float:Speed, Float:delay=0.0)  {
  TE_SetupDust(pos, dir, Size, Speed);
  TE_SendToAll(delay);
}
