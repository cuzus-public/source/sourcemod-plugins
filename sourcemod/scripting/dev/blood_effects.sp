#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
  name = "Blood Effects",
  version = "2022.12.08",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

/*
  TODO:
  check g_hBloodDrips g_hBloodSpray @ ata_rollthedice.sp
*/

#define HITGROUP_HEAD 1
#define HITGROUP_CHEST 2

new String:BLOOD_PARTICLES[][] = {
  "blood_advisor_pierce_spray",
  "blood_advisor_pierce_spray_b",
  "blood_advisor_pierce_spray_c",
  "blood_advisor_puncture_withdraw",
  "blood_advisor_puncture",
  "blood_zombie_split_spray",
  "blood_zombie_split_spray_tiny",
  "blood_zombie_split_spray_tiny2",
  "blood_advisor_shrapnel_spray_1",
  "blood_impact_red_01",
  "blood_impact_red_01_droplets",
  "blood_impact_red_01_smalldroplets",
  "blood_impact_red_01_goop",
  "blood_impact_red_01_mist",
  "blood_impact_synth_01_droplets",

  // headshot (helmet hit sparkles)
  // "blood_spurt_synth_01",
  // "blood_spurt_synth_01b",
};

public OnPluginStart() {
  HookEvent("player_hurt", Event_PlayerHurt);
  HookEvent("player_death", Event_PlayerDeath);
}

public OnMapStart() {
  for (new i = 0; i < sizeof(BLOOD_PARTICLES); i++) {
    ForcePrecache(BLOOD_PARTICLES[i]);
  }
}

AddBloodSprayParticle(victim, Float:position[3], Float:angles[3], bool:attachToBody = false, bool:attachToHead = false) {
  new String:blood_spray_effects[][] = {
    "blood_advisor_pierce_spray",
    "blood_advisor_pierce_spray_b",
    "blood_advisor_pierce_spray_c", // shorter
    "blood_advisor_puncture_withdraw",
    "blood_zombie_split_spray", // shorter
    "blood_zombie_split_spray_tiny",
    "blood_zombie_split_spray_tiny2",
  };
  new particle = CreateParticle(victim, blood_spray_effects[GetRandomInt(0, sizeof(blood_spray_effects) - 1)], position, angles);
  if (particle > 0) {
    AttachParticle(victim, particle, attachToBody, attachToHead);
    ActivateParticle(particle, 3.0);
  }
}

AddBloodExtraParticle(victim, Float:position[3], Float:angles[3], bool:attachToBody = false, bool:attachToHead = false) {
  new String:blood_extra_effects[][] = {
    "blood_advisor_puncture",
    "blood_advisor_shrapnel_spray_1", // green
    "blood_impact_red_01",
    "blood_impact_red_01_goop",
    "blood_impact_red_01_droplets",
    "blood_impact_red_01_smalldroplets",
    "blood_impact_red_01_mist",
    "blood_impact_synth_01_droplets",
  };
  new particle = CreateParticle(victim, blood_extra_effects[GetRandomInt(0, sizeof(blood_extra_effects) - 1)], position, angles);
  if (particle > 0) {
    AttachParticle(victim, particle, attachToBody, attachToHead);
    ActivateParticle(particle, 3.0);
  }
}

AddBloodPositionParticle(victim, Float:position[3], Float:angles[3]) {
  new String:blood_extra_effects[][] = {
    "blood_advisor_puncture",
    "blood_impact_red_01",
    "blood_impact_red_01_goop",
  };
  new particle = CreateParticle(victim, blood_extra_effects[GetRandomInt(0, sizeof(blood_extra_effects) - 1)], position, angles);
  if (particle > 0) {
    AttachParticle(victim, particle, false, false);
    ActivateParticle(particle, 3.0);
  }
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new bool:isHeadshot = GetEventBool(event, "headshot");
  new victim = GetClientOfUserId(GetEventInt(event, "userid"));
  new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));

  if (attacker > 0 && attacker != victim) {
    new Float:victimPos[3];
    new Float:angles[3];
    GetEntPropVector(victim, Prop_Send, "m_vecOrigin", victimPos);
    GetClientEyeAngles(victim, angles);

    victimPos[2] += GetRandomFloat(0.0, 5.0);

    AddBloodSprayParticle(victim, victimPos, angles, true, isHeadshot);

    if (isHeadshot && GetRandomInt(1, 4) == 1) {
      AddBloodExtraParticle(victim, victimPos, angles, true, isHeadshot);
    }
  }

  return Plugin_Continue;
}

public Action:Event_PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast) {
  new victim = GetClientOfUserId(GetEventInt(event, "userid"));
  new attacker = GetClientOfUserId(GetEventInt(event,"attacker"));
  new String:weaponName[64];
  GetEventString(event, "weapon", weaponName, sizeof(weaponName));

  if (attacker > 0 && attacker != victim) {
    new bool:isValidWeapon = StrContains(weaponName, "hegrenade", false) == -1;

    if (GetRandomInt(1, 4) == 1 && isValidWeapon) {
      new Float:victimPos[3];
      new Float:attackerPos[3];
      new Float:angles[3];
      GetEntPropVector(victim, Prop_Send, "m_vecOrigin", victimPos);
      GetEntPropVector(attacker, Prop_Send, "m_vecOrigin", attackerPos);
      CalculateDirection(victimPos, attackerPos, angles);

      new hitgroup = GetEventInt(event, "hitgroup");
      if (hitgroup == HITGROUP_CHEST) {
        // TODO: maybe can calculate bullet hit position
        victimPos[2] += GetRandomFloat(51.0, 61.0);
        AddBloodPositionParticle(victim, victimPos, angles);
      } else if (hitgroup == HITGROUP_HEAD) {
        victimPos[2] += GetRandomFloat(65.0, 71.0);
        AddBloodPositionParticle(victim, victimPos, angles);
      }
    }
  }

  return Plugin_Continue;
}

stock CreateParticle(client, String:effectName[], Float:position[3], Float:angles[3]) {
  new particle = CreateEntityByName("info_particle_system");
  if (!IsValidEdict(particle)) {
    return -1;
  }

  angles[0] = angles[0] != 0.0 ? angles[0] : GetRandomFloat(0.0, 360.0);
  angles[1] = angles[1] != 0.0 ? angles[1] : GetRandomFloat(-15.0, 15.0);
  angles[2] = angles[2] != 0.0 ? angles[2] : GetRandomFloat(-15.0, 15.0);
  TeleportEntity(particle, position, angles, NULL_VECTOR);

  new String:clientName[64];
  GetEntPropString(client, Prop_Data, "m_iName", clientName, sizeof(clientName));

  DispatchKeyValue(particle, "targetname", "cssparticle");
  DispatchKeyValue(particle, "parentname", clientName);
  DispatchKeyValue(particle, "effect_name", effectName);
  DispatchSpawn(particle);

  return particle;
}

stock AttachParticle(client, particle, bool:attachToBody, bool:attachToHead) {
  if (!IsValidEdict(particle)) {
    return -1;
  }

  if (attachToHead && SetParticleAttachment(client, particle, true) != -1) {
    // attached
  } else if (attachToBody && SetParticleAttachment(client, particle) != -1) {
    // attached
  } else {
    // no attachment (create on position)
    new String:clientName[64];
    GetEntPropString(client, Prop_Data, "m_iName", clientName, sizeof(clientName));
    SetVariantString(clientName);
    AcceptEntityInput(particle, "SetParent", particle, particle, 0);
  }

  return particle;
}

stock SetParticleAttachment(client, particle, bool:attachToHead = false) {
  new clientBody = GetEntPropEnt(client, Prop_Send, "m_hRagdoll");
  if (!IsValidEdict(clientBody)) { // IsValidEntity(Body)
    return -1;
  }

  new String:entName[64], String:className[64], String:modelPath[64];
  Format(entName, sizeof(entName), "Body%d", clientBody);
  GetEdictClassname(clientBody, className, sizeof(className));
  GetClientModel(client, modelPath, 64);

  if (StrEqual(className, "cs_ragdoll", false)) {
    DispatchKeyValue(clientBody, "targetname", entName);
    GetEntPropString(clientBody, Prop_Data, "m_iName", entName, sizeof(entName));
    SetVariantString(entName);
    AcceptEntityInput(particle, "SetParent", particle, particle, 0);

    new bool:usePrimary = StrContains(modelPath, "sas", false) != -1 || StrContains(modelPath, "gsg", false) != -1;
    if (attachToHead) {
      if (usePrimary) {
        SetVariantString("primary"); // back
      } else {
        SetVariantString("forward"); // head
      }
    } else {
      SetVariantString("primary"); // back
    }

    AcceptEntityInput(particle, "SetParentAttachment", particle, particle, 0);

    return clientBody;
  }

  return -1;
}

stock ActivateParticle(particle, Float:lifeTime = 0.0) {
  if (!IsValidEdict(particle)) {
    return -1;
  }

  ActivateEntity(particle);
  AcceptEntityInput(particle, "start");

  CreateTimer(lifeTime, DeleteParticle, particle);

  return particle;
}

public Action:DeleteParticle(Handle:timer, any:ent) {
  if (ent == -1 || !IsValidEdict(ent)) {
    return;
  }

  new String:className[64];
  GetEdictClassname(ent, className, sizeof(className));

  if (StrEqual(className, "info_particle_system", false)) {
    // RemoveEdict(ent);
    // AcceptEntityInput(ent, "stop");
    AcceptEntityInput(ent, "kill");
  }
}

stock CalculateDirection(Float:ClientOrigin[3], Float:AttackerOrigin[3], Float:Direction[3]) {
	new Float:RatioDiviser, Float:Diviser, Float:MaxCoord;

	Direction[0] = (ClientOrigin[0] - AttackerOrigin[0]) + GetRandomFloat(-25.0, 25.0);
	Direction[1] = (ClientOrigin[1] - AttackerOrigin[1]) + GetRandomFloat(-25.0, 25.0);
	Direction[2] = (GetRandomFloat(-125.0, -75.0));

	if (FloatAbs(Direction[0]) >= FloatAbs(Direction[1]))
    MaxCoord = FloatAbs(Direction[0]);
	else
    MaxCoord = FloatAbs(Direction[1]);

	RatioDiviser = GetRandomFloat(100.0, 250.0);
	Diviser = MaxCoord / RatioDiviser;
	Direction[0] /= Diviser;
	Direction[1] /= Diviser;
}

ForcePrecache(String:effectName[]) {
  new ent = CreateEntityByName("info_particle_system");
  if (ent == -1 || !IsValidEdict(ent)) {
    return;
  }

  DispatchKeyValue(ent, "effect_name", effectName);
  DispatchSpawn(ent);
  ActivateEntity(ent);
  AcceptEntityInput(ent, "start");

  CreateTimer(0.3, DeleteParticle, ent, TIMER_FLAG_NO_MAPCHANGE);
}
