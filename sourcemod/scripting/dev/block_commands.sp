#pragma semicolon 1
#include <sourcemod>

public Plugin:myinfo = {
  name = "Block Commands",
  version = "2022.10.14",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define COMMANDS_DELIMITER "||"

new String:DEFAULT_BLOCKED_USER_COMMANDS[][] = {
  "status",
  "ping"
};

new String:DEFAULT_BLOCKED_CONSOLE_COMMANDS[][] = {
  "kill",
  "spectate"
};

new String:global_blockedUserCommands[128][64];

new Handle:cvar_blockedUserCommandsList;

public OnPluginStart() {
  // convert default commands to CVAR string
  new String:blockerUserCommandsStr[1024];
  for (new i = 0; i < sizeof(DEFAULT_BLOCKED_USER_COMMANDS); i++) {
    if (i < 1) {
      Format(blockerUserCommandsStr, sizeof(blockerUserCommandsStr), "%s", DEFAULT_BLOCKED_USER_COMMANDS[i]);
    } else {
      Format(blockerUserCommandsStr, sizeof(blockerUserCommandsStr), "%s%s%s", blockerUserCommandsStr, COMMANDS_DELIMITER, DEFAULT_BLOCKED_USER_COMMANDS[i]);
    }
  }

  cvar_blockedUserCommandsList = CreateConVar("sm_blockedusercommandslist", blockerUserCommandsStr, "", FCVAR_NONE, false, 0.0, false, 0.0);

  // convert CVAR value back to commands defined in array
  new String:buffer[1024];
  GetConVarString(cvar_blockedUserCommandsList, buffer, sizeof(buffer));
  ExplodeString(buffer, COMMANDS_DELIMITER, global_blockedUserCommands, sizeof(global_blockedUserCommands), sizeof(global_blockedUserCommands[]), true);

  for (new i = 0; i < sizeof(global_blockedUserCommands); i++) {
    if (strlen(global_blockedUserCommands[i]) > 0) {
      AddCommandListener(CommandExec, global_blockedUserCommands[i]);
    }
  }

  for (new i = 0; i < sizeof(DEFAULT_BLOCKED_CONSOLE_COMMANDS); i++) {
    if (strlen(DEFAULT_BLOCKED_CONSOLE_COMMANDS[i]) > 0) {
      RegConsoleCmd(DEFAULT_BLOCKED_CONSOLE_COMMANDS[i], ConsoleCmd);
    }
  }
}

public Action:CommandExec(client, const String:name[], args) {
  return Plugin_Handled;
}

public Action:ConsoleCmd(client, args) {
	return Plugin_Handled;
}
