#if defined _hint_text_queue_included
  #endinput
#endif
#define _hint_text_queue_included

/**
 * @returns true if message sent successfully.
 */
native bool:PrintQueuedHintText(
  client,
  const String:message[],
  bool:replace = false,
  maxLines = 5
);
