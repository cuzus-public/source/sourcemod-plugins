#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

#define THIS_PLUGIN_LIBRARY "hint_text_queue"

public Plugin:myinfo = {
  name = "Hint Text Queue",
  description = "Queue for Hint Text messages using linebreaks",
  author = "Geekrainian @ cuzus.games",
  version = "2023.01.13",
  url = "https://geekrainian.com",
};

#define MAX_CHAT_TEXT_LENGTH 192
#define BUFFER_MESSAGE_LENGTH 512
#define HINT_TEXT_TIME_SECONDS 6.0

new Handle:g_messageQueueArray;
new Handle:g_timerHandleArray;

public OnPluginStart() {
  g_messageQueueArray = CreateArray();
  g_timerHandleArray = CreateArray();

  for (new k = 0; k <= MAXPLAYERS + 1; k++) {
    new Handle:messagesArr = CreateArray(MAX_CHAT_TEXT_LENGTH);
    PushArrayCell(g_messageQueueArray, messagesArr);

    new Handle:timerHandle = null;
    PushArrayCell(g_timerHandleArray, timerHandle);
  }
}

public APLRes:AskPluginLoad2(Handle:hPlugin, bool:bLate, String:strError[], maxErrors) {
  CreateNative("PrintQueuedHintText", Native_PrintQueuedHintText);
  RegPluginLibrary(THIS_PLUGIN_LIBRARY);
  return APLRes_Success;
}

public Native_PrintQueuedHintText(Handle:hPlugin, numParams) {
  new String:message[BUFFER_MESSAGE_LENGTH];

  new client = GetNativeCell(1);
  new strLen = 0;
  GetNativeStringLength(2, strLen);
  GetNativeString(2, message, strLen * 2); // increasing length twice due to special characters, eg "❱"
  new bool:replace = view_as<bool>(GetNativeCell(3));
  new maxLines = GetNativeCell(4);

  return PrintQueuedHintText(client, message, replace, maxLines);
}

public OnClientPostAdminCheck(client) {
  if (IsFakeClient(client)) {
    return;
  }

  ResetQueue(client);
  ResetTimer(client);
}

bool:PrintQueuedHintText(
  client,
  const String:message[],
  bool:replace,
  maxLines
) {
  if (!IsValidClient(client)) {
    return false;
  }
  if (strlen(message) < 1) {
    return false;
  }

  if (replace) {
    ResetQueue(client);
  }

  QueueMessage(client, message, maxLines);

  new String:outBuffer[BUFFER_MESSAGE_LENGTH];
  GetQueueAsBuffer(client, outBuffer, sizeof(outBuffer));

  PrintHintText(client, outBuffer);

  SetMessageTimer(client, HINT_TEXT_TIME_SECONDS);

  return true;
}

ResetQueue(client) {
  if (!IsClient(client)) {
    return;
  }

  new Handle:messagesArr = GetArrayCell(g_messageQueueArray, client);
  if (messagesArr != INVALID_HANDLE) {
    ClearArray(messagesArr);
  }
}

QueueMessage(client, const String:message[], maxLines) {
  if (!IsClient(client)) {
    return;
  }
  if (maxLines < 1) {
    return;
  }

  new Handle:messagesArr = GetArrayCell(g_messageQueueArray, client);
  if (messagesArr == INVALID_HANDLE) {
    return;
  }

  // remove first item
  if (GetArraySize(messagesArr) == maxLines) {
    RemoveFromArray(messagesArr, 0);
  }

  PushArrayString(messagesArr, message);
}

GetQueueAsBuffer(client, String:buffer[], bufferLength) {
  new Handle:messagesArr = GetArrayCell(g_messageQueueArray, client);
  if (messagesArr == INVALID_HANDLE) {
    return;
  }

  new size = GetArraySize(messagesArr);

  if (size == 0) {
    Format(buffer, bufferLength, "");
  } else {
    for (new i = 0; i < size; i++) {
      new String:msg[MAX_CHAT_TEXT_LENGTH];
      GetArrayString(messagesArr, i, msg, sizeof(msg));

      if (strlen(msg) > 0) {
        if (i == 0) {
          Format(buffer, bufferLength, "%s", msg);
        } else {
          Format(buffer, bufferLength, "%s\n%s", buffer, msg);
        }
      }
    }
  }
}

ResetTimer(client) {
  if (!IsClient(client)) {
    return;
  }

  new Handle:timerHandle = GetArrayCell(g_timerHandleArray, client);

  // if (timerHandle != INVALID_HANDLE && timerHandle != null) {
  //   KillTimer(timerHandle);
  //   timerHandle = null;
  // }
  delete timerHandle;
}

public Action:ResetQueueTimer(Handle:timer, any:client) {
  if (!IsClient(client)) {
    return Plugin_Stop;
  }

  new Handle:timerHandle = GetArrayCell(g_timerHandleArray, client);
  timerHandle = null; // https://forums.alliedmods.net/showpost.php?p=2418982&postcount=6
  SetArrayCell(g_timerHandleArray, client, timerHandle);

  ResetQueue(client);

  return Plugin_Stop;
}

SetMessageTimer(client, Float:time) {
  ResetTimer(client);

  new Handle:timerHandle = CreateTimer(time, ResetQueueTimer, client);
  SetArrayCell(g_timerHandleArray, client, timerHandle);
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
