#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

#include <hint_text_queue>
#define MESSAGE_PLUGIN_LIBRARY "hint_text_queue"

public Plugin:myinfo = {
  name = "Damage Display",
  description = "Show damage as queued hint text",
  author = "Geekrainian @ cuzus.games",
  version = "2023.01.16",
  url = "https://geekrainian.com",
};

#define TRANSLATIONS_FILE "damage_display.phrases"

new bool:g_isNativesLoaded = false;

public OnAllPluginsLoaded() {
	if (LibraryExists(MESSAGE_PLUGIN_LIBRARY)) {
		g_isNativesLoaded = true;
	}
}

public OnLibraryAdded(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = true;
  }
}

public OnLibraryRemoved(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = false;
  }
}

public OnPluginStart() {
  LoadTranslations(TRANSLATIONS_FILE);

  HookEvent("player_hurt", Event_PlayerHurt);
}

public Action:Event_PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast) {
  if (!g_isNativesLoaded) {
    return Plugin_Continue;
  }

  new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
  if (!IsValidClient(attacker) || IsFakeClient(attacker)) {
    return Plugin_Continue;
  }

  new victim = GetClientOfUserId(GetEventInt(event, "userid"));
  if (!IsValidClient(victim)) {
    return Plugin_Continue;
  }

  if (attacker != victim) {
    new dmgTaken = GetEventInt(event, "dmg_health");
    new health = GetEventInt(event, "health");

    new String:victimName[MAX_NAME_LENGTH];
    GetClientName(victim, victimName, sizeof(victimName));

    new String:buf[256];
    if (health == 0) {
      Format(buf, sizeof(buf), "%T", "kill", attacker, victimName);
    } else {
      Format(buf, sizeof(buf), "%T", "damage", attacker, dmgTaken, victimName);
    }

    PrintQueuedHintText(attacker, buf);

    // TODO: add hitgroup to message (convar)
    /*
    #define	HITGROUP_GENERIC	0
    #define	HITGROUP_HEAD		1
    #define	HITGROUP_CHEST		2
    #define	HITGROUP_STOMACH	3
    #define HITGROUP_LEFTARM	4
    #define HITGROUP_RIGHTARM	5
    #define HITGROUP_LEFTLEG	6
    #define HITGROUP_RIGHTLEG	7
    */
    // new hitgroup = GetEventInt(event, "hitgroup");
    // if (hitgroup == HITGROUP_CHEST) {
    // }
  }

  return Plugin_Continue;
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}
