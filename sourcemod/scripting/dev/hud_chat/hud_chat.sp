/*
 *  SM will continue filter chat messages for flood and display chat warnings like
 *  "[SM] You are flooding the server!"
 *  You can disable 'antiflood' plugin to rely only on chat from this plugin.
 */
#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

#include <hud_message_queue>
#define MESSAGE_PLUGIN_LIBRARY "hud_message_queue"

public Plugin:myinfo = {
  name = "HUD Chat",
  description = "Chat using HUD messages",
  author = "Geekrainian @ cuzus.games",
  version = "2023.01.10",
  url = "https://geekrainian.com",
};

/*
  TODO:
  option - dead player chat enabled (for public etc)
*/

#define MAX_CHAT_TEXT_LENGTH 192
#define BUFFER_MESSAGE_LENGTH 512

new Float:g_lastChatMessageTime[MAXPLAYERS + 1];

new Handle:cvar_hudChannel, Handle:cvar_hudPositionX, Handle:cvar_hudPositionY,
  Handle:cvar_hudColorR, Handle:cvar_hudColorG, Handle:cvar_hudColorB, Handle:cvar_messageTime,
  Handle:cvar_chatSound, Handle:cvar_sendToConsole, Handle:cvar_floodDelay;

new bool:g_isNativesLoaded = false;

new const String:SOUNDS[][] = {
  "sys_chat_message.wav", // KFPawnDamageSound/bullethitflesh2.wav
};

public OnAllPluginsLoaded() {
	if (LibraryExists(MESSAGE_PLUGIN_LIBRARY)) {
		g_isNativesLoaded = true;
	}
}

public OnLibraryAdded(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = true;
  }
}

public OnLibraryRemoved(const String:name[]) {
  if (StrEqual(name, MESSAGE_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = false;
  }
}

public OnPluginStart() {
  cvar_hudChannel = CreateConVar("sm_hudchat_hud_channel", "0", "", FCVAR_PROTECTED);
  cvar_hudPositionX = CreateConVar("sm_hudchat_position_x", "0.0235", "", FCVAR_PROTECTED);
  cvar_hudPositionY = CreateConVar("sm_hudchat_position_y", "0.6024", "", FCVAR_PROTECTED);
  cvar_hudColorR = CreateConVar("sm_hudchat_color_r", "200", "", FCVAR_PROTECTED); // TODO: replace 3 cvars with single line eg "200 200 200"
  cvar_hudColorG = CreateConVar("sm_hudchat_color_g", "200", "", FCVAR_PROTECTED);
  cvar_hudColorB = CreateConVar("sm_hudchat_color_b", "200", "", FCVAR_PROTECTED);
  cvar_messageTime = CreateConVar("sm_hudchat_message_time", "10.0", "", FCVAR_PROTECTED, true, 0.1);
  cvar_chatSound = CreateConVar("sm_hudchat_chat_sound", "1", "", FCVAR_PROTECTED);
  cvar_sendToConsole = CreateConVar("sm_hudchat_send_to_console", "1", "", FCVAR_PROTECTED);
  cvar_floodDelay = CreateConVar("sm_hudchat_flood_delay", "0.7", "", FCVAR_PROTECTED, true, 0.0);

  AddCommandListener(Command_Say, "say");
  AddCommandListener(Command_Say, "say2");
  AddCommandListener(Command_SayTeam, "say_team");
}

public OnMapStart() {
  if (GetConVarBool(cvar_chatSound)) {
    new String:buffer[PLATFORM_MAX_PATH];
    for (new i = 0, len = sizeof(SOUNDS); i < len; i++) {
      PrecacheSound(SOUNDS[i], true);
      Format(buffer, sizeof(buffer), "sound/%s", SOUNDS[i]);
      AddFileToDownloadsTable(buffer);
    }
  }
}

public Action:Command_Say(client, const String:command[], argc) {
  new String:argString[MAX_CHAT_TEXT_LENGTH];

  GetCmdArgString(argString, sizeof(argString));
  StripQuotes(argString);
  TrimString(argString);

  // do not display empty messages or commands
  if (strlen(argString) == 0 || String_StartsWith(argString, "/")) {
    return Plugin_Handled;
  }

  for (new p = 1; p <= MaxClients; p++) {
    ProcessChatMessage(p, argString);
  }

  // this blocks default chat display
  return Plugin_Handled;
}

public Action:Command_SayTeam(client, const String:command[], argc) {
  new String:argString[MAX_CHAT_TEXT_LENGTH];

  GetCmdArgString(argString, sizeof(argString));
  StripQuotes(argString);
  TrimString(argString);

  // do not display empty messages
  if (strlen(argString) == 0) {
    return Plugin_Handled;
  }

  new team1 = GetClientTeam(client);
  new team2 = -1;
  for (new p = 1; p <= MaxClients; p++) {
    team2 = IsValidPlayer(p) ? GetClientTeam(p) : -1;
    if (team1 == team2) {
      ProcessChatMessage(p, argString);
    }
  }

  // this blocks default chat display
  return Plugin_Handled;
}

bool:CheckAntiFlood(client) {
  new Float:lastChatMessageTime = g_lastChatMessageTime[client];
  new Float:delay = GetConVarFloat(cvar_floodDelay);
  if (delay > 0 && lastChatMessageTime > 0 && GetGameTime() - lastChatMessageTime < delay) {
    return false;
  }
  return true;
}

ProcessChatMessage(client, const String:message[]) {
  if (!IsValidPlayer(client)) {
    return;
  }

  if (!CheckAntiFlood(client)) {
    return;
  }

  new String:playerName[MAX_NAME_LENGTH];
  GetClientName(client, playerName, sizeof(playerName));

  new String:buffer[BUFFER_MESSAGE_LENGTH];
  Format(buffer, sizeof(buffer), "%s: %s", playerName, message);

  if (g_isNativesLoaded) {
    if (ShowQueuedHudText(
      client,
      GetConVarInt(cvar_hudChannel),
      buffer,
      GetConVarInt(cvar_hudColorR),
      GetConVarInt(cvar_hudColorG),
      GetConVarInt(cvar_hudColorB),
      GetConVarFloat(cvar_hudPositionX),
      GetConVarFloat(cvar_hudPositionY),
      GetConVarFloat(cvar_messageTime)
    )) {
      g_lastChatMessageTime[client] = GetGameTime();

      if (GetConVarBool(cvar_sendToConsole)) {
        PrintToConsole(client, buffer);
      }

      if (GetConVarBool(cvar_chatSound)) {
        new Float:volume = GetRandomFloat(0.25, 0.35);
        new pitch = GetRandomInt(93, 100);
        EmitSoundToClient(client, SOUNDS[0], _, _, _, _, volume, pitch);
      }
    }
  } else {
    PrintToChat(client, buffer);
  }
}

stock bool:IsValidPlayer(client) {
  return client > 0 && IsClientConnected(client) && IsClientInGame(client) && !IsFakeClient(client);
}

// SMLib
stock bool:String_StartsWith(const String:str[], const String:subString[]) {
	new n = 0;
	while (subString[n] != '\0') {
		if (str[n] == '\0' || str[n] != subString[n]) {
			return false;
		}
		n++;
	}
	return true;
}
