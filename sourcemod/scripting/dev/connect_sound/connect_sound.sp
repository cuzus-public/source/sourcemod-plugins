#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
  name = "Connect Sound",
};

new String:SOUNDS[][] = {
  "gamestartup1cut.wav", // gamestartup1cut.mp3
};

public OnPluginStart() {
}

public OnMapStart() {
  PrintToServer("> OnMapStart");
  PrecacheFiles();
}

public OnConfigsExecuted() {
  PrintToServer("> OnConfigsExecuted");
  // PrecacheFiles();
}

PrecacheFiles() {
  new String:buf[PLATFORM_MAX_PATH];

  for (new i = 0, len = sizeof(SOUNDS); i < len; i++) {
    PrecacheSound(SOUNDS[i], true);
    Format(buf, sizeof(buf), "sound/%s", SOUNDS[i]);
    AddFileToDownloadsTable(buf);
  }
}

public OnClientPostAdminCheck(client) {
  if (!IsFakeClient(client)) {
    PrintToServer("@ OnClientPostAdminCheck %d", client);
  }
  // SendSound(client);
}

public bool:OnClientConnect(client) {
  if (!IsFakeClient(client)) {
    PrintToServer("@ OnClientConnect %d", client);
  }
  // SendSound(client);
  return true;
}

public OnClientAuthorized(client) {
  if (!IsFakeClient(client)) {
    PrintToServer("@ OnClientAuthorized %d", client);
  }
  SendSound(client);
}

SendSound(client) {
  CreateTimer(0.1, SendSoundTask, client);
}

public Action:SendSoundTask(Handle:timer, any:client) {
  if (!IsFakeClient(client)) {
    new String:buffer[128];
    Format(buffer, sizeof(buffer), "play *%s", SOUNDS[0]); // GetRandomInt(1, sizeof(SOUNDS))

    ClientCommand(client, buffer);

    // EmitSoundToClient(client, SOUNDS[0]);

    PrintToServer("@ %s %d", buffer, client);
  }
}

// public OnClientOnClientDisconnect(client) {
//   if (!IsFakeClient(client)) {
//     ClientCommand(client, "stopsound");
//   }
// }
