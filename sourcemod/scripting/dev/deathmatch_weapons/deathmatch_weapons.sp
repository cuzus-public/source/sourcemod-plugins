#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools_functions>

public Plugin:myinfo = {
  name = "Deathmatch Weapons (Menu)",
  version = "2023.01.18",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

#define CONFIG_FILE_PATH "configs/deathmatch_weapons.cfg"
#define TRANSLATIONS_FILE "deathmatch_weapons.phrases"
#define MAX_MESSAGE_LENGTH 255

new Handle:g_weaponsPrimary, Handle:g_weaponsSecondary, Handle:g_nades;
new Handle:g_playersLastPrimary, Handle:g_playersLastSecondary;
new bool:g_sameWeapons[MAXPLAYERS + 1];
new bool:g_menuShown[MAXPLAYERS + 1];

new Handle:cvar_giveNadeMode;

public OnPluginStart() {
  LoadTranslations(TRANSLATIONS_FILE);

  g_weaponsPrimary = CreateArray(48);
  g_weaponsSecondary = CreateArray(48);
  g_nades = CreateArray(48);

  g_playersLastPrimary = CreateArray(24);
  g_playersLastSecondary = CreateArray(24);

  for (new i = 0; i <= MAXPLAYERS + 1; i++) {
    PushArrayString(g_playersLastPrimary, "");
    PushArrayString(g_playersLastSecondary, "");
  }

  LoadConfig();

  cvar_giveNadeMode = CreateConVar("sm_deathmatch_weapons_give_nade", "1", "0 - disabled, 1 - random nade, 2 - he grenade", FCVAR_PROTECTED);

  HookEvent("player_spawn", Event_PlayerSpawn, EventHookMode_Post);
  HookEvent("round_start", Event_RoundStart, EventHookMode_PostNoCopy); // player_spawn does not trigger on round start
  HookEvent("player_death", Event_PlayerDeath, EventHookMode_Post);

  AddCommandListener(Command_Say, "say");
  AddCommandListener(Command_Say, "say_team");
}

public Action:Command_Say(client, const String:command[], argc) {
  new String:argString[256];

  GetCmdArgString(argString, sizeof(argString));
  StripQuotes(argString);
  TrimString(argString);

  if ((StrEqual(argString, "/guns", false) || StrEqual(argString, "/weapons", false)) && !g_menuShown[client]) {
    MainMenu(client);
  }

  return Plugin_Continue;
}

public OnClientDisconnect(client) {
  g_sameWeapons[client] = false;
  g_menuShown[client] = false;
}

LoadConfig() {
  new String:configPath[PLATFORM_MAX_PATH];
  BuildPath(Path_SM, configPath, sizeof(configPath), CONFIG_FILE_PATH);

  if (FileExists(configPath) == false) {
    Debug("Unable to find config file: %s", configPath);
    return;
  }

  new Handle:hFile = OpenFile(configPath, "r");
  new String:line[64];
  new bool:readPrimary, bool:readSecondary, bool:readNades;

  while (!IsEndOfFile(hFile) && ReadFileLine(hFile, line, sizeof(line))) {
    if (StrEqual(line, "\0") || StrEqual(line, "\n")) {
      continue;
    }

    if (String_StartsWith(line, "#")) {
      if (StrContains(line, "primary", false) != -1) {
        readSecondary = false;
        readPrimary = true;
        readNades = false;
      } else if (StrContains(line, "secondary", false) != -1) {
        readPrimary = false;
        readSecondary = true;
        readNades = false;
      } else if (StrContains(line, "nades", false) != -1) {
        readPrimary = false;
        readSecondary = false;
        readNades = true;
      }
    } else if (StrContains(line, ":") != -1) {
      if (readPrimary) {
        PushArrayString(g_weaponsPrimary, line);
      } else if (readSecondary) {
        PushArrayString(g_weaponsSecondary, line);
      } else if (readNades) {
        PushArrayString(g_nades, line);
      }
    }
  }

  CloseHandle(hFile);
}

public Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast) {
  for (new client = 1; client <= MaxClients; client++) {
    if (!IsValidClient(client)) {
      continue;
    }
    new team = GetClientTeam(client);
    if (!(team == CS_TEAM_CT || team == CS_TEAM_CT)) {
      continue;
    }
    CreateTimer(0.2, PlayerSpawnTimer, client);
  }
}

public Event_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));
  if (!IsValidClient(client)) {
    return;
  }
  new team = GetClientTeam(client);
  if (!(team == CS_TEAM_CT || team == CS_TEAM_CT)) {
    return;
  }

  CreateTimer(0.2, PlayerSpawnTimer, client);
}

public Action:PlayerSpawnTimer(Handle:timer, any:client) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }

  if (!IsFakeClient(client)) {
    if (g_sameWeapons[client] && GiveSameWeapons(client)) {
      // do nothing
    } else {
      MainMenu(client);
    }
  } else {
    GiveRandomWeapons(client);
  }
}

public Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));
  if (!IsValidClient(client)) {
    return;
  }

  g_menuShown[client] = false;

  if (!IsFakeClient(client)) {
    CancelClientMenu(client, true, INVALID_HANDLE);
  }
}

MainMenu(client) {
  new Handle:menu = CreateMenu(MainMenuHandler);
  new String:buf[64];

  Format(buf, sizeof(buf), "%T", "select_weapons", client);
  SetMenuTitle(menu, buf);

  Format(buf, sizeof(buf), "%T", "new_weapons", client);
  AddMenuItem(menu, "", buf);

  new bool:hasLastWeapons = HasLastWeaponsStored(client);

  Format(buf, sizeof(buf), "%T", "same_weapons", client);
  AddMenuItem(menu, "", buf, hasLastWeapons ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);

  Format(buf, sizeof(buf), "%T", "select_weapons_save", client);
  Format(buf, sizeof(buf), "%s%s", buf, g_sameWeapons[client] ? "✓" : "");
  AddMenuItem(menu, "", buf, hasLastWeapons ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);

  Format(buf, sizeof(buf), "%T", "random_weapons", client);
  AddMenuItem(menu, "", buf);

  DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public MainMenuHandler(Handle:menu, MenuAction:action, client, option) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }

  if (action == MenuAction_Select) {
    CloseHandle(menu);

    if (option == 0) {
      PrimaryWeaponsMenu(client);
    } else if (option == 1 || option == 2) {
      GiveSameWeapons(client);

      if (option == 2) {
        g_sameWeapons[client] = !g_sameWeapons[client];
        if (g_sameWeapons[client]) {
          new String:buf[256];
          Format(buf, sizeof(buf), "%T", "menu_hint", client);
          Client_PrintKeyHintText(client, buf);
        }
      }
    } else if (option == 3) {
      GiveRandomWeapons(client);
    }

    GiveNade(client);

    g_menuShown[client] = true;
  }
}

PrimaryWeaponsMenu(client) {
  new Handle:menu = CreateMenu(PrimaryWeaponsMenuHandler);

  new String:buf[64];

  Format(buf, sizeof(buf), "%T", "primary_weapon", client);
  SetMenuTitle(menu, buf);

  new String:weaponName[2][32];
  new size = GetArraySize(g_weaponsPrimary);

  for (new i = 0; i < size; i++) {
    GetArrayString(g_weaponsPrimary, i, buf, sizeof(buf));
    ExplodeString(buf, ":", weaponName, sizeof weaponName, sizeof weaponName[]);
    if (strlen(weaponName[0]) > 0) {
      AddMenuItem(menu, weaponName[0], weaponName[1]);
    }
  }

  SetMenuExitBackButton(menu, true);

  DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public PrimaryWeaponsMenuHandler(Handle:menu, MenuAction:action, client, option) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }

  if (action == MenuAction_Select) {
    new String:info[32];
    GetMenuItem(menu, option, info, sizeof(info));
    WeaponSelected(client, info, CS_SLOT_PRIMARY);
    SetArrayString(g_playersLastPrimary, client, info);
    CloseHandle(menu);
    SecondaryWeaponsMenu(client);
  }
  else if (action == MenuAction_Cancel && option == MenuCancel_ExitBack) {
    CloseHandle(menu);
    MainMenu(client);
	}
}

SecondaryWeaponsMenu(client) {
  new Handle:menu = CreateMenu(SecondaryWeaponsMenuHandler);

  new String:buf[64];

  Format(buf, sizeof(buf), "%T", "secondary_weapon", client);
  SetMenuTitle(menu, buf);

  new String:weaponName[2][32];
  new size = GetArraySize(g_weaponsSecondary);

  for (new i = 0; i < size; i++) {
    GetArrayString(g_weaponsSecondary, i, buf, sizeof(buf));
    ExplodeString(buf, ":", weaponName, sizeof weaponName, sizeof weaponName[]);
    if (strlen(weaponName[0]) > 0) {
      AddMenuItem(menu, weaponName[0], weaponName[1]);
    }
  }

  SetMenuExitBackButton(menu, true);

  DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public SecondaryWeaponsMenuHandler(Handle:menu, MenuAction:action, client, option) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }

  if (action == MenuAction_Select) {
    new String:info[32];
    GetMenuItem(menu, option, info, sizeof(info));
    WeaponSelected(client, info, CS_SLOT_SECONDARY);
    SetArrayString(g_playersLastSecondary, client, info);
    CloseHandle(menu);
  }
  else if (action == MenuAction_Cancel && option == MenuCancel_ExitBack) {
    CloseHandle(menu);
    PrimaryWeaponsMenu(client);
	}
}

WeaponSelected(client, const String:weaponName[], slot) {
  if (IsValidClient(client) && IsPlayerAlive(client)) {
    new String:cssWeapon[64];
    Format(cssWeapon, sizeof(cssWeapon), "weapon_%s", weaponName);
    RemoveWeapon(client, slot);
    GivePlayerItem(client, cssWeapon);
  }
}

bool:HasLastWeaponsStored(client) {
  new String:weapon[24];
  GetArrayString(g_playersLastPrimary, client, weapon, sizeof(weapon));
  return strlen(weapon) > 0;
}

bool:GiveSameWeapons(client) {
  new bool:succeed;
  if (IsValidClient(client) && IsPlayerAlive(client)) {
    new String:weapon[24];
    GetArrayString(g_playersLastPrimary, client, weapon, sizeof(weapon));
    if (strlen(weapon) > 0) {
      WeaponSelected(client, weapon, CS_SLOT_PRIMARY);
      succeed = true;
    }
    GetArrayString(g_playersLastSecondary, client, weapon, sizeof(weapon));
    if (strlen(weapon) > 0) {
      WeaponSelected(client, weapon, CS_SLOT_SECONDARY);
      succeed = true;
    }
  }
  return succeed;
}

GiveRandomWeapons(client) {
  if (IsValidClient(client) && IsPlayerAlive(client)) {
    new String:buf[64], String:weaponName[2][32];
    GetArrayString(g_weaponsPrimary, GetRandomInt(0, GetArraySize(g_weaponsPrimary) - 1), buf, sizeof(buf));
    ExplodeString(buf, ":", weaponName, sizeof weaponName, sizeof weaponName[]);
    WeaponSelected(client, weaponName[0], CS_SLOT_PRIMARY);
    SetArrayString(g_playersLastPrimary, client, weaponName[0]);

    GetArrayString(g_weaponsSecondary, GetRandomInt(0, GetArraySize(g_weaponsSecondary) - 1), buf, sizeof(buf));
    ExplodeString(buf, ":", weaponName, sizeof weaponName, sizeof weaponName[]);
    WeaponSelected(client, weaponName[0], CS_SLOT_SECONDARY);
    SetArrayString(g_playersLastSecondary, client, weaponName[0]);
  }
}

GiveNade(client) {
  new mode = GetConVarInt(cvar_giveNadeMode);
  if (mode == 0) {
    return;
  }

  if (mode == 1) {
    new String:buf[64], String:weaponName[2][32];
    GetArrayString(g_nades, GetRandomInt(0, GetArraySize(g_nades) - 1), buf, sizeof(buf));
    ExplodeString(buf, ":", weaponName, sizeof weaponName, sizeof weaponName[]);
    WeaponSelected(client, weaponName[0], CS_SLOT_GRENADE);
  }
  else if (mode == 2) {
    WeaponSelected(client, "hegrenade", CS_SLOT_GRENADE);
  }
}

stock RemoveWeapon(client, slot = 0) {
  new ent = GetPlayerWeaponSlot(client, slot);
  if (IsValidEdict(ent)) {
    RemovePlayerItem(client, ent);
    RemoveEdict(ent);
  }
}

stock Debug(String:format[], any:...) {
  new String:pluginName[PLATFORM_MAX_PATH];
  GetPluginFilename(INVALID_HANDLE, pluginName, sizeof(pluginName));

  new String:buffer[2048];
  VFormat(buffer, sizeof(buffer), format, 2);

  PrintToServer("<%s> %s", pluginName, buffer);
}

stock bool:IsClient(client) {
	return (client > 0 && client < MaxClients + 1);
}

stock bool:IsValidClient(client) {
	return (IsClient(client) && IsClientConnected(client) && IsClientInGame(client));
}

// SMLib
stock bool:String_StartsWith(const String:str[], const String:subString[]) {
	new n = 0;
	while (subString[n] != '\0') {
		if (str[n] == '\0' || str[n] != subString[n]) {
			return false;
		}
		n++;
	}
	return true;
}
stock bool:Client_PrintKeyHintText(client, const String:format[], any:...) {
	new Handle:userMessage = StartMessageOne("KeyHintText", client);
	if (userMessage == INVALID_HANDLE) {
		return false;
	}

	new String:buffer[MAX_MESSAGE_LENGTH];

	SetGlobalTransTarget(client);
	VFormat(buffer, sizeof(buffer), format, 3);

	if (GetFeatureStatus(FeatureType_Native, "GetUserMessageType") == FeatureStatus_Available && GetUserMessageType() == UM_Protobuf) {
		PbSetString(userMessage, "hints", format);
	} else {
		BfWriteByte(userMessage, 1);
		BfWriteString(userMessage, buffer);
	}

	EndMessage();

	return true;
}
