#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <stocks2023>

public Plugin:myinfo = {
  name = "Slow Motion",
  version = "2023.01.25",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

/*
  TODO:
  compare time scale/delay with KF, also check effects in game (e.g. how bullets fly)

sv_cheats:
- Turn it on and then back off again immediately after you do the cheat thing
- Only remove the cheat flag from the commands you're trying to allow rather than enabling everything by changing sv_cheats itself (I'm not sure if this is only for convars or if commands work too)
- Utilise ConVar.ReplicateToClient to prevent other clients getting access to the thing (when you have to change sv_cheats itself)
- Reimplement the cheat-protected feature yourself (e.g. sm_noclip)

And SDKHook_FireBulletPost would stop the animation and sound, that doesnt seem to correspond to client side prediction
*/

new const String:SOUNDS[][] = {
  "sys_matrix_enter.wav", // kf_playerglobalsnd/Zedtime_Enter.wav
  "sys_matrix_exit.wav", // kf_playerglobalsnd/Zedtime_Exit.wav
};

new bool:g_isInMatrix;

new Handle:cvar_svCheats, Handle:cvar_matrixChance, Handle:cvar_matrixTimeSeconds, Handle:cvar_matrixScale;

public OnPluginStart() {
  cvar_svCheats = FindConVar("sv_cheats");

  cvar_matrixChance = CreateConVar("sm_slow_motion_matrix_chance", "5", "Chance to enter the matrix", FCVAR_PROTECTED, true, 0.0);
  cvar_matrixTimeSeconds = CreateConVar("sm_slow_motion_matrix_time_seconds", "3.5", "Time in matrix", FCVAR_PROTECTED, true, 0.0);
  cvar_matrixScale = CreateConVar("sm_slow_motion_matrix_scale", "0.25", "", FCVAR_PROTECTED, true, 0.1, true, 1.0);

  HookEvent("player_death", Event_PlayerDeath);
  HookEvent("bomb_defused", Event_BombDefused);
  HookEvent("bomb_exploded", Event_BombExplode);
  HookEvent("round_end", Event_RoundEnd);

  // late load support
  for (new i = 1; i <= MaxClients; i++) {
    if (IsClientInGame(i)) {
      OnClientPutInServer(i);
    }
  }
}

public OnClientPutInServer(client) {
	SDKHook(client, SDKHook_TraceAttack, Hook_TraceAttack);
}

// prevent bullet damage during matrix time
public Action:Hook_TraceAttack(victim, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3]) {
  if (g_isInMatrix) {
    return Plugin_Handled;
  }
  return Plugin_Continue;
}

public OnMapStart() {
  new String:buffer[PLATFORM_MAX_PATH];
  for (new i = 0, len = sizeof(SOUNDS); i < len; i++) {
    PrecacheSound(SOUNDS[i], true);
    Format(buffer, sizeof(buffer), "sound/%s", SOUNDS[i]);
    AddFileToDownloadsTable(buffer);
  }

  ConVarSetup();
}

ConVarSetup() {
	new flags = GetConVarFlags(cvar_svCheats);
	flags &= ~FCVAR_NOTIFY;
	SetConVarFlags(cvar_svCheats, flags);
	if (GetConVarInt(cvar_svCheats) == 1) {
		SetConVarInt(cvar_svCheats, 0, true, false);
	}
}

public Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new victimClient = GetClientOfUserId(GetEventInt(event, "userid"));
  if (!IsValidClient(victimClient)) {
    return;
  }

  new attackerClient = GetClientOfUserId(GetEventInt(event, "attacker"));
  if (!attackerClient || attackerClient == victimClient) {
    return;
  }

  if (GetRandomInt(1, 100) <= GetConVarInt(cvar_matrixChance)) {
    EnterTheMatrix();
  }
}

public Action:Event_RoundEnd(Handle:event, const String:name[], bool:dontBroadcast) {
  ExitTheMatrix();
}

public Action:Event_BombDefused(Handle:event, const String:name[], bool:dontBroadcast) {
  ExitTheMatrix();
}

public Action:Event_BombExplode(Handle:event, const String:name[], bool:dontBroadcast) {
  ExitTheMatrix();
}

new Float:g_timeScale;
// TODO: enter multiple times like in KF (extend time), max <= 3(cvar)
EnterTheMatrix() {
  if (g_isInMatrix) {
    return;
  }
  new Float:matrixTime = GetConVarFloat(cvar_matrixTimeSeconds);
  if (matrixTime < 1) {
    return;
  }

  g_isInMatrix = true;

  new Float:volume = GetRandomFloat(0.95, 1.0);
  EmitSoundToAll(SOUNDS[0], _, _, _, _, volume);

  SetConVarInt(cvar_svCheats, 1, true, false);

  g_timeScale = 1.0;
  new Float:tickScale = (1.0 - GetConVarFloat(cvar_matrixScale)) / 3; // (1.0 - 0.25) / 3 = 0.25
  // PrintToChatAll("START tickScale %f", tickScale);

  CreateTimer(0.1, EnterTheMatrixStepTimer, tickScale);

  ServerCommand("host_timescale %f", g_timeScale);

  new Float:matrixScale = GetConVarFloat(cvar_matrixScale);
  CreateTimer(matrixTime * matrixScale, ExitTheMatrixTimer);
}

public Action:EnterTheMatrixStepTimer(Handle:timer, any:tickScale) {
  new Float:matrixScale = GetConVarFloat(cvar_matrixScale);
  // PrintToChatAll("EnterTheMatrixStepTimer  matrixScale %f  timeScale %f  tickScale %f", matrixScale, g_timeScale, tickScale);

  if (g_timeScale > matrixScale) {
    new Float:tickScaleSafe = tickScale;
    g_timeScale = g_timeScale - tickScaleSafe;

    // PrintToChatAll("g_timeScale after tick  %f", g_timeScale);

    if (g_timeScale < matrixScale) {
      g_timeScale = matrixScale;
    }

    ServerCommand("host_timescale %f", g_timeScale);
    CreateTimer(0.1, EnterTheMatrixStepTimer, tickScaleSafe);

    // PrintToChatAll("EnterTheMatrixStepTimer SET value");
  }
}

public Action:ExitTheMatrixTimer(Handle:timer, any:data) {
  ExitTheMatrix();
}

ExitTheMatrix() {
  if (!g_isInMatrix) {
    return;
  }

  new Float:volume = GetRandomFloat(0.95, 1.0);
  EmitSoundToAll(SOUNDS[1], _, _, _, _, volume);

  new Float:tickScale = (1.0 - GetConVarFloat(cvar_matrixScale)) / 2;
  // PrintToChatAll("EXIT tickScale %f", tickScale);
  CreateTimer(0.1, ExitTheMatrixStepTimer, tickScale);
}

public Action:ExitTheMatrixStepTimer(Handle:timer, any:tickScale) {
  // PrintToChatAll("ExitTheMatrixStepTimer  g_timeScale %f  tickScale %f", g_timeScale, tickScale);

  if (g_timeScale < 1.0) {
    new Float:tickScaleSafe = tickScale;
    g_timeScale = g_timeScale + tickScaleSafe;

    if (g_timeScale > 1.0) {
      g_timeScale = 1.0;
    }

    ServerCommand("host_timescale %f", g_timeScale);

    if (g_timeScale == 1.0) {
      SetConVarInt(cvar_svCheats, 0, true, false);
      ServerCommand("host_timescale 1");
      // PrintToChatAll("FULL EXIT 1");

      g_isInMatrix = false;
    } else {
      CreateTimer(0.1, ExitTheMatrixStepTimer, tickScaleSafe);
    }
  }
}
