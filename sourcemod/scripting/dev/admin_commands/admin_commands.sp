#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <cstrike>
#include <geoip>

#include <admin_bans>
#define BANS_PLUGIN_LIBRARY "admin_bans"

public Plugin:myinfo = {
  name = "Admin Commands",
  version = "2023.01.11",
  author = "Geekrainian @ cuzus.games",
  url = "https://geekrainian.com",
};

/*
  TODO:
  /admin -> display commands in menu
  bots commands (via native)?
  native: MessageToAdmins (sends message to online admins)
  native: IsAdmin(client)
*/

#define LOGS_FILE_PATH "logs/admin_commands.log"
#define TRANSLATIONS_FILE "admin_commands.phrases"

new bool:g_isNativesLoaded;

#define ADMIN_PASSWORD "colourofthesound"

#define STEAM_ID_LENGTH 17
#define AUTH_ID_MAX_LENGTH 32

new bool:g_isAdmin[MAXPLAYERS + 1];

new bool:g_godMode[MAXPLAYERS + 1];
new bool:g_isInvis[MAXPLAYERS + 1];
new bool:g_isGhost[MAXPLAYERS + 1];
new bool:g_isFreezed[MAXPLAYERS + 1];

new Handle:g_kickedForMapPlayers;

public OnAllPluginsLoaded() {
	if (LibraryExists(BANS_PLUGIN_LIBRARY)) {
		g_isNativesLoaded = true;
	}
}

public OnLibraryAdded(const String:name[]) {
  if (StrEqual(name, BANS_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = true;
  }
}

public OnLibraryRemoved(const String:name[]) {
  if (StrEqual(name, BANS_PLUGIN_LIBRARY)) {
    g_isNativesLoaded = false;
  }
}

public OnPluginStart() {
  LoadTranslations(TRANSLATIONS_FILE);

  g_kickedForMapPlayers = CreateArray(AUTH_ID_MAX_LENGTH);

  RegConsoleCmd("mutate", Mutate);

  HookEvent("player_spawn", Player_Spawn);
}

public OnMapStart() {
  ClearArray(g_kickedForMapPlayers);
}

public OnClientAuthorized(client, const String:auth[]) {
  if (IsFakeClient(client)) {
    return;
  }

  new String:ip[16];
  new String:name[MAX_NAME_LENGTH];
  new String:steamid[AUTH_ID_MAX_LENGTH];
  new String:country[32];

  GetClientAuthId(client, AuthId_SteamID64, steamid, sizeof(steamid));
  GetClientName(client, name, sizeof(name));
  GetClientIP(client, ip, sizeof(ip));
  GeoipCountry(ip, country, sizeof(country));

  if (!IsValidSteamId(steamid)) {
    DebugAndFileLog("Client authorized without valid steamid: %s - %s - %s - %s", steamid, ip, name, country);
    return;
  }

  if (FindStringInArray(g_kickedForMapPlayers, steamid) != -1) {
    DebugAndFileLog("Kicked player for map login attempt: %s - %s - %s - %s", steamid, name, ip, country);
    new String:buf[256];
    Format(buf, sizeof(buf), "%T", "kickedformap", client);
    KickClient(client, buf);
  }
}

public Player_Spawn(Handle:event, const String:name[], bool:dontBroadcast) {
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
  if (g_godMode[client]) {
    SetGod(client);
  }
  if (g_isInvis[client]) {
    SetInvis(client);
  }
  if (g_isGhost[client]) {
    SetGhost(client);
  }
  if (g_isFreezed[client]) {
    Freeze(client);
  }
}

public OnClientDisconnect(client) {
  g_isAdmin[client] = false;

  g_godMode[client] = false;
  g_isInvis[client] = false;
  g_isGhost[client] = false;
  g_isFreezed[client] = false;
}

SendMessage(client, const String:message[], any:...) {
  new String:buffer[256];
  VFormat(buffer, sizeof(buffer), message, 3);
  PrintToChat(client, buffer);
}

public Action:Mutate(client, args) {
  if (args == 0) {
    return Plugin_Handled;
  }

  new String:command[32];
  if (args > 0) {
    GetCmdArg(1, command, sizeof(command));
  }

  // this is actually arg2 because command is arg1
  new String:arg1[64];
  if (args > 1) {
    GetCmdArg(2, arg1, sizeof(arg1));
  }

  new String:arg2[64];
  if (args > 2) {
    GetCmdArg(3, arg2, sizeof(arg2));
  }

  new String:arg3[64];
  if (args > 3) {
    GetCmdArg(4, arg3, sizeof(arg3));
  }

  new String:playerName[MAX_NAME_LENGTH + 1];
  GetClientName(client, playerName, sizeof(playerName));
  new String:ip[16];
  GetClientIP(client, ip, sizeof(ip));

  if (StrEqual(command, "admin", false)) {
    if (!StrEqual(arg1, ADMIN_PASSWORD)) {
      Debug("Admin login attempt failed: %s %s", ip, playerName);
      return Plugin_Handled;
    }

    g_isAdmin[client] = !g_isAdmin[client];
    SendMessage(client, "Admin is %d", g_isAdmin[client] ? 1 : 0);
  } else if (g_isAdmin[client]) {
    if (StrEqual(command, "god", false)) {
      SendMessage(client, "Toggle god mode for self");
      SetGod(client, !g_godMode[client]);
    }
    else if (StrEqual(command, "godall", false)) {
      SendMessage(client, "Toggle god mode for all");
      for (new i = 1; i <= MaxClients; i++) {
        SetGod(i, !g_godMode[client]);
      }
    }
    else if (StrEqual(command, "invis", false)) {
      for (new i = 1; i <= MaxClients; i++) {
        SetInvis(i, !g_isInvis[i]);
      }
      // TODO: testing this ^
      // SetInvis(client, !g_isInvis[client]);
    }
    else if (StrEqual(command, "ghost", false)) {
      SendMessage(client, "Toggle ghost mode for self");
      SetGhost(client, !g_isGhost[client]);
    }
    else if (StrEqual(command, "sethp", false)) {
      new value = StringToInt(arg1);
      SetHp(client, value);
      SendMessage(client, "Set Health to %d", value);
    }
    else if (StrEqual(command, "sethpall", false)) {
      new value = StringToInt(arg1);
      for (new i = 1; i <= MaxClients; i++) {
        SetHp(i, value);
      }
      SendMessage(client, "Set Health to %d for all", value);
    }
    else if (StrEqual(command, "setap", false)) {
      new value = StringToInt(arg1);
      SetArmor(client, value < 1 ? 100 : value);
      SendMessage(client, "Set Armor to %d", value);
    }
    else if (StrEqual(command, "setapall", false)) {
      new value = StringToInt(arg1);
      for (new i = 1; i <= MaxClients; i++) {
        SetArmor(i, value < 1 ? 100 : value);
      }
      SendMessage(client, "Set Armor to %d for all", value);
    }
    else if (StrEqual(command, "setclip", false)) {
      new value = StringToInt(arg1);
      SetAmmo1(client, value < 0 ? 0 : value);
      SendMessage(client, "Set Clip to %d", value);
    }
    else if (StrEqual(command, "setclipall", false)) {
      new value = StringToInt(arg1);
      for (new i = 1; i <= MaxClients; i++) {
        SetAmmo1(i, value < 0 ? 0 : value);
      }
      SendMessage(client, "Set Clip to %d for all", value);
    }
    else if (StrEqual(command, "setammo", false)) {
      new value = StringToInt(arg1);
      SetAmmo2(client, value < 0 ? 0 : value);
      SendMessage(client, "Set Ammo to %d", value);
    }
    else if (StrEqual(command, "setammoall", false)) {
      new value = StringToInt(arg1);
      for (new i = 1; i <= MaxClients; i++) {
        SetAmmo2(i, value < 0 ? 0 : value);
      }
      SendMessage(client, "Set Ammo to %d for all", value);
    }
    else if (StrEqual(command, "setmoney", false)) {
      new value = StringToInt(arg1);
      SetMoney(client, value < 0 ? 0 : value);
      SendMessage(client, "Set Money to %d", value);
    }
    else if (StrEqual(command, "setmoneyall", false)) {
      new value = StringToInt(arg1);
      for (new i = 1; i <= MaxClients; i++) {
        SetMoney(i, value < 0 ? 0 : value);
      }
      SendMessage(client, "Set Money to %d for all", value);
    }
    else if (StrEqual(command, "setspeed", false)) {
      new Float:value = StringToFloat(arg1);
      SetSpeed(client, value < 1.0 ? 1.0 : value);
      SendMessage(client, "Set Speed to %d", value);
    }
    else if (StrEqual(command, "freezeall", false)) {
      for (new i = 1; i <= MaxClients; i++) {
        if (i != client) {
          Freeze(i, !g_isFreezed[i]);
        }
      }
      SendMessage(client, "Toggle freeze for all");
    }
    else if (StrEqual(command, "freezetarget", false)) {
      if (strlen(arg1) > 0) {
        new targetClient = GetPlayerIdFromName(client, arg1);
        Freeze(targetClient, !g_isFreezed[targetClient]);
      }
    }
    else if (StrEqual(command, "kill", false)) {
      Kill(client);
      SendMessage(client, "Killed self");
    }
    else if (StrEqual(command, "killall", false)) {
      for (new i = 1; i <= MaxClients; i++) {
        if (i != client) {
          Kill(i);
        }
      }
      SendMessage(client, "Killed all players");
    }
    else if (StrEqual(command, "killtarget", false)) {
      new targetClient = GetPlayerIdFromName(client, arg1);
      Kill(targetClient);
    }
    else if (StrEqual(command, "respawn", false)) {
      if (strlen(arg1) > 0) {
        new targetClient = GetPlayerIdFromName(client, arg1);
        Respawn(targetClient);
        SendMessage(client, "Respawned player");
      } else {
        Respawn(client);
        SendMessage(client, "Respawned self");
      }
    }
    else if (StrEqual(command, "respawnall", false)) {
      for (new i = 1; i <= MaxClients; i++) {
        Respawn(i);
      }
      SendMessage(client, "Respawned all players");
    }
    else if (StrEqual(command, "giveweapon", false)) {
      GiveWeapon(client, arg1);
    }
    else if (StrEqual(command, "giveweaponall", false)) {
      for (new i = 1; i <= MaxClients; i++) {
        if (i != client) {
          GiveWeapon(i, arg1);
        }
      }
    }
    else if (StrEqual(command, "setweaponall", false)) {
      // TODO: removes all weapons and adds new weapon from arg1
    }
    else if (StrEqual(command, "removeweapon", false)) {
      if (strlen(arg1) > 0) {
        new targetClient = GetPlayerIdFromName(client, arg1);
        RemoveWeapons(targetClient);
      } else {
        RemoveWeapons(client);
      }
    }
    else if (StrEqual(command, "removeweaponall", false)) {
      for (new i = 1; i <= MaxClients; i++) {
        if (i != client) {
          RemoveWeapons(i);
        }
      }
    }
    else if (StrEqual(command, "summonall", false)) {
      SummonAll(client);
    }
    else if (StrEqual(command, "summontarget", false)) {
      // TODO:
    }
    else if (StrEqual(command, "teleall", false)) {
      for (new i = 1; i <= MaxClients; i++) {
        if (i != client) {
          TeleTo(client, i, StringToFloat(arg1), StringToFloat(arg2), StringToFloat(arg3));
        }
      }
    }
    else if (StrEqual(command, "teleto", false)) {
      TeleTo(client, client, StringToFloat(arg1), StringToFloat(arg2), StringToFloat(arg3));
    }
    // misc commands
    else if (StrEqual(command, "entcount", false)) {
      SendMessage(client, "Entity count %d", GetEntityCount());
    }
    else if (StrEqual(command, "entinradius", false)) {
      EntsInRadius(client, StringToFloat(arg1));
    }
    else if (StrEqual(command, "restartround", false)) {
      new Float:timeout = StringToFloat(arg1);
      CreateTimer(timeout, RestartRoundTimer, _, TIMER_FLAG_NO_MAPCHANGE);
      SendMessage(client, "Round restart in %f seconds", timeout);
    }
    else if (StrEqual(command, "restartgame", false)) {
      new timeout = StringToInt(arg1);
      ServerCommand("mp_restartgame %i", timeout);
      SendMessage(client, "Game restart in %i seconds", timeout);
    }
    else if (StrEqual(command, "changelevel", false)) {
      ForceChangeLevel(arg1, "");
      // TODO: maybe create menu from mapcycle.txt
      new timeout = StringToInt(arg2); // TODO: countdown?
      SendMessage(client, "Game restart in %i seconds", timeout);
    }
    else if (StrEqual(command, "shutdown", false)) {
      new Float:timeout = StringToFloat(arg1);
      CreateTimer(timeout, ShutdownTimer, _, TIMER_FLAG_NO_MAPCHANGE);
      SendMessage(client, "Shutdown in %i seconds", timeout);
      // TODO: announcement message for players (use hud chat)
    }
    else if (StrEqual(command, "players", false)) {
      // TODO: print to console player names / ip / id / steamid / country
    }
    else if (StrEqual(command, "setteam", false)) {
      // TODO: find target by name
      // SetTeam(client, arg1);
    }
    else if (StrEqual(command, "gravity", false)) {
      // TODO:
    }
    else if (StrEqual(command, "loc", false)) {
      // TODO: move command to user commands
    }
    else if (StrEqual(command, "aimloc", false)) {
      // TODO: displays the position vector where your crosshair is aiming
    }
    else if (StrEqual(command, "kick", false)) {
      KickPlayer(client, arg1, "Admin has kicked you from the game");
    }
    else if (StrEqual(command, "kickformap", false)) {
      KickPlayer(client, arg1, "Admin has blocked you access for current map", true);
    }
    else if (StrEqual(command, "ban", false)) {
      BanPlayer(client, arg1, StringToInt(arg2), arg3);
    }
    else if (StrEqual(command, "retry", false)) {
      if (strlen(arg1) > 0) {
        new targetClient = GetPlayerIdFromName(client, arg1);
        if (targetClient > 0) {
          ClientCommand(targetClient, "retry");
        }
      }
    }
  }

  return Plugin_Handled;
}

stock GetPlayerIdFromName(client, const String:name[]) {
  new String:playerName[MAX_NAME_LENGTH + 1];

  for (new i = 1; i <= MaxClients; i++ ) {
    if (IsValidClient(i)) {
      GetClientName(i, playerName, sizeof(playerName));
      if (StrContains(playerName, name, false) != -1) {
        return i;
      }
    }
  }

  return 0;
}

BanPlayer(client, const String:targetName[], duration, const String:reason[] = "") {
  new targetClient = GetPlayerIdFromName(client, targetName);
  if (!IsValidClient(targetClient)) {
    SendMessage(client, "Target player cannot be found: %s", targetName);
    return;
  }

  if (g_isNativesLoaded) {
    AddClientBan(client, targetClient, reason, duration);
    SendMessage(client, "Sent player ban to processing: %s", targetName);
  } else {
    SendMessage(client, "Ban Module is not loaded. Please try again later");
  }
}

KickPlayer(client, const String:targetName[], const String:reason[] = "", bool:kickForMap = false) {
  new targetClient = GetPlayerIdFromName(client, targetName);
  if (!IsValidClient(targetClient)) {
    SendMessage(client, "Target player cannot be found: %s", targetName);
    return;
  }

  new String:playerName[MAX_NAME_LENGTH + 1];
  GetClientName(targetClient, playerName, sizeof(playerName));

  if (kickForMap) {
    new String:steamid[24];
    GetClientAuthId(targetClient, AuthId_SteamID64, steamid, sizeof(steamid));
    if (FindStringInArray(g_kickedForMapPlayers, steamid) == -1) {
      PushArrayString(g_kickedForMapPlayers, steamid);
    }

    SendMessage(client, "Kicked for map: %s", playerName);
  } else {
    SendMessage(client, "Kicked once: %s", playerName);
  }

  KickClient(targetClient, reason);
}

SetGod(client, bool:value = true) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  if (!value) {
    SetEntProp(client, Prop_Data, "m_takedamage", 2, 1);
  } else {
    SetEntProp(client, Prop_Data, "m_takedamage", 0, 1);
  }
  g_godMode[client] = value;
}

SetInvis(client, bool:value = true) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  if (!value) {
    SetEntityRenderMode(client, RENDER_NORMAL);
    SetInvisForWeapons(client, 255);
  } else {
    // SetEntityRenderMode(client, RENDER_NONE);
    SetEntityRenderMode(client, RENDER_TRANSCOLOR);
    SetEntityRenderColor(client, 255, 255, 255, 0);
    SetInvisForWeapons(client, 0);
  }
  g_isInvis[client] = value;
  SendMessage(client, "Invis is %d", value ? 1 : 0);
}

public SetInvisForWeapons(client, alpha) {
	new iItems = FindSendPropInfo("CBaseCombatCharacter", "m_hMyWeapons");
	if (iItems != -1) {
		for (new i = 0; i <= 128; i += 4) {
			new nEntityID = GetEntDataEnt2(client, (iItems + i));
			if (!IsValidEdict(nEntityID))
				continue;

			SetEntityRenderMode(nEntityID, RENDER_TRANSCOLOR);
			SetEntityRenderColor(nEntityID, 255, 255, 255, alpha);
		}
	}
}

SetGhost(client, bool:value = true) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  if (!value) {
    // SetEntProp(client, Prop_Send, "m_lifeState", 2);
  } else {
    // TODO:
    SetEntProp(client, Prop_Send, "m_lifeState", 0);
  }
  g_isInvis[client] = value;
}

SetHp(client, value) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  new maxHealth = GetEntProp(client, Prop_Data, "m_iMaxHealth");
  if (value < 1) {
    value = maxHealth;
  }
  if (value > maxHealth) {
    SetEntProp(client, Prop_Data, "m_iMaxHealth", value);
  }
  SetEntityHealth(client, value);
}

SetArmor(client, value, bool:helmet = true) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  // new helmet = GetEntProp(client, Prop_Send, "m_bHasHelmet");
  // new armor = GetEntProp(client, Prop_Send, "m_ArmorValue");
  if (helmet) {
    SetEntProp(client, Prop_Send, "m_bHasHelmet", 1);
  }
  SetEntProp(client, Prop_Send, "m_ArmorValue", value);
}

SetAmmo1(client, ammo) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  SetPrimaryAmmo(client, ammo);
}

SetAmmo2(client, ammo) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  SetReserveAmmo(client, ammo);
}

SetMoney(client, value) {
  if (!IsValidClient(client)) {
    return;
  }
  SetEntProp(client, Prop_Send, "m_iAccount", value);
  SetEntProp(client, Prop_Send, "m_iStartAccount", value);
}

stock SetTeam(client, const String:teamName[]) {
  if (strlen(teamName) == 0) {
    return;
  }

  new newTeam = CS_TEAM_SPECTATOR;

  if (StrEqual(teamName, "CT", false))
    newTeam = CS_TEAM_CT;
  else if (StrEqual(teamName, "T", false) || StrEqual(arg1, "Terrorist", false))
    newTeam = CS_TEAM_T;

  RemoveWeapons(client); // so he doesn't drop his weapon
  Kill(client);
  ChangeClientTeam(target, newTeam);
}

SummonAll(client) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }

  new Float:vAngle;
  new Float:vVec[3];
  new Float:vPos[3];
  new Float:vAng[3];
  GetClientAbsOrigin(client, vPos);

  new Float:distance = 100.0;

  new Handle:arr = CreateArray();
  for (new i = 1; i <= MaxClients; i++ ) {
    if (client == i || !IsValidClient(i) || !IsPlayerAlive(i)) {
      continue;
    }
    PushArrayCell(arr, i);
  }

  // loop through 12 positions around the player
  for (new i = 0, len = GetArraySize(arr); i < len; i++) {
    vVec = vPos;
    vAngle = i * 360.0 / len; // divide circle into parts

    // draw in a circle around player
    vVec[0] += distance * (Cosine(vAngle));
    vVec[1] += distance * (Sine(vAngle));
		vVec[2] += 10.0;

		MakeVectorFromPoints(vPos, vVec, vAng);
		GetVectorAngles(vAng, vAng);
		vAng[0] = 0.0;
		vAng[1] -= 90.0;
		vAng[2] = 0.0;

		TeleportEntity(GetArrayCell(arr, i), vVec, vAng, NULL_VECTOR);
	}

  ClearArray(arr);
  delete arr;
}

TeleTo(client, target, Float:x = 0.0, Float:y = 0.0, Float:z = 0.0) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  if (!IsValidClient(target) || !IsPlayerAlive(target)) {
    return;
  }

  new Float:vPos[3];

  if (x > 0 && y > 0 && z > 0) {
    vPos[0] = x;
    vPos[1] = y;
    vPos[2] = z;
  } else {
    new Float:vBuffer[3], Float:vAng[3];
    GetClientEyePosition(client, vPos);
    GetClientEyeAngles(client, vAng);

    new Handle:hTrace = TR_TraceRayFilterEx(vPos, vAng, MASK_SHOT, RayType_Infinite, _TraceFilter);
    if (TR_DidHit(hTrace)) {
      TR_GetEndPosition(vPos, hTrace);
      GetAngleVectors(vAng, vBuffer, NULL_VECTOR, NULL_VECTOR);
      vPos[0] += vBuffer[0] * -10;
      vPos[1] += vBuffer[1] * -10;
      vPos[2] += vBuffer[2] * -10;
    }
    CloseHandle(hTrace);
  }

  if (vPos[0] > 0 && vPos[1] > 0 && vPos[2] > 0) {
    TeleportEntity(target, vPos, NULL_VECTOR, view_as<float>({ 0.0, 0.0, 0.0}));
  }
}
public bool:_TraceFilter(entity, contentsMask) {
	return entity > MaxClients || !entity;
}

EntsInRadius(client, Float:radius = 100.0) {
  new Float:vPos[3];
  new Float:vEnt[3];
  GetClientAbsOrigin(client, vPos);
  new count = 0;

  if (radius < 10.0) {
    radius = 100.0;
  }

  new String:sTemp[64];

  for (new i = 0; i < 4096; i++) {
    if (IsValidEntity(i) && IsValidEdict(i)) {
      GetEntPropVector(i, Prop_Data, "m_vecAbsOrigin", vEnt);
      if (GetVectorDistance(vPos, vEnt) <= radius) {
        count++;
        GetEdictClassname(i, sTemp, sizeof(sTemp));
        PrintToConsole(client, "%d. %f - %s", i, GetVectorDistance(vPos, vEnt), sTemp);
      }
    }
  }

  SendMessage(client, "%d entities total in radius %f, see console for full list", count, radius);
}

GiveWeapon(client, const String:weaponSuf[]) {
  if (strlen(weaponSuf) == 0) {
    return;
  }
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  new String:weaponName[64];
  Format(weaponName, sizeof(weaponName), "weapon_%s", weaponSuf);
  new bool:isC4ForCT = StrEqual("weapon_c4", weaponName, false) && GetClientTeam(client) == CS_TEAM_CT;
  if (isC4ForCT) {
    SetEntProp(client, Prop_Send, "m_iTeamNum", CS_TEAM_T);
  }
  GivePlayerItem(client, weaponName);
  if (isC4ForCT) {
    SetEntProp(client, Prop_Send, "m_iTeamNum", CS_TEAM_CT);
  }
}

// Removes all weapons including knife
RemoveWeapons(client) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  new weaponIndex;
  for (new i = 0; i <= 5; i++) {
    while ((weaponIndex = GetPlayerWeaponSlot(client, i)) != -1) {
      new String:sClassName[128];
      GetEdictClassname(weaponIndex, sClassName, sizeof(sClassName));
      if (StrEqual(sClassName, "weapon_c4")) {
        CS_DropWeapon(client, weaponIndex, true);
      } else {
        RemovePlayerItem(client, weaponIndex);
        RemoveEdict(weaponIndex);
      }
    }
  }
}

SetSpeed(client, Float:mult = 1.0) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", mult);
}

Freeze(client, bool:value = true) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  if (!value) {
    SetEntityMoveType(client, MOVETYPE_WALK);
  } else {
    SetEntityMoveType(client, MOVETYPE_NONE);
  }
  g_isFreezed[client] = value;
}

Kill(client, bool:hideFeed = false) {
  if (!IsValidClient(client) || !IsPlayerAlive(client)) {
    return;
  }
  SDKHooks_TakeDamage(client, client, client, 999999.0);
}

Respawn(client) {
  if (!IsValidClient(client) || IsPlayerAlive(client)) {
    return;
  }
  CS_RespawnPlayer(client);
}

public Action:RestartRoundTimer(Handle:timer) {
  CS_TerminateRound(0.1, CSRoundEnd_Draw, true);
  return Plugin_Handled;
}

public Action:ShutdownTimer(Handle:timer) {
	for (new i = 1; i <= MaxClients; i++) {
    if (IsClientInGame(i) && !IsFakeClient(i))
      ClientCommand(i, "retry");
  }
	InsertServerCommand("quit");
	ServerExecute();
	return Plugin_Handled;
}

// https://forums.alliedmods.net/showthread.php?t=243347 how to get weapon max clip size
// https://github.com/bcserv/smlib/blob/transitional_syntax/scripting/include/smlib/weapons.inc
// https://forums.alliedmods.net/showthread.php?t=281178
// https://forums.alliedmods.net/showpost.php?p=1959526&postcount=3
stock SetPrimaryAmmo(client, ammo) {
  new weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon"); // GetEntDataEnt2(client, FindSendPropInfo("CCSPlayer", "m_hActiveWeapon"));
  if (weapon < 1) return;
  SetEntData(weapon, FindSendPropInfo("CBaseCombatWeapon", "m_iClip1"), ammo);
}

stock SetReserveAmmo(client, ammo) {
  new weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
  if (weapon < 1) return;
  new ammoType = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
  if (ammoType == -1) return;
  SetEntProp(client, Prop_Send, "m_iAmmo", ammo, _, ammoType);
}

stock bool:IsValidClient(client) {
  return client >= 1 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client);
}

stock bool:IsNumeric(const String:str[]) {
	for (new i = 0; i < strlen(str); i++)
		if (!IsCharNumeric(str[i]) && str[i])
			return false;
	return true;
}

stock bool:IsValidSteamId(const String:steamid[]) {
	return strlen(steamid) == STEAM_ID_LENGTH && IsNumeric(steamid);
}

stock Debug(String:format[], any:...) {
  new String:pluginName[PLATFORM_MAX_PATH];
  GetPluginFilename(INVALID_HANDLE, pluginName, sizeof(pluginName));

  new String:buffer[2048];
  VFormat(buffer, sizeof(buffer), format, 2);

  PrintToServer("<%s> %s", pluginName, buffer);
}

stock DebugAndFileLog(String:format[], any:...) {
  new String:buffer[2048];
  VFormat(buffer, sizeof(buffer), format, 2);

  Debug(buffer);

  new String:path[PLATFORM_MAX_PATH];
  BuildPath(Path_SM, path, sizeof(path), LOGS_FILE_PATH);

  LogToFileEx(path, "%s", buffer);
}
