#### Arrays

// Get size of multi-dimensional arrays
new String:arr[MAXPLAYERS+1][4][512];
new len1, len2, len3;
len1 = sizeof(arr);
len2 = sizeof(arr[]);
len3 = sizeof(arr[][]);
// len1 = 65, len2 = 4, len3 = 512


#### Data Structures

enum struct PlayerInfo {
	float lastTime;
	int tokenCount;
}
PlayerInfo playerinfo[MAXPLAYERS+1];
// ...
playerinfo[client].lastTime = 0.0;
playerinfo[client].tokenCount = 0;


#### Enums

enum {
  Type_Connect,
  Type_PutInServer,
  Type_Disconnected,
};
// ...
if (type == Type_Connect) {}


#### Strings

// Format - Используйте при форматировании строки, когда буфер назначения является также одним из параметров
Format(buffer, sizeof buffer, "%i %s", someInt, buffer);

// FormatEx - Используйте, когда форматируемый и целевой буферы НЕ совпадают (она быстрее чем Format)
FormatEx(buffer, sizeof buffer, "%i", someInt);

// Formats a string according to the SourceMod format rules
int VFormat(char[] buffer, int maxlength, const char[] format, int varpos);

// strcopy - Используйте для копирования строки, не использующей правила форматирования
strcopy(buffer, sizeof buffer, "Some string");

// StrCat - Используйте для добавления в конец целевой строки, без необходимости в форматировании.
StrCat(buffer, sizeof buffer, "this text is appended to buffer");

// Breaks a string into pieces and stores each piece into an array of buffers
int ExplodeString(const char[] text, const char[] split, char[][] buffers, int maxStrings, int maxStringLength, bool copyRemainder)

// Remove linebreaks from string
ReplaceString(line, sizeof(line), "\n", "", false);

int strcmp(const char[] str1, const char[] str2, bool caseSensitive)
bool StrEqual(const char[] str1, const char[] str2, bool caseSensitive)
int StrContains(const char[] str, const char[] substr, bool caseSensitive)


#### Date and Time

// %w - week
new String:timeStr[22];
FormatTime(timeStr, sizeof(timeStr), "%Y.%m.%d %H:%M:%S", GetTime());


#### Defines

#define LoopClients(%1) for(int %1 = 1;%1 <= MaxClients;%1++) if(IsValidClient(%1))
// ...
LoopClients(i) {
  PrintToChat(i, "test");
}


#### Plugin Functions

public OnConfigsExecuted()
public OnGameFrame()
public OnClientAuthorized(client, const String:auth[])
public OnClientPostAdminCheck(client)
public OnClientPutInServer(client)
public OnClientDisconnect(client)
public OnClientDisconnect_Post(client)
public OnClientCookiesCached(client)
public OnEntityCreated(entity, const String:className[])
public OnEntityDestroyed(entity)


#### Utility Functions

GetGameTime()
GetMaxClients()
GetMaxHumanPlayers()
GetCurrentMap(sTempMap, sizeof(sTempMap));
GetMapDisplayName(sTempMap, sMapName, sizeof(sMapName));
GetAngleVectors(angle[3], fwd[3], right[3], up[3])
GetClientAbsOrigin(client, vec[3])
GetClientEyeAngles(client, ang[3])
GetClientCookie(client, handle, buffer, maxlen)
GetClientSerial(client)
GetClientFromSerial(serial)
GetVectorDistance(vec1, vec2)
IsClientConnected(client)
IsClientInGame(client)
IsClientInKickQueue(client)
IsClientSourceTV(client)
IsClientReplay(client)
IsPlayerAlive(client)
IsFakeClient(client)
IsValidEntity(ent)
IsValidEdict(ent)
GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon")
ForceChangeLevel(const char[] map, const char[] reason)
Action CS_OnTerminateRound(float& delay, CSRoundEndReason& reason)


#### HookEvent Definitions

https://wiki.alliedmods.net/Events_(SourceMod_Scripting)
https://wiki.alliedmods.net/Counter-Strike:_Source_Events

HookEvent("player_disconnect", Event_PlayerDisconnect, EventHookMode_Pre);
HookEvent("player_connect", Event_PlayerConnect);
HookEvent("player_connect_client", Event_PlayerConnect, EventHookMode_Pre);
HookEvent("player_hurt", Event_PlayerHurt);
HookEvent("player_death", Event_PlayerDeath);
HookEvent("player_spawn", Event_PlayerSpawn);
HookEvent("player_use", Event_PlayerUse);
HookEvent("player_team", Event_OnPlayerTeam, EventHookMode_Pre);
HookEvent("player_jump", Event_OnPlayerJump, EventHookMode_Pre);
HookEvent("player_changename", Event_OnPlayerName, EventHookMode_Pre);
HookEvent("grenade_thrown", Event_GrenadeThrown);
HookEvent("hegrenade_detonate", Event_OnGrenadeDetonate);
HookEvent("flashbang_detonate", Event_OnFlashDetonate);
HookEvent("smokegrenade_detonate", Event_OnSmokeDetonate);
HookEvent("weapon_fire", Event_WeaponFire);
HookEvent("bullet_impact", OnBulletImpact);
HookEvent("bomb_defused", Event_BombDefused);
HookEvent("item_pickup", Event_ItemPickup);
HookEvent("bomb_pickup", Event_BombPickup);
HookEvent("round_start", Event_RoundStart);
HookEvent("round_end", Event_RoundEnd);
HookEvent("round_end", Event_RoundEnd_Pre, EventHookMode_Pre);
HookEvent("round_mvp", Event_RoundMVP);
HookEvent("round_freeze_end", Event_RoundFreezeEnd, EventHookMode_PostNoCopy);
HookEvent("object_destroyed", Event_ObjectDestroyed_Pre, EventHookMode_Pre);
HookEvent("object_destroyed", Event_ObjectDestroyed);
HookEvent("object_destroyed", Event_ObjectDestroyed_Post, EventHookMode_Post);
HookEvent("player_pick_squad", Event_PlayerPickSquad_Post, EventHookMode_Post);
HookEvent("controlpoint_captured", Event_ControlPointCaptured_Pre, EventHookMode_Pre);
HookEvent("controlpoint_captured", Event_ControlPointCaptured);
HookEvent("controlpoint_captured", Event_ControlPointCaptured_Post, EventHookMode_Post);
HookEvent("teamplay_round_win", hook_Win, EventHookMode_Post);
HookEvent("teamplay_round_start", hook_RoundStart, EventHookMode_PostNoCopy);
HookEvent("teamplay_win_panel", Event_WinPanel, EventHookMode_Post);
HookEvent("game_start", hook_Event_GameStart);
HookEventEx("teamplay_restart_round", hook_Event_TFRestartRound);
HookEvent("cs_win_panel_round", WinPanel, EventHookMode_Pre);
HookEvent("server_cvar", Event_ServerCvar, EventHookMode_Pre);

HookEntityOutput(const char[] classname, const char[] output, EntityOutput callback)

SDKHook(iClient, SDKHook_PreThink, OnClientThink);
SDKHook(iClient, SDKHook_PreThinkPost, OnClientThink);
SDKHook(iClient, SDKHook_PostThink, OnClientThink);
SDKHook(iClient, SDKHook_PostThinkPost, OnClientThink);

// https://wiki.alliedmods.net/Temp_Entity_Lists_(Source)#Counter-Strike:_Source
AddTempEntHook("Shotgun Shot", Hook_BlockTE);
AddTempEntHook("EffectDispatch", Hook_BlockTE);
public Action Hook_BlockTE(const char[] te_name, const int[] Players, int numClients, float delay) {
  new client = TE_ReadNum( "m_iPlayer" ) + 1;
	new weapon_index = TE_ReadNum( "m_iWeaponID" );
  if (cond) {
    return Plugin_Stop; // prevet TE from drawing
  }
	return Plugin_Continue;
}


#### HookEvent "player_death"

HookEvent("player_death", Event_PlayerDeath);
// ...
public Action Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  new client = GetClientOfUserId(GetEventInt(event, "userid"));
  new victimTeam = GetClientTeam(client);

  new attacker = GetClientOfUserId(event.GetInt("attacker"));
  new bool:isWorldKiller = !attacker;
  new bool:isEnemyKiller = attacker;

  new ragdoll = GetEntPropEnt(client, Prop_Send, "m_hRagdoll");
 
  new String:weapon[32];
  GetEventString(event, "weapon", weapon, sizeof(weapon));
}


#### HookEvent "player_hurt"

HookEvent("player_hurt", Event_PlayerHurt);
// ...
public Action:Event_PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast) {
  new victim = GetClientOfUserId(GetEventInt(event, "userid"));
  new attacker = GetClientOfUserId(GetEventInt(event,"attacker"));
  new String:weaponName[64];
  GetEventString(event, "weapon", weaponName, sizeof(weaponName));
  new dmg_health = GetEventInt(event, "dmg_health");
	new dmg_armor = GetEventInt(event, "dmg_armor");
  new hp = GetEventInt(event, "health");
}


#### HookEvent("round_end", EventRoundEnd);

public Action EventRoundEnd(Event event, const char[] name, bool dontBroadcast) {
  int winningTeam = GetEventInt(event, "winner");
  if (winningTeam == CS_TEAM_T) {}
}


#### Trigger Events

new Handle:event = CreateEvent("player_changename");
if (event != INVALID_HANDLE) {
  SetEventInt(event, "userid", GetClientUserId(client));
  SetEventString(event, "oldname", oldname);
  SetEventString(event, "newname", name);
  FireEvent(event);
}


#### Replace Original Event

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
  SetEventBroadcast(event, /*dontBroadcast*/ true);

  new Handle:newEvent = CreateEvent("player_death");
  SetEventInt(newEvent, "userid", GetEventInt(event, "userid"));
  SetEventInt(newEvent, "attacker", GetEventInt(event, "attacker"));
  SetEventInt(newEvent, "assister", GetEventInt(event, "assister"));
  SetEventBool(newEvent, "headshot", GetEventBool(event, "headshot"));
  new String:weapon[64];
  GetEventString(event, "weapon", weapon, sizeof(weapon));
  SetEventString(newEvent, "weapon", weapon);
  SetEventInt(newEvent, "weaponid", GetEventInt(event, "weaponid"));
  SetEventInt(newEvent, "damagebits", GetEventInt(event, "damagebits"));
  SetEventBool(newEvent, "assistedflash", GetEventBool(event, "assistedflash"));
  SetEventBool(newEvent, "dominated", GetEventBool(event, "dominated"));
  SetEventBool(newEvent, "revenge", GetEventBool(event, "revenge"));
  SetEventBool(newEvent, "wipe", GetEventBool(event, "wipe"));
  SetEventBool(newEvent, "penetrated", GetEventBool(event, "penetrated"));
  SetEventBool(newEvent, "noreplay", GetEventBool(event, "noreplay"));
  SetEventBool(newEvent, "noscope", GetEventBool(event, "noscope"));
  SetEventBool(newEvent, "thrusmoke", GetEventBool(event, "thrusmoke"));
  SetEventBool(newEvent, "attackerblind", GetEventBool(event, "attackerblind"));
  SetEventInt(newEvent, "custom", 1);
  FireEvent(newEvent, false);
  delete newEvent;
}


#### Trigger Messages

new Handle:msg = StartMessageAll("SayText2");
if (msg != INVALID_HANDLE) {
  BfWriteByte(msg, client);
  BfWriteByte(msg, true);
  BfWriteString(msg, "Cstrike_Name_Change");
  BfWriteString(msg, oldname);
  BfWriteString(msg, name);
  EndMessage();
}


#### SDK Hooks

// Hook projectile touch
public OnEntityCreated(entity, const String:classname[]) {
	if (StrEqual(classname,"tf_projectile_rocket"))
		SDKHook(entity, SDKHook_Touch, OnProjectileTouch);
}
public OnProjectileTouch(entity, other) {
	if (other > 0 && other <= MaxClients)
		g_someVar[other] = true;
}

// Hook take damage
SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype) {
	if (!IsValidClient(victim) || !IsValidClient(attacker))
		return Plugin_Continue;

	// Fall damage negation.
	if ((damagetype & DMG_FALL) && g_bBlockFallDamage) {
		damage = 0.0;
		return Plugin_Changed;
	}

	return Plugin_Continue;
}

// Hook touch start and end
SDKHook(entity, SDKHook_StartTouch, OnTouchPoint);
SDKHook(entity, SDKHook_EndTouch, OnEndTouchPoint);
public Action:OnTouchPoint(entity, other) {
	new client = other;
	if (!IsValidClient(client))
		return;
}
public Action:OnEndTouchPoint(entity, other) {
	new client = other;
	if (!IsValidClient(client))
		return;
}


#### Global Forward

// file.inc
forward OnAssistedKill( const any:assisters[], const nbAssisters, const killerId, const victimId );

// plugin.sm
new Handle:g_hForwardKillAssist = INVALID_HANDLE;
OnPluginStart() {
  g_hForwardKillAssist = CreateGlobalForward("OnAssistedKill", ET_Ignore, Param_Array, Param_Cell, Param_Cell, Param_Cell);
}
// call forward from the same plugin
PlayerDeathEvent() {
  // ...
  Call_StartForward( g_hForwardKillAssist );
  Call_PushArray( assisters, nbAssisters );
  Call_PushCell( nbAssisters );
  Call_PushCell( killerId );
  Call_PushCell( victimId );
  Call_Finish();
}


#### HookConVarChange

new Handle:hRGB, String:sRGB[18],
	Handle:hDelay, Float:fDelay;

public OnPluginStart() {
  hDelay = CreateConVar("sm_bullet_trace_delay", "0.1");
  fDelay = GetConVarFloat(hDelay);
	HookConVarChange(hDelay, OnConVarChanges);

	hRGB = CreateConVar("sm_bullet_trace_color", "200 200 0");
  GetConVarString(hRGB, sRGB, sizeof(sRGB));
	HookConVarChange(hRGB, OnConVarChanges);
}

public OnConVarChanges(Handle:convar, const String:oldVal[], const String:newVal[]) {
	if (convar == hDelay) {
		fDelay = StringToFloat(newVal);
	} else if (convar == hRGB) {
		strcopy(sRGB, sizeof(sRGB), newVal);
	}
}


#### ConVar Bitflags

UnsetCheatVar(Handle:hndl) {
	new flags = GetConVarFlags(hndl)
	flags &= ~FCVAR_CHEAT
	SetConVarFlags(hndl, flags)
}
 
SetCheatVar(Handle:hndl) {
	new flags = GetConVarFlags(hndl)
	flags |= FCVAR_CHEAT
	SetConVarFlags(hndl, flags)
}


#### Health

GetClientHealth(client);
new maxHealth = GetEntProp(client, Prop_Data, "m_iMaxHealth");
new health = GetEntProp(client, Prop_Data, "m_iHealth");


#### Entity/client position

new Float:fOrigin[3];
GetEntPropVector(entity, Prop_Send, "m_vecOrigin", fOrigin);
// equals to
GetClientAbsOrigin(entity, fOrigin);


#### Player eyes position

decl Float:fPlayerOrigin[3];
GetClientEyePosition(iPlayer, fPlayerOrigin);
if (GetVectorDistance(fPlayerOrigin, fOrigin) <= Healthkit_Radius) {
  new Handle:trace = TR_TraceRayFilterEx(fPlayerOrigin, fOrigin, MASK_SOLID, RayType_EndPoint, Filter_ClientSelf, hData);
  if (!TR_DidHit(trace)) {
    // do something
  }
}


#### Get view point (trace end)

decl Float:vAngles[3];
decl Float:vOrigin[3];
decl Float:vBuffer[3];
decl Float:vStart[3];
decl Float:Distance;

GetClientEyePosition(client, vOrigin);
GetClientEyeAngles(client, vAngles);

new Handle:trace = TR_TraceRayFilterEx(vOrigin, vAngles, MASK_SHOT, RayType_Infinite);

if (TR_DidHit(trace)) {
  TR_GetEndPosition(vStart, trace);
  GetVectorDistance(vOrigin, vStart, false);
  Distance = -35.0;
  GetAngleVectors(vAngles, vBuffer, NULL_VECTOR, NULL_VECTOR);
  g_pos[0] = vStart[0] + (vBuffer[0]*Distance);
  g_pos[1] = vStart[1] + (vBuffer[1]*Distance);
  g_pos[2] = vStart[2] + (vBuffer[2]*Distance);
}


#### Write to file

new Handle:g_file_handle = INVALID_HANDLE;
if (!FileExists(g_particle_file, true)) {
  g_file_handle = OpenFile(g_particle_file,"a");
  if (g_file_handle != INVALID_HANDLE) {
    WriteFileString(g_file_handle, g_particle, false);
    CloseHandle(g_file_handle);
  }
}


#### Entity Spawn

public OnMapStart() {
  PrecacheModel("models/gibs/hgibs.mdl", true);
  SpawnTest();
}
SpawnTest() {
  gib = CreateEntityByName("prop_physics");
  if (gib == -1)
    return;

  DispatchKeyValue(gib, "targetname", ent);
  DispatchKeyValue(gib, "model", "models/gibs/hgibs.mdl");
  SetEntProp(gib, Prop_Data, "m_spawnflags", 4|8192|1048576);

  new Float:origin[3];
  GetClientAbsOrigin(id, origin);

  new Float:vel[3];
  vel[0] = GetRandomFloat(-200.0, 300.0);
  vel[1] = GetRandomFloat(-200.0, 300.0);
  vel[2] = GetRandomFloat(-200.0, 300.0);

  DispatchSpawn(gib);

  TeleportEntity(gib, origin, NULL_VECTOR, vel);
}


#### Entity Dissolver

// set name of the parent entity
Format(targetname, sizeof(targetname), "healthkit_%i", ent);
// ...
new entd;
if ((entd = CreateEntityByName("env_entity_dissolver")) != -1) {
  DispatchKeyValueFloat(entd, "dissolvetype", 2.0);
  /*
  0 	Energy
  1 	Heavy electrical
  2 	Light electrical
  3 	Core effect
  */

  DispatchKeyValue(entd, "magnitude", "250"); // How strongly to push away from the center. Maybe not work
  DispatchKeyValue(entd, "target", targetname); // "Targetname of the entity you want to dissolve."

  // Parent dissolver to healthkit. When entity destroyed, dissolver also.
  TeleportEntity(entd, pos, NULL_VECTOR, NULL_VECTOR);
  SetVariantString("!activator");
  AcceptEntityInput(entd, "SetParent", ent);

  Format(targetname, sizeof(targetname), "OnUser1 !self:Dissolve::%0.2f:-1", lifeTime); // Delay dissolve
  SetVariantString(targetname);
  AcceptEntityInput(entd, "AddOutput");

  // Not need this when parent dissolver to other entity
  // Format(targetname, sizeof(targetname), "OnUser1 !self:kill::7.1:-1");
  // SetVariantString(targetname);
  // AcceptEntityInput(entd, "AddOutput");

  AcceptEntityInput(entd, "FireUser1");
}


#### Weapons and Ammo

new String:g_Weapons[24][] = {
	"weapon_glock", "weapon_usp", "weapon_p228", "weapon_deagle", "weapon_elite", "weapon_fiveseven",
	"weapon_m3", "weapon_xm1014",
	"weapon_mac10", "weapon_mp5navy", "weapon_p90", "weapon_ump45", "weapon_tmp",
	"weapon_galil", "weapon_famas", "weapon_ak47", "weapon_m4a1", "weapon_sg552", "weapon_aug",
	"weapon_g3sg1", "weapon_sg550", "weapon_awp", "weapon_scout",
	"weapon_m249"
};

new g_WeaponCaps[24] = {
	120, 100, 52, 35, 120, 100,
	32, 32,
	100, 120, 100, 100, 120,
	90, 90, 90, 90, 90, 90,
	90, 90, 30, 90,
	200
};
